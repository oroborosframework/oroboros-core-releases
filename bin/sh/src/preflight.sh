#!/bin/bash
# Preflight command aggregation script
# echo "Total args: "$@;
declare _function=0;
declare _command=1;
declare _args="";
while [[ $# -gt 0 ]]
do
key="$1";
if [[ $_function == 0 ]];
  then
    case $key in
        -a|--app)
          declare -r _function="_app";
          shift
        ;;
        -b|--build)
          declare -r _function="_build";
          shift
        ;;
        -c|--cron)
          declare -r _function="_cron";
          shift
        ;;
        -d|--deploy)
          declare -r _function="_deploy";
          shift
        shift # past argument
        ;;
        -e|--execute)
          declare -r _function="_execute";
          shift
        ;;
        -g|--git)
          declare -r _function="_git";
          shift
        ;;
        -h|--help)
          declare -r _function="_help";
          shift
        ;;
        -i|--info)
          declare -r _function="_info";
          shift
        ;;
        --installer)
          declare -r _function="_install";
          shift
        ;;
        -p|--pull)
          declare -r _function="_pull";
          shift
        ;;
        -v|--version)
          declare -r _function="_version";
          shift
        ;;
        *)
          # unknown option
          if [[ $_function == 0 ]]; 
            then
              echo -e "\033[0;31mUnknown Action: $1\033[0m" 1>&2;
              _exit_clean;
              shift
          fi
        ;;
    esac;
else
  if [[ $_command == 1 ]];
    then
      declare -r _command="$key";
      shift;
    else
      _tmp="$(echo -e "${key}" | sed -e 's/^[[:space:]]*//')"
      if [[ $_args == "" ]];
        then
          _args=$_tmp;
          shift;
      else
        _args="$_args $_tmp";
      shift;
      fi;
  fi;
fi;
done;
if [[ $_function == 0 ]]; 
  then
    declare -r _function="_info";
fi
$_function "c=$_command args=$_args";
_exit_clean;