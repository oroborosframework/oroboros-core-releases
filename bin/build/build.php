#!/usr/bin/env php
<?php
/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * <Oroboros Core Build>
 * This initializes the build routine. This should be run prior to first launch from the command line.
 *
 * From the root directory, run the following command
 * in your console to run the build.
 *
 * Use the following on any UNIX platform (linux, Mac OS, etc)
 *
 * @example ./oroboros.sh --build
 *
 * if you are on Windows, use the following instead:
 *
 * @example ./oroboros.bat --build
 *
 * Builds will ignore this command if there is an oroboros.lock file
 * in your root directory. Otherwise it will erase all build files
 * and regenerate them even if they exist.
 *
 * You must delete this file to run an override build.
 *
 * --------
 * @important READ THE OVERRIDE DOCS BEFORE ATTEMPTING TO MODIFY BUILD PARAMETERS
 *
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/build.md
 * --------
 */

/**
 * Proceeding with initialization.
 */

/**
 * Declare the root directory (you MAY NOT override this).
 * --------
 * since we have no idea what version of PHP we are using yet,
 * we will declare this using the legacy-safe method.
 * --------
 * All other declarations will occur during the bootload routine.
 */
require_once '../../init.php';
require_once 'build.routine.php';

