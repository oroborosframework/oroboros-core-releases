<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

//this will prevent any variable declarations from being used out of scope.

namespace oroboros\core\bin\bootload\subroutines;

/**
 * <Application Routine>
 *
 * This routine parses the application settings,aggregates them against
 * any defined overrides declared prior to bootload, and produces a
 * fully defined application configuration set that will persist
 * for the remainder of runtime.
 *
 * --------
 *
 * Routines represent a subset of execution that assumes control.
 * These are separated under a different naming convention, so
 * they are not accidentally included where it is no appropriate.
 * Anything that runs imperative commands (eg: executes logic in the
 * global scope as soon as the file is included) falls under the
 * classification of a routine.
 *
 * --------
 * 
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/
 * @category routines
 * @package oroboros/core
 * @subpackage bootload
 * @version 0.2.5
 * @since 0.2.4-alpha
 */
$settings = json_decode( file_get_contents( OROBOROS_CONFIG . 'settings_default.json' ),
    1 );
//Merge the custom settings from the conf directory
if ( is_readable( OROBOROS_CONFIG . 'settings.json' ) )
{
    //load the custom settings from the conf directory
    $settings = array_replace_recursive( $default_settings,
        json_decode( file_get_contents( OROBOROS_CONFIG . 'settings.json' ), 1 ) );
}
//Merge the custom settings from the defined constant
if ( defined( 'OROBOROS_SETTINGS' ) && is_readable( OROBOROS_SETTINGS ) )
{
    //load the custom settings from the defined constant
    $settings = array_replace_recursive( $settings,
        json_decode( file_get_contents( OROBOROS_SETTINGS ), 1 ) );
}
//Merge the custom settings from the environment variable
if ( getenv( 'OROBOROS_SETTINGS' ) && is_readable( getenv( 'OROBOROS_SETTINGS' ) ) )
{
    //load the custom settings from the environment variable
    $settings = array_replace_recursive( $settings,
        json_decode( file_get_contents( getenv( 'OROBOROS_SETTINGS' ) ), 1 ) );
}
if ( getenv( 'OROBOROS_ENVIRONMENT_MODE' ) && in_array( getenv( 'OROBOROS_ENVIRONMENT_MODE' ),
        array(
        'production',
        'staging',
        'development' ) ) )
{
    $settings['core']['mode'] = getenv( 'OROBOROS_ENVIRONMENT_MODE' );
} elseif ( defined( 'OROBOROS_ENVIRONMENT_MODE' ) && in_array( OROBOROS_ENVIRONMENT_MODE,
        array(
        'production',
        'staging',
        'development' ) ) )
{
    $settings['core']['mode'] = OROBOROS_ENVIRONMENT_MODE;
}
