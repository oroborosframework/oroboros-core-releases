<?php

namespace oroboros\core\patterns\creational;

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Description of LibraryFactory
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
final class AppFactory
    extends \oroboros\core\abstracts\patterns\creational\AbstractFactory
{
//    private $_sources = array(
//        "template" => OROBOROS_APPS . '_templates' . DIRECTORY_SEPARATOR,
//        "development" => OROBOROS_APPS . 'development' . DIRECTORY_SEPARATOR,
//        "shared" => OROBOROS_APPS . 'shared' . DIRECTORY_SEPARATOR,
//        "production" => OROBOROS_APPS . 'production' . DIRECTORY_SEPARATOR
//    );
//
//    public function __construct( $params = null, $flags = null ) {
//        parent::__construct($params, $flags);
//        $this->_validSources($this->_sources);
//    }
//
//    public function load($type, array $params = array(), array $flags = array()) {
//        $path = $this->_getSourceResource() . $type;
//        if (realpath($path) && realpath($path . DIRECTORY_SEPARATOR . 'Bootstrap.php')) {
//            $vendor = $type;
//            \dev\Utils::crash("Cannot proceed, need to declare vendor with autoloader here.");
//            \oroboros\Oroboros::autoload($vendor, $path);
//        } else {
//            throw new \oroboros\core\utilities\exception\Exception("Invalid Application [" . (string) $type . "]", self::ERROR_LOGIC_BAD_PARAMETERS);
//        }
//    }
}
