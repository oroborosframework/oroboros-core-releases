<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\enumerated\flags;

/**
 * <Core Error Handler Flag Enumerated Api Interface>
 * This enumerated interface designates valid flags for error handlers.
 *
 * --------
 *
 * Enumerated Api Interfaces
 * Enumerated Api Interfaces provide sets of fixed,
 * enumerated values that must maintain consistency,
 * and should not be overridden at compile time.
 *
 * In some cases, these values are considered absolute,
 * and the program will always reference them as defined.
 * In other cases, they provide baseline definitions,
 * which the individual api implementing them may allow
 * for extension upon. In these cases, the fixed values
 * MAY NOT be removed or altered, but additions and aliases
 * will be allowed for. Check the api of the individual
 * contract being considered for extension for information
 * how to accomplish this as it applies.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/enumerated_api_interface.md
 * @category enumerated-interfaces
 * @package oroboros/core
 * @subpackage flags
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 */
interface ErrorHandlerFlags
extends FlagBase
{

    /**
     * Designates that the error handler should fire the
     * previous error handler if one exists.
     */
    const FLAG_ERROR_HANDLER_CALL_PREVIOUS = '::call-previous::';

    /**
     * Designates that the error handler should use the handling
     * capabilities of the previous error handler in place of its own.
     */
    const FLAG_ERROR_HANDLER_DEFER_PREVIOUS = '::defer-previous::';

    /**
     * Designates that the error handler should recast recoverable errors
     * as ErrorExceptions if they are not handled.
     */
    const FLAG_ERROR_HANDLER_ERROR_EXCEPTION = '::cast-to-exception::';

    /**
     * Designates that the error handler should
     * track statistics on all errors handled.
     */
    const FLAG_ERROR_HANDLER_TRACK_STATISTICS = '::track-statistics::';

    /**
     * Designates that the error handler should
     * automatically log unhandled errors.
     */
    const FLAG_ERROR_HANDLER_LOG_UNHANDLED = '::log-unhandled::';

}
