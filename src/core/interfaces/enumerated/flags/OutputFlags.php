<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\enumerated\flags;

/**
 * <Output Flag Enumerated Api Interface>
 * This is the master interface output flag definitions.
 *
 * --------
 *
 * Enumerated Api Interfaces
 * Enumerated Api Interfaces provide sets of fixed,
 * enumerated values that must maintain consistency,
 * and should not be overridden at compile time.
 *
 * In some cases, these values are considered absolute,
 * and the program will always reference them as defined.
 * In other cases, they provide baseline definitions,
 * which the individual api implementing them may allow
 * for extension upon. In these cases, the fixed values
 * MAY NOT be removed or altered, but additions and aliases
 * will be allowed for. Check the api of the individual
 * contract being considered for extension for information
 * how to accomplish this as it applies.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/enumerated_api_interface.md
 * @category enumerated-interfaces
 * @package oroboros/core
 * @subpackage flags
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 */
interface OutputFlags
extends FlagBase
{
    const FLAG_OUTPUT_NULL = "::flag-null-output::";
    const FLAG_OUTPUT_DEFAULT = '::flag-default-output::';
    const FLAG_OUTPUT_PLAINTEXT = "::flag-plaintext-output::";
    const FLAG_OUTPUT_PLAINTEXT_CGI = "::flag-cgi-output::";
    const FLAG_OUTPUT_PLAINTEXT_CGI_CONSOLE = "::flag-cgi-console-output::";
    const FLAG_OUTPUT_PLAINTEXT_CGI_CRON = '::flag-cgi-cron-output::';
    const FLAG_OUTPUT_PLAINTEXT_ROBOTS = "::flag-robots-output::";
    const FLAG_OUTPUT_HTML = "::flag-html-output::";
    const FLAG_OUTPUT_CSS = "::flag-css-output::";
    const FLAG_OUTPUT_CSS_SASS = "::flag-css-output::";
    const FLAG_OUTPUT_CSS_LESS = "::flag-css-output::";
    const FLAG_OUTPUT_JAVASCRIPT = "::flag-javascript-output::";
    const FLAG_OUTPUT_JAVASCRIPT_PRETTIFIED = "::flag-javascript-output::";
    const FLAG_OUTPUT_JAVASCRIPT_PRETTIFIED_DEBUG = "::flag-javascript-output::";
    const FLAG_OUTPUT_JAVASCRIPT_MINIFIED = "::flag-javascript-output::";
    const FLAG_OUTPUT_JAVASCRIPT_MINIFIED_DEBUG = "::flag-javascript-output::";
    const FLAG_OUTPUT_EMAIL = "::flag-email-output::";
    const FLAG_OUTPUT_EMAIL_PLAINTEXT = "::flag-email-plaintext-output::";
    const FLAG_OUTPUT_EMAIL_HTML = "::flag-email-html-output::";
    const FLAG_OUTPUT_XML = "::flag-xml-output::";
    const FLAG_OUTPUT_XML_RSS = "::flag-xml-rss-output::";
    const FLAG_OUTPUT_XML_SITEMAP = "::flag-xml-sitemap-output::";
    const FLAG_OUTPUT_SQL = "::flag-sql-output::";
    const FLAG_OUTPUT_CSV = "::flag-csv-output::";
    const FLAG_OUTPUT_IMG = "::flag-img-output::";
    const FLAG_OUTPUT_IMG_JPG = "::flag-img-jpg-output::";
    const FLAG_OUTPUT_IMG_JPEG = '::flag-img-jpg-output::';
    const FLAG_OUTPUT_IMG_GIF = "::flag-img-gif-output::";
    const FLAG_OUTPUT_IMG_PNG = "::flag-img-png-output::";
    const FLAG_OUTPUT_IMG_SVG = "::flag-img-svg-output::";
    const FLAG_OUTPUT_AUDIO_MP3 = "::flag-audio-mp3-output::";
    const FLAG_OUTPUT_AUDIO_AIFF = "::flag-audio-aiff-output::";
    const FLAG_OUTPUT_AUDIO_WAV = "::flag-video-wav-output::";
    const FLAG_OUTPUT_VIDEO_MP4 = "::flag-video-mp4-output::";
}
