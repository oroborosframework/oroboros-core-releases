<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\enumerated;

/**
 * <Base Enumerated Api Interface>
 * This interface represents the root level enumerated api interface.
 * All other enumerated api interfaces extend from this root. This is a null api,
 * meant to designate the origin of a chain of related interface constructs.
 *
 * As Enumerated Api Interfaces only provide distinct key value stores,
 * they should be used with the following considerations:
 *
 * These interfaces DO NOT enforce methods.
 * These interfaces DO declare numerous constants.
 * There is a low probability of constant collision with external codebases.
 * If this causes an issue, wrap the object that implements
 * the api in one that doesn't, and use a pass-through to obtain it's values.
 *
 * There is a trait that can accomplish this strict enumeration based off of
 * any interface attached to a class that uses it, which can also filter
 * results by prefix or suffix of the constant name. It's super handy for
 * indexing these in any class that uses them.
 *
 * @see \oroboros\enum\traits\EnumTrait
 *
 * There are also sets of provided defaults under the concrete namespace
 *
 * @see \oroboros\enum
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/enumerated_api_interface.md
 * @category enumerated-interfaces
 * @package oroboros/core
 * @subpackage abstraction
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 */
interface BaseEnum
{

}
