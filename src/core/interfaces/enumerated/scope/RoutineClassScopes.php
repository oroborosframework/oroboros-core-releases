<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\enumerated\scope;

/**
 * <Routine Class Scope Base Enumerated Api Interface>
 * This is the base enumerated api interface for Routine class scope definitions.
 *
 * --------
 *
 * Enumerated Api Interfaces
 * Enumerated Api Interfaces provide sets of fixed,
 * enumerated values that must maintain consistency,
 * and should not be overridden at compile time.
 *
 * In some cases, these values are considered absolute,
 * and the program will always reference them as defined.
 * In other cases, they provide baseline definitions,
 * which the individual api implementing them may allow
 * for extension upon. In these cases, the fixed values
 * MAY NOT be removed or altered, but additions and aliases
 * will be allowed for. Check the api of the individual
 * contract being considered for extension for information
 * how to accomplish this as it applies.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/enumerated_api_interface.md
 * @category enumerated-interfaces
 * @package oroboros/core
 * @subpackage classes
 * @version 0.2.5
 * @since 0.2.5
 */
interface RoutineClassScopes
extends ClassScopeBase
{

    /**
     * <Routine>
     *
     * ----------------
     * 
     * Routines are classes that wrap procedural code.
     * Their task is to encapsulate non-object oriented constructs
     * in a way that is compatible with object-oriented architecture.
     * Their task is to manage a subset of specific linear execution
     * that is not an object (but may internally use objects), understand
     * and report its scope and magnitude, and execute it on command,
     * while managing any errors it arises appropriately.
     *
     * Internally, these classes are only used to parse initial bootloading
     * and arrive at a useable bootstrap object correctly.
     *
     * Externally, they can be used as a wrapper for any independently
     * executiing construct, and quickly creating a module around it.
     *
     * It is bad practice to base your system too heavily upon
     * this class type. Use this extremely sparingly unless you
     * are stuck with an already locked-in procedural approach,
     * say from a legacy codebase or similar.
     */

    /**
     * <Generic Routine>
     * This classification designates only that the class is a routine object,
     * and makes no further assumption about it. You will need to provide your
     * own documentation if you use this scope.
     */
    const CLASS_SCOPE_ROUTINE = "::routine::";
    const CLASS_SCOPE_ROUTINE_ABSTRACT = "::abstract-routine::";
    const CLASS_SCOPE_ROUTINE_NULL = '::null-routine::';
    const CLASS_SCOPE_ROUTINE_BOOTLOAD = '::bootload-routine::';
    const CLASS_SCOPE_ROUTINE_SUBSYSTEM = '::subystem-routine::';
    const CLASS_SCOPE_ROUTINE_PROCEDURAL = '::procedural-routine::';
    const CLASS_SCOPE_ROUTINE_DECLARATIVE = '::declarative-routine::';
    const CLASS_SCOPE_ROUTINE_WEB = '::web-routine::';
    const CLASS_SCOPE_ROUTINE_CGI = '::cgi-routine::';
    const CLASS_SCOPE_ROUTINE_BUILD = '::build-routine::';
    const CLASS_SCOPE_ROUTINE_TEST = '::test-routine::';
    const CLASS_SCOPE_ROUTINE_DEBUG = '::debug-routine::';
    const CLASS_SCOPE_ROUTINE_PROXY = '::proxy-routine::';

}
