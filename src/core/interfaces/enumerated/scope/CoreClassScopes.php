<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\enumerated\scope;

/**
 * <Core Class Scope Base Enumerated Api Interface>
 * This is the base enumerated api interface for core internal class scope definitions.
 *
 * --------
 *
 * Enumerated Api Interfaces
 * Enumerated Api Interfaces provide sets of fixed,
 * enumerated values that must maintain consistency,
 * and should not be overridden at compile time.
 *
 * In some cases, these values are considered absolute,
 * and the program will always reference them as defined.
 * In other cases, they provide baseline definitions,
 * which the individual api implementing them may allow
 * for extension upon. In these cases, the fixed values
 * MAY NOT be removed or altered, but additions and aliases
 * will be allowed for. Check the api of the individual
 * contract being considered for extension for information
 * how to accomplish this as it applies.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/enumerated_api_interface.md
 * @category enumerated-interfaces
 * @package oroboros/core
 * @subpackage classes
 * @version 0.2.5
 * @since 0.2.5
 */
interface CoreClassScopes
extends ClassScopeBase
{
    /**
     * <Core Class Scopes>
     *
     * ----------------
     * 
     * Core classes represent very system-specific classes provided to access
     * functionality, that are not appropriate to override. Do not use these
     * class scopes for 3rd party integrations.
     */

    /**
     * Represents core internal classes that run the guts of the framework.
     * These should not be extended. This is the general, generic class scope assignment.
     */
    const CLASS_SCOPE_CORE = "::core::";

    /**
     * Represents an abstract core class.
     */
    const CLASS_SCOPE_CORE_ABSTRACT = "::abstract-core::";

    /**
     * Represents a common asset core class.
     * These provide generic universal functionality
     * within their scope to accomplish various
     * common internal tasks.
     * These should not be extended.
     */
    const CLASS_SCOPE_CORE_COMMON = "::common-core::";

    /**
     * Represents an outward api for interacting with the system.
     * These classes present the final api that other 3rd party programs
     * utilize to leverage the functionality of this system
     * through the provided facade.
     * These classes should not be overridden.
     */
    const CLASS_SCOPE_CORE_GLOBAL = "::global-core::";

    /**
     * Represents a module package designed specifically to extend the core.
     * These are typically released by the same vendor for the
     * explicit purpose of adding additional functionality,
     * but are not appropriate for the main package.
     * These classes should not be overridden.
     */
    const CLASS_SCOPE_CORE_MODULE = "::module-core::";

    /**
     * Represents an extension package designed specifically
     * to provide additional options to the core.
     * These are typically released by the same vendor
     * for the  explicit purpose of adding additional,
     * more refined, or more specialized base libraries to the core.
     * These classes should not be overridden.
     */
    const CLASS_SCOPE_CORE_EXTENSION = "::extension-core::";

}
