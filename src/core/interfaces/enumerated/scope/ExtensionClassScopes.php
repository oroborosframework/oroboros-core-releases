<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\enumerated\scope;

/**
 * <Extension Class Scope Base Enumerated Api Interface>
 * This is the base enumerated api interface for component class scope definitions.
 *
 * --------
 *
 * Enumerated Api Interfaces
 * Enumerated Api Interfaces provide sets of fixed,
 * enumerated values that must maintain consistency,
 * and should not be overridden at compile time.
 *
 * In some cases, these values are considered absolute,
 * and the program will always reference them as defined.
 * In other cases, they provide baseline definitions,
 * which the individual api implementing them may allow
 * for extension upon. In these cases, the fixed values
 * MAY NOT be removed or altered, but additions and aliases
 * will be allowed for. Check the api of the individual
 * contract being considered for extension for information
 * how to accomplish this as it applies.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/enumerated_api_interface.md
 * @category enumerated-interfaces
 * @package oroboros/core
 * @subpackage classes
 * @version 0.2.5
 * @since 0.2.5
 */
interface ExtensionClassScopes
extends ClassScopeBase
{
    /**
     * <Extension>
     *
     * ----------------
     *
     * Components add enhanced output to existing return results in a way that
     * does not invalidate the original output. Components within the context of
     * Oroboros are designed to enrich return data with additional context,
     * theme, or templating data, without altering the underlying intention of
     * the output. An example of a component might be a class that colorizes
     * cli console output, adds SERP tags to existing html, or binds additional
     * content hooks into output content for external content injection.
     *
     * In Oroboros, Components are typically bound to an explicit
     * output type (eg: html, plaintext, etc), and also are usually designated
     * to work in an explicit scope (public facing site, command line api, etc).
     */

    /**
     * <Generic Extendable>
     * This classification designates only that the class is an extendable object,
     * and makes no further assumption about it. You will need to provide your
     * own documentation if you use this scope.
     */
    const CLASS_SCOPE_EXTENDABLE = "::extendable::";

    /**
     * <Generic Extension>
     * This classification designates only that the class is an extension object,
     * and makes no further assumption about it. You will need to provide your
     * own documentation if you use this scope.
     */
    const CLASS_SCOPE_EXTENSION = "::extension::";

    /**
     * <Extension Configuration>
     * Provides a configuration for an extension.
     */
    const CLASS_SCOPE_EXTENSION_CONFIG = '::config-extension::';

    /**
     * <Extendable Extension>
     * An extension that is also extendable.
     */
    const CLASS_SCOPE_EXTENSION_EXTENDABLE = '::extendable-extension::';

    /**
     * <Abstract Extension>
     * Provides a basis for extending other classes, but is not usable by itself.
     */
    const CLASS_SCOPE_EXTENSION_ABSTRACT = '::abstract-extension::';

    /**
     * <Null Extension>
     * Extends another class without providing any additional functionality.
     */
    const CLASS_SCOPE_EXTENSION_NULL = '::null-extension::';

    /**
     * <Core Extension>
     * Extends the base accessor class.
     * This extension will provide additional root level commands
     * that can be used globally.
     */
    const CLASS_SCOPE_EXTENSION_CORE = '::core-extension::';

    /**
     * <Codex Extension>
     * Extends the Codex.
     * This extension provides additional contextual information
     * search functionality.
     */
    const CLASS_SCOPE_EXTENSION_CODEX = '::codex-extension::';
    const CLASS_SCOPE_EXTENSION_ADAPTER = '::adapter-extension::';
    const CLASS_SCOPE_EXTENSION_BENCHMARK = '::benchmark-extension::';
    const CLASS_SCOPE_EXTENSION_AUTH = '::auth-extension::';
    const CLASS_SCOPE_EXTENSION_BOOTSTRAP = '::bootstrap-extension::';
    const CLASS_SCOPE_EXTENSION_CLUSTER = '::cluster-extension::';
    const CLASS_SCOPE_EXTENSION_CONTROLLER = '::controller-extension::';
    const CLASS_SCOPE_EXTENSION_DATA = '::data-extension::';
    const CLASS_SCOPE_EXTENSION_DEBUG = '::debug-extension::';
    const CLASS_SCOPE_EXTENSION_ENTITY = '::entity-extension::';
    const CLASS_SCOPE_EXTENSION_ENUM = '::enum-extension::';
    const CLASS_SCOPE_EXTENSION_ERROR = '::error-extension::';
    const CLASS_SCOPE_EXTENSION_EVENT = '::event-extension::';
    const CLASS_SCOPE_EXTENSION_FLAG = '::flag-extension::';
    const CLASS_SCOPE_EXTENSION_JOB = '::job-extension::';
    const CLASS_SCOPE_EXTENSION_MODEL = '::model-extension::';
    const CLASS_SCOPE_EXTENSION_MODULE = '::module-extension::';
    const CLASS_SCOPE_EXTENSION_PATTERN = '::pattern-extension::';
    const CLASS_SCOPE_EXTENSION_PERFORMANCE = '::performance-extension::';
    const CLASS_SCOPE_EXTENSION_PROXY = '::proxy-extension::';
    const CLASS_SCOPE_EXTENSION_ROUTINE = '::routine-extension::';
    const CLASS_SCOPE_EXTENSION_SECURITY = '::security-extension::';
    const CLASS_SCOPE_EXTENSION_SERVICE = '::service-extension::';
    const CLASS_SCOPE_EXTENSION_SHELL = '::shell-extension::';
    const CLASS_SCOPE_EXTENSION_SOURCE = '::source-extension::';
    const CLASS_SCOPE_EXTENSION_SYSTEM = '::system-extension::';
    const CLASS_SCOPE_EXTENSION_TEMPLATE = '::template-extension::';
    const CLASS_SCOPE_EXTENSION_TEST = '::test-extension::';
    const CLASS_SCOPE_EXTENSION_THIRDPARTY = '::thirdparty-extension::';
    const CLASS_SCOPE_EXTENSION_UTILITY = '::utility-extension::';
    const CLASS_SCOPE_EXTENSION_VALIDATION = '::validation-extension::';
    const CLASS_SCOPE_EXTENSION_VIEW = '::view-extension::';
    const CLASS_SCOPE_EXTENSION_WRAPPER = '::wrapper-extension::';

}
