<?php

namespace oroboros\core\interfaces\api;

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * <Oroboros API>
 * Defines the standard methods accessible through the oroboros core object.
 * @see \oroboros\Oroboros
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
interface OroborosApi
extends BaseApi
{
    /**
     * Designates the type of this Api Interface, and what realm of
     * responsibilities it is classified as.
     */
    const API_TYPE = 'core';

    /**
     * Determines the focused goal within the api type.
     * The Api Scope reveals the underlying goal of this specific Api Interface,
     * and what the specific purpose of this collection of classes, traits,
     * and interfaces is meant to collectively accomplish.
     *
     * @note This api scope is concerned with the underlying
     * correlation of classes into organized families of
     * related constructs. High cohesion is generally
     * a measure of code quality.
     */
    const API_SCOPE = 'core';

    /**
     * Designates a parent package or namespace that this Api adheres to.
     * This value should be false if no such parent exists,
     * but this constant should still be declared.
     *
     * @note This is the primary Oroboros Core api.
     */
    const API_PARENT_NAMESPACE = false;

    /**
     * Designates the namespace provided by the package, if one exists.
     * This value should be false if the package does not provide a namespace,
     * but the constant should still be declared.
     */
    const API_PROVIDES_NAMESPACE = 'oroboros\\core';

    /**
     * Designates the codex index for this Api, which can be used to reference
     * this collection of classes, traits, and interfaces as a related family
     * working toward a specific goal.
     *
     * This value should always be provided for any valid Api Interface.
     */
    const API_CODEX = 'oroboros';

    /**
     * Designates the map file for determining sub-packages and parent packages,
     * which is parsed by the Codex. This should be false if the package does not
     * provide a package map. If this constant is not defined or the file cannot be
     * found from the package root, then the package will contain an incomplete
     * Codex assignment.
     *
     * This value should always be provided for any valid Api Interface.
     */
    const API_PACKAGE_MAP = false;

    /**
     * Designates the package name of the api.
     * All packages must declare a package name to be considered valid.
     *
     * This value should always be provided for any valid Api Interface.
     */
    const API_PACKAGE = 'oroboros/core';

    /**
     * Designates the parent package of the package, if one exists,
     * which is parsed by the Codex. This should be false if the package does not
     * have a parent package. If this constant is not defined or the file cannot be
     * found from the package root, then the package will be assumed not to have
     * a parent package by the Codex.
     *
     * This value should always be provided for any valid Api Interface.
     */
    const API_PARENT_PACKAGE = 'oroboros/false';


    /**
     * Designates the primary category of responsibility of the api.
     * All packages must declare some category to be considered valid,
     * which allows them to be indexed by the Codex in terms of relationship
     * to other packages.
     *
     * This value should always be provided for any valid Api Interface.
     */
    const API_CATEGORY = 'core';

    /**
     * Designates the subcategory of responsibility of the api.
     * Packages may not need to declare a subcategory, but should provide
     * this constant as false if no subcategory exists. The subcategory
     * determines what realm of responsibility the package has within
     * a broader category.
     *
     * If this constant is omitted, a default value of false will be assumed.
     */
    const API_SUBCATEGORY = 'core';
}
