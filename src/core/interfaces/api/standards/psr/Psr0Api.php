<?php

/*
 * The MIT License
 *
 * Copyright 2017 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\api\standards\psr;

/**
 * <Psr0 Api Interface>
 * This interface represents the api interface for Psr-0 compliance
 * within Oroboros Core. It designates all related classes, interfaces,
 * and traits used to satisfy the requirements of Psr-0, and any related
 * constructs that can be accessed to provide related information.
 *
 * This api is most easily provided by accessing the codex entry [psr0].
 *
 * ----------------
 *
 * PURPOSE
 *
 * ----------------
 *
 * An Api Interface is used to declare a family of related classes, and what
 * they are meant to satisfy. It also declares any dependencies, and any
 * interoperability that can occur with it, or any additional apis that it provides.
 *
 * This information is used internally to validate the execution plan.
 *
 * This information can be indexed by checking the Codex
 *
 * ----------------
 *
 * USAGE
 *
 * ----------------
 *
 * Effective useage of this interface construct without collisions requires
 * that these interfaces ONLY BE IMPLEMENTED BY CONCRETE, FINAL CLASSES
 *
 * The traits and abstractions of this system are built to honor the
 * considerations if these without directly implementing them.
 *
 * Concrete classes can implement them as proof that they honor their api.
 * Internally, the system will FAVOR the object for jobs related to it's
 * DECLARED API when it has MULTIPLE AVAILABLE OPTIONS that all honor a scope.
 *
 * DECLARE THE API IN THE CONCRETE CLASS if it is not met by inheritance
 * (traits cannot accomplish this for you. You must either declare this
 * yourself or inherit it from one of our base classes).
 *
 * const OROBOROS_API = '\\oroboros\\core\\interfaces\\api\\adapters\AdapterApi'; //honors the adapter api
 *
 * Api Interfaces serve as an index of how class type and class scope
 * relate to specific api use cases.
 * This information is available by checking the Codex
 *
 * @see \oroboros\core\codex\Codex
 *
 * Which can also be done by any class using the codex trait
 *
 * @see \oroboros\core\traits\codex\CodexTrait
 *
 * Or by extending the abstract
 *
 * @see \oroboros\core\abstracts\codex\Codex
 *
 * ----------------
 *
 * CONSIDERATIONS
 *
 * ----------------
 *
 * These interfaces DO enforce methods, for objects to report their api.
 * This condition can be satisfied by including the api trait in your class.
 * Most traits use this one, so it is likely already available if you are
 * implementing any other trait.
 *
 * @see \oroboros\core\traits\api\ApiTrait
 *
 * These interfaces DO declare numerous constants.
 * There is a low probability of constant collision with external codebases.
 * If this causes an issue, wrap the object that implements
 * the api in one that doesn't, and use a pass-through to obtain it's values.
 *
 * There is a trait that can accomplish this strict enumeration based off of
 * any interface attached to a class that uses it, which can also filter
 * results by prefix or suffix of the constant name. It's super handy for
 * indexing these in any class that uses them.
 *
 * @see \oroboros\enum\traits\EnumTrait
 *
 * There are also sets of provided defaults under the concrete namespace
 *
 * @see \oroboros\enum
 *
 * @satisfies \oroboros\core\interfaces\enumerated\type\ClassType::CLASS_TYPE_API_VALID
 * @author Brian Dayhoff <brian@mopsyd.me>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/api_interface.md
 * @category api-interfaces
 * @package oroboros/core
 * @subpackage psr0
 * @version 0.2.5
 * @since 0.2.4-alpha
 */
interface Psr0Api
extends \oroboros\core\interfaces\api\standards\StandardsApi
{
    /**
     * -------------------------------------------------------------------------
     *                          Api Details
     * -------------------------------------------------------------------------
     */

    /**
     * Designates the type of this Api Interface, and what realm of
     * responsibilities it is classified as.
     */
    const API_TYPE = 'standards';

    /**
     * Determines the focused goal within the api type.
     * The Api Scope reveals the underlying goal of this specific Api Interface,
     * and what the specific purpose of this collection of classes, traits,
     * and interfaces is meant to collectively accomplish.
     */
    const API_SCOPE = 'psr0';

    /**
     * Designates a namespace provided by the package if one exists.
     * Packages that are distributed as a sub-package of a
     * larger parent package should still declare this constant,
     * but set its value to false.
     */
    const API_PROVIDES_NAMESPACE = false;

    /**
     * Designates a parent package or namespace that this Api adheres to.
     * This value should be false if no such parent exists,
     * but this constant should still be declared.
     */
    const API_PARENT_NAMESPACE = false;

    /**
     * Designates the codex index for this Api, which can be used to reference
     * this collection of classes, traits, and interfaces as a related family
     * working toward a specific goal.
     *
     * This value should always be provided for any valid Api Interface.
     */
    const API_CODEX = 'psr0';

    /**
     * Designates the map file for determining sub-packages and parent packages,
     * which is parsed by the Codex. This should be false if the package does not
     * provide a package map. If this constant is not defined or the file cannot be
     * found from the package root, then the package will contain an incomplete
     * Codex assignment.
     *
     * This value should always be provided for any valid Api Interface.
     */
    const API_PACKAGE_MAP = false;

    /**
     * Designates the package name of the api.
     * All packages must declare a package name to be considered valid.
     *
     * This value should always be provided for any valid Api Interface.
     */
    const API_PACKAGE = 'psr/autoload-deprecated';

    /**
     * Designates the parent package of the package, if one exists,
     * which is parsed by the Codex. This should be false if the package does not
     * have a parent package. If this constant is not defined or the file cannot be
     * found from the package root, then the package will be assumed not to have
     * a parent package by the Codex.
     *
     * This value should always be provided for any valid Api Interface.
     */
    const API_PARENT_PACKAGE = false;

    /**
     * Designates the primary category of responsibility of the api.
     * All packages must declare some category to be considered valid,
     * which allows them to be indexed by the Codex in terms of relationship
     * to other packages.
     *
     * This value should always be provided for any valid Api Interface.
     */
    const API_CATEGORY = 'standards';

    /**
     * Designates the subcategory of responsibility of the api.
     * Packages may not need to declare a subcategory, but should provide
     * this constant as false if no subcategory exists. The subcategory
     * determines what realm of responsibility the package has within
     * a broader category.
     *
     * If this constant is omitted, a default value of false will be assumed.
     */
    const API_SUBCATEGORY = 'autoloading';

    /**
     * -------------------------------------------------------------------------
     *                          Class Declarations
     * -------------------------------------------------------------------------
     */
}
