<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\api\patterns;

/**
 * <Pattern Api Interface>
 * This interface represents the root level pattern api interface.
 *
 * This interface may be placed on any class that declares it's class type as pattern.
 * @see \oroboros\core\interfaces\enumerated\type\ClassType::CLASS_TYPE_PATTERN
 *
 * For more information on Api Interfaces
 * @see \oroboros\core\interfaces\api\BaseApi
 *
 * Api Interfaces serve as an index of how class type and class scope
 * relate to specific api use cases.
 * This information is available by checking the Codex
 *
 * (the given example is actually an instance of this interface itself)
 * @see \oroboros\core\codex\Codex
 *
 * Which can also be done by any class using the codex trait
 *
 * @see \oroboros\core\traits\codex\CodexTrait
 *
 * Or by extending the abstract
 *
 * @see \oroboros\core\abstracts\codex\Codex
 *
 * @satisfies \oroboros\core\interfaces\enumerated\type\ClassType::CLASS_TYPE_API_VALID
 * @author Brian Dayhoff <brian@mopsyd.me>
 * @since 0.2.5
 */
interface PatternApi extends \oroboros\core\interfaces\api\BaseApi
{
    //no-op
}
