<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\contract\patterns;

/**
 * <Pattern Contract Interface>
 * Patterns represent software Design Patterns, which are used to solve
 * complex problems in a scalable way through a uniform approach.
 * This contract does not enforce requirements, but does designate
 * that an implementing class is capable of fielding such behavior.
 * There are also contracts for specific subsets of design patterns,
 * which are pre-categorized to their typical scope. This family of
 * contracts may be easily extended upon to add your own custom categorization.
 * This package distributes the four most common categories out of the box:
 * (behavioral, creational, structural, and concurrency), and provides generic
 * implementations of most design patterns that will fit the majority of
 * use cases. These are by far not the only solutions, and should not be taken
 * to be mandatory answers in any regard.
 *
 * The Contract Interfaces in this system that designate categories of patterns
 * DO NOT enforce methods, and should be used only for categorization.
 * The specific patterns DO enforce methods, which correspond to our released
 * traits to honor them. If you want to fully develop your own that has no
 * inherent correlation to our approach, you should
 * IMPLEMENT THE CATEGORICAL CONTRACT, and NOT THE PATTERN SPECIFIC CONTRACT,
 * then fill in the blanks as needed. Given the broad application of
 * design patterns and huge variance of scope, we CAN NOT apply an enforced
 * standard approach to them without enforcing opinion that reduces options
 * in various cases, which is against the mission of this software.
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 * 
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @package oroboros/core
 * @subpackage patterns
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 */
interface PatternContract
extends \oroboros\core\interfaces\contract\BaseContract,
// \oroboros\core\interfaces\contract\core\BaselineContract,
 \oroboros\core\interfaces\contract\core\StaticBaselineContract
{
    //no-op, only designates the implementer as a design pattern
}
