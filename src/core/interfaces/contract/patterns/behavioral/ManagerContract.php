<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\contract\patterns\behavioral;

/**
 * <Manager Pattern Contract Interface>
 * Enforces the methods required for a class to be recognized as a valid Worker
 * that can be handled by a Director.
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @package oroboros/core
 * @subpackage patterns
 * @version 0.2.5
 * @since 0.2.5
 * @see \oroboros\core\interfaces\contract\patterns\behavioral\DirectorContract
 * @see \oroboros\core\traits\patterns\behavioral\ManagerTrait
 */
interface ManagerContract
extends DirectorContract
{

    /**
     * <Manager Constructor>
     * This is the standard library constructor in Oroboros Core.
     * It presents the opportunity to inject dependencies, settings,
     * and flags to modify behavior throughout its entire category of work.
     *
     * This constructor is a proxy to the initialize method, which does the
     * actual effort of initializing. These methods are separated so that
     * re-initialization can occur on the same object without the weight of
     * re-instantiation, which allows objects to be recycled or reset more
     * performantly than creating a new object.
     *
     * @param type $dependencies (optional) An array of dependencies to inject
     * @param type $settings (optional) An array of settings to pass to the manager to determine its operation setup
     * @param type $flags (optional) An array of flags to pass to the manager to determine modifiers to execution
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if any parameter is provided that does not validate
     * @since 0.2.5
     */
    public function __construct( $dependencies = array(), $settings = array(),
        $flags = array() );

    /**
     * <Manager Initialization>
     * This method performs the actual initialization of the object.
     * By default, this is called during instantiation, but may be called again
     * at any time to reset initialization state. This allows an instantiated
     * object to be templated out as a base instance and populated with settings
     * post-instantiation as needed, making it compatible with prototypical object
     * creation approaches.
     *
     * @param type $dependencies (optional) An array of dependencies to inject
     * @param type $settings (optional) An array of settings to pass to the manager to determine its operation setup
     * @param type $flags (optional) An array of flags to pass to the manager to determine modifiers to execution
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if any parameter is provided that does not validate
     * @throws \oroboros\core\utilities\exception\RuntimeException if any dependency is created that cannot resolve
     * @since 0.2.5
     */
    public function initialize( $dependencies = array(), $settings = array(),
        $flags = array() );

    /**
     * <Initialization Check>
     * Returns a boolean determination as to whether the manager
     * is already initialized or not.
     * @return bool
     */
    public function isInitialized();
}
