<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\contract\patterns\structural;

/**
 * <Control Api Contract Interface>
 * Enforces expected methods for an object providing the
 * StaticControlApi as a means of interaction. The StaticControlApi is an alternative to
 * the Decorator, and solves a number of issues that the Decorator typically
 * encounters. With decorators, the initial object is wrapped, and it is
 * difficult to determine direct association between the initial object
 * and the decorator if care is not explicitly taken to provide them both
 * with a consistent interface. This requires constantly writing a lot of
 * interfaces for numerous use cases, which adds a lot of unwarranted
 * complexity in many cases. The StaticControlApi inverts this process, so that
 * objects providing extended functionality are ingested instead of wrapping
 * the base object. This allows you to always work with one consistent object
 * with one consistent interface. The StaticControlApi requires an "api" method be
 * implemented to get a runtime analysis of any methods that have been added
 * to it, so additional extensions provide a simple means of reporting their
 * capabilities at all times. StaticControlApi instances may also be extended with
 * Closures or invokeable objects, which is not possible with a decorator.
 * This should make them appealing to Functional programmers who prefer
 * stateless operations.
 *
 * Api methods must be recognizable along the following parameters:
 *
 * In contrast to the ControlApi, the StaticControlApi MUST consider methods
 * immutable once they are set. Static passthroughs and api methods MUST NOT
 * be overridden if they already exist or unregistered. Additionally the
 * StaticControlApi MUST NOT maintain any internal state other than
 * keeping their registry of methods and dependencies of those methods.
 * This is to prevent the typical difficulty debugging static state,
 * and insure that changes are not made that affect other unrelated scopes.
 *
 * camelCased and BruteCased methods must represent MUST be represented
 * in api shorthand as a dash-separated canonicalized word, and
 * return an array with keys corresponding to this format.
 * Underscores also MUST be returned as hyphens.
 * BruteCased method names MUST omit prefixing the initial letter with a dash in the api response.
 * Prefixing underscores on method names MUST be ignored if they are single underscores,
 * and the api MUST NOT represent prefixing underscores as hyphens in the key name.
 *
 * Double underscores MUST raise an exception if registration is
 * attempted against them, as this will cause collisions with PHP magic methods.
 *
 * Api methods MUST contain as their first word an api index
 * keyword that designates which index they belong to.
 *
 * Methods that contain only one word MUST raise an exception if they are not public when registration occurs.
 *
 * Public single word methods MUST be treated as a passthrough to that specific api index,
 * which provides a means to filter parameters and pass them to child methods properly if required.
 *
 * Indexes MUST NOT require a passthrough method. Methods MUST be reached correctly using __call()
 *
 * The index keys "api", "register", "unregister", and "check" MUST NOT be registerable,
 * as these represent expected methods for api interaction that should not be
 * confused with provided apis.
 *
 * Api extension methods SHOULD have a visibility of protected
 * on all classes that are capable of being extended further.
 * Extension methods MAY have a visibility of private on final classes.
 *
 * Api passthroughs and extension methods MUST NOT be static.
 *
 * Api passthroughs and extension methods MUST NOT require state locally,
 * or use objects that maintain state, unless the reset that state
 * back to the original state prior to returning a result.
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @package oroboros/core
 * @subpackage patterns
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 */
interface StaticControlApiContract
extends StructuralContract
{

    /**
     * <Api Index Method>
     * This method acts as a means of checking the dynamically generated api at runtime.
     *
     * This method MUST return false if there are no known matches for the provided api.
     *
     * This method MUST be able to return case-insensitive api results in a dash-separated format.
     *
     * This method MUST return [false] if [$index] is null and [$action] is not.
     * This prevents possible ambiguity when two or more indexes have the same
     * method declared.
     *
     * Example:
     *   - function exampleTestMethod() exists under index "example" and is represented as "test-method" in the api.
     *
     *   - function ExampleTestMethod() follows identical criteria to the above.
     *
     *   - function _exampleTestMethod() follows identical criteria to the above.
     *
     *   - function _ExampleTestMethod() follows identical criteria to the above.
     *
     * In this case, calling $object->api() would return:
     *
     * array{
     *     "example" => array{
     *         "test-method" => array( ... )
     *     }
     * }
     *
     * Calling $object->api("example") would return:
     *
     * array{
     *     "test-method" => array ( ... )
     * }
     *
     * And calling $object->api("example", "test-method") would return
     * only the detail array for the test method.
     *
     * Calling $object->api("test-method") would return [false],
     * because "test-method" is not a valid api index.
     *
     * Api arrays for valid actions MUST contain all of the following keys:
     *
     * "params"
     *   What parameters the function takes if any, or false if none.
     *   the params array should have a key that corresponds to the param name
     *   without the dollar sign, and the type or types it represents.
     *   If there are multiple, it MAY be represented by the keyword "mixed"
     *   or list individual parameter types separated by pipes.
     *
     * "returns"
     *   What to expect the method to return, or "void" if nothing is returned.
     *   If multiple return formats are possible, it MAY be represented by the
     *   keyword "mixed" or list individual parameter types separated by pipes.
     *   If this cannot be determined, false should be returned.
     *
     * "throws"
     *   Any exceptions that are thrown by the method, or false if no
     *   exceptions are thrown. This MUST anticipate if further stack levels
     *   will throw exceptions within it's logic that are not caught by the method.
     *   If one or more exceptions are thrown, this should be an associative array
     *   with keys corresponding to the exception names, and values corresponding
     *   to the reason they are thrown, or an empty string if no reason is supplied.
     *   If an exception can be thrown for multiple reasons, then it should return
     *   a numerically keyed array of reasons for that exception represented as
     *   strings, or empty strings if no reason is provided.
     *
     * @param string $index (optional) If provided, returned api will be scoped to the provided set, if it exists.
     * @param type $action (optional) If provided, returned api will be scoped to the provided action, if it exists.
     * @return array|boolean Returns false if the specified parameters do not exist, otherwise returns an array of methods within the provided scope.
     */
    public static function api( $index = null, $action = null );

    /**
     * <Api Check Method>
     * Checks if a supplied index or method of an index exists.
     *
     * This method MUST NOT throw an exception under any circumstances.
     *
     * This method MUST return true if the index exists and $action is null,
     * or if $action is supplied and it exists within the supplied index.
     *
     * This method MUST return false if the specified params do not exist,
     * or if any invalid parameters are passed.
     *
     * @param string $index (optional) If provided, searches only in the scope of the provided index.
     * @param string $action (optional) If provided, searches only for the provided api method.
     * @return bool
     */
    public static function check( $index, $action = null );

    /**
     * <Control Api Extension Method>
     * This method allows the ControlApi to be extended with a nested ControlApi,
     * which will allow the base object to provide the api of sub objects as if
     * it were part of it's own. All StaticControlApiExtensionContract instances are
     * also StaticControlApis, which makes this pretty simple to do.
     *
     * Objects passed through the extend method are always StaticControlApis.
     * The object receiving the extension must add its api to its own,
     * and have a means of designating which api methods internally are
     * representative of other control apis and which are locally accessible.
     * The public provided api does not reveal this information, however the
     * internal selector should be aware of it, and not waste a ton of resources
     * trying all possibilities before reaching the correct method.
     *
     * Extension apis MUST receive their own index. They MUST NOT be mixed
     * with non-extension indexes. Commands that pass through to the existing
     * api of the extension should be prefixed with this index key, and the
     * key should be stripped before passing the subsequent key to the nested
     * api.
     *
     * If this causes an illegal collision within the logic of the StaticControlApi,
     * it MUST throw an InvalidArgumentException, with an explanation of what
     * the collision entails. This will most generally occur when attempting to
     * override an existing index, which is not allowed in the case of static
     * control apis.
     *
     * @param \oroboros\core\interfaces\contract\patterns\structural\StaticControlApiExtensionContract $extension
     * @throws \InvalidArgumentException If any invalid parameter is passed
     */
    public static function extend( $extension );

    /**
     * <Index Registration>
     * Allows external registration of an Api index. This is most useful to decorators.
     *
     * This method MUST take a string or any value that can resolve to a valid
     * string representation of a method name when cast to a string with (string).
     *
     * This method MUST raise an InvalidArgumentException if it receives an invalid parameter.
     *
     * This method MAY raise an InvalidArgumentException if this specific
     * object does not allow external population, or if an attempt is made to
     * override an existing method and the implementing class does not allow this.
     *
     * This method MUST NOT resolve without raising an exception if it did not
     * retain the passed index.
     *
     * The implementing object MAY refuse to accept external registration of indexes,
     * and MUST NOT override existing indexes.
     *
     * This method SHOULD log any errors within the scope of it's implementation
     * if logging is possible.
     *
     * @param string $index
     * @throws \InvalidArgumentException If any invalid parameter is passed
     */
    public static function registerIndex( $index );

    /**
     * <Method Registration>
     * Allows external method registration for a specified index. This is most
     * useful to decorators.
     *
     * This method MUST take a string or any value that can resolve to a valid
     * string representation of a method name when cast to a string with (string).
     *
     * This method MUST take any callable parameter for $method.
     *
     * This method MUST NOT accept a callable parameter that is not enclosed
     * in it's own scope. For this purpose, the following things are considered
     * to be enclosed in it's scope correctly:
     *
     * - A method the class has.
     * - A closure passed as a variable.
     * - An array containing an instantiated object and a public method.
     * - An invokeable object that is already instantiated.
     *
     * The following are NOT considered to be enclosed in it's scope,
     * and should raise an exception:
     *
     * - A string representing a function name.
     * - A string representing a static method that does not exist
     *   in this object, or comes from a class that this object is
     *   not an instance of.
     * - An array containing a method name and a string representation
     *   of a class that this object is not an instance of.
     *
     * These MUST NOT be accepted. This is to prevent ambiguity
     * and sphaghetti code-ish practices.
     *
     * This method MUST raise an InvalidArgumentException if it receives an invalid parameter.
     *
     * This method MAY raise an InvalidArgumentException if this specific
     * object does not allow external population.
     *
     * This method MUST raise an InvalidArgumentException on any attempt
     * to override an existing method.
     *
     * This method MUST NOT resolve without raising an exception if it did not
     * retain the passed index.
     *
     * The implementing object MAY refuse to accept external registration if indexes,
     * and MAY simply throw an InvalidArgumentException any time this method is called.
     *
     * This method SHOULD log any errors within the scope of it's implementation
     * if logging is possible.
     *
     * @param string $index
     * @param string|callable $method
     * @param callable $callable (optional) If $method is not a callable, this MUST be a callable to represent the key
     * @throws \InvalidArgumentException If any invalid parameter is passed
     */
    public static function registerMethod( $index, $method, $callable = null );

    /**
     * <Api Call Magic Method>
     * Allows api indexes to have their methods called,
     * even if no passthrough was registered for that api.
     *
     * The __call method for the ControlApi is tasked with executing
     * canonicalized methods,and resolving their names to a set of
     * internal methods that exist for the object.
     *
     * The [$name] parameter MUST correspond to an existing api index.
     *
     * An InvalidArgumentException MUST be raised if the [$name]
     * parameter is not a valid index.
     *
     * If a method exits named [$name], or an object has been supplied
     * representing that index and that object has that method name,
     * then this method MUST return a call to that method passing $args
     * as its only parameter.
     *
     * If no method representing a passthrough exists and $args is empty,
     * this method MUST raise an InvalidArgumentException.
     *
     * If no method representing a passthrough exists and $args is not empty,
     * it's first parameter should be anticipated to be the correct canonicalized
     * method name.
     *
     * If the above is true and the parameter is not a string,
     * this method MUST raise an InvalidArgumentException.
     *
     * If the first key of $args is a string, it must reverse canonicalize
     * to a valid method registered for a registered index and method.
     * If such a method exists and no passthrough for the index exists,
     * it MUST be called passing args as it's only argument, WITHOUT the
     * key representing the method name being passed in that array. If such a
     * method exists and a passthrough for that index DOES exist,
     * the return value for this method should be the result of the passthrough
     * for that method MUST be called instead, providing the first key of $args
     * as it's first parameter, and the rest of $args without that key as it's
     * second parameter.
     *
     * This method MUST wrap method calls in a try catch block, and anticipate
     * the possibility of an exception being thrown. If the exception is an
     * instance of InvalidArgumentException, then that exception MUST be
     * rethrown as is. If ANY OTHER exception is thrown, It must raise a
     * RuntimeException and be passed as the third parameter of that exception.
     *
     * This method SHOULD log any errors within the scope of it's implementation
     * if logging is possible.
     *
     * @param string $name The api index name
     * @param array $args The api index arguments
     * @throws \InvalidArgumentException if any invalid parameter is passed.
     * @throws \RuntimeException if the referenced method does exist but raises an exception other than an InvalidArgumentException.
     * @return mixed
     */
    public static function __callStatic( $name, $args = array() );
}
