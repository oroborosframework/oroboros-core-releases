<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\contract\patterns\structural;

/**
 * <Decorator Contract Interface>
 * The Decorator pattern is a well known approach to extending functionality
 * of a class at runtime. The basic operation is that an object is packaged
 * within a decorator, where each decorator adds functionality and passes
 * any remaining functionality on to the original object it received if it
 * does not have that functionality itself.
 *
 * @see \oroboros\core\interfaces\contract\patterns\structural\DecorateeContract
 * @see \oroboros\core\traits\patterns\structural\DecoratorTrait
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 * 
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @package oroboros/core
 * @subpackage patterns
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 */
interface DecoratorContract
extends DecorateeContract
{
    /**
     * <Decorator Constructor>
     * The decorator constructor must receive the object it is intended to decorate.
     * If that object is capable of being decorated, it must implement the
     * Decoratee object.
     *
     * @param \oroboros\core\interfaces\contract\patterns\structural\Decoratee $object
     */
    public function __construct( Decoratee $object );

    /**
     * <Decorator call method>
     * This method represents the passthrough to methods provided by the decoratee.
     * If this instance does not have a method called, then it MUST use this
     * method to check if the parent class has it and return the results of that
     * unmodified.
     *
     * This MUST be implemented in such a way that the parameters are
     * called as expected in the decoratee.
     *
     * This MUST return the result from the decoratee unmodified.
     *
     * This MUST allow exeptions to bubble up unhandled.
     *
     * @param string $name
     * @param args $args
     * @return mixed
     * @throws \BadMethodCallException if a method is called that does not exist in the decoratee scope.
     * @throws \Exception any exceptions resulting from the decoratee are to be left unhandled in this scope.
     */
    public function __call( $name, $args = array() );

    /**
     * <Decorator call static method>
     * This method represents the passthrough to static methods provided by the decoratee.
     * If this instance does not have a static method, then it MUST use this
     * method to check if the parent class has it and return the results of that
     * unmodified.
     *
     * This MUST be implemented in such a way that the parameters are
     * called as expected in the decoratee.
     *
     * This MUST return the result from the decoratee unmodified.
     *
     * This MUST allow exeptions to bubble up unhandled.
     *
     * @param string $name
     * @param args $args
     * @return mixed
     * @throws \BadMethodCallException if a method is called that does not exist in the decoratee scope.
     * @throws \Exception any exceptions resulting from the decoratee are to be left unhandled in this scope.
     */
    public static function __callStatic( $name, $args = array() );

    /**
     * <Decorator getter method>
     * This method represents the passthrough to decoratee class properties.
     * If this instance does not have a property, then it MUST use this method
     * to check if the decoratee has the property.
     *
     * This MUST return the result from the decoratee unmodified.
     *
     * This MUST allow errors or exceptions to bubble up unhandled.
     *
     * @param string $name
     * @return mixed
     */
    public function __get( $name );

    /**
     * <Decorator setter method>
     * This method represents the passthrough to the decoratee setter (if one exists).
     * If this instance does not have a property, then it MUST use this method to
     * attempt to set the property on the decoratee.
     *
     * This MUST be implemented in such a way where the property is set in the
     * decoratee EXACTLY as passed.
     *
     * This MUST allow errors or exceptions to bubble up unhandled.
     *
     * @param type $name
     * @param type $value
     */
    public function __set( $name, $value );
}
