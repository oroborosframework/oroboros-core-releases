<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\contract\utilities\error;

/**
 * <Error Handler Utility Contract Interface>
 * This contract interface requires the methods for a valid
 * Oroboros error handler.
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @package oroboros/core
 * @subpackage utilities
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 * @see \oroboros\core\traits\utilities\exception\ExceptionTrait
 */
interface ErrorHandlerContract
extends \oroboros\core\interfaces\contract\utilities\UtilityContract
{

    /**
     * <Error Handler Initialization>
     * Initializes the error handler.
     *
     * @param array $params
     * @param array $dependencies
     * @param array $flags
     * @return void
     */
    public function initialize( $params = null, $dependencies = null,
        $flags = null );

    /**
     * <Error Handler Registration Method>
     * Registers the error handler with PHP to accept inbound errors from PHP.
     * The previous error handler will be captured when this occurs, if one
     * existed. If the internal switch to defer to previous is set, then error
     * handling return values will be provided by the previous error handler,
     * otherwise they will be handled as per the internal settings of the
     * error handler.
     * @return $this
     */
    public function register();

    /**
     * <Error Handler Unregistration Method>
     * Unregisters the error handler, and restores the previous error handler.
     * @return $this
     */
    public function unregister();

    /**
     * <Error Handler Registration Check Method>
     * Returns a boolean determination as to whether the error handler
     * is currently registered. True if it is active, and false if
     * it is inactive.
     * @return bool
     */
    public function isRegistered();

    /**
     * <Error Handler Previous Error Callback Getter Method>
     * Returns the previous error handler callback if one was declared.
     * If the error handler is currently in an unregistered state, this
     * will always return null. If the error handler is registered, this
     * will return null if no previous error handler was captured, and
     * otherwise will return the callable declaration of the previous
     * error handler.
     * @return null|callable
     */
    public function getPreviousHandler();

    /**
     * <Error Handler Previous Error Handler Check Method>
     * Returns a boolean determination as to whether the error handler has
     * detected a prior error handler registered after its registration.
     * If the error handler is currently in an unregistered state, this will
     * always return false even if one is set. If the error handler has
     * been registered, then this will return true if another error handler
     * method was obtained during registration, and false if there was no
     * prior error handler.
     * @return bool
     */
    public function hasPreviousHandler();

    /**
     * <Error Handler Error Level Setter Method>
     * Recieves an array of the error levels to set handling for, and declares
     * them as reportable error levels. These must correspond to the
     * canonicalized string keys that can be obtained by calling
     * getErrorLevelKeys, and must exist in getCatchableErrorLevels,
     * or an InvalidArgumentException will be raised.
     * @param array $levels The error levels to handle
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If an invalid key is provided, or an error level that is not
     *     runtime catchable is provided.
     */
    public function setErrorLevels( $levels );

    /**
     * <Error Handler Error Level Identifying Key Getter Method>
     * Returns an indexed array of the valid canonicalized keys corresponding
     * to valid PHP error levels. These may be used interchangeably with the
     * integer bitmask value to designate how to handle errors throughout the
     * other methods of the error handler.
     * @return array
     */
    public function getErrorLevelKeys();

    /**
     * <Error Handler Runtime Catchable Error Level Getter Method>
     * Returns an associative array of the canonicalized slug designations
     * of catchable error levels, with the value being the bitmask integer
     * value for the error level. The return values are levels that can be
     * handled at runtime by the error handler. Error levels that are not
     * in this result cannot be caught at runtime by the error handler.
     * @return array
     */
    public function getCatchableErrorLevels();

    /**
     * <Error Handler Error Level Getter>
     * Refreshes the internal bitwise mask of the current PHP error
     * level visibility, and then returns an associative array of each
     * valid error key, with a boolean value designating whether that
     * error level is currently visible.
     * @return array
     */
    public function getErrorLevels();

    /**
     * <Error Handler Error Level Check Method>
     * Checks if visibility is enabled for a given error level, and
     * returns a boolean determination as to whether the given error
     * level exists in PHPs current error handling scope.
     *
     * This method can receive a predefined PHP constant designation
     * of an error level (eg: E_ALL, E_DEPRECATED, E_USER_ERROR, etc),
     * the integer value corresponding to one of these, or the canonical
     * string of the internal error value designators. All of these will
     * resolve to a valid response.
     *
     * @param int|string $level
     * @return bool
     */
    public function hasErrorLevel( $level );

    /**
     * <Error Handler Error Level Add Method>
     * Adds a given error level to the handled visibility level.
     *
     * This method can receive a predefined PHP constant designation
     * of an error level (eg: E_ALL, E_DEPRECATED, E_USER_ERROR, etc),
     * the integer value corresponding to one of these, or the canonical
     * string of the internal error value designators. All of these will
     * resolve correctly.
     *
     * @param int|string $level
     * @return void
     */
    public function addErrorLevel( $level );

    /**
     * <Error Handler Error Level Remove Method>
     * Removes a given error level to the handled visibility level.
     *
     * This method can receive a predefined PHP constant designation
     * of an error level (eg: E_ALL, E_DEPRECATED, E_USER_ERROR, etc),
     * the integer value corresponding to one of these, or the canonical
     * string of the internal error value designators. All of these will
     * resolve correctly.
     *
     * @param int|string $level
     * @return void
     */
    public function removeErrorLevel( $level );

    /**
     * <Error Handler Bitwise Visibility Setter Method>
     * Sets the full visibility scope by the provided bitmask.
     * This will override all other set levels that currently exist.
     * @param int $bitmask
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If an invalid bitmask is supplied.
     */
    public function setErrorVisibility( $bitmask );

    /**
     * <Error Handler Bitmask Error Visibility Getter Method>
     * Returns the integer bitmask value of the current error level.
     * @return int
     */
    public function getErrorVisibility();

    /**
     * <Error Handler Bitmask Error Visibility Setter Method>
     * Checks if a given bitmask integer value exists, and returns
     * a boolean determination as to whether or not it is defined.
     *
     * This method should be used for complex bitwise operators in place
     * of hasErrorLevel. For single error level checks, hasErrorLevel can
     * handle both canonical strings and integer checks, and getErrorLevels
     * will return the full designation for all valid error levels as an
     * associative array by bitwise key slug identifier. Those methods
     * should be more often used for general checks, but this method is
     * provided for automatic checking with configurations from 3rd party
     * systems that track by integer or bitwise math values.
     *
     * @param int $bitmask
     * @return bool
     */
    public function hasVisibility( $bitmask );

    /**
     * <Error Handler Method>
     * This is the method that will be called directly for all versions of PHP
     * prior to 7.2.0, and is the method registered when the register() method
     * is called prior to PHP 7.2.0. This method will loop through the given
     * internals and pass the listed parameters to the correct methods as well
     * as the previous error handler, if one existed.
     *
     * This method will still be called for PHP version 7.2.0 and later
     * internally, but will not be directly called by PHP, as the $errcontext
     * parameter raises a deprecated warning if called directly after version
     * 7.2. For versions later than this, the method with this parameter
     * omitted will be directly registered, and it will instead call this
     * method and always pass null for $errcontext, but this method will
     * still handle the actual error.
     *
     * The return value of this method depends on the internal settings,
     * and whether or not the option to allow the previous handler to handle
     * the error is set to true.
     *
     * @param int $severity Contains the level of error raised as an integer.
     * @param string $message Contains the error message as a string.
     *     This parameter is always defined.
     * @param string $file (optional) Contains the name of the file
     *     where the error occurred as a string, or null if not defined.
     * @param int $line (optional) Contains the line that the error
     *     occurred at as an integer, or null if not defined.
     * @param array $context (optional) Contains an array of all variables
     *     that existed in the scope of where the method arose. This parameter
     *     was deprecated in PHP 7.2, and will always be null for versions later
     *     than that.
     * @return bool If true, the PHP internal error handler will fire.
     *     If false, the error will be considered to be handled correctly
     *     and the program will continue to operate.
     */
    public function handleError( $severity, $message, $file = null,
        $line = null, $context = null );

    /**
     * <PHP 7.2 Error Handler Method>
     * This is the method that will be called directly for all versions of PHP
     * after 7.2.0. This methods purpose is to suppress the deprecation warning
     * for the error context parameter. Otherwise it is only a proxy to the
     * standard error handler method.
     *
     * The return value of this method depends on the internal settings,
     * and whether or not the option to allow the previous handler to handle
     * the error is set to true.
     *
     * @param int $severity Contains the level of error raised as an integer.
     * @param string $message Contains the error message as a string.
     *     This parameter is always defined.
     * @param string $file (optional) Contains the name of the file
     *     where the error occurred as a string, or null if not defined.
     * @param int $line (optional) Contains the line that the error
     *     occurred at as an integer, or null if not defined.
     * @param array $context (optional) Contains an array of all variables
     *     that existed in the scope of where the method arose. This parameter
     *     was deprecated in PHP 7.2, and will always be null for versions later
     *     than that.
     * @return bool If true, the PHP internal error handler will fire.
     *     If false, the error will be considered to be handled correctly
     *     and the program will continue to operate.
     * @see handleError
     */
    public function handleError72( $severity, $message, $file = null,
        $line = null );

    /**
     * <Error Handler Fatal Error Handler Method>
     * Handles fatal on shutdown if the error handler is registered,
     * and if it has any operations registered for fatal errors.
     */
    public function handleFatalError();

    /**
     * <Error Handler Error Statistics Getter Method>
     * Returns an array of all errors collected by the error handler,
     * which are keyed by the microtime that they occurred, and contain the
     * error details, the methods assigned to handle the error, the previous
     * error handler if it fired, whether or not the error was suppressed,
     * and which method suppressed it if any did.
     * @return array
     */
    public function getErrorStatistics();

    /**
     * <Error Handler Statistic Tracking Enable Method>
     * Turns on error statistic tracking.
     *
     * Error statistics keeps an internal log of all errors received,
     * whether they were handled, when and where they originated from,
     * and a number of other contextual details about the error. This
     * information is not automatically logged externally, but may be
     * retrieved at any time by calling getErrorStatistics. Only errors
     * that fire while the error handler is registered and error statistics
     * are enabled will be tracked. If disabled, no statistical tracking
     * will take place, but existing tracking entries will be retained.
     *
     * @return void
     */
    public function enableErrorStatistics();

    /**
     * <Error Handler Statistic Tracking Disable Method>
     * Turns off error statistic tracking.
     *
     * Error statistics keeps an internal log of all errors received,
     * whether they were handled, when and where they originated from,
     * and a number of other contextual details about the error. This
     * information is not automatically logged externally, but may be
     * retrieved at any time by calling getErrorStatistics. Only errors
     * that fire while the error handler is registered and error statistics
     * are enabled will be tracked. If disabled, no statistical tracking
     * will take place, but existing tracking entries will be retained.
     *
     * @return void
     */
    public function disableErrorStatistics();

    /**
     * <Error Handler Automatic Logging Enable Method>
     * Turns on error automatic logging of unhandled errors.
     *
     * Errors that are not handled by any internal registered runtime operation
     * as well as fatal errors will be automatically logged to the standard
     * Oroboros logger if automatic logging is enabled. If disabled, no logging
     * will automatically take place.
     *
     * @return void
     */
    public function enableErrorLogging();

    /**
     * <Error Handler Automatic Logging Disable Method>
     * Turns off error automatic logging of unhandled errors.
     *
     * Errors that are not handled by any internal registered runtime operation
     * as well as fatal errors will be automatically logged to the standard
     * Oroboros logger if automatic logging is enabled. If disabled, no logging
     * will automatically take place.
     *
     * @return void
     */
    public function disableErrorLogging();

    /**
     * <Error Handler Error Exception Enable Method>
     * Turns on error automatic conversion of unhandled errors to ErrorExceptions.
     *
     * Errors that are not handled by any internal registered runtime operation
     * will be recast to an oroboros instance of the ErrorException class at the
     * end of the handler operation if this is enabled, which allows native try/catch
     * blocks to manage errors without the overhead of complex integration of an
     * error handler. If disabled, they will simply propogate forward as the
     * same error that was originally triggered.
     *
     * @return void
     */
    public function enableUseExceptions();

    /**
     * <Error Handler Error Exception Disable Method>
     * Turns off error automatic conversion of unhandled errors to ErrorExceptions.
     *
     * Errors that are not handled by any internal registered runtime operation
     * will be recast to an oroboros instance of the ErrorException class at the
     * end of the handler operation if this is enabled, which allows native try/catch
     * blocks to manage errors without the overhead of complex integration of an
     * error handler. If disabled, they will simply propogate forward as the
     * same error that was originally triggered.
     *
     * @return void
     */
    public function disableUseExceptions();

    /**
     * <Error Handler Defer-to-Previous Error Handler Enable Method>
     * Turns on deferral of handling to the previously registered error handler,
     * if one exists. This can be useful for applications that already have
     * their own error handling schema, but want to collect additional
     * information about errors or log the ones that are not handled
     * automatically, or otherwise have a simple means of binding additional
     * error operations without disrupting the existing error handling logic.
     *
     * If enabled, errors will only be flagged as handled when a previous error handler
     * exists if that handler returns true. The internal operations of this
     * handler will still run, but their output will be ignored when this is
     * enabled. This cannot be simultaneously enabled with the call-previous
     * switch, and enabling either will disable the other. If this is disabled,
     * the previous error handler will not run at all unless the call-previous
     * switch is set to true.
     *
     * @return void
     */
    public function enableDeferPrevious();

    /**
     * <Error Handler Defer-to-Previous Error Handler Disable Method>
     * Turns off deferral of handling to the previously registered error handler,
     * if one exists.
     *
     * If enabled, errors will only be flagged as handled when a previous error handler
     * exists if that handler returns true. The internal operations of this
     * handler will still run, but their output will be ignored when this is
     * enabled. This cannot be simultaneously enabled with the call-previous
     * switch, and enabling either will disable the other. If this is disabled,
     * the previous error handler will not run at all unless the call-previous
     * switch is set to true.
     *
     * @return void
     */
    public function disableDeferPrevious();

    /**
     * <Error Handler Call Previous Error Handler Enable Method>
     * Turns on calls to the previous error handler during error handling.
     *
     * If enabled, errors will also be passed to the previous error handler if
     * one existed after all operations of this error handler resolve, and its
     * return response will also be considered in addition to the internal
     * responses as to whether the error was appropriately handled or not.
     * If this is disabled, the previous error handler will not run at all
     * unless the defer-previous switch is set to true.
     *
     * @return void
     */
    public function enableCallPrevious();

    /**
     * <Error Handler Call Previous Error Handler Disable Method>
     * Turns off calls to the previous error handler during error handling.
     *
     * If enabled, errors will also be passed to the previous error handler if
     * one existed after all operations of this error handler resolve, and its
     * return response will also be considered in addition to the internal
     * responses as to whether the error was appropriately handled or not.
     * If this is disabled, the previous error handler will not run at all
     * unless the defer-previous switch is set to true.
     *
     * @return void
     */
    public function disableCallPrevious();
}
