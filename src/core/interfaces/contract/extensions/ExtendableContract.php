<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\contract\extensions;

/**
 * <Extendable Contract Interface>
 * Enforces a set of methods to make a class extendable.
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @category extensions
 * @package oroboros/core
 * @subpackage extensions
 * @version 0.2.5
 * @since 0.2.5
 */
interface ExtendableContract
extends \oroboros\core\interfaces\contract\patterns\structural\StaticControlApiContract
{

    /**
     * <Extendable Contract Getter Method>
     * Returns the name of the valid extendable contract declared
     * that extensions are expected to implement.
     * @return string
     */
    public static function getExtendableContract();

    /**
     * <Extendable Contract Check Method>
     * Checks if the given contract interface honors
     * the expected contract interface.
     * @param string $contract
     * @return bool
     */
    public static function checkExtendableContract( $extension );

    /**
     * <Extendable Context Getter Method>
     * Returns the declared extension context that proposed
     * extensions are expected to match.
     * @return string
     */
    public static function getExtendableContext();

    /**
     * <Extendable Context Check Method>
     * Checks if the given context matches the expected context.
     * @param string $context
     * @return bool
     */
    public static function checkExtendableContext( $context );

    /**
     * <Extendable Extension Registration Method>
     * Registers a new extension into the extendable class.
     * @param \oroboros\core\interfaces\contract\extensions\ExtensionContract $extension
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the given extension is not valid
     */
    public static function extend( $extension );

    /**
     * <Extendable Extension Api Getter Method>
     * Passes an api command along to a specific extension by its
     * extension identifier key, which the underlying StaticControlApi
     * does not normally expose publicly. Returns false if the extension
     * does not exist, or if the specified index and/or command do not
     * exist in the extension, otherwise returns the standard collection
     * output of a StaticControlApi api call.
     * @param string $extension The extension slug identifier
     * @param string $index (optional) An optional index to check in the api of the extension
     * @param string $method (optional) An optional command to check within an index of the extension
     * @return bool|\oroboros\collection\Collection
     * @see \oroboros\core\interfaces\contract\patterns\structural\StaticControlApi::api
     * @see \oroboros\core\traits\patterns\structural\StaticControlApiTrait::api
     */
    public static function extensionApi( $extension, $index = null,
        $method = null );
}
