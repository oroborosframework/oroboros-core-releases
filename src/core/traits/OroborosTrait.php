<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2011-2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\traits;

/**
 * <Oroboros Trait>
 * Provides functionality for the global static accessor contract.
 * This trait represents the baseline logic for the front-most api.
 * of this package.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @category core
 * @package oroboros/core
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\core\interfaces\contract\OroborosContract
 * @satisfies \oroboros\core\interfaces\contract\patterns\structural\StaticControlApiContract
 */
trait OroborosTrait
{

    /**
     * Most of the underlying logic for the base accessor
     * class is provided by the static control api trait.
     */
    use \oroboros\core\traits\extensions\ExtendableTrait;

    /**
     * Represents a record of all of the times initialization has fired
     * for the base level accessor. In the event that debugging is required,
     * this will reveal when and where each initialization step occurred.
     * @var array
     */
    private static $_oroboros_initialization_registry = array();

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\core\interfaces\contract\OroborosContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Initialization>
     * This is the initialization method for the global accessor.
     * It will load the default dependencies of the system and insure
     * that they are available for further requests. This is typically
     * called during the bootload routine, and does not need to be
     * called again. However, for unit testing or customization,
     * you may reinitialize with specified parameters at any time.
     *
     * An internal registry of initializations will be kept, so in the
     * event of an error or mis-configuration or unexpected results,
     * some record of when/where the reinitialization can be easily
     * determined and corrected.
     *
     * This method will take three optional parameters.
     *
     * $dependencies is an optional associative array of dependencies for
     * dependency injection. If any of these are passed, it will
     * check that they honor the corresponding contract, and if so,
     * use them in place of it's defaults. This is useful for unit
     * testing or custom configuration. Any parameters that do not
     * implement the expected contract interface will cause an exception
     * to be raised and break initialization. The system will not make
     * any effort to catch this exception, as it will lead to potentially
     * unstable behavior if it is allowed to proceed.
     *
     * $options is an optional associative array of settings or configuration
     * details to pass into initialization. These correspond to the keys listed
     * in the conf/settings_default.json file. You may override these in bulk by
     * creating a file named settings.json and overriding any keys that you wish
     * to provide other defaults for. You only need to override the keys you want
     * to change defaults for, as the result will be array_merge'd with the default,
     * so any defaults that are not provided are retained. You may also provide a
     * settings file outside of the project directory structure by designating
     * its fully qualified path by defining the constant OROBOROS_BOOTLOAD_SETTINGS
     * as its fully qualified file path. The file must be in json format, but may
     * exist anywhere in your filesystem. You may also set an environment
     * variable with this same name in place of defining the constant, and if
     * such an environment variable exists, its value will be used to define
     * the constant in place of the default.
     *
     * $flags is an optional array of bootload flags that may be passed into
     * initialization. No flags are used by default, but any available in the
     * BootloadFlag enumerated interface may be used. These will modify behavior
     * of the system as described in their block comment in the enumerated api
     * interface. If reinitialization occurs, these flags will all be cleared
     * if they are not explicitly passed again. You may override these in bulk
     * by defining a constant named OROBOROS_BOOTLOAD_FLAGS as the fully
     * qualified path to a json file containing an array of the key names of
     * the flags you wish to pass. Only the flags in the BootloadFlag interface
     * will be honored, and all others will be silently discarded.
     * You may also accomplish this by declaring an environment variable
     * of the same name, which will be used in place of the constant if
     * such an environment variable exists.
     *
     * @param array $dependencies
     * @param array $options
     * @param array $flags
     * @return void
     */
    public static function initialize( array $dependencies = array(),
        array $options = array(), array $flags = array() )
    {
        self::_oroborosHandleInitialization( $dependencies, $options, $flags );
    }

    /**
     * <Initialization History Log Getter>
     * This method will recover the full history of all times that the
     * initialization method was called, along with from which line of
     * which file, what microtime was set at the time of the request,
     * and all arguments passed in to initialization.
     * @return array
     */
    public static function getInitializationHistory()
    {
        return self::_oroborosReturnInitializationHistory();
    }

    /**
     * <Initialization Check Method>
     * This method returns a boolean determination as to whether
     * it is safe to proceed calling other methods. If this
     * returns false, initialization needs to occur prior
     * to other usage.
     * @return bool
     * @since 0.0.0.1a
     */
    public static function isInitialized()
    {
        return self::isStaticInitialized();
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the internal api.
     * -------------------------------------------------------------------------
     */

    /**
     * <Extension Contract Declaration Method>
     * This method should be overridden with a contract interface
     * expected to be honored by valid extensions.
     * @return string
     */
    protected static function _declareExtensionContract()
    {
        return '\\oroboros\\core\\interfaces\\contract\\extensions\\core\\CoreExtensionContract';
    }

    /**
     * <Extension Context Declaration Method>
     * This method should be overridden with the slug identifier
     * that the extension is expected to match.
     * @return string
     */
    protected static function _declareExtensionContext()
    {
        return \oroboros\core\interfaces\api\OroborosApi::API_SCOPE;
    }

    /**
     * Sets the static baseline default parameters
     * @return array
     */
    protected static function _staticBaselineSetParametersDefault()
    {
        return \oroboros\core\utilities\core\CoreConfig::get( 'settings', 'core' );
    }

    /**
     * Sets the static baseline default parameters
     * @return array
     */
    protected static function _staticBaselineSetParametersValid()
    {
        return array_keys( self::_staticBaselineSetParametersDefault() );
    }

    /**
     * Sets the static baseline valid flags
     * @return array
     */
    protected static function _staticBaselineSetFlagsValid()
    {
        $validator = new \oroboros\enum\InterfaceEnumerator(
            '\\oroboros\\core\\interfaces\\enumerated\\flags\\BootloadFlags'
        );
        $valid = $validator->toArray();
        return $valid;
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * <Initialization Logic Method>
     * Handles the actual initialization. The public method is just a
     * passthrough to this method.
     * @param array $dependencies Any dependencies that should override the defaults.
     * @param array $options Any options that should override the defaults.
     * @param array $flags Any flags that should be retained to modify functionality.
     * @return string current class name for static method chaining
     * @since 0.2.5
     */
    private static function _oroborosHandleInitialization( array $dependencies,
        array $options, array $flags )
    {
        //The system is now usable
        self::_staticBaselineEnableFlagValidation();
        self::_staticBaselinePreventFlagUnSetting();
        self::_staticBaselineSetParameterPersistence( true );
        self::_staticBaselineSetDependencyPersistence( true );
        self::_staticBaselineSetFlagPersistence( true );
        self::_staticBaselineRegisterInitializationTask( '_oroborosPostInitialization' );
        self::_staticBaselineRegisterInitializationTask( '_oroborosRegisterInitialization' );
        self::staticInitialize( $options, $dependencies, $flags );
        return get_called_class();
    }

    /**
     * Handles the post-initialization operations after
     * the baseline initialization.
     * @return void
     */
    private static function _oroborosPostInitialization()
    {
        $settings = self::$_static_baseline_parameters;
        $mode = \oroboros\core\utilities\core\CoreConfig::get( 'mode' );
        foreach ( self::$_static_baseline_dependencies as
            $key =>
            $dependency )
        {
            $dependency_settings = array();
            if ( array_key_exists( $key, $settings ) )
            {
                $dependency_settings = $settings[$key];
                if ( array_key_exists( $mode, $dependency_settings ) )
                {
                    $dependency_settings = $dependency_settings[$mode];
                }
            }
            $dependency::staticInitialize( $dependency_settings, null,
                self::$_static_baseline_flags );
            if ( !is_object( $dependency ) )
            {
                $dependency = new $dependency( $dependency_settings, null,
                    self::$_static_baseline_flags );
            }
            self::$_static_baseline_dependencies[$key] = $dependency;
        }
    }

    /**
     * <Initialization Registration Method>
     * Registers any time initialization is called, and keeps an internal record
     * of how many times it was called, where it was called from, and what
     * parameters were passed. Dependencies passed will be recorded as serialized
     * lexicon entries to prevent excessive memory usage.
     * @return void
     * @since 0.2.5
     */
    private static function _oroborosRegisterInitialization()
    {
        $bt = debug_backtrace( 1 );
        $dependencies = (isset( $bt[1]['args'][0] )
            ? $bt[1]['args'][0]
            : array() );
        $options = (isset( $bt[1]['args'][1] )
            ? $bt[1]['args'][1]
            : array() );
        $flags = (isset( $bt[1]['args'][2] )
            ? $bt[1]['args'][2]
            : array() );
        $record = array(
            'file' => $bt[2]['file'],
            'line' => $bt[2]['line'],
            'microtime' => microtime( 1 ),
            'reinit' => self::isStaticInitialized(),
            'dependencies' => $dependencies,
            'options' => $options,
            'flags' => $flags,
        );
        foreach ( $dependencies as
            $key =>
            $dependency )
        {
            try
            {
                $lex = new \oroboros\codex\LexiconEntry( $dependency );
                $record['dependencies']['$key'] = serialize( $lex );
            } catch ( \Exception $e )
            {
                $record['dependencies']['$key'] = serialize( $dependency );
            }
        }
        unset( $bt );
        self::$_oroboros_initialization_registry[] = serialize( $record );
    }

    /**
     * <Initialization History Unpack Method>
     * Unpacks the initialization history and returns
     * an array of all of the entries that have been called.
     * @return array
     */
    private static function _oroborosReturnInitializationHistory()
    {
        $history = array();
        if ( !self::isStaticInitialized() )
        {
            return $history();
        }
        foreach ( self::$_oroboros_initialization_registry as
            $key =>
            $entry )
        {
            $tmp = unserialize( $entry );
            foreach ( $tmp['dependencies'] as
                $dk =>
                $d )
            {
                $tmp['dependencies'][$dk] = unserialize( $d );
            }
            $history[$key] = $tmp;
        }
        return $history;
    }

}
