<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\traits\extensions;

/**
 * <Extendable Trait>
 * Provides a set of methods for making objects extendable with extensions.
 * This trait provides wrapper logic for the StaticControlApi that registers
 * extensions for lexical referencing and more optimized searchability than
 * the underlying StaticControlApi design pattern accomplishes by itself,
 * and allows for simplified registration by keyword for easier tracking
 * of extension scope. It is suggested to use this logic for all externally
 * extendable classes that want to leverage the StaticControlApi functionality.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @category extension
 * @package oroboros/core
 * @subpackage extension
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\core\interfaces\contract\extensions\ExtendableContract
 */
trait ExtendableTrait
{

    use \oroboros\core\traits\patterns\structural\StaticControlApiTrait
    {
        \oroboros\core\traits\patterns\structural\StaticControlApiTrait::staticInitialize as private sca_staticInitialize;
        \oroboros\core\traits\patterns\structural\StaticControlApiTrait::extend as private sca_extend;
    }

    /**
     * Represents the contract interface expected to be honored
     * for the extension to be valid.
     * @var string
     */
    private static $_extendable_contract;

    /**
     * Represents the context key expected of the extension.
     * @var string
     */
    private static $_extendable_context;

    /**
     * Represents the extensions that have been registered
     * with the extendable class.
     * @var \oroboros\collection\Collection
     */
    private static $_extendable_extensions;

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\core\interfaces\contract\extensions\ExtendableContract
     *
     * @execution \ClassName::staticInitialize();
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Static Baseline Initialization Method>
     * Initializes a class in the static scope, so that related configurations,
     * dependencies, flags, and settings are accessible across a range of child
     * classes. This also allows for extension logic to be immediately accessible
     * to all child classes if extensions are used.
     *
     * Static initialization should be stateless, only providing base level
     * dependencies and configurations that are required across a family of
     * related classes. Any class extending upon this logic SHOULD NOT mutate
     * the static state in any way that would interrupt other objects operating
     * from the same parent elsewhere.
     *
     * Most Oroboros internals that extend upon this and work in the active
     * object scope will lock static initialization so it remains immutable
     * after its first run.
     *
     * Stateful static initialization should only ever occur in a
     * dedicated base object that does not have a deep inheritance chain,
     * and only in very specialized cases. The functionality is not outright
     * prevented, however it does require several overrides to enable, because
     * its use in such a manner is highly discouraged to avoid introducing
     * instability in your code. Internal Oroboros logic always disables this
     * functionality, but you may roll your own classes using this trait that
     * implement it pretty easily if need be.
     *
     * @param type $params (optional) Any parameters that need to be accessible across all child objects.
     * @param type $dependencies (optional) Any dependencies that need to be accessible across all child objects.
     * @param type $flags (optional) Any flags that need to be accessible across all child objects.
     * @return void
     *
     * @throws \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract
     * This interface is attached to all exceptions that arise from this trait. If you want
     * to handle everything in one catch block, use this for your catch statement.
     *
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     * If provided parameters are not valid
     *
     * @throws \oroboros\core\utilities\exception\RuntimeException
     * If initialization has already occurred and reinitialization is disabled.
     *
     * @throws \oroboros\core\utilities\exception\LogicException
     * If the class is misconfigured. This is a serious error that
     * requires developer intervention on the part of whoever wrote
     * the override that caused the error, because it means the class
     * is entirely unusable.
     */
    public static function staticInitialize( $params = null,
        $dependencies = null, $flags = null )
    {
        self::_staticBaselineRegisterInitializationTask( '_extendablePostInitialization' );
        self::_staticBaselineInitialize( $params, $dependencies, $flags );
        return get_called_class();
    }

    /**
     * <Extendable Contract Getter Method>
     * Returns the name of the valid extendable contract declared
     * that extensions are expected to implement.
     * @return string
     */
    public static function getExtendableContract()
    {
        $class = get_called_class();
        return $class::_extendableGetContract();
    }

    /**
     * <Extendable Contract Check Method>
     * Checks if the given contract interface honors
     * the expected contract interface.
     * @param string $contract
     * @return bool
     */
    public static function checkExtendableContract( $extension )
    {
        $class = get_called_class();
        return $class::_extendableCheckContract( $extension );
    }

    /**
     * <Extendable Context Getter Method>
     * Returns the declared extension context that proposed
     * extensions are expected to match.
     * @return string
     */
    public static function getExtendableContext()
    {
        $class = get_called_class();
        return $class::_extendableGetContext();
    }

    /**
     * <Extendable Context Check Method>
     * Checks if the given context matches the expected context.
     * @param string $context
     * @return bool
     */
    public static function checkExtendableContext( $context )
    {
        $class = get_called_class();
        return $class::_extendableCheckContext( $context );
    }

    /**
     * <Extendable Extension Registration Method>
     * Registers a new extension into the extendable class.
     * @param \oroboros\core\interfaces\contract\extensions\ExtensionContract $extension
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the given extension is not valid
     * @throws \oroboros\core\utilities\exception\RuntimeException
     *     If the given extension would cause command collisions
     *     with already declared extensions
     */
    public static function extend( $extension )
    {
        self::_extendableExtend( $extension );
    }

    /**
     * <Extendable Index Check Method>
     * Returns a boolean determination as to whether the given index
     * is registered for the extendable or any of its extensions.
     * @param string $index
     * @return bool
     */
    public static function hasIndex( $index )
    {
        return self::_extendableHasIndex( $index );
    }

    /**
     * <Extendable Method Index Getter>
     * Returns a list of the commands within the given index within
     * the extendable or any of its extensions.
     * If the index is not registered, returns false.
     * @param string $index The index to check within
     * @return array|bool Returns false if the index is not registered,
     *     otherwise returns an array of the valid commands
     *     within the index.
     */
    public static function getIndex( $index )
    {
        return self::_extendableGetIndex( $index );
    }

    /**
     * <Extendable Command Check Method>
     * Returns a boolean determination as to whether the given index
     * contains the specified method for the extendable or any of its
     * extensions. If no method is supplied, will return whether the
     * index has a default method.
     * @param string $index The index to check for the method within
     * @param string $method (optional) If not supplied, will return whether
     *     the index has a default. Default not supplied.
     * @return bool
     */
    public static function hasMethod( $index, $method = null )
    {
        $class = get_called_class();
        return $class::check( $index, $method );
    }

    /**
     * <Extendable Check Method>
     * Returns a boolean determination as to whether the given extension
     * is currently implemented. This method may receive either the
     * slug identifier or an instance of the extension to check.
     * @param string $extension The identifier of the extension to check for
     * @return bool
     */
    public static function hasExtension( $extension )
    {
        return self::_extendableHasExtension( $extension );
    }

    /**
     * <Extendable Extension Api Getter Method>
     * Passes an api command along to a specific extension by its
     * extension identifier key, which the underlying StaticControlApi
     * does not normally expose publicly. Returns false if the extension
     * does not exist, or if the specified index and/or command do not
     * exist in the extension, otherwise returns the standard collection
     * output of a StaticControlApi api call.
     * @param string $extension The extension slug identifier
     * @param string $index (optional) An optional index to check in the api of the extension
     * @param string $method (optional) An optional command to check within an index of the extension
     * @return bool|\oroboros\collection\Collection
     */
    public static function extensionApi( $extension, $index = null,
        $method = null )
    {
        return self::_extendableGetExtensionApi( $extension, $index, $method );
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * <Extension Contract Declaration Method>
     * This method should be overridden with a contract interface
     * expected to be honored by valid extensions.
     * @return string
     */
    protected static function _declareExtensionContract()
    {
        return '\\oroboros\\core\\interfaces\\contract\\extensions\\ExtensionContract';
    }

    /**
     * <Extension Context Declaration Method>
     * This method should be overridden with the slug identifier
     * that the extension is expected to match.
     * @return string
     */
    abstract protected static function _declareExtensionContext();

    /**
     * <Extension Getter>
     * Returns the given extension by identifier.
     * @param type $extension
     * @return bool|\oroboros\core\interfaces\contract\extensions\ExtensionContract
     */
    protected static function _getExtension( $extension )
    {
        if ( !is_string( $extension ) && array_key_exists( $extension,
                self::$_extendable_extensions ) )
        {
            return false;
        }
        return self::$_extendable_extensions[$extension];
    }

    /**
     * <Extension Set Getter>
     * Returns all currently registered extensions.
     * @return array
     */
    protected static function _getExtensions()
    {
        return self::$_extendable_extensions;
    }

    /**
     * <Extension Identifier Getter>
     * Returns the identifiers for all currently registered extensions.
     * @return array
     */
    protected static function _getExtensionIdentifiers()
    {
        return array_keys( self::$_extendable_extensions );
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * Performs the setting logic for the given extension, first running
     * validation to insure that it is a valid instance for the current
     * extendable class.
     * @param \oroboros\core\interfaces\contract\extensions\ExtensionContract $extension
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the given extension is invalid
     * @throws \oroboros\core\utilities\exception\RuntimeException
     *     If the given extension would cause command collisions
     *     with already declared extensions
     * @internal
     */
    private static function _extendableExtend( $extension )
    {
        self::_extendableValidateExtension( $extension );
        self::_extendableCheckCollisions( $extension );
        try
        {
            //Attempt to initialize the extension with the internals of this class.
            $param_key = $extension::getExtensionId();
            $params = ( array_key_exists( $param_key,
                    self::$_static_baseline_parameters )
                ? self::$_static_baseline_parameters[$param_key]
                : null);
            $extension::staticInitialize( $params, null,
                self::$_static_baseline_flags );
        } catch ( \Exception $e )
        {
            /**
             * Pre-initialized extensions that supply their own details
             * and prevent secondary initialization are ok. Initialization
             * needs to fire if it has not already though.
             *
             * no-op
             */
        }
        self::sca_extend( $extension );
        self::_extendableRegisterCodexExtensionEntry( $extension );
        if ( is_object( $extension ) )
        {
            $extension = get_class( $extension );
        }
        self::$_extendable_extensions[$extension::getExtensionId()] = $extension;
    }

    /**
     * Performs validation of the given extension, to insure that it implements
     * the required methods and declares the correct context for extending
     * functionality within the current class.
     * @param \oroboros\core\interfaces\contract\extensions\ExtensionContract $extension
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the context or contract of the given extension are not a match
     * @internal
     */
    private static function _extendableValidateExtension( $extension )
    {
        self::_validate( 'type-of', $extension, 'object', true );
        self::_validate( 'instance-of', $extension,
            '\\oroboros\\core\\interfaces\\contract\\extensions\\ExtensionContract',
            true );
        $class = get_called_class();
        $context = $extension::getContext();
        if ( !$class::_extendableCheckContext( $context ) )
        {
            //context does not match
            self::_throwException( 'invalid-argument-exception',
                sprintf( 'Error encountered at [%s]. Instance of [%s] does not match expected context [%s]',
                    __METHOD__, get_class( $extension ),
                    $class::getExtendableContext() ),
                \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS );
        }
        if ( !$class::_extendableCheckContract( $extension ) )
        {
            //expected contract does not match
            self::_throwException( 'invalid-argument-exception',
                sprintf( 'Error encountered at [%s]. Instance of [%s] does not match expected contract interface [%s]',
                    __METHOD__, get_class( $extension ),
                    $class::getExtendableContract() ),
                \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS );
        }
    }

    /**
     * Returns the name of the contract expected to be honored
     * by extensions of the class.By default, returns the baseline
     * extension contract if no other contract is declared.
     * @return string
     * @internal
     */
    private static function _extendableGetContract()
    {
        $class = get_called_class();
        if ( is_null( $class::$_extendable_contract ) )
        {
            return '\\oroboros\\core\\interfaces\\contract\\extensions\\ExtensionContract';
        }
        return $class::$_extendable_contract;
    }

    /**
     * Returns a boolean determination as to whether
     * the given extension honors the expected contract.
     * @param type $extension
     * @return bool
     * @internal
     */
    private static function _extendableCheckContract( $extension )
    {
        $class = get_called_class();
        $contract = ltrim( $class::getExtendableContract(), '\\' );
        return in_array( $contract, class_implements( $extension ) );
    }

    /**
     * Returns the declared context that extensions need
     * to honor to be accepted.
     * @return string|bool Returns false if no context has been set yet,
     *     otherwise returns the context.
     * @internal
     */
    private static function _extendableGetContext()
    {
        $class = get_called_class();
        if ( is_null( $class::$_extendable_context ) )
        {
            return false;
        }
        return $class::$_extendable_context;
    }

    /**
     * Returns a boolean determination as to whether a given
     * context matches the expected context.
     * @param type $context
     * @return bool
     * @internal
     */
    private static function _extendableCheckContext( $context )
    {
        $class = get_called_class();
        if ( is_null( $class::$_extendable_context ) || $context === false )
        {
            return false;
        }
        return $context === $class::$_extendable_context;
    }

    /**
     * Sets the internal class static contract property
     * @param string $contract An interface name that
     *     extends \oroboros\core\interfaces\contract\extensions\ExtensionContract
     * @return void
     * @throws \oroboros\core\utilities\exeption\InvalidArgumentException
     *     If an invalid parameter is passed
     * @internal
     */
    private static function _extendableSetContract( $contract )
    {
        self::_validate( 'type-of', $contract, 'string', true );
        self::_validate( 'instance-of', $contract,
            '\\oroboros\\core\\interfaces\\contract\\extensions\\ExtensionContract',
            true );
        self::$_extendable_contract = $contract;
    }

    /**
     * Sets the internal static context property
     * @param string $context The slug identifier for the class
     *     expected to be honored by valid extensions
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If an invalid context is passed
     * @internal
     */
    private static function _extendableSetExtensionContext( $context )
    {
        self::_validate( 'type-of', $context, 'string', true );
        self::$_extendable_context = $context;
    }

    /**
     * Performs the post-initialization tasks for the extendable
     * class needed to setup its extendable status.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If an invalid extension context or contract are defined by the class.
     * @internal
     */
    private static function _extendablePostInitialization()
    {
        $class = get_called_class();
        if ( is_null( self::$_extendable_extensions ) )
        {
            self::$_extendable_extensions = new \oroboros\collection\Collection();
        }
        self::_extendableSetContract( $class::_declareExtensionContract() );
        self::_extendableSetExtensionContext( $class::_declareExtensionContext() );
        self::_extendableRegisterCodexExtendableEntry();
    }

    /**
     * Performs a check against the registered indexes,
     * and returns a determination as to whether the
     * given index is registered.
     * @param type $index
     * @return bool
     * @internal
     */
    private static function _extendableHasIndex( $index )
    {
        $class = get_called_class();
        return $class::api( $index ) !== false;
    }

    /**
     * Returns a set of the valid commands for the given index,
     * or false if the index is not registered.
     * @param type $index
     * @return array|bool
     * @internal
     */
    private static function _extendableGetIndex( $index )
    {
        $class = get_called_class();
        $api = $class::api( $index );
        if ( !$api )
        {
            return false;
        }
        return json_decode( json_encode( $api ), 1 );
    }

    /**
     * Performs a check for command collisions, to prevent ambiguous references
     * to index/command pairs during execution. This method is called when an
     * extension is registered to prevent any unforseen logic errors that would
     * otherwise occur.
     * @param \oroboros\core\interfaces\contract\extensions\ExtensionContract $extension
     * @return void
     * @throws \oroboros\core\utilities\exception\RuntimeException
     *     If any of the declared index and command combinations collide
     *     with an already registered extension within the current extendable.
     * @internal
     */
    private static function _extendableCheckCollisions( $extension )
    {

    }

    /**
     * Registers the extension as a valid extension for this class with
     * the Codex, so its cohesion mapping can be generated if it is not
     * already.
     * @param \oroboros\core\interfaces\contract\extensions\ExtensionContract $extension
     * @return void
     * @internal
     * @todo Complete the Codex logic so extensions can be registered properly.
     */
    private static function _extendableRegisterCodexExtensionEntry( $extension )
    {
        //no-op
    }

    /**
     * Registers the current class as a valid extendable class with
     * the Codex, so its cohesion mapping can be generated if it is not
     * already.
     * @return void
     * @internal
     * @todo Complete the Codex logic so extendables can be registered properly.
     */
    private static function _extendableRegisterCodexExtendableEntry()
    {
        //no-op
    }

    /**
     * Performs a check to see if the given extension is currently registered.
     * This method may receive either the slug identifier or an instance
     * of the extension to check.
     * @param string|\oroboros\core\interfaces\contract\extensions\ExtensionContract $extension
     * @return bool
     * @internal
     */
    private static function _extendableHasExtension( $extension )
    {
        $contract = ltrim( self::$_extendable_contract, '\\' );
        if (
            ( is_string( $extension ) //if the extension is a string,
            && class_exists( $extension ) //and a valid class name (extensions deal with mostly static logic)
            && self::_validate( 'instance-of', $extension, $contract ) //and is an instance of the extendable contract
            )
            || ( is_object( $extension ) //or if extension is an object
            && ( in_array( $contract, class_implements( $extension ) ) ) //and an instance of the extendable contract
            )
        )
        {
            $extension = $extension::getExtensionId();
        }
        return is_string( $extension ) && self::$_extendable_extensions->has( $extension );
    }

    /**
     * Passes an api command along to a specific extension by its
     * extension identifier key, which the underlying StaticControlApi
     * does not normally expose publicly. Returns false if the extension
     * does not exist, or if the specified index and/or command do not
     * exist in the extension, otherwise returns the standard collection
     * output of a StaticControlApi api call.
     * @param string $extension The extension slug identifier
     * @param string $index (optional) An optional index to check in the api of the extension
     * @param string $method (optional) An optional command to check within an index of the extension
     * @return bool|\oroboros\collection\Collection
     * @internal
     */
    private static function _extendableGetExtensionApi( $extension,
        $index = null, $method = null )
    {
        if ( !self::$_extendable_extensions->has( $extension ) )
        {
            return false;
        }
        $class = self::$_static_control_api_extensions[self::$_extendable_extensions[$extension]];
        return $class::api( $index, $method );
    }

}
