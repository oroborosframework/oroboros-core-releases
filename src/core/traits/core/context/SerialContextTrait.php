<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\traits\core\context;

/**
 * <Oroboros Serial Context Trait>
 * This trait provides a simple serializable container for a contextual reference.
 *
 * This trait satisfies both the baseline contract and the static baseline
 * contract without additional configuration, and resolves the conflicts
 * btween the shared dependencies of the two traits.. For objects that need
 * to simply present both and extend further upon their logic, it should
 * cover all of the required methods for this without any additional work.
 *
 * It is designed to have a default way of fulfilling baseline logic without
 * additional overhead aside from calling one method.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @category internal
 * @package oroboros/core
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\core\abstracts\core\context\SerialContextContract
 * @satisfies \oroboros\core\abstracts\core\context\ContextContract
 */
trait SerialContextTrait
{

    use ContextTrait;

    /**
     * <Context Serialization Method>
     * Encapsulates the object into a very lightweight
     * representation of its primary values.
     * @return string
     */
    public function serialize()
    {
        return serialize( array(
            'context' => $this->_context,
            'value' => $this->_value,
            'type' => $this->_type,
            'category' => $this->_category,
            'subcategory' => $this->_subcategory,
            ) );
    }

    /**
     * <Context Unserialization Method>
     * Restores a fully qualified object from the
     * lightweight serialized data.
     * @param string $serialized
     * @return void
     */
    public function unserialize( $serialized )
    {
        $values = unserialize( $serialized );
        $this->_setContext( $values['context'], $values['value'] );
        $this->_setValue( $values['context'], $values['value'] );
        $this->_setType( $values['type'] );
        $this->_setCategory( $values['category'] );
        $this->_setSubcategory( $values['subcategory'] );
        $this->initialize();
    }

}
