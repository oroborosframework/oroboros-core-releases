<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\traits\core\context;

/**
 * <Oroboros Meta Context Trait>
 * This trait provides meta contexts, which define valid properties of other
 * context objects. Extending from a meta context object will only allow
 * its values to be scoped to a specific property the meta context
 * represents.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @category internal
 * @package oroboros/core
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\core\abstracts\core\context\JsonSerialContextContract
 * @satisfies \oroboros\core\abstracts\core\context\SerialContextContract
 * @satisfies \oroboros\core\abstracts\core\context\ContextContract
 */
trait MetaContextTrait
{

    use CoreContextTrait
    {
        CoreContextTrait::initialize as private core_initialize;
    }

    /**
     * <Meta Context Basic Initialization>
     * Sets the context type to "meta-context"
     *
     * @param type $params (optional) Not used in this context
     *     provided for compatibility.
     * @param type $dependencies (optional) Not used in this context,
     *     provided for compatibility.
     * @param type $flags (optional) Not used in this context,
     *     provided for compatibility.
     * @return void
     */
    public function initialize( $params = null, $dependencies = null,
        $flags = null )
    {
        $this->_setType( 'meta-context' );
        $this->core_initialize( $params, $dependencies, $flags );
    }

    /**
     * <Context Validation Method>
     * Override this method to provide a validation method,
     * which should return a boolean value as to whether
     * the context is valid. If false is returned, then the
     * object will not allow instantiation with the provided
     * context.
     * @param scalar $context
     * @return bool
     */
    protected function _validateContext( $context, $value )
    {
        return in_array( $context, self::$_context_modes );
    }

    /**
     * <Value Validation Method>
     * Override this method to provide a validation method,
     * which should return a boolean value as to whether
     * the value is valid. If false is returned, then the
     * object will not allow instantiation with the provided
     * value.
     * @param scalar $value
     * @return bool
     */
    protected function _validateValue( $context, $value )
    {
        return is_string( $value );
    }

}
