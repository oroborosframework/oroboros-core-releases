<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\traits\core\context;

/**
 * <Oroboros Context Trait>
 * This trait provides a simple container for a contextual reference.
 * It provides an extremely lightweight categorical matching object that can be
 * used to designate a simple key/value identity to a specific optionally set
 * internal category and subcategory for contextual matching. Context objects
 * are immutable once created, and can be used as a reliable way to collect and
 * check expected properties, or to provide default properties in lieu of
 * defined ones from expected sources.
 *
 * As this trait has a very simplistic task, it does not include the default
 * baseline logic so multitudes of them can be instantiated with a very low
 * overhead.
 *
 * The context and value may be provided publicly by the constructor,
 * whereas the type, category and subcategory are set internally if they
 * are used. They may be exposed for public setting by implementing classes
 * as needed also, but are primarily for filing fixed indexes along related
 * lines that should not be externally mutable in most cases.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @category internal
 * @package oroboros/core
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\core\interfaces\contract\core\context\ContextTrait
 */
trait ContextTrait
{

    private static $_context_modes = array(
        \oroboros\core\interfaces\enumerated\context\MetaContextTypes::CONTEXT_TYPE_CONTEXT_META,
        \oroboros\core\interfaces\enumerated\context\MetaContextTypes::CONTEXT_TYPE_VALUE_META,
        \oroboros\core\interfaces\enumerated\context\MetaContextTypes::CONTEXT_TYPE_TYPE_META,
        \oroboros\core\interfaces\enumerated\context\MetaContextTypes::CONTEXT_TYPE_CATEGORY_META,
        \oroboros\core\interfaces\enumerated\context\MetaContextTypes::CONTEXT_TYPE_SUBCATEGORY_META,
    );

    /**
     *
     * @var type Represents a scalar context provided on instantiation.
     * @var scalar
     */
    private $_context;

    /**
     * Represents a scalar value provided on instantiation.
     * @var scalar
     */
    private $_value;

    /**
     * Represents an optional internally set scalar type.
     * @var null|scalar
     */
    private $_type;

    /**
     * Represents an optional internally set scalar category.
     * @var null|scalar
     */
    private $_category;

    /**
     * Represents an optional internally set scalar subcategory.
     * @var null|scalar
     */
    private $_subcategory;

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\core\interfaces\contract\core\context\ContextTrait
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Context Constructor>
     * Creates a contextual value, with an optional
     * type parameter for additional filtering.
     *
     * This object can only be created with
     * scalar or stringable values.
     *
     * @param scalar $context Also accepts an object with __toString
     * @param scalar $value Also accepts an object with __toString
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If a context is passed that is not scalar or a stringable object,
     *     or does not pass the overrideable validation function
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If a value is passed that is not scalar or a stringable object,
     *     or does not pass the overrideable validation function
     */
    public function __construct( $context, $value )
    {
        $this->initialize();
        $context = $this->_castValue( $context, 'context' );
        $value = $this->_castValue( $value, 'value' );
        $this->_setContext( $context, $value );
        $this->_setValue( $context, $value );
    }

    /**
     * <Context Basic Initialization>
     * By default, initialization does nothing.
     *
     * This should be overridden to set the type,
     * category and subcategory in implementing
     * classes if needed.
     *
     * @param type $params (optional) Not used in this context
     *     provided for compatibility.
     * @param type $dependencies (optional) Not used in this context,
     *     provided for compatibility.
     * @param type $flags (optional) Not used in this context,
     *     provided for compatibility.
     * @return void
     */
    public function initialize( $params = null, $dependencies = null,
        $flags = null )
    {

    }

    /**
     * <Context String Casting Method>
     * Casts the value to a string.
     * @return string
     */
    public function __toString()
    {
        return (string) $this->_value;
    }

    /**
     * <Context Context Getter Method>
     * Returns the context.
     * @return scalar
     */
    public function getContext()
    {
        return $this->_context;
    }

    /**
     * <Context Context Check Method>
     * Checks if a given context matches the internally set context.
     * Other instances of the context contract may be passed,
     * and will be checked against their declared context.
     *
     * Objects with __toString that are not an instance of the
     * context contract will be cast to string before type checking.
     *
     * All other contexts will always return false if they are non-scalar.
     * If the internal context is an exact match and not null, true will be
     * returned, otherwise false will be returned.
     *
     * @param scalar|object|\oroboros\core\interfaces\contract\core\context\ContextContract $context
     * @return bool
     */
    public function hasContext( $context )
    {
        $context = $this->_castValue( $context, 'context' );
        return (is_scalar( $context ) && $context === $this->_context);
    }

    /**
     * <Context Value Getter Method>
     * Returns the value.
     * @return scalar
     */
    public function getValue()
    {
        return $this->_value;
    }

    /**
     * <Context Type Check Method>
     * Checks if a given value matches the internally set value.
     * Other instances of the context contract may be passed,
     * and will be checked against their declared value.
     *
     * Objects with __toString that are not an instance of the
     * context contract will be cast to string before type checking.
     *
     * All other values will always return false if they are non-scalar.
     * If the internal value is an exact match and not null, true will be
     * returned, otherwise false will be returned.
     *
     * @param scalar|object|\oroboros\core\interfaces\contract\core\context\ContextContract $value
     * @return bool
     */
    public function hasValue( $value )
    {
        $value = $this->_castValue( $value, 'value' );
        return (is_scalar( $value ) && $value === $this->_value);
    }

    /**
     * <Context Type Getter Method>
     * Returns the type.
     * @return scalar
     */
    public function getType()
    {
        return $this->_type;
    }

    /**
     * <Context Type Check Method>
     * Checks if a given type matches the internally set type.
     * Other instances of the context contract may be passed,
     * and will be checked against their declared type.
     *
     * Objects with __toString that are not an instance of the
     * context contract will be cast to string before type checking.
     *
     * All other values will always return false if they are non-scalar.
     * If the internal type is an exact match and not null, true will be
     * returned, otherwise false will be returned.
     *
     * @param scalar|object|\oroboros\core\interfaces\contract\core\context\ContextContract $type
     * @return bool
     */
    public function hasType( $type )
    {
        $type = $this->_castValue( $type, 'type' );
        return (is_scalar( $type ) && $type === $this->_type);
    }

    /**
     * <Context Category Getter Method>
     * Returns the category.
     * @return scalar
     */
    public function getCategory()
    {
        return $this->_category;
    }

    /**
     * <Context Category Check Method>
     * Checks if a given category matches the internally set category.
     * Other instances of the context contract may be passed,
     * and will be checked against their declared type.
     *
     * Objects with __toString that are not an instance of the
     * context contract will be cast to string before type checking.
     *
     * All other values will always return false if they are non-scalar.
     * If the internal type is an exact match and not null, true will be
     * returned, otherwise false will be returned.
     *
     * @param scalar|object|\oroboros\core\interfaces\contract\core\context\ContextContract $category
     * @return bool
     */
    public function hasCategory( $category )
    {
        $category = $this->_castValue( $category, 'category', true );
        return (is_scalar( $category ) && $category === $this->_category);
    }

    /**
     * <Context Sub-Category Getter Method>
     * Returns the sub-category.
     * @return scalar
     */
    public function getSubcategory()
    {
        return $this->_subcategory;
    }

    /**
     * <Context Sub-Category Check Method>
     * Checks if a given sub-category matches the internally set sub-category.
     * Other instances of the context contract may be passed,
     * and will be checked against their declared sub-category.
     *
     * Objects with __toString that are not an instance of the
     * context contract will be cast to string before type checking.
     *
     * All other values will always return false if they are non-scalar.
     * If the internal sub-category is an exact match and not null, true will be
     * returned, otherwise false will be returned.
     *
     * @param scalar|object|\oroboros\core\interfaces\contract\core\context\ContextContract $subcategory
     * @return bool
     */
    public function hasSubcategory( $subcategory )
    {
        $subcategory = $this->_castValue( $subcategory, 'subcategory', true );
        return (is_scalar( $subcategory ) && $subcategory === $this->_subcategory);
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * <Context Context Setter Method>
     * Sets the context. Type checks the context to insure it is scalar,
     * and passes the user-defined validation method if one has
     * been defined. Both the context and the value are provided to the
     * user-defined validation method for cross-association if required.
     * @param scalar $context
     * @param scalar $value
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If a context is passed that is not scalar or a stringable object
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the overridable context validation method returns false
     */
    protected function _setContext( $context, $value )
    {
        $this->_checkValue( $context );
        \oroboros\validate\Validator::validate( 'exact',
            $this->_validateContext( $context, $value ), true, true );
        $this->_context = $context;
    }

    /**
     * <Context Value Setter Method>
     * Sets the value. Type checks the value to insure it is scalar,
     * and passes the user-defined validation method if one has
     * been defined. Both the context and the value are provided to the
     * user-defined validation method for cross-association if required.
     * @param scalar $context
     * @param scalar $value
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If a value is passed that is not scalar or a stringable object
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the overridable value validation method returns false
     */
    protected function _setValue( $context, $value )
    {
        $this->_checkValue( $value );
        \oroboros\validate\Validator::validate( 'exact',
            $this->_validateValue( $context, $value ), true, true );
        $this->_value = $value;
    }

    /**
     * <Context Type Setter Method>
     * Sets the type. Casts stringable objects to string, and
     * then type checks the value to insure it is scalar.
     * @param scalar|object|null $type
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If a non-null type is passed that is not scalar or a stringable object
     */
    protected function _setType( $type = null )
    {
        $type = $this->_castValue( $type, 'type', true );
        $this->_checkValue( $type, true );
        $this->_type = $type;
    }

    /**
     * <Context Category Setter Method>
     * Sets the category. Casts stringable objects to string, and
     * then type checks the value to insure it is scalar.
     * @param scalar|object|null $category
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If a non-null category is passed that is not scalar or a stringable object
     */
    protected function _setCategory( $category = null )
    {
        $category = $this->_castValue( $category, 'category', true );
        $this->_checkValue( $category, true );
        $this->_category = $category;
    }

    /**
     * <Context Sub-Category Setter Method>
     * Sets the sub-category. Casts stringable objects to string, and
     * then type checks the value to insure it is scalar.
     * @param scalar|object|null $subcategory
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If a non-null sub-category is passed that is not scalar or a
     *     stringable object
     */
    protected function _setSubCategory( $subcategory = null )
    {
        $subcategory = $this->_castValue( $subcategory, 'subcategory', true );
        $this->_checkValue( $subcategory, true );
        $this->_subcategory = $subcategory;
    }

    /**
     * <Context Validation Method>
     * Override this method to provide a validation method,
     * which should return a boolean value as to whether
     * the context is valid. If false is returned, then the
     * object will not allow instantiation with the provided
     * context.
     * @param scalar $context The context provided on instantiation
     * @param scalar $value The value provided on instantiation
     * @return bool
     */
    protected function _validateContext( $context, $value )
    {
        return true;
    }

    /**
     * <Value Validation Method>
     * Override this method to provide a validation method,
     * which should return a boolean value as to whether
     * the value is valid. If false is returned, then the
     * object will not allow instantiation with the provided
     * value.
     * @param scalar $context The context provided on instantiation
     * @param scalar $value The value provided on instantiation
     * @return bool
     */
    protected function _validateValue( $context, $value )
    {
        return true;
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * Casts the object if it is non scalar and meets castable criteria.
     * If the object is an instance of \oroboros\core\interfaces\contract\core\context\ContextContract,
     * then the value will be derived from the given mode.
     *
     * If the value is null and allow null is true, then null will be returned.
     *
     * If the value is an object with __toString, then the string representation will be returned.
     *
     * All other types will be returned unmodified.
     *
     * @param mixed $value The candidate value to cast
     * @param string $mode [context|value|category|subcategory]
     *     Corresponds to the four contextual properties of the context object.
     * @param bool $allow_null (optional) If true and the given value is null,
     *     will return null. Default false.
     * @return mixed
     * @throws \oroboros\core\interfaces\exception\InvalidArgumentException
     *     If an invalid mode is passed.
     * @internal
     */
    private function _castValue( $value, $mode, $allow_null = false )
    {
        \oroboros\validate\Validator::validate( 'any-of', $mode,
            self::$_context_modes, true );
        if ( is_null( $value ) && $allow_null )
        {
            return $value;
        }
        if ( is_object( $value )
            && ($value instanceof \oroboros\core\interfaces\contract\core\context\ContextContract) )
        {
            $method = 'get' . ucfirst( $mode );
            $value = $value->$method();
        }
        if ( is_object( $value )
            && \oroboros\validate\Validator::validate( 'stringable', $value ) )
        {
            //If non-scalar but stringable, such as an object with __toString,
            //cast to string to prevent an error
            $value = (string) $value;
        }
        return $value;
    }

    /**
     * Checks if the given value is a valid scalar value
     * @param scalar $value The value to evaluate
     * @param bool $allow_null (optional) If true, will allow null values. Default false.
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If an invalid value is passed
     * @internal
     */
    private function _checkValue( $value, $allow_null = false )
    {
        if ( is_null( $value ) && $allow_null )
        {
            return;
        }
        \oroboros\validate\Validator::validate( 'scalar', $value, null, true );
    }

}
