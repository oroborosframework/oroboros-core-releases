<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\traits\core\context;

/**
 * <Oroboros Core Class Context Trait>
 * Declares the context of class contexts
 * used throughout the Oroboros Core system.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @category internal
 * @package oroboros/core
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\core\abstracts\core\context\JsonSerialContextContract
 * @satisfies \oroboros\core\abstracts\core\context\SerialContextContract
 * @satisfies \oroboros\core\abstracts\core\context\ContextContract
 */
trait ClassContextTrait
{

    use CoreContextTrait
    {
        CoreContextTrait::initialize as private core_initialize;
    }

    /**
     * Represents the enumerator used to reference valid core class contexts.
     * @var \oroboros\enum\InterfaceEnumerator
     */
    private static $_enum;

    /**
     * /**
     * <Core Internal Meta Context Initialization>
     * Declares the context of the Oroboros Core
     * class context context objects.
     * @param type $params (optional) Not used.
     * @param type $dependencies (optional) Not used.
     * @param type $flags (optional) Not used.
     * @return void
     */
    public function initialize( $params = null, $dependencies = null,
        $flags = null )
    {
        $this->_setType( 'class-context' );
        $this->_setEnumerator();
        $this->core_initialize( $params, $dependencies, $flags );
    }

    /**
     * <Class Context Context Declaration Validation>
     * Checks that the class context exists in the enumerator.
     * @param string $context The provided context from the constructor.
     *     Must be a value that exists in the enumerator.
     * @param string $value The provided value from the constructor.
     *     This parameter is not used in this validation method.
     * @return bool Returns true if the given context exists
     *     in the enumerated api interface results, and false otherwise.
     */
    protected function _validateContext( $context, $value )
    {
        $this->_setEnumerator();
        return in_array( $context, self::$_enum->toArray() );
    }

    /**
     * <Class Context Value Declaration Validation>
     * This validation method checks a number of contextual criteria.
     * * The value must be a string
     * * The value must be an existing fully qualified class name
     * * The class that the value represents MUST have defined the
     * class constant OROBOROS_CLASS_CONTEXT
     * * The class constant OROBOROS_CLASS_CONTEXT class constant
     * bound to the class must EXACTLY match the given context value.
     *
     * @param type $context
     * @param type $value
     * @return type
     */
    protected function _validateValue( $context, $value )
    {
        $this->_setEnumerator();
        return is_string( $value ) && class_exists( $value, true )
            && defined( $value . '::OROBOROS_CLASS_CONTEXT' )
            && $context === constant( $value . '::OROBOROS_CLASS_CONTEXT');
    }

    /**
     * Sets the internal enumerator used for validation
     * of the provided context and value.
     * @return null
     * @internal
     */
    private function _setEnumerator()
    {
        if ( is_null( self::$_enum ) )
        {
            self::$_enum = new \oroboros\enum\InterfaceEnumerator(
                \oroboros\core\interfaces\enumerated\index\ContextIndex::INDEX_CLASS_CONTEXT
            );
        }
    }

}
