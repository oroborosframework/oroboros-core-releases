<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\traits\core\context;

/**
 * <Oroboros Context Category Trait>
 * This trait provides a matching mechanism for contextual entries.
 * It allows setting of only entries that match its type, category and
 * subcategory (or whichever of them are defined).
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @category internal
 * @package oroboros/core
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\core\interfaces\contract\core\context\ContextIndexTrait
 */
trait ContextIndexTrait
{

    /**
     * The name of the contract interface that context indexes must adhere to.
     * This allows for the context index to filter against only serializable or
     * json serializable context objects as needed for save and restore
     * functionality.
     * @var string
     */
    private $_context_contract;

    /**
     * Represents a scalar type that entries are expected to honor to be filed.
     * @var scalar
     */
    private $_type;

    /**
     * Represents an optional scalar category that entries are expected
     * to honor to be filed.
     * @var null|scalar
     */
    private $_category;

    /**
     * Represents am optional scalar subcategory that entries are expected
     * to honor to be filed.
     * @var null|scalar
     */
    private $_subcategory;

    /**
     * Represents an optional internally set scalar category.
     * @var null|scalar
     */
    private $_contexts = array();

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\core\interfaces\contract\core\context\ContextTrait
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Default Context Index Constructor>
     * Builds the context index instance.
     */
    public function __construct()
    {
        $this->_initializeContextIndex();
    }

    /**
     * <Context Index Getter Method>
     * Returns a list of the index of context objects that the index can create.
     * @return array
     */
    public function listContexts()
    {
        return $this->_contexts;
    }

    /**
     * <Context Index Context Object Instance Setter>
     * Sets a given context object class into the internal index if it matches
     * the check for contract, type, category, and subcategory. Returns true if
     * the object was set into the index, and false otherwise.
     * @param type $context
     * @return bool Returns true if the context was added, and false otherwise.
     */
    public function setContext( $context )
    {
        return $this->_contextIndexSetContext( $context );
    }

    /**
     * <Context Index Context Object Check Method>
     * Returns a boolean determination as to whether
     * the given context exists in the context index.
     * @param string|object|\oroboros\core\interfaces\contract\core\context\ContextContract $context
     * @return bool
     */
    public function hasContext( $context )
    {
        if ( is_object( $context ) )
        {
            $context = get_class( $context );
        }
        return ( is_string( $context )
            && in_array( ltrim( $context, '\\', $this->_contexts ) ));
    }

    /**
     * <Context Index Context Object Validation Method>
     * Checks a given context object or class name, and returns a
     * boolean determination as to whether it can be successfully
     * added to the context index.
     *
     * This method does not add the context class, it only returns a
     * designation as to whether it can be added.
     *
     * @param string|object|\oroboros\core\interfaces\contract\core\context\ContextContract $context
     * @return bool
     */
    public function isContextValid( $context )
    {
        return $this->_contextIndexValidateContext( $context );
    }

    /**
     * <Context Index Context Object Set Getter Method>
     * Creates a set of all valid context indexes that the provided context
     * and value can resolve against. If no instances are contained in the
     * index that the provided arguments can resolve against, the method will
     * return false. Otherwise it returns an array of all valid context object
     * instances that could be created by the provided parameters.
     *
     * This method is non-blocking.
     *
     * @param scalar $context The context of the context objects to create
     * @param scalar $value The value of the context objects to create
     * @return bool|array Returns false if no contexts could be made,
     *     otherwise returns an array of the context objects
     */
    public function getContextInstanceSet( $context, $value )
    {
        return $this->_contextIndexCreateContext( $context, $value );
    }

    /**
     * <Context Index Context Object Getter Method>
     * Creates a single context object instance from the given parameters,
     * if the index contains the specified instance.
     * @param string|object|\oroboros\core\interfaces\contract\core\context\ContextContract $instance
     *     Should resolve to the classname of a context object class contained
     *     in the context index. this may be a context object, string of the
     *     class name, or an object that can be cast to a string, but the
     *     provided class name of whichever is given must resolve to a
     *     class name that the index contains.
     * @param scalar $context The context of the context objects to create
     * @param scalar $value The value of the context objects to create
     * @return bool|\oroboros\core\interfaces\contract\core\context\ContextContract
     * @throws \oroboros\core\utilities\exception\InvalidArugmentException
     *     If an instance is given that is not known to the context index
     */
    public function getContextInstance( $instance, $context, $value )
    {
        return $this->_contextIndexCreateContext( $context, $value, $instance );
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * <Context Index Initialization Method>
     * Performs the context index setup operation.
     * @return void
     */
    protected function _initializeContextIndex()
    {
        $this->_contextIndexSetContract();
        $this->_contextIndexSetType();
        $this->_contextIndexSetCategory();
        $this->_contextIndexSetSubcategory();
    }

    /**
     * <Type Declaration Method>
     * This method must be overridden to provide
     * a type filter for context objects.
     *
     * All provided context objects must validate against this
     * type declaration to be included in the context index.
     *
     * @return scalar
     */
    abstract protected function _declareType();

    /**
     * <Category Declaration Method>
     * This method may be overridden to provide
     * a category filter for context objects.
     *
     * If this value is scalar, all provided context objects must
     * validate against this category declaration to be included
     * in the context index.
     *
     * @return scalar|null
     */
    protected function _declareCategory()
    {
        return null;
    }

    /**
     * <Sub-Category Declaration Method>
     * This method may be overridden to provide
     * a sub-category filter for context objects.
     *
     * If this value is scalar, all provided context objects must
     * validate against this sub-category declaration to be included
     * in the context index.
     *
     * @return scalar|null
     */
    protected function _declareSubcategory()
    {
        return null;
    }

    /**
     * <Context Contract Declaration Method>
     * This method may be overridden to provide a more finely
     * grained contract scope for context objects.
     *
     * This method should return the name of an interface
     * that extends upon the default context object contract interface.
     * The object will not instantiate if the declared contract is not valid.
     *
     * If a valid interface is given, then all provided context objects MUST
     * validate against the given interface to be included in the context
     * index.
     *
     * @see \oroboros\core\interfaces\contract\core\context\ContextContract
     * @return string
     */
    protected function _declareContextContract()
    {
        return '\\oroboros\\core\\interfaces\\contract\\core\\context\\ContextContract';
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * Checks all criteria in the context index to see if a supplied
     * context object is valid. Returns true if it can be set in the
     * context index, false otherwise.
     * @param \oroboros\core\interfaces\contract\core\context\ContextContract $context
     * @return bool
     * @internal
     */
    private function _contextIndexValidateContext( $context )
    {
        try
        {
            \oroboros\validate\Validator::validate( 'type-of', $context,
                'object', true );
            $class = get_class( $context );
            if ( in_array( $class, $this->_contexts ) )
            {
                return true;
            }
            $this->_contextIndexValidateContextContract( $context );
            $this->_contextIndexValidateContextType( $context );
            $this->_contextIndexValidateContextCategory( $context );
            $this->_contextIndexValidateContextSubcategory( $context );
            return true;
        } catch ( \oroboros\core\utilities\exception\InvalidArgumentException $e )
        {
            return false;
        }
    }

    /**
     * Validates a candidate context object to see
     * if it fits the required contract.
     * @param \oroboros\core\interfaces\contract\core\context\ContextContract $context
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the provided context object does not honor the required contract
     * @internal
     */
    private function _contextIndexValidateContextContract( $context )
    {
        \oroboros\validate\Validator::validate( 'instance-of', $context,
            $this->_context_contract, true );
    }

    /**
     * Validates a candidate context object to see
     * if it fits the required type.
     * @param \oroboros\core\interfaces\contract\core\context\ContextContract $context
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the provided context object does not honor the required type
     * @internal
     */
    private function _contextIndexValidateContextType( $context )
    {
        \oroboros\validate\Validator::validate( 'equals', $context->getType(),
            $this->_type, true );
    }

    /**
     * Validates a candidate context object to see
     * if it fits the required category, if one is defined.
     * @param \oroboros\core\interfaces\contract\core\context\ContextContract $context
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If a category filter is defined and the provided context object
     *     does not match it
     * @internal
     */
    private function _contextIndexValidateContextCategory( $context )
    {
        if ( is_null( $this->_category ) )
        {
            return;
        }
        \oroboros\validate\Validator::validate( 'equals',
            $context->getCategory(), $this->_category, true );
    }

    /**
     * Validates a candidate context object to see
     * if it fits the required sub-category, if one is defined.
     * @param \oroboros\core\interfaces\contract\core\context\ContextContract $context
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If a sub-category filter is defined and the provided context object
     *     does not match it
     * @internal
     */
    private function _contextIndexValidateContextSubcategory( $context )
    {
        if ( is_null( $this->_subcategory ) )
        {
            return;
        }
        \oroboros\validate\Validator::validate( 'equals',
            $context->getCategory(), $this->_category, true );
    }

    /**
     * Validates that the declared contract interface is either the default
     * context contract or an extension of it, and sets it as the required
     * contract if it is.
     * @see _declareContextContract
     * @return void
     * @throws \oroboros\core\interfaces\exception\InvalidArgumentException
     *     If the class supplied context contract is not a valid instance of
     *     the default context contract
     * @see \oroboros\core\interfaces\contract\core\context\ContextContract
     * @internal
     */
    private function _contextIndexSetContract()
    {
        $contract = $this->_declareContextContract();
        \oroboros\validate\Validator::validate( 'instance-of', $contract,
            '\\oroboros\\core\\interfaces\\contract\\core\\context\\ContextContract',
            true );
        $this->_context_contract = $contract;
    }

    /**
     * Validates the provided type as a scalar value,
     * and sets it as the type filter if it is.
     * @see _declareType
     * @return void
     * @throws \oroboros\core\interfaces\exception\InvalidArgumentException
     *     If the class supplied type is not scalar
     * @internal
     */
    private function _contextIndexSetType()
    {
        $type = $this->_declareType();
        \oroboros\validate\Validator::validate( 'scalar', $type, null, true );
        $this->_type = $type;
    }

    /**
     * Validates the provided category as a scalar value if it is defined,
     * and sets it as the type filter if it is.
     * @see _declareCategory
     * @return void
     * @throws \oroboros\core\interfaces\exception\InvalidArgumentException
     *     If the class supplied category is not null or scalar
     * @internal
     */
    private function _contextIndexSetCategory()
    {
        $category = $this->_declareCategory();
        if ( is_null( $category ) )
        {
            return;
        }
        \oroboros\validate\Validator::validate( 'scalar', $category, null, true );
        $this->_category = $category;
    }

    /**
     * Validates the provided sub-category as a scalar value if it is defined,
     * and sets it as the type filter if it is.
     * @see _declareSubcategory
     * @return void
     * @throws \oroboros\core\interfaces\exception\InvalidArgumentException
     *     If the class supplied sub-category is not null or scalar
     * @internal
     */
    private function _contextIndexSetSubcategory()
    {
        $subcategory = $this->_declareSubcategory();
        if ( is_null( $subcategory ) )
        {
            return;
        }
        \oroboros\validate\Validator::validate( 'scalar', $subcategory, null,
            true );
        $this->_subcategory = $subcategory;
    }

    /**
     * Validates a proposed context object, and sets it into
     * the internal index if it passes the required filters.
     *
     * True will always be returned if it is valid, even if it was already
     * set and the duplicate was discarded. False will be returned if any
     * exceptions arise from the validation process, which indicates that
     * it was not set.
     *
     * @param type $context
     * @return bool Returns true if the context was added, false if
     *     it fails any validation criteria.
     * @internal
     */
    private function _contextIndexSetContext( $context )
    {
        if ( !$this->_contextIndexCheckContext( $context ) )
        {
            return false;
        }
        $this->_contexts[] = ltrim( $class, '\\' );
        return true;
    }

    /**
     * Creates a context instance, or an array of context instances from the
     * given context and value. If instance is specified, it will return that
     * specific context object instance, otherwise it will return all of the
     * valid context object instances in the context index that the provided
     * arguments resolve against successfully.
     * @param type $context
     * @param type $value
     * @param type $instance
     * @return bool|array|\oroboros\core\interfaces\contract\core\context\ContextContract
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If an instance is explicitly provided, and it is not
     *     a valid member of the context index
     * @internal
     */
    private function _contextIndexCreateContext( $context, $value,
        $instance = null )
    {
        if ( !is_null( $instance ) )
        {
            return $this->_contextIndexCreateContextByInstance( $instance,
                    $context, $value );
        }
        $instances = array();
        foreach ( $this->_contexts as
            $class )
        {
            $obj = $this->_contextIndexCreateContextByInstance( $class,
                $context, $value );
            if ( $obj )
            {
                $instances[] = $obj;
            }
        }
        if ( empty( $instances ) )
        {
            return false;
        }
        return $instances;
    }

    /**
     * Preflights a supplied context instance to check if it is a
     * valid instance in the context index.
     * @param type $context
     * @return string
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the given context does not resolve to a known
     *     context object instance
     * @internal
     */
    private function _contextIndexGetContextInstance( $context )
    {
        if ( is_object( $context ) )
        {
            $context = get_class( $context );
        }
        \oroboros\validate\Validator::validate( 'stringable', $context, null,
            true );
        $context = ltrim( (string) $context, '\\' );
        \oroboros\validate\Validator::valiate( 'any-of', $context,
            $this->_contexts, true );
        return $context;
    }

    /**
     * Creates a context instance by the given instance, and returns the
     * instance if it resolves correctly, or false if it does not resolve.
     * @param \oroboros\core\interfaces\contract\core\context\ContextContract|object|string $instance
     * @param scalar $context The context for creating the context object instance
     * @param scalar $value The value for creating the context object instance
     * @return bool|\oroboros\core\interfaces\contract\core\context\ContextContract
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the provided instance is not a valid member of the context index
     *     set of known context objects
     * @internal
     */
    private function _contextIndexCreateContextByInstance( $instance, $context,
        $value )
    {
        $instance = $this->_contextInstanceGetContextInstance( $instance );
        try
        {
            return new $instance( $context, $value );
        } catch ( Exception $e )
        {
            return false;
        }
    }

}
