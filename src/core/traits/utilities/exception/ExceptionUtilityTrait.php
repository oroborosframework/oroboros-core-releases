<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\traits\utilities\exception;

/**
 * <Exception Trait>
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage exception
 * @version 0.2.4
 * @since 0.2.4-alpha
 */
trait ExceptionUtilityTrait
{

    use \oroboros\core\traits\utilities\core\StringUtilityTrait;
    use \oroboros\core\traits\utilities\core\BacktraceUtilityTrait;
    use \oroboros\core\traits\utilities\core\CallableUtilityTrait;

    /**
     * Represents the internal exception parser.
     * @var \oroboros\enum\InterfaceEnumerator
     */
    private static $_exception_utility_enumerator;

    /**
     * Represents the internal registry of exception messages by code
     * @var array
     */
    private static $_exception_utility_messages = array();

    /**
     * Represents the designation as to whether to use verbose messages,
     * so a round-trip back to the core config is not neccessary more than
     * one time.
     * @var bool
     */
    private static $_exception_utility_use_verbose = false;

    /**
     * Represents the message prefix data, which is always prefixed to messages.
     * @var array
     */
    private static $_exception_utility_prefix = array();

    /**
     * Represents the message verbose contextual data, which is suffixed to
     * messages if the verbose logging setting is true in the settings loaded
     * during bootload.
     * @var array
     */
    private static $_exception_utility_verbose = array();

    /**
     * Represents the categorical ranges of error codes, for
     * statistical analysis of exception messages by code when
     * the message is unclear.
     * @var array
     */
    private static $_exception_utility_code_ranges = array();

    /**
     * Represents whether the codes, messages, and error
     * ranges have been correctly parsed yet.
     * @var bool
     */
    private static $_exception_utility_settings_loaded = false;

    /**
     * Represents the partial namespace segment leading to the exception path,
     * for recasting PHP exceptions as their Oroboros equivalent.
     * @var string
     */
    private static $_exception_utility_namespace_prefix = '\\oroboros\\core\\utilities\\exception\\';

    /**
     * Represents the recast types, for conversion of vanilla PHP exceptions
     * into their Oroboros equivalent, so that the robust error analysis
     * utilities of Oroboros can be leveraged if need be.
     * @var array
     */
    private static $_exception_recast_types = array(
        //Covers Codex Exceptions
        array(
            'LexiconQueryException' => 'codex\\LexiconQueryException' ),
        array(
            'LexiconArchiveException' => 'codex\\LexiconArchiveException' ),
        array(
            'LexiconIndexException' => 'codex\\LexiconIndexException' ),
        array(
            'LexiconException' => 'codex\\LexiconException' ),
        //Covers Psr6 Cache Exceptions
        array(
            'Psr\\Cache\\CacheException' => 'cache\\CacheException' ),
        array(
            'Psr\\Cache\\InvalidArgumentException' => 'cache\\InvalidArgumentException' ),
        //Covers Psr11 Container Exceptions
        array(
            'Psr\\Container\\ContainerExceptionInterface' => 'container\\ContainerException' ),
        array(
            'Psr\\Container\\NotFoundExceptionInterface' => 'container\\NotFoundException' ),
        //Covers Psr3 Logger Exceptions
        array(
            'Psr\\Log\\InvalidArgumentException' => 'log\\InvalidArgumentException' ),
        //Covers Database Exceptions
        array(
            'PDOException' => 'database\\PDOException' ),
        //Covers General PHP Exceptions
        //As these inherit from each other, they are ordered from specific to general.
        //Do not reorder this array.
        array(
            'BadMethodCallException' => 'BadMethodCallException' ),
        array(
            'BadFunctionCallException' => 'BadFunctionCallException' ),
        array(
            'DomainException' => 'DomainException' ),
        array(
            'InvalidArgumentException' => 'InvalidArgumentException' ),
        array(
            'LengthException' => 'LengthException' ),
        array(
            'OutOfRangeException' => 'OutOfRangeException' ),
        array(
            'OutOfBoundsException' => 'OutOfBoundsException' ),
        array(
            'OverflowException' => 'OverflowException' ),
        array(
            'RangeException' => 'RangeException' ),
        array(
            'UnderflowException' => 'UnderflowException' ),
        array(
            'UnexpectedValueException' => 'UnexpectedValueException' ),
        array(
            'LogicException' => 'LogicException' ),
        array(
            'RuntimeException' => 'RuntimeException' ),
        array(
            'Exception' => 'Exception' ),
    );

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Exception Code Getter>
     * Returns the designated code for a canonicalized string.
     *
     * @param string $slug The canonicalized exception code identifier
     * @return int Returns 0 if code not found, otherwise returns the specified code.
     */
    public static function getCode( $slug = 'unknown' )
    {
        self::_exceptionUtilityLoadSettings();
        $slug = 'ERROR_' . str_replace( '-', '_', strtoupper( $slug ) );
        if ( self::_exceptionUtilityGetEnumerator()->has( $slug ) )
        {
            return self::_exceptionUtilityGetEnumerator()->get( $slug );
        }
        return \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_UNKNOWN;
    }

    /**
     * <Exception Message Getter>
     * Returns the designated message for a canonicalized string.
     *
     * @param string $slug The canonicalized exception message identifier
     * @return string Returns "An unknown error has occurred." if code not found,
     * otherwise returns the designated code for the specified message.
     */
    public static function getMessage( $slug = 'unknown' )
    {
        self::_exceptionUtilityLoadSettings();
        $slug = 'ERROR_' . str_replace( '-', '_', strtoupper( $slug ) ) . '_MESSAGE';
        if ( self::_exceptionUtilityGetEnumerator()->has( $slug ) )
        {
            return self::_exceptionUtilityGetEnumerator()->get( $slug );
        }
        return \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_UNKNOWN_MESSAGE;
    }

    /**
     * <Exception Identifier Getter>
     * Returns the designated canonicalized identifier for an exception code.
     *
     * @param int $code The exception code to reverse parse.
     * @return bool|string Returns false if code not found, otherwise returns
     * the canonicalized identifier for the code. The same identifier can be
     * used to return the corresponding default message.
     */
    public static function getCodeIdentifier( $code = 0 )
    {
        self::_exceptionUtilityLoadSettings();
        $key = array_search( $code,
            self::_exceptionUtilityGetEnumerator()->filterKey( '_MESSAGE', true,
                true )->filterKey( 'DEFAULT_EXCEPTION_MESSAGE_', false, true )->filterKey( '_MINIMUM_ERROR_RANGE',
                true, true )->filterKey( 'RANGE_', false, true )->toArray() );
        if ( !$key )
        {
            return false;
        }
        return str_replace( '_', '-', strtolower( substr( $key, 6 ) ) );
    }

    /**
     * <Exception Code Identifier Set Getter>
     * Returns an array of all acceptable canonicalized code/message identifiers.
     *
     * @return array
     */
    public static function getCodeIdentifiers()
    {
        self::_exceptionUtilityLoadSettings();
        $codes = array();
        foreach ( array_keys( self::_exceptionUtilityGetEnumerator()->filterKey( '_MESSAGE',
                true, true )->filterKey( 'DEFAULT_EXCEPTION_MESSAGE_', false,
                true )->filterKey( '_MINIMUM_ERROR_RANGE', true, true )->filterKey( 'RANGE_',
                false, true )->toArray() ) as
            $key )
        {
            $codes[] = str_replace( '_', '-', strtolower( substr( $key, 6 ) ) );
        }
        return $codes;
    }

    /**
     * <Exception Identifier Set Getter>
     * Returns an array of all acceptable canonicalized exception identifiers.
     *
     * @return array
     */
    public static function getExceptionIdentifiers()
    {
        self::_exceptionUtilityLoadSettings();
        return self::_exceptionUtilityGetExceptionIdentifiers();
    }

    /**
     * <Exception Generator Method>
     * Throws a generated exception using standardized Oroboros canonicalization.
     * This method uses standardized exception messages found in
     * the ExceptionMessage enumerated interface, and exception codes
     * found in the ExceptionCode enumerated interface.
     *
     * This method returns the exception but does not throw it.
     *
     * @example self::throwException( 'invalid-argument', 'logic-bad-parameters', array( 'string', 'boolean' );
     *
     * @param type $type
     * @param type $slug
     * @param type $params
     * @param \Exception $previous
     * @param array $context
     * @see \oroboros\core\interfaces\enumerated\exception\ExceptionCode
     * @see \oroboros\core\interfaces\enumerated\exception\ExceptionMessage
     * @return \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract
     */
    public static function getException( $type = 'exception', $slug = 'unknown',
        $params = array(), \Exception $previous = null, $context = array() )
    {
        self::_exceptionUtilityLoadSettings();
        $class = self::_exceptionUtilityMatchCanonicalizedExceptionName( $type );
        $bt = debug_backtrace( 2 );
        $trace = ( array_key_exists( 1, $bt )
            ? $bt[1]
            : $bt[0]);
        unset( $bt );
        if ( $trace['function'] === __FUNCTION__ && $trace['class'] === __CLASS__ )
        {
            unset( $trace['class'] );
            unset( $trace['function'] );
            unset( $trace['type'] );
        }
        $sprintf = array();
        $sprintf[] = self::getMessage( $slug );
        $sprintf[] = self::_exceptionUtilityBuildTracebackString( $trace );
        foreach ( $params as
            $param )
        {
            $sprintf[] = $param;
        }
        $message = self::_callableUtilityCall( 'sprintf', $sprintf );
        $code = self::getCode( $slug );
        $message = self::_exceptionUtilityGetContextualMessageBody( $message,
                $code, $context );
        return new $class( $message, $code, $previous );
    }

    /**
     * <Exception Throw Method>
     * Throws an Oroboros exception by canonicalized keyword.
     * @param type $type (optional) The canonicalized keyword selector for the exception. Default is the Oroboros standard exception.
     * @param type $message (optional) Standard exception message. Default is no message.
     * @param type $code (optional) Standard exception code. May also be a canonicalized string. Default is 0
     * @param \Exception $previous (optional) An optional previous exception to pass into the exception stack. Default is null.
     * @return void
     * @throws \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract
     * @codeCoverageIgnore This class always throws an exception,
     *     and cannot be fully tested. It works fine, it's used
     *     all over the place.
     */
    public static function throwException( $type = 'exception', $message = '',
        $code = 0, \Exception $previous = null, $context = array() )
    {
        self::_exceptionUtilityLoadSettings();
        $class = self::_exceptionUtilityMatchCanonicalizedExceptionName( $type );
        if ( is_string( $code ) )
        {
            $code = self::getCode( $code );
        }
        $message = self::_exceptionUtilityGetContextualMessageBody( $message,
                $code, $context );
        throw new $class( $message, $code, $previous );
    }

    /**
     * <Exception Recast Method>
     * Recasts a given exception as an equivalent that honors its inheritance
     * and is recast as an Oroboros Exception that honors the internal exception
     * contract interface.
     *
     * This method returns the exception but does not throw it.
     *
     * @param \Exception $exception
     * @param string $type (optional) If provided, will recast based on a canonicalized name
     * @return \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract
     */
    public static function recastException( \Exception $exception, $type = null )
    {
        self::_exceptionUtilityLoadSettings();
        if ( !is_null( $type ) )
        {
            $class = self::_exceptionUtilityMatchType( $type );
        } else
        {
            $class = self::_exceptionUtilityGetExceptionRecastClass( $exception );
        }
        return new $class( $exception->getMessage(), $exception->getCode(),
            $exception );
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */
    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * <Exception Recast Getter>
     * @param \Exception $exception The exception to evaluate
     * @return string The Oroboros Core Contract equivalent of the given exception.
     * @internal
     */
    private static function _exceptionUtilityGetExceptionRecastClass( \Exception $exception )
    {
        return self::_exceptionUtilityMatchType( get_class( $exception ) );
    }

    /**
     * <Exception Contract Class Getter>
     * <Gets a exception instance name that honors the Oroboros ExceptionContract,
     * and is an equivalent instance of the given exception class.
     * @param string $classname
     * @return string
     * @internal
     */
    private static function _exceptionUtilityMatchType( $classname = 'Exception' )
    {
        //Set the default
        $new = self::$_exception_utility_namespace_prefix . 'Exception';
        if ( in_array( $classname, self::getExceptionIdentifiers() ) )
        {
            return self::_exceptionUtilityMatchCanonicalizedExceptionName( $classname );
        }
        foreach ( self::$_exception_recast_types as
            $key =>
            $array )
        {
            $keyset = array_keys( $array );
            $parent = $keyset[0];
            $recast = $array[$parent];
            unset( $keyset );
            if ( $classname === $parent || is_subclass_of( $classname,
                    '\\' . $parent ) )
            {
                $new = self::$_exception_utility_namespace_prefix . $recast;
                break;
            }
        }
        return $new;
    }

    /**
     * Matches a canonicalized exception name, and returns the
     * contract honoring fully qualified instance name.
     * @param string $name The canonicalized exception name (eg: invalid-argument )
     * @return string The fully qualified exception name
     * @internal
     */
    private static function _exceptionUtilityMatchCanonicalizedExceptionName( $name
    = 'exception' )
    {
        $class = self::_exceptionUtilityMatchType();
        if ( !($name === 'exception' || $name === 'Exception') )
        {
            $name = str_replace( '-exception', null,
                    self::_stringUtilityCanonicalize( $name ) ) . '-exception';
            foreach ( self::$_exception_recast_types as
                $key =>
                $exception )
            {
                $keyset = array_keys( $exception );
                $parent = $keyset[0];
                $recast = $exception[$parent];
                unset( $keyset );
                $keyfix = self::_stringUtilityCanonicalize( str_replace( '\\',
                            '-', str_replace( 'PDO', 'pdo', $parent ) ) );
                if ( $name === $keyfix )
                {
                    $class = self::_exceptionUtilityMatchType( $parent );
                    break;
                }
            }
        }
        return $class;
    }

    /**
     * <Backtrace Localization String Getter>
     * Builds a localization string from a debug backtrace stack level array
     * @param array $trace_instance a key from debug_backtrace()
     * @return string
     * @internal
     */
    private static function _exceptionUtilityBuildTracebackString( $trace_instance )
    {
        if ( !(array_key_exists( 'class', $trace_instance ) && array_key_exists( 'function',
                $trace_instance ) && array_key_exists( 'type', $trace_instance ) ) )
        {
            //procedural context
            $result = sprintf( 'line %s of file: %s', $trace_instance['line'],
                $trace_instance['file'] );
        } else
        {
            //object oriented context
            $result = sprintf( '%s%s%s', $trace_instance['class'],
                $trace_instance['type'], $trace_instance['function'] );
        }
        return $result;
    }

    /**
     * Gets the canonicalized identifier names for handled exceptions.
     * @return array
     * @internal
     */
    private static function _exceptionUtilityGetExceptionIdentifiers()
    {
        $exceptions = array();
        foreach ( self::$_exception_recast_types as
            $key =>
            $exception )
        {
            $keyset = array_keys( $exception );
            $parent = $keyset[0];
            $recast = $exception[$parent];
            unset( $keyset );
            $exceptions[] = self::_stringUtilityCanonicalize( str_replace( '\\',
                        '-', str_replace( 'PDO', 'pdo', $parent ) ) );
        }
        return $exceptions;
    }

    /**
     * Instantiates the enumerator if it is not already, and returns it.
     * @return \oroboros\enum\InterfaceEnumerator
     * @internal
     */
    private static function _exceptionUtilityGetEnumerator()
    {
        if ( is_null( self::$_exception_utility_enumerator ) )
        {
            self::$_exception_utility_enumerator = new \oroboros\enum\InterfaceEnumerator( '\\oroboros\\core\\interfaces\\enumerated\\exception\\ExceptionMessage' );
        }
        return self::$_exception_utility_enumerator;
    }

    /**
     * Builds the contextual message from the supplied text
     * and the code-based error message.
     * @param string $text (optional) The exception message
     * @param int $code  (optional) The exception code
     * @param array $context (optional) The exception contextual information.
     * @return string
     */
    private static function _exceptionUtilityGetContextualMessageBody( $text = '',
        $code = 0, $context = array() )
    {
        if ( !array_key_exists( 'context', $context ) )
        {
            $context['context'] = $text;
            $text = '';
            if ( trim( $context['context'] ) == '' )
            {
                $context['context'] = '(no context was provided)';
            }
        }
        $err_context = self::_exceptionUtilityGetErrorContext( $code );
        if ( $err_context )
        {
            $context['error-context'] = $err_context;
        }
        if ( array_key_exists( 'backtrace', $context ) )
        {
            $backtrace = $context['backtrace'];
            if ( is_array( $backtrace ) )
            {
                $backtrace = self::_backtraceUtilityTraceAsString( $backtrace,
                        true, true );
            }
            $backtrace = self::_stringUtilityIndentLines( $backtrace, 8 );
            $context['backtrace'] = PHP_EOL . $backtrace;
        }
        $prefix = self::_exceptionUtilityGetMessagePrefix( $context );
        $body = self::_exceptionUtilityGetMessageTemplate( $code );
        $suffix = self::_exceptionUtilityGetMessageSuffix( $context );
        $message = self::_stringUtilityInterpolate( $prefix . $body . $text . $suffix,
                $context, '{', '}' );
        return $message;
    }

    /**
     * Returns the localized language string message template for interpolation.
     * @param int $code
     * @return string
     * @internal
     */
    private static function _exceptionUtilityGetMessageTemplate( $code )
    {
        if ( is_int( $code ) && array_key_exists( $code,
                self::$_exception_utility_messages ) )
        {
            return self::$_exception_utility_messages[$code];
        }
        return self::$_exception_utility_messages[0];
    }

    private static function _exceptionUtilityGetMessagePrefix( $context )
    {
        return implode( ', ', self::$_exception_utility_prefix ) . '. ';
    }

    /**
     *
     * @param int $code
     * @return bool|string
     */
    private static function _exceptionUtilityGetErrorContext( $code )
    {
        foreach ( self::$_exception_utility_code_ranges as
            $context =>
            $range )
        {
            if ( is_int( $code ) && $code >= $range['min'] && $code <= $range['max'] )
            {
                return $context;
            }
        }
        return false;
    }

    private static function _exceptionUtilityGetMessageSuffix( $context )
    {
        $suffix = PHP_EOL;
        if ( !self::$_exception_utility_use_verbose )
        {
            return $suffix;
        }
        foreach ( self::$_exception_utility_verbose as
            $key =>
            $verbose )
        {
            if ( !array_key_exists( $key, $context ) )
            {
                continue;
            }
            $suffix .= self::_stringUtilityIndentLines( $verbose, 4 ) . PHP_EOL;
        }
        return $suffix;
    }

    /**
     * Parses the exception code language file values to get the localized
     * messages in the current server admin language, and sets the code
     * ranges by category.
     * @note Currently only english code messages are provided [en-us].
     * @todo Translate exception codes into french, german, arabic, etc.
     * @return void
     * @internal
     */
    private static function _exceptionUtilityLoadSettings()
    {
        if ( self::$_exception_utility_settings_loaded )
        {
            return;
        }
        $settings = \oroboros\core\utilities\core\CoreConfig::get( 'lang',
                'exception' );
        self::$_exception_utility_verbose = $settings['verbose'];
        self::$_exception_utility_prefix = $settings['prefix'];
        self::$_exception_utility_use_verbose = \oroboros\core\utilities\core\CoreConfig::get( 'settings',
                'core' )['log']['verbose'];
        unset( $settings['verbose'] );
        unset( $settings['prefix'] );
        $enum = self::_exceptionUtilityGetEnumerator();
        $prefix = 'RANGE_ERROR_';
        $ranges = $enum->filterKey( $prefix )->toArray();
        foreach ( $settings as
            $category =>
            $codes )
        {
            $const_key = $prefix . strtoupper( $category );
            $floor = $ranges[$const_key . '_MIN'];
            $ceil = $ranges[$const_key . '_MAX'];
            self::$_exception_utility_code_ranges[$category] = array(
                'min' => $floor,
                'max' => $ceil );
            foreach ( $codes as
                $code =>
                $message )
            {
                self::$_exception_utility_messages[$code] = $message;
            }
        }
        self::$_exception_utility_settings_loaded = true;
    }

}
