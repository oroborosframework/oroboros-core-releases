<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\traits\utilities\error;

/**
 * <Error Handler Trait>
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage utilities
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\core\interfaces\contract\utilities\error\ErrorHandlerContract
 */
trait ErrorHandlerTrait
{

    use \oroboros\core\traits\utilities\UtilityTrait;
    use \oroboros\core\traits\core\BaselineTrait
    {
        \oroboros\core\traits\core\BaselineTrait::initialize as private baseline_initialize;
    }
    use \oroboros\core\traits\utilities\core\BitmaskUtilityTrait;
    use \oroboros\core\traits\utilities\core\CallableUtilityTrait;

    /**
     * Represents the total set of error handler levels. This will be compiled
     * from the cachable and uncachable error levels on first instantiation
     * of any error handler, and will remain in use as the basis of checks
     * for severity level thereafter.
     * @var array
     */
    private static $_error_handler_levels = array();

    /**
     * Represents the catchable error levels that runtime error handling can
     * handle. These levels will operate normally during runtime operation
     * in conjunction with the error handler.
     * @var array
     */
    private static $_error_handler_catchable_levels = array(
        'warning' => E_WARNING,
        'notice' => E_NOTICE,
        'user-error' => E_USER_ERROR,
        'user-warning' => E_USER_WARNING,
        'user-notice' => E_USER_NOTICE,
        'strict' => E_STRICT,
        'recoverable' => E_RECOVERABLE_ERROR,
        'deprecated' => E_DEPRECATED,
        'user-deprecated' => E_USER_DEPRECATED,
        'all' => E_ALL
    );

    /**
     * Represents error levels that do not automatically call the user-defined error handler.
     *
     * These references may still come up when a fatal error occurs, and a
     * fatal error instance should still check these and allow delegation
     * to methods for these, however runtime errors cannot use these.
     *
     * @var array
     */
    private static $_error_handler_uncatchable_levels = array(
        'error' => E_ERROR,
        'parser-error' => E_PARSE,
        'core-error' => E_CORE_ERROR,
        'core-warning' => E_CORE_WARNING,
        'compile-error' => E_COMPILE_ERROR,
        'compile-warning' => E_COMPILE_WARNING
    );

    /**
     * Represents errors that cause a fatal error if not handled.
     * These error indicate that a fatal error should be treated as
     * the origin point being in an unusable state.
     * @var array
     */
    private static $_error_handler_fatal_errors = array(
        'error' => E_ERROR,
        'parser-error' => E_PARSE,
        'core-error' => E_CORE_ERROR,
        'compile-error' => E_COMPILE_ERROR,
        'user-error' => E_USER_ERROR,
        'recoverable' => E_RECOVERABLE_ERROR
    );

    /**
     * Represents errors that do not interrupt script execution.
     * These errors may be able to be handled, depending on the
     * context in which the error arose, but may still indicate
     * that an exit should be called.
     * @var array
     */
    private static $_error_handler_warning_errors = array(
        'warning' => E_WARNING,
        'user-warning' => E_USER_WARNING,
        'core-warning' => E_CORE_WARNING,
        'compile-warning' => E_COMPILE_WARNING
    );

    /**
     * Represents errors that do not interrupt script execution,
     * and generally can be discarded without issue, though not always.
     * These errors are likely candidates for suppression in the
     * majority of cases, but no automatic assumption that they
     * should be suppressed is made without instruction to do so.
     * @var array
     */
    private static $_error_handler_notice_errors = array(
        'notice' => E_NOTICE,
        'user-notice' => E_USER_NOTICE,
        'strict' => E_STRICT,
        'deprecated' => E_DEPRECATED,
        'user-deprecated' => E_USER_DEPRECATED,
    );

    /**
     * Represents the value to set to disable error reporting entirely,
     * which does not have a consistent constant declaration in
     * all PHP versions.
     * @var int
     */
    private static $_error_handler_disable_level = 0;

    /**
     * Contains a reference to the previous error handler, if one existed.
     * @var null|callable
     */
    private $_error_handler_previous = null;

    /**
     * Represents the error levels handled by the error handler.
     * @var array
     */
    private $_error_handler_handled_levels = array();

    /**
     * Represents the queue of shutdown operations that occurs when
     * a fatal error occurs.
     * @var array
     */
    private $_error_handler_fatal_error_operations = array();

    /**
     * Represents the queue of operations that occurs when
     * a runtime error occurs.
     * @var array
     */
    private $_error_handler_runtime_operations = array();

    /**
     * Represents internal statistics on all errors
     * captured by the error handler, which can be obtained
     * at any time.
     * @var array
     */
    private static $_error_handler_error_statistics = array();

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\core\interfaces\contract\utilities\error\ErrorHandlerContract
     *
     * @execution $object->register();
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Error Handler Initialization>
     * Initializes the error handler.
     *
     * @param array $params
     * @param array $dependencies
     * @param array $flags
     * @return void
     */
    public function initialize( $params = null, $dependencies = null,
        $flags = null )
    {
        $this->_baselineSetParameterPersistence( true );
        $this->_baselineSetDependencyPersistence( true );
        $this->_baselineSetFlagPersistence( true );
        $this->_baselineRegisterInitializationTask( '_errorHandlerPostInitialization' );
        $this->baseline_initialize( $params, $dependencies, $flags );
    }

    /**
     * <Error Handler Registration Method>
     * Registers the error handler with PHP to accept inbound errors from PHP.
     * The previous error handler will be captured when this occurs, if one
     * existed. If the internal switch to defer to previous is set, then error
     * handling return values will be provided by the previous error handler,
     * otherwise they will be handled as per the internal settings of the
     * error handler.
     * @return $this
     */
    public function register()
    {
        $this->_errorHandlerRegister();
        return $this;
    }

    /**
     * <Error Handler Unregistration Method>
     * Unregisters the error handler, and restores the previous error handler.
     * @return $this
     */
    public function unregister()
    {
        $this->_errorHandlerUnregister();
        return $this;
    }

    /**
     * <Error Handler Registration Check Method>
     * Returns a boolean determination as to whether the error handler
     * is currently registered. True if it is active, and false if
     * it is inactive.
     * @return bool
     */
    public function isRegistered()
    {
        return $this->_errorHandlerCheckRegistration();
    }

    /**
     * <Error Handler Previous Error Callback Getter Method>
     * Returns the previous error handler callback if one was declared.
     * If the error handler is currently in an unregistered state, this
     * will always return null. If the error handler is registered, this
     * will return null if no previous error handler was captured, and
     * otherwise will return the callable declaration of the previous
     * error handler.
     * @return null|callable
     */
    public function getPreviousHandler()
    {
        return $this->_error_handler_previous;
    }

    /**
     * <Error Handler Previous Error Handler Check Method>
     * Returns a boolean determination as to whether the error handler has
     * detected a prior error handler registered after its registration.
     * If the error handler is currently in an unregistered state, this will
     * always return false even if one is set. If the error handler has
     * been registered, then this will return true if another error handler
     * method was obtained during registration, and false if there was no
     * prior error handler.
     * @return bool
     */
    public function hasPreviousHandler()
    {
        return !is_null( $this->_error_handler_previous );
    }

    /**
     * <Error Handler Error Level Setter Method>
     * Recieves an array of the error levels to set handling for, and declares
     * them as reportable error levels. These must correspond to the
     * canonicalized string keys that can be obtained by calling
     * getErrorLevelKeys, and must exist in getCatchableErrorLevels,
     * or an InvalidArgumentException will be raised.
     * @param array $levels The error levels to handle
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If an invalid key is provided, or an error level that is not
     *     runtime catchable is provided.
     */
    public function setErrorLevels( $levels )
    {
        \oroboros\validate\Validator::validate( 'type-of', $levels, 'array',
            true );
        $old_level = $this->getErrorVisibility();
        try
        {
            $this->_errorHandlerSetErrorLevel( self::$_error_handler_disable_level );
            foreach ( $levels as
                $level )
            {
                \oroboros\validate\Validator::validate( 'any-of', $level,
                    array_merge( array_keys( self::$_error_handler_levels ),
                        self::$_error_handler_levels ), true );
                if ( is_string( $level ) )
                {
                    $level = array_search( $level, self::$_error_handler_levels );
                }
                $this->_errorHandlerAddErrorLevel( $level );
            }
        } catch ( \oroboros\core\utilities\exception\InvalidArgumentException $e )
        {
            $this->setErrorVisibility( $old_level );
            throw $e;
        }
        return $this;
    }

    /**
     * <Error Handler Error Level Identifying Key Getter Method>
     * Returns an indexed array of the valid canonicalized keys corresponding
     * to valid PHP error levels. These may be used interchangeably with the
     * integer bitmask value to designate how to handle errors throughout the
     * other methods of the error handler.
     * @return array
     */
    public function getErrorLevelKeys()
    {
        return array_keys( self::$_error_handler_levels );
    }

    /**
     * <Error Handler Runtime Catchable Error Level Getter Method>
     * Returns an associative array of the canonicalized slug designations
     * of catchable error levels, with the value being the bitmask integer
     * value for the error level. The return values are levels that can be
     * handled at runtime by the error handler. Error levels that are not
     * in this result cannot be caught at runtime by the error handler.
     * @return array
     */
    public function getCatchableErrorLevels()
    {
        return array_keys( self::$_error_handler_catchable_levels );
    }

    /**
     * <Error Handler Error Level Getter>
     * Refreshes the internal bitwise mask of the current PHP error
     * level visibility, and then returns an associative array of each
     * valid error key, with a boolean value designating whether that
     * error level is currently visible.
     * @return array
     */
    public function getErrorLevels()
    {
        return $this->_errorHandlerGetErrorVisibility();
    }

    /**
     * <Error Handler Error Level Check Method>
     * Checks if visibility is enabled for a given error level, and
     * returns a boolean determination as to whether the given error
     * level exists in PHPs current error handling scope.
     *
     * This method can receive a predefined PHP constant designation
     * of an error level (eg: E_ALL, E_DEPRECATED, E_USER_ERROR, etc),
     * the integer value corresponding to one of these, or the canonical
     * string of the internal error value designators. All of these will
     * resolve to a valid response.
     *
     * @param int|string $level
     * @return bool
     */
    public function hasErrorLevel( $level )
    {
        return $this->_errorHandlerHasErrorVisibility( $level );
    }

    /**
     * <Error Handler Error Level Add Method>
     * Adds a given error level to the handled visibility level.
     *
     * This method can receive a predefined PHP constant designation
     * of an error level (eg: E_ALL, E_DEPRECATED, E_USER_ERROR, etc),
     * the integer value corresponding to one of these, or the canonical
     * string of the internal error value designators. All of these will
     * resolve correctly.
     *
     * @param int|string $level
     * @return void
     */
    public function addErrorLevel( $level )
    {
        $this->_errorHandlerAddErrorLevel( $level );
    }

    /**
     * <Error Handler Error Level Remove Method>
     * Removes a given error level to the handled visibility level.
     *
     * This method can receive a predefined PHP constant designation
     * of an error level (eg: E_ALL, E_DEPRECATED, E_USER_ERROR, etc),
     * the integer value corresponding to one of these, or the canonical
     * string of the internal error value designators. All of these will
     * resolve correctly.
     *
     * @param int|string $level
     * @return void
     */
    public function removeErrorLevel( $level )
    {
        $this->_errorHandlerRemoveErrorLevel( $level );
    }

    /**
     * <Error Handler Bitwise Visibility Setter Method>
     * Sets the full visibility scope by the provided bitmask.
     * This will override all other set levels that currently exist.
     * @param int $bitmask
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If an invalid bitmask is supplied.
     */
    public function setErrorVisibility( $bitmask )
    {
        $this->_errorHandlerSetErrorLevel( $bitmask );
    }

    /**
     * <Error Handler Bitmask Error Visibility Getter Method>
     * Returns the integer bitmask value of the current error level.
     * @return int
     */
    public function getErrorVisibility()
    {
        return $this->_errorHandlerGetErrorLevel();
    }

    /**
     * <Error Handler Bitmask Error Visibility Setter Method>
     * Checks if a given bitmask integer value exists, and returns
     * a boolean determination as to whether or not it is defined.
     *
     * This method should be used for complex bitwise operators in place
     * of hasErrorLevel. For single error level checks, hasErrorLevel can
     * handle both canonical strings and integer checks, and getErrorLevels
     * will return the full designation for all valid error levels as an
     * associative array by bitwise key slug identifier. Those methods
     * should be more often used for general checks, but this method is
     * provided for automatic checking with configurations from 3rd party
     * systems that track by integer or bitwise math values.
     *
     * @param int $bitmask
     * @return bool
     */
    public function hasVisibility( $bitmask )
    {
        \oroboros\validate\Validator::validate( 'type-of', $bitmask, 'integer',
            true );
        $this->_errorHandlerUpdateVisibilityFromPhp();
        return $this->_bitmaskUtilityGet( $bitmask );
    }

    /**
     * <Error Handler Method>
     * This is the method that will be called directly for all versions of PHP
     * prior to 7.2.0, and is the method registered when the register() method
     * is called prior to PHP 7.2.0. This method will loop through the given
     * internals and pass the listed parameters to the correct methods as well
     * as the previous error handler, if one existed.
     *
     * This method will still be called for PHP version 7.2.0 and later
     * internally, but will not be directly called by PHP, as the $errcontext
     * parameter raises a deprecated warning if called directly after version
     * 7.2. For versions later than this, the method with this parameter
     * omitted will be directly registered, and it will instead call this
     * method and always pass null for $errcontext, but this method will
     * still handle the actual error.
     *
     * The return value of this method depends on the internal settings,
     * and whether or not the option to allow the previous handler to handle
     * the error is set to true.
     *
     * @param int $severity Contains the level of error raised as an integer.
     * @param string $message Contains the error message as a string.
     *     This parameter is always defined.
     * @param string $file (optional) Contains the name of the file
     *     where the error occurred as a string, or null if not defined.
     * @param int $line (optional) Contains the line that the error
     *     occurred at as an integer, or null if not defined.
     * @param array $context (optional) Contains an array of all variables
     *     that existed in the scope of where the method arose. This parameter
     *     was deprecated in PHP 7.2, and will always be null for versions later
     *     than that.
     * @return bool If true, the PHP internal error handler will fire.
     *     If false, the error will be considered to be handled correctly
     *     and the program will continue to operate.
     */
    public function handleError( $severity, $message, $file = null,
        $line = null, $context = null )
    {
        return $this->_errorHandlerHandleError( $severity, $message, $file,
                $line, $context );
    }

    /**
     * <PHP 7.2 Error Handler Method>
     * This is the method that will be called directly for all versions of PHP
     * after 7.2.0. This methods purpose is to suppress the deprecation warning
     * for the error context parameter. Otherwise it is only a proxy to the
     * standard error handler method.
     *
     * The return value of this method depends on the internal settings,
     * and whether or not the option to allow the previous handler to handle
     * the error is set to true.
     *
     * @param int $severity Contains the level of error raised as an integer.
     * @param string $message Contains the error message as a string.
     *     This parameter is always defined.
     * @param string $file (optional) Contains the name of the file
     *     where the error occurred as a string, or null if not defined.
     * @param int $line (optional) Contains the line that the error
     *     occurred at as an integer, or null if not defined.
     * @param array $context (optional) Contains an array of all variables
     *     that existed in the scope of where the method arose. This parameter
     *     was deprecated in PHP 7.2, and will always be null for versions later
     *     than that.
     * @return bool If true, the PHP internal error handler will fire.
     *     If false, the error will be considered to be handled correctly
     *     and the program will continue to operate.
     * @see handleError
     */
    public function handleError72( $severity, $message, $file = null,
        $line = null )
    {
        return $this->handleError( $severity, $message, $file, $line, null );
    }

    /**
     * <Error Handler Fatal Error Handler Method>
     * Handles fatal on shutdown if the error handler is registered,
     * and if it has any operations registered for fatal errors.
     */
    public function handleFatalError()
    {
        $this->_errorHandlerHandleFatal();
    }

    /**
     * <Error Handler Error Statistics Getter Method>
     * Returns an array of all errors collected by the error handler,
     * which are keyed by the microtime that they occurred, and contain the
     * error details, the methods assigned to handle the error, the previous
     * error handler if it fired, whether or not the error was suppressed,
     * and which method suppressed it if any did.
     * @return array
     */
    public function getErrorStatistics()
    {
        return self::$_error_handler_error_statistics[get_called_class()];
    }

    /**
     * <Error Handler Statistic Tracking Enable Method>
     * Turns on error statistic tracking.
     *
     * Error statistics keeps an internal log of all errors received,
     * whether they were handled, when and where they originated from,
     * and a number of other contextual details about the error. This
     * information is not automatically logged externally, but may be
     * retrieved at any time by calling getErrorStatistics. Only errors
     * that fire while the error handler is registered and error statistics
     * are enabled will be tracked. If disabled, no statistical tracking
     * will take place, but existing tracking entries will be retained.
     *
     * @return void
     */
    public function enableErrorStatistics()
    {
        if ( !$this->checkFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_TRACK_STATISTICS ) )
        {
            $this->setFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_TRACK_STATISTICS );
        }
    }

    /**
     * <Error Handler Statistic Tracking Disable Method>
     * Turns off error statistic tracking.
     *
     * Error statistics keeps an internal log of all errors received,
     * whether they were handled, when and where they originated from,
     * and a number of other contextual details about the error. This
     * information is not automatically logged externally, but may be
     * retrieved at any time by calling getErrorStatistics. Only errors
     * that fire while the error handler is registered and error statistics
     * are enabled will be tracked. If disabled, no statistical tracking
     * will take place, but existing tracking entries will be retained.
     *
     * @return void
     */
    public function disableErrorStatistics()
    {
        if ( $this->checkFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_LOG_UNHANDLED ) )
        {
            $this->unsetFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_LOG_UNHANDLED );
        }
    }

    /**
     * <Error Handler Automatic Logging Enable Method>
     * Turns on error automatic logging of unhandled errors.
     *
     * Errors that are not handled by any internal registered runtime operation
     * as well as fatal errors will be automatically logged to the standard
     * Oroboros logger if automatic logging is enabled. If disabled, no logging
     * will automatically take place.
     *
     * @return void
     */
    public function enableErrorLogging()
    {
        if ( !$this->checkFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_LOG_UNHANDLED ) )
        {
            $this->setFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_LOG_UNHANDLED );
        }
    }

    /**
     * <Error Handler Automatic Logging Disable Method>
     * Turns off error automatic logging of unhandled errors.
     *
     * Errors that are not handled by any internal registered runtime operation
     * as well as fatal errors will be automatically logged to the standard
     * Oroboros logger if automatic logging is enabled. If disabled, no logging
     * will automatically take place.
     *
     * @return void
     */
    public function disableErrorLogging()
    {
        if ( $this->checkFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_LOG_UNHANDLED ) )
        {
            $this->unsetFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_LOG_UNHANDLED );
        }
    }

    /**
     * <Error Handler Error Exception Enable Method>
     * Turns on error automatic conversion of unhandled errors to ErrorExceptions.
     *
     * Errors that are not handled by any internal registered runtime operation
     * will be recast to an oroboros instance of the ErrorException class at the
     * end of the handler operation if this is enabled, which allows native try/catch
     * blocks to manage errors without the overhead of complex integration of an
     * error handler. If disabled, they will simply propogate forward as the
     * same error that was originally triggered.
     *
     * @return void
     */
    public function enableUseExceptions()
    {
        if ( !$this->checkFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_ERROR_EXCEPTION ) )
        {
            $this->setFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_ERROR_EXCEPTION );
        }
    }

    /**
     * <Error Handler Error Exception Disable Method>
     * Turns off error automatic conversion of unhandled errors to ErrorExceptions.
     *
     * Errors that are not handled by any internal registered runtime operation
     * will be recast to an oroboros instance of the ErrorException class at the
     * end of the handler operation if this is enabled, which allows native try/catch
     * blocks to manage errors without the overhead of complex integration of an
     * error handler. If disabled, they will simply propogate forward as the
     * same error that was originally triggered.
     *
     * @return void
     */
    public function disableUseExceptions()
    {
        if ( $this->checkFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_ERROR_EXCEPTION ) )
        {
            $this->unsetFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_ERROR_EXCEPTION );
        }
    }

    /**
     * <Error Handler Defer-to-Previous Error Handler Enable Method>
     * Turns on deferral of handling to the previously registered error handler,
     * if one exists. This can be useful for applications that already have
     * their own error handling schema, but want to collect additional
     * information about errors or log the ones that are not handled
     * automatically, or otherwise have a simple means of binding additional
     * error operations without disrupting the existing error handling logic.
     *
     * If enabled, errors will only be flagged as handled when a previous error handler
     * exists if that handler returns true. The internal operations of this
     * handler will still run, but their output will be ignored when this is
     * enabled. This cannot be simultaneously enabled with the call-previous
     * switch, and enabling either will disable the other. If this is disabled,
     * the previous error handler will not run at all unless the call-previous
     * switch is set to true.
     *
     * @return void
     */
    public function enableDeferPrevious()
    {
        if ( $this->checkFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_CALL_PREVIOUS ) )
        {
            $this->unsetFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_CALL_PREVIOUS );
        }
        if ( !$this->checkFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_DEFER_PREVIOUS ) )
        {
            $this->setFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_DEFER_PREVIOUS );
        }
    }

    /**
     * <Error Handler Defer-to-Previous Error Handler Disable Method>
     * Turns off deferral of handling to the previously registered error handler,
     * if one exists.
     *
     * If enabled, errors will only be flagged as handled when a previous error handler
     * exists if that handler returns true. The internal operations of this
     * handler will still run, but their output will be ignored when this is
     * enabled. This cannot be simultaneously enabled with the call-previous
     * switch, and enabling either will disable the other. If this is disabled,
     * the previous error handler will not run at all unless the call-previous
     * switch is set to true.
     *
     * @return void
     */
    public function disableDeferPrevious()
    {
        if ( $this->checkFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_CALL_PREVIOUS ) )
        {
            $this->unsetFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_CALL_PREVIOUS );
        }
        if ( $this->checkFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_DEFER_PREVIOUS ) )
        {
            $this->unsetFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_DEFER_PREVIOUS );
        }
    }

    /**
     * <Error Handler Call Previous Error Handler Enable Method>
     * Turns on calls to the previous error handler during error handling.
     *
     * If enabled, errors will also be passed to the previous error handler if
     * one existed after all operations of this error handler resolve, and its
     * return response will also be considered in addition to the internal
     * responses as to whether the error was appropriately handled or not.
     * If this is disabled, the previous error handler will not run at all
     * unless the defer-previous switch is set to true.
     *
     * @return void
     */
    public function enableCallPrevious()
    {
        if ( $this->checkFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_DEFER_PREVIOUS ) )
        {
            $this->unsetFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_DEFER_PREVIOUS );
        }
        if ( !$this->checkFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_CALL_PREVIOUS ) )
        {
            $this->setFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_CALL_PREVIOUS );
        }
    }

    /**
     * <Error Handler Call Previous Error Handler Disable Method>
     * Turns off calls to the previous error handler during error handling.
     *
     * If enabled, errors will also be passed to the previous error handler if
     * one existed after all operations of this error handler resolve, and its
     * return response will also be considered in addition to the internal
     * responses as to whether the error was appropriately handled or not.
     * If this is disabled, the previous error handler will not run at all
     * unless the defer-previous switch is set to true.
     *
     * @return void
     */
    public function disableCallPrevious()
    {
        if ( $this->checkFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_CALL_PREVIOUS ) )
        {
            $this->unsetFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_CALL_PREVIOUS );
        }
        if ( $this->checkFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_DEFER_PREVIOUS ) )
        {
            $this->unsetFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_DEFER_PREVIOUS );
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * <Fatal Error Callback Registration Method>
     * Registers a callback to fire on fatal errors.
     *
     * The error handler will attempt to pass the results of the last error into
     * the registered method if it has parameters that will receive them.
     *
     * @param string $id A string index id to identify the error operation by.
     *     Stringable objects are acceptable.
     * @param callable $method Must be a callable method in the same
     *     class scope as the class registering the method.
     * @param int|string $severity (optional) If provided, the method will only fire
     *     when the given severity level occurs. The severity level must be
     *     either a string identifier of an error level, bitmask of error levels,
     *     or integer designating a specific error level. If the severity is
     *     omitted, the method will fire on all errors that register.
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the provided method is not callable, or does not exist in the
     *     current class scope. External methods may not be called.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If severity is provided and is not valid
     */
    protected function _registerFatalErrorMethod( $id, $method, $severity = null )
    {
        $this->_errorHandlerRegisterFatalHandlerMethod( $id, $method, $severity );
    }

    /**
     * <Error Handler Fatal Operation Unsetter Method>
     * Removes a fatal error operation from the index.
     * @param string $id A string index id to identify the error operation by.
     *     Stringable objects are acceptable.
     * @return void
     */
    protected function _unregisterFatalErrorMethod( $id )
    {
        \oroboros\validate\Validator::validate( 'stringable', $id, null, true );
        $id = (string) $id;
        foreach ( $this->_error_handler_fatal_error_operations as
            $level =>
            $operations )
        {
            if ( array_key_exists( $id, $operations ) )
            {
                unset( $this->_error_handler_fatal_error_operations[$level][$id] );
            }
        }
    }

    /**
     * <Runtime Error Callback Registration Method>
     * Registers a callback to fire on runtime errors.
     *
     * The error handler will attempt to pass the results of the last error into
     * the registered method if it has parameters that will receive them.
     *
     * @param string $id A string index id to identify the error operation by.
     *     Stringable objects are acceptable.
     * @param callable $method Must be a callable method in the same
     *     class scope as the class registering the method.
     * @param int|string $severity (optional) If provided, the method will only fire
     *     when the given severity level occurs. The severity level must be
     *     either a string identifier of an error level, bitmask of error levels,
     *     or integer designating a specific error level. If the severity is
     *     omitted, the method will fire on all errors that register.
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the provided method is not callable, or does not exist in the
     *     current class scope. External methods may not be called.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If severity is provided and is not valid
     */
    protected function _registerErrorMethod( $id, $method, $severity = null )
    {
        $this->_errorHandlerRegisterRuntimeHandlerMethod( $id, $method,
            $severity );
    }

    /**
     * <Error Handler Runtime Operation Unsetter Method>
     * Removes a runtime error operation from the index.
     * @param string $id A string index id to identify the error operation by.
     *     Stringable objects are acceptable.
     * @return void
     */
    protected function _unregisterErrorMethod( $id )
    {
        \oroboros\validate\Validator::validate( 'stringable', $id, null, true );
        $id = (string) $id;
        foreach ( $this->_error_handler_runtime_operations as
            $level =>
            $operations )
        {
            if ( array_key_exists( $id, $operations ) )
            {
                unset( $this->_error_handler_runtime_operations[$level][$id] );
            }
        }
    }

    /**
     * <Baseline Valid Flag Setter Method>
     * May be overridden to provide a validation array for provided flags.
     * If provided, any provided flags MUST exist in the given array of
     * valid values.
     *
     * The array should be a standard numerically keyed list of valid flags.
     *
     * If a non-empty validation array is given and any flags are passed
     * that do not exist in the validation array, an exception will be raised
     * that blocks initialization from resolving.
     *
     * @return array
     */
    protected function _baselineSetFlagsValid()
    {
        return array(
            \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_CALL_PREVIOUS,
            \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_DEFER_PREVIOUS,
            \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_ERROR_EXCEPTION,
            \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_LOG_UNHANDLED,
            \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_TRACK_STATISTICS,
        );
    }

    /**
     * <Baseline Default Flag Setter Method>
     * May be overridden to provide a set of default flags.
     * If provided, the given set will act as a baseline set of
     * templated flags, and public provided parameters will
     * be recursively appended given array, and ignored if they
     * duplicate provided flags.
     *
     * This allows for flags to be provided in a way
     * that appends the defaults rather than forcing
     * external usage to provide a full set.
     *
     * @return array
     */
    protected function _baselineSetFlagsDefault()
    {
        return array(
            \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_CALL_PREVIOUS,
            \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_LOG_UNHANDLED,
            \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_TRACK_STATISTICS,
        );
    }

    /**
     * <Error Handler Registered Operation Call Method>
     * Fires a registered error operation.
     * Both runtime and fatals use this method.
     *
     * If an exception is raised in this scope by a registered method,
     * the program will terminate immedately.
     *
     * If the registered method returns a value of -1, the program will
     * terminate after the rest of the registered operations have run,
     * if the operation is not part of the fatal error handler set.
     *
     * If the result returns true, the error will be marked as resolved.
     *
     * If the result returns false, the error will not be marked as resolved.
     *
     * Any other return result will evaluate to true, except null,
     * which will raise an error and terminate the program.
     *
     * @param callable $callable The callable method
     * @param type $args (optional) Contains the error arguments, if any are provided
     * @return bool|-1 Returns true if the error was resolved, false if it
     *     was not resolved, and -1 if the program needs to terminate
     */
    protected function _fireOperation( $callable, $args = null )
    {
        $result = $this->_callableUtilityCall( $callable, $args );
        return $result;
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * Performs the post-initialization setup for the error handler.
     * @return void
     * @internal
     */
    private function _errorHandlerPostInitialization()
    {
        $this->_errorHandlerCompileSeverity();
        $this->_errorHandlerCompileDefaultErrorReporting();
        $this->_errorHandlerCompileErrorReportIndex();
        $this->_errorHandlerCompileRuntimeHandleLevels();
        $this->_errorHandlerCompileFatalHandleLevels();
        register_shutdown_function( array(
            $this,
            'handleFatalError' ) );
    }

    /**
     * Performs the error handler registration logic.
     * @return void
     * @internal
     */
    private function _errorHandlerRegister()
    {
        if ( $this->_errorHandlerCheckRegistration() )
        {
            //Do not double register the error handler
            return;
        }
        $levels = $this->_error_handler_handled_levels;
        if ( PHP_VERSION_ID >= 70000 )
        {
            //PHP 7.2 has deprecated the last parameter, so this additional
            //method will suppress deprecation warnings.
            $previous_handler = set_error_handler(
                array(
                $this,
                'handleError72' ), $this->_errorHandlerGetErrorLevel()
            );
        } else
        {
            //Earlier versions of PHP can accept the last parameter,
            //though it is not used in internal Oroboros logic anyhow,
            //It is only made available for extension classes.
            $previous_handler = set_error_handler(
                array(
                $this,
                'handleError' ), $this->_errorHandlerGetErrorLevel()
            );
        }
        $this->_errorHandlerRegisterPrevious( $previous_handler );
    }

    /**
     * Performs the error handler unregistration logic.
     * @return void
     * @internal
     */
    private function _errorHandlerUnregister()
    {
        $this->_errorHandlerRegisterPrevious();
        restore_error_handler();
    }

    /**
     * Fires the runtime operations if the error handler is registered,
     * and if any exist for the given severity.
     *
     * If no operations are registered for the given severity level,
     * this method will not run any operations.
     *
     * Error operations should return true if they handled the error,
     * false if they did not handle the error, and -1 if the program
     * needs to exit.
     *
     * If [_error_handler_defer_previous] is not true and ANY registered
     * operation returns true, the error handler will mark the error
     * as resolved and suppress the error.
     *
     * If [_error_handler_defer_previous] is not true and ANY method returns -1,
     * the program will exit immediately.
     *
     * If [_error_handler_defer_previous] is not true and no method returns true,
     * the error handler will fall back to the PHP internal error handler.
     *
     * If the [_error_handler_defer_previous] switch is true, the error handler
     * will ignore all prior parameters and call the previous error handler
     * to handle the error.
     *
     * If the [_error_handler_defer_previous] switch is true, registered
     * operations will still fire, but their error response value will
     * be ignored. This allows statistical collection methods to still operate
     * cleanly without interruption, but prevents them from altering the error
     * handling process.
     *
     * If ANY operation throws an exception, it will be suppressed,
     * and the remaining queue will still fire.
     *
     * If the [_error_handler_defer_previous] switch is true and the previous
     * error handler throws an exception, the program will exit immediately.
     *
     * @param int $severity Contains the level of error raised as an integer.
     * @param string $message Contains the error message as a string.
     *     This parameter is always defined.
     * @param string $file (optional) Contains the name of the file
     *     where the error occurred as a string, or null if not defined.
     * @param int $line (optional) Contains the line that the error
     *     occurred at as an integer, or null if not defined.
     * @param array $context (optional) Contains an array of all variables
     *     that existed in the scope of where the method arose. This parameter
     *     was deprecated in PHP 7.2, and will always be null for versions later
     *     than that.
     * @return bool If true, the PHP internal error handler will fire.
     *     If false, the error will be considered to be handled correctly
     *     and the program will continue to operate.
     *
     * @return bool
     * @internal
     */
    private function _errorHandlerFireRuntimeOperations( $severity, $message,
        $file = null, $line = null, $context = null )
    {
        $result = false;
        $handled_by_previous = false;
        $key = array_search( $severity, self::$_error_handler_catchable_levels );
        $operation_handled = array();
        $opts = array(
            $severity,
            $message,
            $file,
            $line,
            $context
        );
        foreach ( $this->_error_handler_runtime_operations[$key] as
            $operation )
        {
            $operation_handled[] = $this->_fireOperation( $operation, $opts );
        }
        if ( !is_null( $this->_error_handler_previous )
            && ( $this->checkFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_DEFER_PREVIOUS ) ||
            $this->checkFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_CALL_PREVIOUS ) ) )
        {
            $previous = $this->_fireOperation( $this->_error_handler_previous,
                    $opts )
                ? true
                : false;
            if ( $this->checkFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_DEFER_PREVIOUS ) )
            {
                $handled_by_previous = true;
                $result = $previous;
            } else
            {
                $handled_by_previous = $previous;
                $operation_handled[] = $previous;
            }
        }
        if ( is_null( $this->_error_handler_previous )
            || !$this->checkFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_DEFER_PREVIOUS ) )
        {
            foreach ( $operation_handled as
                $designation )
            {
                if ( $designation === -1 )
                {
                    //This indicates that the program must terminate
                    die( $message );
                }
                if ( $designation && $designation !== -1 )
                {
                    $result = true;
                }
            }
        }
        if ( $this->checkFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_TRACK_STATISTICS ) )
        {
            //Add a statistical entry about the error handler outcome
            self::$_error_handler_error_statistics[get_called_class()][microtime( true )]
                = $this->_errorHandlerGenerateErrorReport( 'runtime', $severity,
                $message, $file, $line, $context, $result, $handled_by_previous,
                $this->_error_handler_previous );
            //This sometimes gets overwritten on fatal errors because
            //the fatal report somehow generates in the same microsecond,
            //so this should insure both get entered.
            usleep( 1 );
        }
        if ( !$result
            && $this->checkFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_LOG_UNHANDLED )
            && \oroboros\Oroboros::hasExtension( 'log' ) )
        {
            //Automatically log uncaught errors
            if ( \oroboros\validate\Validator::validate( 'any-of', $severity,
                    self::$_error_handler_fatal_errors ) )
            {
                $log_level = 'error';
                $log_message = 'Registered error handler {handler} failed to handle error generated at line {line} of file {file} with error message: {message}.';
            } elseif ( \oroboros\validate\Validator::validate( 'any-of',
                    $severity, self::$_error_handler_warning_errors ) )
            {
                $log_level = 'warning';
                $log_message = 'Registered error handler {handler} failed to handle warning generated at line {line} of file {file} with error message: {message}.';
            } else
            {
                $log_level = 'notice';
                $log_message = 'Registered error handler {handler} failed to handle notice generated at line {line} of file {file} with error message: {message}.';
            }
            \oroboros\Oroboros::log( $log_level, $log_message,
                array(
                'handler' => get_class( $this ),
                'line' => $line,
                'file' => $file,
                'message' => $message
            ) );
        }
        if ( !$result
            && $this->checkFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_ERROR_EXCEPTION ) )
        {
            //Recast error to exception
            throw self::_getException( 'error-exception',
                'An unhandled error occurred at line {line} of file {file} with message: {message}',
                \oroboros\core\interfaces\enumerated\exception\CoreExceptionCodes::ERROR_CORE_CORE_FAILURE,
                null,
                array(
                'line' => $line,
                'file' => $file,
                'message' => $message
            ) );
        }
        return $result;
    }

    /**
     * Fires the fatal error operations if the error handler is registered,
     * if there was an error, and if any exist.
     *
     * If no error occurred, this method will return without
     * running any operations.
     *
     * If no operations are registered to the given error level,
     * this method will return without running any operations.
     *
     * Any additional errors or uncaught exceptions that occur in
     * fatal error operations will exit the program immediately.
     * @return void
     * @internal
     * @codeCoverageIgnore
     */
    private function _errorHandlerFireFatalErrorOperations( $last )
    {
        $result = false;
        $key = array_search( $last['type'], self::$_error_handler_levels );
        $operation_handled = array();
        $opts = array(
            $last['type'],
            $last['message'],
            $last['file'],
            $last['line'],
            null
        );
        foreach ( $this->_error_handler_fatal_error_operations[$key] as
            $operation )
        {
            $operation_handled[] = $this->_fireOperation( $operation, $opts );
        }
        if ( $this->checkFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_TRACK_STATISTICS ) )
        {
            //Add a statistical entry about the fatal error handler outcome
            self::$_error_handler_error_statistics[get_called_class()][microtime( true )]
                = $this->_errorHandlerGenerateErrorReport( 'fatal',
                $last['type'], $last['message'], $last['file'], $last['line'],
                null, false, false, $this->_error_handler_previous );
        }
        if ( $this->checkFlag( \oroboros\core\interfaces\enumerated\flags\ErrorHandlerFlags::FLAG_ERROR_HANDLER_LOG_UNHANDLED )
            && \oroboros\Oroboros::hasExtension( 'log' ) )
        {
            //Automatically log uncaught fatal errors
            if ( \oroboros\validate\Validator::validate( 'any-of',
                    $last['type'], self::$_error_handler_fatal_errors ) )
            {
                $log_level = 'critical';
                $log_message = 'Fatal error detected in {handler} at line {line} of file {file} with error message: {message}.';
            } elseif ( \oroboros\validate\Validator::validate( 'any-of',
                    $last['type'], self::$_error_handler_warning_errors ) )
            {
                $log_level = 'warning';
                $log_message = 'Unhandled warning deteced on shutdown in {handler} at line {line} of file {file} with error message: {message}.';
            } else
            {
                $log_level = 'notice';
                $log_message = 'Unhandled notice detected on shutdown in {handler} at line {line} of file {file} with error message: {message}.';
            }
            \oroboros\Oroboros::log( $log_level, $log_message,
                array(
                'handler' => get_class( $this ),
                'line' => $last['line'],
                'file' => $last['file'],
                'message' => $last['message']
            ) );
        }
    }

    /**
     * Registers a method to fire on runtime errors handled
     * by the error handler.
     *
     * @param string $id A string index id to identify the error operation by.
     *     Stringable objects are acceptable.
     * @param string $callable A callable method in the current class scope
     * @param int|string $severity (optional) If provided, the method will
     *     be scoped to only fire on the given error severity. If not
     *     provided, the method will fire on all severity levels handled
     *     by the error handler. May be the canonicalized key of the
     *     error level (suggested),or the integer value of a PHP
     *     error level severity.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the provided method is not a valid callable method within
     *     the current class scope.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If a severity is provided that does not resolve to
     *     a valid PHP error level.
     * @internal
     */
    private function _errorHandlerRegisterRuntimeHandlerMethod( $id, $callable,
        $severity = null )
    {
        \oroboros\validate\Validator::validate( 'stringable', $id, null, true );
        \oroboros\validate\Validator::validate( 'any-of', $callable,
            get_class_methods( $this ), true );
        $id = (string) $id;
        if ( is_null( $severity ) )
        {
            foreach ( array_keys( self::$_error_handler_catchable_levels ) as
                $level )
            {
                $this->_errorHandlerRegisterRuntimeHandlerMethod( $id,
                    $callable, $level );
            }
        } else
        {
            $this->_error_handler_runtime_operations[$severity][(string) $id] = array(
                $this,
                $callable );
        }
    }

    /**
     * Registers a method to fire on runtime errors handled
     * by the error handler.
     *
     * @param string $id A string index id to identify the error operation by.
     *     Stringable objects are acceptable.
     * @param string $callable A callable method in the current class scope
     * @param int|string $severity (optional) If provided, the method will
     *     be scoped to only fire on the given error severity. If not
     *     provided, the method will fire on all severity levels handled
     *     by the error handler. May be the canonicalized key of the
     *     error level (suggested),or the integer value of a PHP
     *     error level severity.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the provided method is not a valid callable method within
     *     the current class scope.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If a severity is provided that does not resolve to
     *     a valid PHP error level.
     * @internal
     */
    private function _errorHandlerRegisterFatalHandlerMethod( $id, $callable,
        $severity = null )
    {
        \oroboros\validate\Validator::validate( 'stringable', $id, null, true );
        \oroboros\validate\Validator::validate( 'any-of', $callable,
            get_class_methods( $this ), true );
        $id = (string) $id;
        if ( is_null( $severity ) )
        {
            foreach ( array_keys( self::$_error_handler_levels ) as
                $level )
            {
                $this->_errorHandlerRegisterFatalHandlerMethod( $id, $callable,
                    $level );
            }
        } else
        {
            $this->_error_handler_fatal_error_operations[$severity][$id] = array(
                $this,
                $callable );
        }
    }

    /**
     * If a previous error handler callback is returned from the registration
     * method, this method will capture it and register it internally, so it
     * can still operate if desired as per the given settings.
     * @param null|callable $previous
     */
    private function _errorHandlerRegisterPrevious( $previous = null )
    {
        if ( is_null( $previous ) )
        {
            $this->_error_handler_previous = null;
            return;
        }
        \oroboros\validate\Validator::validate( 'callable', $previous, null,
            true );
        $this->_error_handler_previous = $previous;
    }

    /**
     * Performs the internal error handling logic.
     * @param int $severity Contains the level of error raised as an integer.
     * @param string $message Contains the error message as a string.
     *     This parameter is always defined.
     * @param string $file (optional) Contains the name of the file
     *     where the error occurred as a string, or null if not defined.
     * @param int $line (optional) Contains the line that the error
     *     occurred at as an integer, or null if not defined.
     * @param array $context (optional) Contains an array of all variables
     *     that existed in the scope of where the method arose. This parameter
     *     was deprecated in PHP 7.2, and will always be null for versions later
     *     than that.
     * @return bool If true, the PHP internal error handler will fire.
     *     If false, the error will be considered to be handled correctly
     *     and the program will continue to operate.
     * @internal
     */
    private function _errorHandlerHandleError( $severity, $message,
        $file = null, $line = null, $context = null )
    {
        $this->_errorHandlerUpdateVisibilityFromPhp();
        return $this->_errorHandlerFireRuntimeOperations( $severity, $message,
                $file, $line, $context );
    }

    /**
     * Performs the internal fatal error handling logic.
     * @return bool If true, the PHP internal error handler will fire.
     *     If false, the error will be considered to be handled correctly
     *     and the program will continue to operate.
     * @internal
     */
    private function _errorHandlerHandleFatal()
    {
        $last = error_get_last();
        if ( is_null( $last ) )
        {
            //No operation if no error.
            return;
        }
        $this->_errorHandlerUpdateVisibilityFromPhp();
        $this->_errorHandlerFireFatalErrorOperations( $last );
    }

    /**
     * Compiles the severity levels into a single array,
     * if this operation has not already been done.
     * @return void
     * @internal
     */
    private function _errorHandlerCompileSeverity()
    {
        if ( empty( self::$_error_handler_levels ) )
        {
            self::$_error_handler_levels = array_merge( self::$_error_handler_catchable_levels,
                self::$_error_handler_uncatchable_levels );
        }
    }

    /**
     * Compiles the severity levels into a single array,
     * if this operation has not already been done.
     * @return void
     * @internal
     */
    private function _errorHandlerCompileErrorReportIndex()
    {
        if ( !array_key_exists( get_called_class(),
                self::$_error_handler_error_statistics ) )
        {
            self::$_error_handler_error_statistics[get_called_class()] = array();
        }
    }

    /**
     * Compiles the default runtime error level operation indexes
     * if they are not already declared.
     * @return void
     * @internal
     */
    private function _errorHandlerCompileRuntimeHandleLevels()
    {
        if ( empty( $this->_error_handler_runtime_operations ) )
        {
            //Initialize the baseline runtime operation index
            foreach ( array_keys( self::$_error_handler_catchable_levels ) as
                $level )
            {
                $this->_error_handler_runtime_operations[$level] = array();
            }
        }
    }

    /**
     * Compiles the default fatal error level operation indexes
     * if they are not already declared.
     * @return void
     * @internal
     */
    private function _errorHandlerCompileFatalHandleLevels()
    {
        if ( empty( $this->_error_handler_fatal_error_operations ) )
        {
            //Initialize the baseline fatal operation index
            foreach ( array_keys( self::$_error_handler_levels ) as
                $level )
            {
                $this->_error_handler_fatal_error_operations[$level] = array();
            }
        }
    }

    /**
     * Compiles the default error reporting based on the existing
     * error reporting levels at initialization.
     * @return void
     * @internal
     */
    private function _errorHandlerCompileDefaultErrorReporting()
    {
        $existing = $this->_errorHandlerGetCurrentReporting();
        $this->_errorHandlerSetErrorLevelDefault( $existing );
        $this->_errorHandlerSetErrorLevel( $existing );
    }

    /**
     * Returns the current bitmask error level declared in the error handler.
     * @return int
     * @internal
     */
    private function _errorHandlerGetErrorLevel()
    {
        $this->_errorHandlerUpdateVisibilityFromPhp();
        return $this->_bitmaskUtilityGetValue();
    }

    /**
     * Gets the currently defined PHP error visibility setting.
     * @return int
     * @internal
     */
    private function _errorHandlerGetCurrentReporting()
    {
        return error_reporting();
    }

    /**
     * Sets the current error level to the provided value.
     * @param int $level
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the provided level is not an integer
     * @internal
     */
    private function _errorHandlerSetErrorLevel( $level )
    {
        \oroboros\validate\Validator::validate( 'type-of', $level, 'integer',
            true );
        $this->_bitmaskUtilityDeclare( $level );
        $this->_errorHandlerSetErrorVisibility();
    }

    /**
     * Sets the default error level. Calling _errorHandlerResetErrorLevel
     * will consequently reset to this level thereafter.
     * @param int $level
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the provided level is not an integer
     * @internal
     */
    private function _errorHandlerSetErrorLevelDefault( $level )
    {
        \oroboros\validate\Validator::validate( 'type-of', $level, 'integer',
            true );
        $this->_bitmaskUtilityDeclareDefault( $level );
    }

    /**
     * Adds a reporting level to existing error reporting.
     * @param int|string $level
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the provided level is not an integer
     * @internal
     */
    private function _errorHandlerAddErrorLevel( $level )
    {
        if ( \oroboros\validate\Validator::validate( 'any-of', $level,
                array_keys( self::$_error_handler_levels ) ) )
        {
            $this->_errorHandlerUpdateVisibilityFromPhp();
            $this->_bitmaskUtilitySet( self::$_error_handler_levels[$level] );
            $this->_errorHandlerSetErrorVisibility();
            return;
        }
        \oroboros\validate\Validator::validate( 'type-of', $level, 'integer',
            true );
        $this->_errorHandlerUpdateVisibilityFromPhp();
        $this->_bitmaskUtilitySet( $level );
        $this->_errorHandlerSetErrorVisibility();
    }

    /**
     * Removes a reporting level from existing error reporting.
     * @param int|string $level
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the provided level is not an integer
     * @internal
     */
    private function _errorHandlerRemoveErrorLevel( $level )
    {
        if ( \oroboros\validate\Validator::validate( 'any-of', $level,
                array_keys( self::$_error_handler_levels ) ) )
        {
            $this->_errorHandlerUpdateVisibilityFromPhp();
            $this->_bitmaskUtilityClear( self::$_error_handler_levels[$level] );
            $this->_errorHandlerSetErrorVisibility();
            return;
        }
        \oroboros\validate\Validator::validate( 'type-of', $level, 'integer',
            true );
        $this->_errorHandlerUpdateVisibilityFromPhp();
        $this->_bitmaskUtilityClear( $level );
        $this->_errorHandlerSetErrorVisibility();
    }

    /**
     * Resets the error level to the defined default value.
     * @return void
     * @internal
     */
    private function _errorHandlerResetErrorLevel()
    {
        $this->_bitmaskUtilityReset();
        $this->_errorHandlerSetErrorVisibility();
    }

    /**
     * Sets the error visibility to the defined value.
     *
     * This method fires whenever a visibility level is added or removed,
     * or when the level is set or reset.
     *
     * @return void
     * @internal
     */
    private function _errorHandlerSetErrorVisibility()
    {
        error_reporting( $this->_bitmaskUtilityGetValue() );
    }

    /**
     * Returns the current error level designations as an associative array.
     * @return array
     */
    private function _errorHandlerGetErrorVisibility()
    {
        $this->_errorHandlerUpdateVisibilityFromPhp();
        $levels = array();
        foreach ( self::$_error_handler_levels as
            $key =>
            $level )
        {
            $levels[$key] = $this->_bitmaskUtilityGet( $level );
        }
        return $levels;
    }

    /**
     * Updates the internal bitmask to reflect the real error level
     * currently defined in PHPs error reporting.
     *
     * This method fires on all getters so that external changes to
     * error reporting are reflected in return results.
     * @return void
     */
    private function _errorHandlerUpdateVisibilityFromPhp()
    {
        $this->_bitmaskUtilityDeclare( $this->_errorHandlerGetCurrentReporting() );
    }

    /**
     * Checks if the given bitmask contains the given level, is the given level,
     * or has the level associated with the given string key identifier.
     * This method can take a string of the canonicalized bitmask key,
     * the bitmask key itself, or the entire bitmask. Returns a boolean
     * determination as to whether the given criteria match the error level.
     * @param type $level
     * @return bool
     * @internal
     */
    private function _errorHandlerHasErrorVisibility( $level )
    {
        $this->_errorHandlerUpdateVisibilityFromPhp();
        if ( \oroboros\validate\Validator::validate( 'any-of', $level,
                array_keys( self::$_error_handler_levels ) ) )
        {
            return $this->_bitmaskUtilityGet( self::$_error_handler_levels[$level] );
        }
        if ( \oroboros\validate\Validator::validate( 'any-of', $level,
                self::$_error_handler_levels ) )
        {
            return $this->_bitmaskUtilityGet( $level );
        }
        return $this->_bitmaskUtility === $level;
    }

    /**
     * Returns a boolean determination as to whether the error handler
     * is currently registered as the PHP error handler.
     * @return bool
     * @internal
     */
    private function _errorHandlerCheckRegistration()
    {
        set_error_handler( $registered_handler = set_error_handler( 'var_dump' ) );
        if (
        //This logic always registers as an array
            is_array( $registered_handler )
            //This rules out static classes
            && is_object( $registered_handler[0] )
            //This rules out other error handler extensions,
            //and insures the same method set, so the
            //fingerprint check will not cause an error
            && get_class( $registered_handler[0] ) === get_class( $this )
            //This insures that the registered handler is the same instance as this one.
            //That way other error instances of this class do not interrupt the check
            //and cause false positives.
            //Only the exact same instance will have an identical object fingerprint.
            && $registered_handler[0]->getFingerprint() === $this->getFingerprint()
        )
        {
            return true;
        }
        //All other instances are false.
        return false;
    }

    /**
     * Generates an error statistic report based on a given error,
     * which is stored in the internal error stats for external
     * analysis if needed.
     * @param int $severity
     * @param string $message
     * @param string $file
     * @param int $line
     * @param array $context
     * @param bool $handled
     * @param bool $handled_by_previous
     * @param null|callable $previous
     * @return array
     * @internal
     */
    private function _errorHandlerGenerateErrorReport( $type = 'runtime',
        $severity = null, $message = null, $file = null, $line = null,
        $context = null, $handled = false, $handled_by_previous = false,
        $previous = null )
    {
        $report = array(
            'type' => $type,
            'identifier' => array_search( $severity,
                self::$_error_handler_levels ),
            'severity' => $severity,
            'message' => $message,
            'file' => $file,
            'line' => $line,
            'context' => $context,
            'handled' => $handled,
            'handled-by-previous' => $handled_by_previous,
            'previous' => $previous,
            'timezone' => date_default_timezone_get()
        );
        return $report;
    }

}
