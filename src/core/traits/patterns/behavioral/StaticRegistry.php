<?php
/**
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @copyright (c) 2014, Brian Dayhoff all rights reserved.
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace oroboros\core\traits\patterns\behavioral;

/**
 * <Static Registry Trait>
 * Provides a set of methods to create a complex protected internal registry of values.
 * This version of the registry pattern uses a static registry to insure accessibility across
 * a range of objects. This is appropriate for registering autoloader keys, and other values that
 * require consistent state across all instances. This trait should not be used when data needs to
 * be encapsulated to a single instance.
 * @version 0.1.1
 * @category traits
 * @category design patterns
 */
trait StaticRegistry {

    private static $_registry_values = [];

    protected static function _get($key, $subkey = null) {
        if (!is_string($key) || (isset($subkey) && !is_string($subkey) ) ) {
            throw new \oroboros\core\utilities\exception\Exception('Invalid parameters passed at ' . __LINE__ . ' of ' . __METHOD__, self::EXCEPTION_CODE_BAD_PARAMETERS);
        }
        if (!array_key_exists($key, self::$_registry_values)) {
            throw new \oroboros\core\utilities\exception\Exception('Registry value: ' . $key . ' is not defined at ' . __LINE__ . ' of ' . __METHOD__, self::EXCEPTION_CODE_BAD_PARAMETERS);
        }
        if (array_key_exists($key, self::$_registry_values) && isset($subkey) && ((is_array(self::$_registry_values[$key]) && !isset(self::$_registry_values[$key][$subkey])) || (is_object(self::$_registry_values[$key]) && !isset(self::$_registry_values[$key]->$subkey)))) {
            throw new \oroboros\core\utilities\exception\Exception('Registry subvalue: ' . $subkey . ' is not defined in registry key: ' . $key . ' at ' . __LINE__ . ' of ' . __METHOD__, self::EXCEPTION_CODE_BAD_PARAMETERS);
        }
        return (!isset($subkey) ? self::$_registry_values[$key] : (is_object(self::$_registry_values[$key]) ? self::$_registry_values[$key]->$subkey : self::$_registry_values[$key][$subkey]));
    }

    protected static function _register($key, $value, $subkey = null) {
        if (!is_string($key) || ((isset($subkey) && !is_string($subkey) ) ) ) {
            throw new \oroboros\core\utilities\exception\Exception('Invalid parameters passed at ' . __LINE__ . ' of ' . __METHOD__, self::EXCEPTION_CODE_BAD_PARAMETERS);
        }
        if (!isset($subkey)) {
            self::$_registry_values[$key] = $value;
        } else {
            if (is_array(self::$_registry_values[$key])) {
                self::$_registry_values[$key][$subkey] = $value;
            } elseif (is_object(self::$_registry_values[$key])) {
                self::$_registry_values[$key]->$subkey = $value;
            } else {
                throw new \oroboros\core\utilities\exception\Exception('Specified registry key: ' . $key . ' cannot accept subvalues because it is not an array or object at ' . __LINE__ . ' of ' . __METHOD__, self::_EXCEPTION_CODE_GENERAL_BAD_PARAMETERs);
            }

        }
    }

    protected static function _unregister($key, $subkey = null) {
        if (!is_string($key) || (isset($subkey) && !is_string($subkey) ) ) {
            throw new \oroboros\core\utilities\exception\Exception('Invalid parameters passed at ' . __LINE__ . ' of ' . __METHOD__, self::EXCEPTION_CODE_BAD_PARAMETERS);
        }
        if (isset(self::$_registry_values[$key]) && !isset($subkey)) {
            unset(self::$_registry_values[$key]);
            return true;
        } elseif (isset($subkey) && is_array(self::$_registry_values[$key]) && isset(self::$_registry_values[$key][$subkey])) {
            unset(self::$_registry_values[$key][$subkey]);
            return true;
        } elseif (isset($subkey) && is_object(self::$_registry_values[$key]) && isset(self::$_registry_values[$key]->$subkey)) {
            unset(self::$_registry_values[$key]->$subkey);
            return true;
        }
        return false;
    }

    protected static function _registryKeys($key = null) {
        if (isset($key) && !is_string($key)) {
            throw new \oroboros\core\utilities\exception\Exception('Invalid parameters passed at ' . __LINE__ . ' of ' . __METHOD__, self::EXCEPTION_CODE_BAD_PARAMETERS);
        }
        if (isset($key) && !isset(self::$_registry_values[$key])) {
            throw new \oroboros\core\utilities\exception\Exception('Specified key: ' . $key . ' does not exist in registry at ' . __LINE__ . ' of ' . __METHOD__);
        } elseif (isset($key) && isset(self::$_registry_values[$key]) && (!(is_array(self::$_registry_values[$key]) || is_object(self::$_registry_values[$key])))) {
            throw new \oroboros\core\utilities\exception\Exception('Specified key: ' . $key . ' is not an array or object, and keys cannot be returned at ' . __LINE__ . ' of ' . __METHOD__);
        } else {
            return array_keys(((isset($key)) ? ((is_object(self::$_registry_values[$key])) ? get_object_vars(self::$_registry_values[$key]) : self::$_registry_values[$key]) : self::$_registry_values));
        }
    }

    protected static function _check($key, $subkey = null) {
        if (!is_string($key) || (isset($subkey) && !is_string($subkey))) {
            throw new \oroboros\core\utilities\exception\Exception('Invalid parameters passed at ' . __LINE__ . ' of ' . __METHOD__, self::EXCEPTION_CODE_BAD_PARAMETERS);
        }
        return ((isset($subkey)) ? (array_key_exists($key, self::$_registry_values) && ((is_array(self::$_registry_values[$key])) ? array_key_exists($subkey, self::$_registry_values[$key]): ((is_object(self::$_registry_values[$key])) ? isset(self::$_registry_values[$key]->$subkey) : false ) )) : (array_key_exists($key, self::$_registry_values)));
    }
}
