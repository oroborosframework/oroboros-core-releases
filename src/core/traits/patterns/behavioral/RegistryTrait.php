<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2014-2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\traits\patterns\behavioral;

/**
 * <Private Registry Pattern>
 * Allows for private values to be declared at runtime,
 * and maintain scope with specific traits or classes,
 * despite inheritance.
 *
 * Provides a set of methods to abstract complex,
 * scoped private values into a simple dot separated
 * naming convention. Works with both arrays as well
 * as objects, respecting visibility.
 *
 * Array or object nesting is represented as a dot
 * separated string notation, consistent with other
 * languages namespacing such as javascript or MySQL.
 * This allows for broader universalization of schema
 * represenation without as much one-off parsing logic
 * as PHP's internal schema normally allows without
 * additional abstraction.
 *
 * Public facing api's in this system honor standard notation.
 * As such, this trait does not provide public methods, and you
 * will have to expose them manually if used for
 * interactive containerization.
 *
 * @planned This trait is set up to eventually also support tree search algorithms, though that is not yet implemented.
 * @planned The indexing functionality will eventually be separated from the container functionality into it's own trait, which will be used by this one. This should not change the protected or public api when this occurs.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage patterns
 * @version 0.2.4-alpha
 * @since 0.1.1
 */
trait RegistryTrait
{

    /**
     * We will get a reference to the initializing instance here,
     * so that we can scope separately between traits
     * and preserve value privacy.
     */
    use \oroboros\core\traits\utilities\logic\BackreferenceTrait;

    /**
     * Primary container for private registry encapsulation.
     * @var array
     */
    private $_registry_values = [
        ];

    /**
     * the default registry index separator.
     * This will be allowed to be overidden later,
     * but that will need a lot of testing.
     * @var string
     */
    private $_registry_default_index_separator = '.';

    /**
     * the registry index separator.
     * This will be allowed to be overidden later,
     * but that will need a lot of testing.
     * @var string
     */
    private $_registry_index_separator = '.';

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * <Registry Index Getter>
     * Returns the requested value, defined by dot separated depth notation
     * (works with both arrays and objects, so they can be treated interchangably
     * with by higher logic without mutating their state).
     *
     * Implementing classes are expected to know their key structure on request,
     * and to not ask for invalid keys. Operation will break with an exception
     * if invalid keys are requested. This is a control structure, so if you
     * need to handle ambiguity, either handle that locally, or containerize
     * it within a scope that tolerates ambiguity internally before registering it).
     *
     * @example 'classname.dependencies' resolves to $this->_registry_values['\namespace\of\local\scope\ClassOrTrait']['classname']['dependencies']
     *
     * @param scalar $key The name of the registry key, typically a string. May follow dot separated notation to declare depth.
     * @param bool $safe (optional) Prevents overwrites if true. Default false.
     * @return bool True if the value was set, false if the old value was retained
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if passed a non-scalar key
     * @version 0.2.4-alpha
     * @since 0.2.4-alpha
     */
    protected function _getRegistryValue( $key = null )
    {
        $value = $this->_getRegistryValueReference( $key );
        $non_referential_value = $value;
        return $non_referential_value;
    }

    /**
     * <Reset Registry>
     * Sets the localized registry back to an empty state.
     * @return void
     */
    protected function _resetRegistry()
    {
        $this->_registryInitialize( true );
    }

    /**
     * <Registry Fetch>
     * Returns the entire localized registry.
     * @return array
     */
    protected function _fetchRegistry()
    {
        $localized_index_key = $this->_registryExtractLocalizedIndex( __CLASS__ );
        $localized_index = $this->_registry_values[$localized_index_key];
        return $localized_index;
    }

    /**
     * <Registry Value Count>
     * Returns a count of the registry keys by dot separated scope.
     * If $key is null, returns the entire localized scope.
     * @param scalar $key a dot separated key to check the registry for
     * @return int|bool returns false if $key is not an array or does not exist, otherwise returns the count of values.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if a non-scalar key is passed and $key is not null
     */
    protected function _countRegistryKeys( $key = null )
    {
        $this->_registryInitialize();
        if ( !is_null( $key ) && !is_scalar( $key ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'scalar value', gettype( $key ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS );
        }

        $localized_index_key = $this->_registryExtractLocalizedIndex( __CLASS__ );
        $localized_index = $this->_registry_values[$localized_index_key];

        if ( is_null( $key ) )
        {
            return count( $localized_index );
        }

        $values = $this->_registryGetKeyMapping( $localized_index, $key, true );
        if ( is_array( $values ) )
        {
            return count( $values );
        }
        return false;
    }

    /**
     * <Registry Index By-Reference Getter>
     * Returns the specified value from the registry,
     * but returns a reference to the original instead of a copy.
     * Otherwise this method works identically to _getRegistryValue
     *
     * This method honors dot separated notation (the dot separator is overrideable)
     *
     * @example 'classname.dependencies' resolves to $this->_registry_values['\namespace\of\local\scope\ClassOrTrait']['classname']['dependencies']
     *
     * @param scalar $key The name of the registry key, typically a string. May follow dot separated notation to declare depth.
     * @param bool $safe (optional) Prevents overwrites if true. Default false.
     * @return bool True if the value was set, false if the old value was retained
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if passed a non-scalar key
     * @version 0.2.4-alpha
     * @since 0.2.4-alpha
     */
    protected function &_getRegistryValueReference( $key = null )
    {
        $this->_registryInitialize();
        if ( !is_null( $key ) && !is_scalar( $key ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'scalar value', gettype( $key ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS );
        }

        $localized_index_key = $this->_registryExtractLocalizedIndex( __CLASS__ );
        $localized_index = $this->_registry_values[$localized_index_key];
        if ( array_key_exists( $key, $localized_index ) )
        {
            return $localized_index[$key];
        } elseif ( is_null( $key ) )
        {
            return $localized_index;
        }
        return $this->_registryGetKeyMapping( $localized_index, $key, true );
    }

    /**
     * <Registry Index Check Method>
     * Returns a boolean determination as to whether a specified
     * dot separated key exists in the registry for the current scope.
     * @param scalar $key
     * @return bool
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     */
    protected function _checkRegistryKey( $key )
    {
        $this->_registryInitialize();
        if ( !is_null( $key ) && !is_scalar( $key ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'scalar value', gettype( $key ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS );
        }

        $localized_index_key = $this->_registryExtractLocalizedIndex( __CLASS__ );
        $localized_index = $this->_registry_values[$localized_index_key];

        return $this->_registryCheckKeyMapping( $localized_index, $key );
    }

    /**
     * <Registry Index Setter>
     * Creates a new key in the registry index
     * with an empty container as a value.
     *
     * Honors dot separated notation, and will create
     * all non-existent parent keys leading up to the
     * leaf-node key also if they do not exist.
     *
     * If $safe is true, it will not overwrite an
     * existing one of the same key.
     *
     * The default behavior is to overwrite, just like
     * any other variable being directly manipulated.
     *
     * @param scalar $key The name of the registry key, typically a string. May follow dot separated notation to declare depth.
     * @param bool $safe (optional) Prevents overwrites if true. Default false.
     * @return bool True if the value was set, false if the old value was retained
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if passed a non-scalar key
     * @version 0.2.4-alpha
     * @since 0.2.4-alpha
     */
    protected function _setRegistryValue( $key, $value = array(), $safe = false )
    {
        if ( !is_scalar( $key ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'scalar value', gettype( $key ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS );
        }
        $this->_registryInitialize();
        $localized_index_key = $this->_registryExtractLocalizedIndex( __CLASS__ );

        //exclude existing values if safe flag is passed
        if ( !( $safe && $this->_registryCheckKeyMapping( $this->_registry_values[$localized_index_key],
                $key ) ) )
        {
            $this->_registry_values[$localized_index_key] = $this->_registrySetKeyMapping( $this->_registry_values[$localized_index_key],
                $key, $value );
            return true;
        }
        return false;
    }

    /**
     * <Registry Index Unsettter Method>
     * Deletes a registry key by dot separated key name.
     * @param type $key
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if a non-scalar key is passed
     * @version 0.2.4-alpha
     * @since 0.2.4-alpha
     */
    protected function _unsetRegistryIndex( $key )
    {
        if ( !is_scalar( $key ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'scalar value', gettype( $key ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS );
        }
        $this->_registryInitialize();
        $localized_index_key = $this->_registryExtractLocalizedIndex( __CLASS__ );

        $this->_registry_values[$localized_index_key] = $this->_registryDeleteKeyMapping( $this->_registry_values[$localized_index_key],
            $key );
    }

    /**
     * <Registry Index Separator Getter Method>
     * Returns the current registry index separator.
     * @return string
     * @version 0.2.4-alpha
     * @since 0.2.4-alpha
     */
    protected function _getRegistryIndexSeparator()
    {
        return $this->_registry_index_separator;
    }

    /**
     * <Registry Index Separator Resetter Method>
     * Resets the recognized registry index separator to it's default ["."].
     * @return void
     * @version 0.2.4-alpha
     * @since 0.2.4-alpha
     */
    protected function _resetRegistryIndexSeparator()
    {
        $this->_registry_index_separator = $this->_registry_default_index_separator;
    }

    /**
     * <Registry Index Separator Setter Method>
     * Sets a new registry index separator value, which can then be used to
     * determine index depth in place of the default dot syntax.
     * This is provided for compatibility with programming logic
     * that is not inherently compatible with the dot syntax,
     * so that it may still work interchangeably with the registry.
     * @example If you want to represent separation within the system $PATH variable, change the separator to a colon [":"], and then your registry functionality can operate equivalently to the system path notation.
     * @param string $separator
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the supplied value is not a string
     * @version 0.2.4-alpha
     * @since 0.2.4-alpha
     */
    protected function _setRegistryIndexSeparator( $separator )
    {
        //type check for string
        if ( !is_string( $separator ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'string', gettype( $separator ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS );
        }
        $this->_registry_index_separator = $separator;
    }

    /**
     * <Registry Tree Getter Method>
     * Returns a flat, indexed array of all nested
     * multidimensional subkeys represented as dot
     * separated paths.
     * @return array
     * @version 0.2.5
     * @since 0.2.5
     */
    protected function _getRegistryTree()
    {
        return $this->_registryGetTree();
    }

    /**
     * <Registry Tree Search Method>
     * Searches the registry tree for a given key segment.
     * @param scalar $segment
     * @return array
     * @version 0.2.5
     * @since 0.2.5
     */
    protected function _treeSearchKey( $segment )
    {
        return $this->_registryTreeSearchKey( $segment );
    }

    /**
     * <Registry Value Tree Search Method>
     * Searches the registry tree for a given value.
     * @param mixed $value
     * @return array
     * @version 0.2.5
     * @since 0.2.5
     */
    protected function _treeSearchValue( $value )
    {
        return $this->_registryTreeSearchValue( $value );
    }

    /**
     * <Registry Leaf Node Getter Method>
     * Returns an array of all leaf nodes of the registry.
     * @return array
     * @version 0.2.5
     * @since 0.2.5
     */
    protected function _getRegistryLeaves()
    {
        return $this->_registryGetLeafNodes();
    }

    /**
     * <Registry Leaf Node Filter Method>
     * Returns an array of all leaf nodes with keys that
     * match the specified segment.
     * @param scalar $segment
     * @return array
     * @version 0.2.5
     * @since 0.2.5
     */
    protected function _searchRegistryLeaves( $segment )
    {
        _registryFilterLeafNodes( $segment );
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * This must be called to initialize an instance of the registry at
     * least one time, so all protected methods in this trait do so.
     * The root registry index will be scoped to the fully qualified
     * trait or class name that called registration.
     * It will only distribute values in scope, maintaining privacy,
     * and leaving it to the referencing construct to expose those values
     * as needed.
     *
     * This method may be called redundantly without mutating state.
     *
     * @return void
     * @version 0.2.4-alpha
     * @since 0.2.4-alpha
     */
    private function _registryInitialize( $reset = false )
    {
        $root_index = $this->_registryExtractLocalizedIndex( __CLASS__ );
        if ( !array_key_exists( $root_index, $this->_registry_values ) || $reset )
        {
            $this->_registry_values[$root_index] = array();
        }
    }

    /**
     * Returns the root localized key reference from a supplied fully qualified method name
     * @param string $backreference fully qualified method name (eg: \namespace\of\reference\ClassOrTraitName::methodName)
     * @return string Returns the fully namespaced reference, without the function name
     * @version 0.2.4-alpha
     * @since 0.2.4-alpha
     */
    private function _registryExtractLocalizedIndex( $backreference )
    {
        //chop off the method name, so we can treat this as a private class resource
        $tmp = explode( '::', $backreference );
        return array_shift( $tmp );
    }

    /**
     * Returns whether a specified index exists or not,
     * honoring dot separated index notation.
     * @param type $index
     * @param type $key
     * @return type
     * @version 0.2.4-alpha
     * @since 0.2.4-alpha
     */
    private function _registryResolveKeyMapping( $index, $key )
    {
        $stack = explode( $this->_registry_index_separator, $key );
        $next_key = array_shift( $stack );
        if ( empty( $stack ) )
        {
            //resolve the index
            return (array_key_exists( $next_key, $index )
                ? $index[$next_key]
                : false );
        }
        return $this->_registryResolveKeyMapping( $index[$next_key],
                implode( $this->_registry_index_separator, $stack ) );
    }

    /**
     * Returns a determination as to whether or
     * not a key exists in any provided subset, as defined in its dot separated notation
     * (or literal representation, if no dos separation occurs)
     * @recursive
     * @param type $index The array or object to begin the search against.
     * @param type $key the key or key tree to isolate
     * @return bool true if found, false if not
     * @throws \oroboros\core\utilities\exception\LogicException as a redundancy if some specific edge case was not considered. This means the core code needs to be patched if this occurs, and otherwise should never occur.
     * @version 0.2.4-alpha
     * @since 0.2.4-alpha
     */
    private function _registryCheckKeyMapping( $index, $key )
    {
        $stack = ( ( strpos( $key, $this->_registry_index_separator ) !== false )
            ? explode( $this->_registry_index_separator, $key )
            : array(
            $key ) );
        if ( count( $stack ) === 1 )
        {
            return array_key_exists( $key, $index );
        } else
        {
            $next_key = array_shift( $stack );
            if ( array_key_exists( $next_key, $index ) )
            {
                return $this->_registryCheckKeyMapping( $index[$next_key],
                        implode( $this->_registry_index_separator, $stack ) );
            }
            return false;
        }
    }

    /**
     * Returns the specified key from the registry,
     * honoring dot separated index depth notation.
     * @param array|object $index the specific index of the registry to start searching within
     * @param scalar $key the key to identify the value by
     * @return mixed a reference to the value within the registry corresponding to the key. Higher visibility methods are responsible for conversion if a return reference is not desireable.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if the key does not exist in the registry
     * @version 0.2.4-alpha
     * @since 0.2.4-alpha
     */
    private function &_registryGetKeyMapping( $index, $key = null )
    {
        if ( is_null( $key ) )
        {
            return $index;
        }
        if ( !$this->_registryCheckKeyMapping( $index, $key ) )
        {
            //this means there is no value to get
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_PHP_KEY_NOT_FOUND_MESSAGE,
                $key, __TRAIT__ ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_PHP_KEY_NOT_FOUND
            );
        }
        if ( strpos( $key, $this->_registry_index_separator ) )
        {
            $stack = explode( $this->_registry_index_separator, $key );
            $next_key = array_shift( $stack );
            return $this->_registryGetKeyMapping( $index[$next_key],
                    implode( $this->_registry_index_separator, $stack ) );
        }
        //find the correct index and return the value
        return $index[$key];
    }

    /**
     * Sets a value in the registry by dot notated index reference.
     * @param array|object $index A specific index of the registry to match within
     * @param scalar $key a key identifier for the value
     * @param mixed $value the value to set
     * @param bool $safe (optional) if true, will not overwrite existing keys. Default false.
     * @return bool if true, the value was set. If false, safe mode was declared and the key already existed, or you tried to set a value in a leaf that can't take one (like a string or integer).
     * @version 0.2.4-alpha
     * @since 0.2.4-alpha
     */
    private function _registrySetKeyMapping( &$index, $key, $value = array() )
    {
        $stack = explode( $this->_registry_index_separator, $key );
        $next_key = array_shift( $stack );
        //Create the value directly if it does not exist, and there are no subkeys
        if ( empty( $stack ) )
        {
            $index[$next_key] = $value;
            return $index;
        }
        $remaining_keys = implode( $this->_registry_index_separator, $stack );
        $index[$next_key] = $this->_registrySetKeyMapping( $index[$next_key],
            $remaining_keys, $value );
        return $index;
    }

    /**
     * Deletes a key from the registry
     * @param type $index
     * @param type $key
     * @return type
     * @internal
     * @version 0.2.4-alpha
     * @since 0.2.4-alpha
     */
    private function _registryDeleteKeyMapping( &$index, $key )
    {
        if ( !$this->_registryCheckKeyMapping( $index, $key ) )
        {
            //this means there is no value to delete
            return;
        }
        //find and delete the value
        $stack = explode( $this->_registry_index_separator, $key );
        $next_key = array_shift( $stack );
        if ( empty( $stack ) )
        {
            unset( $index[$next_key] );
        } else
        {
            return $this->_registryDeleteKeyMapping( $index[$next_key],
                    implode( $this->_registry_index_separator, $stack ) );
        }
        return $index;
    }

    /**
     * Builds the subtree dot separated notation as a flat
     * indexed array from the scoped registry index.
     * @return array
     * @internal
     * @version 0.2.5
     * @since 0.2.5
     */
    private function _registryGetTree()
    {
        $keys = array();
        foreach ( $this->_fetchRegistry() as
            $key =>
            $value )
        {
            $this->_registryRecurseSubtree( $key, $value, $keys );
        }
        return $keys;
    }

    /**
     * Builds the subtree dot separated notation as a flat indexed array.
     * @param string|int $index The array key
     * @param mixed $subtree If an array, will build out a dot separated index of all nested keys
     * @param array $tree The working flat tree set
     * @return array
     * @internal
     * @version 0.2.5
     * @since 0.2.5
     */
    private function _registryRecurseSubtree( $index, $subtree, &$tree )
    {
        if ( !is_array( $subtree ) )
        {
            $tree[] = $index;
            return $index;
        } else
        {
            foreach ( $subtree as
                $key =>
                $value )
            {
                $this->_registryRecurseSubtree( $index . $this->_registry_index_separator . $key,
                    $value, $tree );
            }
        }
    }

    /**
     * Performs a tree search for a specified key segment or set of segments
     * and returns an associative array of the matches where the key is the
     * dot separated path, and the value is the leaf value.
     * @param type $segment
     * @return array
     * @internal
     * @version 0.2.5
     * @since 0.2.5
     */
    private function _registryTreeSearchKey( $segment )
    {
        $matches = array();
        foreach ( $this->_registryGetTree() as
            $leaf )
        {
            if ( strpos( $leaf, (string) $segment ) !== false )
            {
                $matches[] = $leaf;
            }
        }
        return $matches;
    }

    private function _registryTreeSearchValue( $value )
    {
        if ( !is_array( $value ) )
        {

        }
    }

    /**
     * Returns all leaf nodes of the registry as a flat associative array,
     * where the key is the dot separated path name, and the value is
     * the leaf value.
     * @return array
     * @internal
     * @version 0.2.5
     * @since 0.2.5
     */
    private function _registryGetLeafNodes()
    {
        $leaves = array();
        $reg = $this->_fetchRegistry();
        if ( empty( $reg ) )
        {
            return $reg;
        }
        foreach ( $this->_registryGetTree() as
            $key )
        {
            $value = $reg;
            $stack = explode( $this->_registry_index_separator, $key );
            do
            {
                $next_key = array_shift( $stack );
                $value = $value[$next_key];
            } while ( !empty( $stack ) );
            $leaves[$key] = $value;
        }
        return $leaves;
    }

    /**
     * Filters leaf nodes by a specified key segment, and returns an
     * associative array of matches, where the key is the dot separated
     * registry path, and the value is the registry value.
     * @param type $segment
     * @return array
     * @internal
     * @version 0.2.5
     * @since 0.2.5
     */
    private function _registryFilterLeafNodes( $segment )
    {
        $leaves = $this->_registryGetLeafNodes();
        $valid_leaves = array();
        foreach ( $leaves as
            $key =>
            $value )
        {
            if ( strpos( $key, $segment ) !== false )
            {
                $valid_leaves[$key] = $value;
            }
        }
        return $valid_leaves;
    }

}
