<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\traits\patterns\behavioral;

/**
 * <Director Pattern>
 * Directors retain a subset of worker objects that handle similar work with a
 * uniform api and differing internals. The director is tasked with knowing
 * which worker to assign to any given instance of work, but does not need to
 * know anything about how the work is actually done. The worker must adhere
 * to a common interface amongst all similar workers in the same subset, but
 * its internal mechanism for performing the work can differ wildly, provided
 * it is capable of accepting expected parameters and producing the expected
 * result.
 *
 * This separation eases the extension of additional cases, as the only thing
 * that needs to happen to create additional functionality in the same scope
 * is to add another worker and notify the director when to pass to that one.
 * A good example of this would be a director that handles parsers. The addition
 * of a new parser means that the director only has to know what file extension
 * corresponds to the worker, and the worker can handle the rest of the parsing
 * or casting task with no additional refactoring elsewhere.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 * 
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @category patterns
 * @package oroboros/core
 * @subpackage patterns
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\core\interfaces\contract\core\BaselineContract
 * @satisfies \oroboros\core\interfaces\contract\patterns\behavioral\WorkerContract
 * @satisfies \oroboros\core\interfaces\contract\patterns\behavioral\DirectorContract
 */
trait DirectorTrait
{

    /**
     * Directors are also Workers
     */
    use WorkerTrait;

    /**
     * Represents the pool of available worker objects.
     * @var array
     */
    private $_director_workers = array();

    /**
     * Represents the category of work the Director is expected to cover.
     * Workers must match this category in order to be considered valid.
     * @var string
     */
    private $_director_category;

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\core\interfaces\contract\patterns\behavioral\DirectorContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Director Category Getter>
     * This determines the category of work that the Director expects
     * workers to adhere to. As Directors are also Workers, this may
     * not be the same category as their Worker category. If a Director
     * has no director category, it will refuse all workers passed to
     * it and be unable to delegate anything.
     * @return string|bool Returns false if the director category was not set,
     *     otherwise returns the director category.
     */
    public function getDirectorCategory()
    {
        if ( is_null( $this->_director_category ) )
        {
            return false;
        }
        return $this->_director_category;
    }

    /**
     * <Director Available Worker Scope Getter>
     * This will return an array of all of the scopes within the category
     * that the Director has a worker to delegate work to.
     * @return array
     */
    public function getWorkerScopes()
    {
        return array_keys( $this->_director_workers );
    }

    /**
     * <Director Worker Scope Check>
     * This will return a boolean determination as to whether a
     * given scope is covered by an already validated worker.
     * @param string $scope
     * @return bool
     */
    public function checkWorkerScope( $scope )
    {
        return in_array( $scope, $this->getWorkerScopes() );
    }

    /**
     * <Worker Setter>
     * This is the dependency injection method for Worker objects.
     * This method will allow the Director to receive a Worker,
     * and will check if the worker category is congruent with
     * the director category, and that the worker has a scope
     * that it can fulfill. The Director does not care what the
     * Workers scope is, only that it has one.
     *
     * Workers lacking either of these will be rejected and cause
     * the method to return false. Workers that fulfill these will
     * be retained and will cause the method to return true.
     *
     * Workers provided with a scope that matches an existing scope MUST
     * replace the existing worker.
     *
     * @param \oroboros\core\interfaces\contract\patterns\behavioral\WorkerContract $worker
     * @return bool
     */
    public function setWorker( \oroboros\core\interfaces\contract\patterns\behavioral\WorkerContract $worker )
    {
        return $this->_directorRegisterWorker( $worker );
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * Registers a new that has passed validation into the pool.
     * @param \oroboros\core\interfaces\contract\patterns\behavioral\WorkerContract $worker
     * @return bool
     */
    protected function _directorRegisterWorker( \oroboros\core\interfaces\contract\patterns\behavioral\WorkerContract $worker )
    {
        if ( !$worker->getWorkerCategory() || $worker->getWorkerCategory() !== $this->getDirectorCategory() ||
            !$worker->getWorkerScope() )
        {
            return false;
        }
        $this->_director_workers[$worker->getWorkerScope()] = $worker;
        return true;
    }

    /**
     * <Director Category Setter>
     * Internally used to set the category for the director.
     * This should be called in the constructor or initialization method
     * of implementing classes or traits.
     * @param string $category
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If a value is passed that is not a string
     */
    protected function _setDirectorCategory( $category )
    {
        if ( !is_string( $category ) && $category !== false )
        {
            throw $this->_getException( 'invalid-argument-exception',
                'logic-bad-parameters',
                array(
                'string|false',
                gettype( $category ) ) );
        }
        $this->_director_category = $category;
    }

}
