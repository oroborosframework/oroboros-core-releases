<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\traits\patterns\structural;

/**
 * <Control Api Extension Trait>
 * Provides an easy means of creating extensions for static control apis. Static
 * control api extensions inject additional functionality into a control api
 * through its extend method. All static control api extensions are also static
 * control apis.
 *
 * To use this trait the simplest way, attach it to your class,
 * and then set a static class property called [$extension_api].
 * This should be an associative array, with the first key designating
 * the api index, and a string or array of method names that should be
 * entered into that index key. The methods should follow the standard naming
 * convention for StaticControlApi's, where the method is prefixed by the index
 * key associated with it. Do not populate this array with closures or callables
 * or they will raise an exception when initialization is called.
 *
 * If you need more functionality than this, you
 * should override the initialize() method.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage patterns
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\core\interfaces\contract\patterns\structural\StaticControlApiContract
 * @satisfies \oroboros\core\interfaces\contract\libraries\patterns\structural\StaticControlApiExtensionContract
 */
trait StaticControlApiExtensionTrait
{

    use StaticControlApiTrait
    {
        StaticControlApiTrait::staticInitialize as private sca_staticInitialize;
    }
    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\core\interfaces\contract\patterns\structural\StaticControlApiContract
     * @satisfies \oroboros\core\interfaces\contract\patterns\structural\StaticControlApiExtensionContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Static Control Api Initialization>
     * This method will be called during the
     * extension process in place of a constructor.
     *
     * This method must always fire clean, and the full
     * api must be available when it has resolved.
     *
     * Calling this method again should not produce a different result,
     * and should not be blocked by internal logic or bypassed if called
     * a second time.
     *
     * This method MUST NOT be made into a Singleton.
     *
     * @return void
     */
    public static function staticInitialize( $params = null,
        $dependencies = null, $flags = null )
    {
        $class = get_called_class();
        if ( property_exists( $class, 'extension_api' ) )
        {
            foreach ( $class::$extension_api as
                $index )
            {
                try
                {
                    self::_registerStaticControlApiIndex( $index );
                } catch ( \Exception $e )
                {
                    //no-op
                }
            }
        }
        self::sca_staticInitialize( $params, $dependencies, $flags );
    }

}
