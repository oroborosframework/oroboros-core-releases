<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\traits\patterns\structural;

/**
 * <Decorator Trait>
 * Provides an easy means of creating decorators. Decorators are objects
 * that wrap decoratees and add additional functionality to them.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage patterns
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\core\interfaces\contract\libraries\patterns\structural\DecoratorContract
 */
trait DecoratorTrait
{

    use \oroboros\core\traits\utilities\core\CallableUtilityTrait;

    /**
     * Represents the decoratee object being wrapped by this object.
     * @var \oroboros\core\interfaces\contract\patterns\structural\DecorateeContract
     */
    private $_decoratee;

    /**
     * Represents whether the decorator is initialized.
     * @var bool
     */
    private $_decorator_initialized = false;

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies ...
     *
     * @execution Default Execution Plan (minimal)
     *
     * @execution Default Execution Plan (commented)
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Decorator Constructor>
     * The decorator constructor must receive the object it is intended to decorate.
     * If that object is capable of being decorated, it must implement the
     * Decoratee object.
     *
     * @param \oroboros\core\interfaces\contract\patterns\structural\Decoratee $object
     */
    public function __construct( \oroboros\core\interfaces\contract\patterns\structural\Decoratee $object )
    {
        $this->_initializeDecorator( $object );
    }

    /**
     * <Decorator call method>
     * This method represents the passthrough to methods provided by the decoratee.
     * If this instance does not have a method called, then it MUST use this
     * method to check if the parent class has it and return the results of that
     * unmodified.
     *
     * This MUST be implemented in such a way that the parameters are
     * called as expected in the decoratee.
     *
     * This MUST return the result from the decoratee unmodified.
     *
     * This MUST allow exeptions to bubble up unhandled.
     *
     * @param string $name
     * @param args $args
     * @return mixed
     * @throws \BadMethodCallException if a method is called that does not exist in the decoratee scope.
     * @throws \Exception any exceptions resulting from the decoratee are to be left unhandled in this scope.
     * @return mixed
     */
    public function __call( $name, $args = array() )
    {
        if ( method_exists( $this->_decoratee, $name ) || method_exists( $this->_decoratee,
                '__call' ) )
        {
            return $this->_callDecorateeMethod( $name, $args );
        }
        throw new \oroboros\core\utilities\exception\BadMethodCallException(
        sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_PHP_BAD_METHOD_CALL_MESSAGE,
            $name, get_class( $this ) ),
        \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_PHP_BAD_METHOD_CALL
        );
    }

    /**
     * <Decorator call static method>
     * This method represents the passthrough to static methods provided by the decoratee.
     * If this instance does not have a static method, then it MUST use this
     * method to check if the parent class has it and return the results of that
     * unmodified.
     *
     * This MUST be implemented in such a way that the parameters are
     * called as expected in the decoratee.
     *
     * This MUST return the result from the decoratee unmodified.
     *
     * This MUST allow exeptions to bubble up unhandled.
     *
     * @param string $name
     * @param args $args
     * @return mixed
     * @throws \MethodNotFoundException if a method is called that does not exist in the decoratee scope.
     * @throws \Exception any exceptions resulting from the decoratee are to be left unhandled in this scope.
     */
    public static function __callStatic( $name, $args = array() )
    {
        if ( method_exists( $this->_decoratee, $name ) || method_exists( $this->_decoratee,
                '__callStatic' ) )
        {
            return $this->_callDecorateeStaticMethod( $name, $args );
        }
        throw new \oroboros\core\utilities\exception\BadMethodCallException(
        sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_PHP_BAD_METHOD_CALL_MESSAGE,
            $name, get_class( $this ) ),
        \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_PHP_BAD_METHOD_CALL
        );
    }

    /**
     * <Decorator getter method>
     * This method represents the passthrough to decoratee class properties.
     * If this instance does not have a property or a paradigm to get a property,
     * then it MUST use this method to check if the decoratee has the property.
     *
     * This MUST return the result from the decoratee unmodified.
     *
     * This MUST allow errors or exceptions to bubble up unhandled.
     *
     * @param string $name
     * @return mixed
     */
    public function __get( $name )
    {
        if ( property_exists( $this->_decoratee, $name ) || method_exists( $this->_decoratee,
                '__get' ) )
        {
            return $this->_decoratee->$name;
        }
        throw new \oroboros\core\utilities\exception\BadMethodCallException(
        sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_PHP_GETTER_FAILURE_MESSAGE,
            get_class( $this ) ),
        \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_PHP_GETTER_FAILURE
        );
    }

    /**
     * <Decorator setter method>
     * This method represents the passthrough to the decoratee setter (if one exists).
     * If this instance does not have a property or a paradigm to set a property,
     * then it MUST use this method to attempt to set the property on the decoratee.
     *
     * This MUST be implemented in such a way where the property is set in the
     * decoratee EXACTLY as passed.
     *
     * This MUST allow errors or exceptions to bubble up unhandled.
     *
     * @param type $name
     * @param type $value
     */
    public function __set( $name, $value )
    {
        $this->_decoratee->$name = $value;
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * Performs the correct instantiation of the decorator.
     * @param \oroboros\core\interfaces\contract\patterns\structural\Decoratee $decoratee
     * @throws \oroboros\core\utilities\exception\RuntimeException if called again after already initialized
     */
    protected function _initializeDecorator( \oroboros\core\interfaces\contract\patterns\structural\Decoratee $decoratee )
    {
        if ( $this->_decorator_initialized )
        {
            throw new \oroboros\core\utilities\exception\RuntimeException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_PATTERN_FAILURE_MESSAGE,
                get_class( $this ),
                'Decorators may not be reinitialized after instantiation.' ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_PATTERN_FAILURE_DECORATOR
            );
        }
        $this->_decoratee = $decoratee;
        $this->_decorator_initialized = true;
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * Calls a method in a decoratee. Passes arguments as they would be received normally.
     * This will call up to 14 arguments directly for performance reasons.
     * More than that will utilize call_user_func_array.
     * @param string $name
     * @param array $args
     * @return mixed
     */
    private function _callDecorateeMethod( $name, $args = array() )
    {
        return _callableUtilityCall( array(
            $this->_decoratee,
            $name ), $args );
    }

    /**
     * Calls a method statically in a decoratee. Passes arguments as they would be received normally.
     * @param string $name
     * @param array $args
     */
    private function _callDecorateeStaticMethod( $name, $args = array() )
    {
        return _callableUtilityCall( array(
            $this->_decoratee,
            $name ), $args );
    }

}
