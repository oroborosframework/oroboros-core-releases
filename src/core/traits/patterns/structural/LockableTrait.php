<?php

/**
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @copyright (c) 2014, Brian Dayhoff all rights reserved.
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace oroboros\core\traits\patterns\structural;

/**
 * <Lockable Pattern Trait>
 * Provides a basic pattern for making objects only
 * accessible to other objects based on specified criteria.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 * 
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage patterns
 * @version 0.1.1
 * @version 0.1.1 alpha
 */
trait LockableTrait {

    private $_accessible = [];
    private $_keys = [];
    private $_tokens = [];
    private $_namespaces = [];
    private $_functions = [];
    private $_methods = [];
    private $_locks = [
        'read' => false,
        'write' => false,
        'execute' => false,
        'all' => false,
        'conditions' => false,
    ];
    private $_conditions = [
        'read' => [],
        'write' => [],
        'execute' => [],
        'all' => [],
    ];
    private $_conditions_valid = [
        'owner',
        'interface',
        'namespace',
        'key',
        'token',
        'function',
        'methods',
        'extends',
    ];

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    protected function _lock($type = ['write', 'execute'], $args = [], $flags = []) {
        if (is_array($type)) {
            array_map(function($typeof) use($args, $flags) {
                $this->_lock($typeof, $args, $flags);
            }, $type);
            return;
        }
        if (!in_array($type, array_keys($this->_locks))) {
            throw new \oroboros\core\utilities\exception\Exception('Invalid parameters passed at ' . __LINE__ . ' of ' . __METHOD__, self::EXCEPTION_CODE_BAD_PARAMETERS);
        }
        if (in_array(true, array_map(function($condition) use ($args) {
                            return (in_array($condition, array_keys($args)));
                        }, $this->_conditions_valid))) {
            //conditional unlock
            array_map(function($condition) use ($args, $type) {
                if (array_key_exists($condition, $args)) {
                    $this->_conditions[$type][$condition] = $args[$condition];
                }
            }, $this->_conditions_valid);
        } else {
            //default behavior
            $this->_lockSet($type);
        }
    }

    protected function _unlock($type = 'read', $args = [], $flags = []) {
        if (is_array($type)) {
            array_map(function($typeof) use($args, $flags) {
                $this->_unlock($typeof, $args, $flags);
            }, $type);
        }
        if (!in_array($type, array_keys($this->_locks))) {
            throw new \oroboros\core\utilities\exception\Exception('Invalid parameters passed at ' . __LINE__ . ' of ' . __METHOD__, self::EXCEPTION_CODE_BAD_PARAMETERS);
        }
        $this->_lockRelease($type);
    }

    protected function _registerAccess($type, $args = [], $flags = []) {
        if (is_array($type)) {
            array_map(function($typeof) use($args, $flags) {
                $this->_registerAccess($typeof, $args, $flags);
            }, $type);
        }
    }

    protected function _unregisterAccess($type, $args = [], $flags = []) {
        if (is_array($type)) {
            array_map(function($typeof) use($args, $flags) {
                $this->_unregisterAccess($typeof, $args, $flags);
            }, $type);
        }
    }

    protected function _checkAccess($type, $args = [], $flags = []) {
        if (is_array($type)) {
            array_map(function($typeof) use($args, $flags) {
                $this->_checkAccess($typeof, $args, $flags);
            }, $type);
        }
        if (!array_key_exists($type, $this->_locks)) {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException('Invalid lock type passed at ' . __LINE__ . ' of ' . __METHOD__, self::EXCEPTION_CODE_BAD_PARAMETERS);
        }
        if ($this->_locks[$type]) {
            throw new \oroboros\core\utilities\exception\Exception('Requested operation cannot complete because ' . get_class($this) . ' is locked.', self::EXCEPTION_CODE_SECURITY_LOCKED_DATA_OBJECT);
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    private function _lockRelease($type) {
        $this->_locks[$type] = false;
    }

    private function _lockSet($type) {
        $this->_locks[$type] = true;
    }

}
