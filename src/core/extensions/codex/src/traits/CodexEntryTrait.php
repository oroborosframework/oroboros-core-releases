<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\codex\traits;

/**
 * <Codex Entry Trait>
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/codex
 * @subpackage codex
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\codex\interfaces\contract\CodexEntryContract
 */
trait CodexEntryTrait
{

    use \oroboros\core\traits\core\BaselineTrait
    {
        \oroboros\core\traits\core\BaselineTrait::initialize as private baseline_initialize;
    }

    /**
     * Represents the identifying key of the codex entry,
     * which corresponds to the identifier that it is selected
     * from its index by.
     * @var string
     */
    private $_codex_entry_id;

    /**
     * Represents the value of the codex entry, which corresponds
     * to the return value of the entry when a lookup occurs.
     * @var string
     */
    private $_codex_entry_value;

    /**
     * Represents whether the codex entry value has been defined,
     * in the event that it is set as a null value.
     * As null values are acceptable, the check for definition cannot
     * just check if it is null to see if it is already defined.
     * @var bool
     */
    private $_codex_entry_value_defined = false;

    /**
     * Represents the context of the codex entry, which corresponds
     * to the codex index it is listed in.
     * @var string
     */
    private $_codex_entry_context;

    /**
     * Represents the metadata for the codex entry, which corresponds to
     * any additional details supplied when the getMeta method is called.
     * @var \oroboros\collection\Collection
     */
    private $_codex_entry_meta;

    /**
     * Represents the relational data for the codex entry, which corresponds
     * to an associative array of other codex entries that the entry bears
     * relation to.
     * @var \oroboros\collection\Collection
     */
    private $_codex_entry_relations;

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\core\interfaces\contract\codex\CodexEntryContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Codex Index Initialization Method>
     * Fires the codex index initialization.
     *
     * Codex Indexes cannot be reinitialized after initial initialization.
     *
     * @param mixed $params Any parameters that should be passed into initialization.
     * @param array $dependencies Any dependencies to inject into initialization.
     * @param array $flags
     * @return $this
     *
     * @throws \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract
     * This interface is attached to all exceptions that arise from this trait. If you want
     * to handle everything in one catch block, use this for your catch statement.
     *
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     * If provided parameters are not valid
     *
     * @throws \oroboros\core\utilities\exception\RuntimeException
     * If initialization has already occurred and reinitialization is disabled.
     *
     * @throws \oroboros\core\utilities\exception\LogicException
     * If the class is misconfigured. This is a serious error that
     * requires developer intervention on the part of whoever wrote
     * the override that caused the error, because it means the class
     * is entirely unusable.
     */
    public function initialize( $params = null, $dependencies = null,
        $flags = null )
    {
        if ( is_object( $params ) && ($params instanceof \oroboros\codex\interfaces\contract\CodexEntryContract) )
        {
            $params = self::_codexEntryExtractDetails( $params );
        }
        $this->_baselineSetParameterPersistence( true );
        $this->_baselineEnableParameterValidation( true );
        $this->_baselineRegisterInitializationTask( '_codexEntryInitializeContainers' );
        $this->_baselineRegisterInitializationTask( '_codexEntryPostInitialization' );
        $this->_baselineDisableReinitialization();
        $this->baseline_initialize( $params, $dependencies, $flags );
        return $this;
    }

    /**
     * <Codex Entry Id Getter Method>
     * Returns the id of the codex entry
     * @return string
     */
    public function getId()
    {
        return $this->_codex_entry_id;
    }

    /**
     * <Codex Entry Context Getter Method>
     * Returns the context of the codex entry
     * @return string
     */
    public function getContext()
    {
        return $this->_codex_entry_context;
    }

    /**
     * <Codex Entry Value Getter Method>
     * Returns the value of the codex entry
     * @return scalar|null
     */
    public function getValue()
    {
        return $this->_codex_entry_value;
    }

    /**
     * <Codex Entry Metadata Getter Method>
     * Returns a collection of any meta information
     * associated with the codex entry.
     * @return bool|scalar|array
     */
    public function getMeta( $key = null )
    {
        if ( is_null( $key ) )
        {
            return $this->_codex_entry_meta->toArray();
        }
        if ( is_scalar( $key ) && $this->_codex_entry_meta->has( $key ) )
        {
            return $this->_codex_entry_meta[$key];
        }
        return false;
    }

    /**
     * <Codex Entry Metadata Check Method>
     * Returns a boolean determination as to whether a given metadata key
     * exists in the codex entry.
     * @param scalar $key
     * @return bool
     */
    public function hasMeta( $key )
    {
        return is_scalar( $key ) && $this->_codex_entry_meta->has( $key );
    }

    /**
     * <Codex Entry Metadata Setter Method>
     * @param string $key The metadata key to set
     * @param scalar $value The metadata value to set
     */
    public function setMeta( $key, $value )
    {
        $this->_codexEntrySetMetadata( $key, $value );
    }

    /**
     * <Codex Entry Relations Getter Method>
     * Returns a list of associated codex entries
     * that relate to the current one.
     *
     * Returns null if no relations exist, otherwise returns an array of the relations.
     *
     * @return null|array
     */
    public function getRelations()
    {
        if ( empty( $this->_codex_entry_relations ) )
        {
            return null;
        }
        return $this->_codex_entry_relations->toArray();
    }

    /**
     * <Codex Entry Relation Setter Method>
     * Sets a relation to another codex entry.
     * @param string $index
     * @param scalar $relation
     */
    public function setRelation( $index, $relation )
    {
        $this->_codexEntrySetRelation( $index, $relation );
    }

    /**
     * <Codex Entry Relation Check Method>
     * Returns a boolean determination as to whether a relation to another
     * given codex entry exists.
     * @param string $index
     * @param scalar $relation
     * @return bool
     */
    public function hasRelation( $index, $relation )
    {
        return $this->_codexEntryHasRelation( $index, $relation );
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * <Baseline Required Parameter Setter Method>
     * May be overridden to provide a set of parameters that
     * are required to be provided for initialization to resolve.
     *
     * Any parameters that exist in the given method MUST be provided
     * for to initialization or alternately honored by the provided
     * defaults, or an exception will be raised that blocks initialization.
     *
     * @return array
     */
    protected function _baselineSetParametersRequired()
    {
        return array(
            'id',
            'value',
            'context',
        );
    }

    /**
     * <Baseline Valid Parameter Setter Method>
     * May be overridden to provide a validation array for provided parameters.
     * If provided, any provided parameters matching the specified keys MUST
     * resolve to the given type or instance to be accepted.
     *
     * The array should be associative, where the key corresponds to the inbound parameter key,
     * and the value corresponds to the expected type or object instance.
     *
     * An array of acceptable values may be provided, in which case ANY
     * of the given values will be considered acceptable for that key.
     *
     * If a non-empty validation array is given and any keys match from the
     * provided parameters, but validation fails, an exception will be raised
     * that blocks initialization from resolving.
     *
     * @return array
     */
    protected function _baselineSetParametersValid()
    {
        return array(
            'id' => 'scalar',
            'value' => 'scalar',
            'context' => 'scalar',
            'meta' => 'array',
            'relations' => 'array',
        );
    }

    /**
     * <Baseline Default Parameter Setter Method>
     * May be overridden to provide a set of default parameters.
     * If provided, the given set will act as a baseline set of
     * templated parameters, and public provided parameters will
     * be recursively replaced or appended into the given array
     * as appropriate, with preference going to the publicly provided
     * parameters.
     *
     * This allows for parameters to be provided in a way
     * that appends or replaces the defaults rather than forcing
     * external usage to provide a full set.
     *
     * @return array
     */
    protected function _baselineSetParametersDefault()
    {
        return array(
            'meta' => array(),
            'relations' => array(),
        );
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * Performs the post-initialization tasks for the codex entry
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If any invalid properties are passed
     * @throws \oroboros\core\utilities\exception\RuntimeException
     *     If any duplicate parameters are declared, or any parameters
     *     are given that are already declared
     * @internal
     */
    private function _codexEntryPostInitialization()
    {
//        d(
//            $this->_baselineGetParameter( 'id' ),
//            $this->_baselineGetParameter( 'value' ),
//            $this->_baselineGetParameter( 'context' ),
//            $this->_baselineGetParameter( 'meta' ),
//            $this->_baselineGetParameter( 'relations' )
//        );
        $this->_codexEntrySetId( $this->_baselineGetParameter( 'id' ) );
        $this->_codexEntrySetValue( $this->_baselineGetParameter( 'value' ) );
        $this->_codexEntrySetContext( $this->_baselineGetParameter( 'context' ) );
        foreach ( $this->_baselineGetParameter( 'meta' ) as
            $key =>
            $value )
        {
            $this->_codexEntrySetMetadata( $key, $value );
        }
        foreach ( $this->_baselineGetParameter( 'relations' ) as
            $key =>
            $value )
        {
            $this->_codexEntrySetRelation( $key, $value );
        }
    }

    /**
     * Initializes the internal containers used to store
     * information about the codex entry
     * @return void
     * @internal
     */
    private function _codexEntryInitializeContainers()
    {
        if ( is_null( $this->_codex_entry_meta ) )
        {
            $this->_codex_entry_meta = new \oroboros\collection\Collection();
        }
        if ( is_null( $this->_codex_entry_relations ) )
        {
            $this->_codex_entry_relations = new \oroboros\collection\Collection();
        }
    }

    /**
     * Sets the id property of the codex entry
     * @param scalar $id
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the provided id is not a scalar
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If an attempt is made to redefine the id
     * @internal
     */
    private function _codexEntrySetId( $id )
    {
        self::_validate( 'scalar', $id, true, true );
        if ( !is_null( $this->_codex_entry_id )
            && $id !== $this->_codex_entry_id )
        {
            self::_throwException( 'invalid-argument-exception',
                sprintf( 'Error encountered at [%s]. Cannot redefine the given '
                    . 'codex entry id [%s] as [%s].', __METHOD__,
                    $this->_codex_entry_id, $id ),
                self::_getExceptionCode( 'logic-improper-value-mutation' ) );
        }
        $this->_codex_entry_id = $id;
    }

    /**
     * Sets the value property of the codex entry
     * @param scalar|null $value
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the provided value is not a scalar
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If an attempt is made to redefine the value
     * @internal
     */
    private function _codexEntrySetValue( $value )
    {
        if ( !is_null( $value ) )
        {
            self::_validate( 'scalar', $value, true, true );
        }
        if ( $this->_codex_entry_value_defined
            && $value !== $this->_codex_entry_value )
        {
            self::_throwException( 'invalid-argument-exception',
                sprintf( 'Error encountered at [%s]. Cannot redefine the given '
                    . 'codex entry value [%s] as [%s].', __METHOD__,
                    $this->_codex_entry_value, $value ),
                self::_getExceptionCode( 'logic-improper-value-mutation' ) );
        }
        $this->_codex_entry_value = $value;
        $this->_codex_entry_value_defined = true;
    }

    /**
     * Sets the context property of the codex entry
     * @param scalar $context
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the provided context is not a scalar
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If an attempt is made to redefine the context
     * @internal
     */
    private function _codexEntrySetContext( $context )
    {
        self::_validate( 'scalar', $context, true, true );
        if ( !is_null( $this->_codex_entry_context )
            && $context !== $this->_codex_entry_context )
        {
            self::_throwException( 'invalid-argument-exception',
                sprintf( 'Error encountered at [%s]. Cannot redefine the given '
                    . 'codex entry context [%s] as [%s].', __METHOD__,
                    $this->_codex_entry_context, $context ),
                self::_getExceptionCode( 'logic-improper-value-mutation' ) );
        }
        $this->_codex_entry_context = $context;
    }

    /**
     * Sets a metadata property of the codex entry
     * @param scalar $meta_key
     * @param scalar $meta_value
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the provided meta key is not a string
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the provided meta value is not scalar
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If an attempt is made to redefine an existing meta value
     * @internal
     */
    private function _codexEntrySetMetadata( $meta_key, $meta_value )
    {
        self::_validate( 'scalar', $meta_key, true, true );
        self::_validate( 'scalar', $meta_value, true, true );
        if ( $this->_codex_entry_meta->has( $meta_key )
            && $this->_codex_entry_meta->get( $meta_key ) !== $meta_value )
        {
            self::_throwException( 'invalid-argument-exception',
                sprintf( 'Error encountered at [%s] in instance of [%s]. Cannot redefine the given '
                    . 'codex metadata key [%s] as [%s], because it is already '
                    . 'defined as [%s].', __METHOD__, get_class($this), $meta_key, $meta_value,
                    $this->_codex_entry_meta->get( $meta_key ) ),
                self::_getExceptionCode( 'logic-improper-value-mutation' ) );
        }
        $this->_codex_entry_meta[$meta_key] = $meta_value;
    }

    /**
     * Sets an association between the codex entry and another codex entry.
     * @param string $index The codex index of the relation
     * @param scalar $relation The id of the relation
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the provided index is not a string
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the provided relation is not scalar
     * @internal
     */
    private function _codexEntrySetRelation( $index, $relation )
    {
        self::_validate( 'type-of', $index, 'string', true );
        if ( !is_array( $relation ) )
        {
            self::_validate( 'scalar', $relation, true, true );
        } else
        {
            foreach ( $relation as
                $key =>
                $subrelation )
            {
                self::_validate( 'scalar', $subrelation, true, true );
            }
        }
        if ( !$this->_codex_entry_relations->has( $index ) )
        {
            //Create the internal index if it does not already exist.
            $this->_codex_entry_relations[$index] = array();
        }
        if ( $this->_codexEntryHasRelation( $index, $relation ) )
        {
            //Do not double-set relations
            return;
        }
        $updated_index = $this->_codex_entry_relations[$index];
        if ( is_array( $relation ) )
        {
            foreach ($relation as $key => $sub)
            {
                $updated_index[$key] = $sub;
            }
        } else {
            $updated_index[$index] = $relation;
        }
        $this->_codex_entry_relations[$index] = $updated_index;
    }

    /**
     * Returns whether or not a given relation exists in the given index
     * @param string $index
     * @param scalar $relation
     * @return bool
     * @internal
     */
    private function _codexEntryHasRelation( $index, $relation )
    {
        return $this->_codex_entry_relations->has( $index )
            && in_array( $relation, $this->_codex_entry_relations->get( $index ) );
    }

    private static function _codexEntryExtractDetails( \oroboros\codex\interfaces\contract\CodexEntryContract $entry )
    {
        $result = array(
            'id' => $entry->getId(),
            'context' => $entry->getContext(),
            'meta' => $entry->getMeta(),
            'relations' => $entry->getRelations()
        );
        return $result;
    }

}
