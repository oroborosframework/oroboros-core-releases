<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\codex\traits\generators\entry\contract;

/**
 * <Contract Trait>
 * Parses Contract Interfaces and determines their value structure
 * for codex declaration.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/codex
 * @subpackage
 * @version
 * @since
 */
trait ContractTrait
{

    use \oroboros\codex\traits\generators\entry\EntryGeneratorTrait;

    //declare

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies ...
     *
     * @execution Default Execution Plan (minimal)
     *
     * @execution Default Execution Plan (commented)
     *
     * -------------------------------------------------------------------------
     */

    //public methods

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * <Codex Context Declaration Method>
     * This method will fire during initialization, and is used to set the codex
     * context that the generators records are associated with.
     * @return string
     */
    protected function _declareCodexContext()
    {
        return 'contract';
    }

    /**
     * <Context Generator Valid Type Declaration Method>
     * This method will fire during initialization, and is used to
     * determine what constitutes a valid generator parse subject.
     * The response should be a valid php variable type, "class",
     * "trait", "interface", or an interface
     * that designates a valid type of object.
     */
    protected function _declareValidType()
    {
        return '\\oroboros\\core\\interfaces\\contract\\BaseContract';
    }

    /**
     * <Context Generator Subject Parser Method>
     * This method will fire whenever a request to parse a subject occurs
     * and a valid type that matches the designated type filter is passed.
     * Non-matches will be automatically rejected, so this method should
     * only be concerned with parsing a valid subject for the implementing
     * class's declared type.
     */
    protected function _parseSubject( $subject )
    {
        return array(
            'id' => 'test',
            'value' => 'test'
        );
    }

    /**
     * <Context Generator Default Entry Declaration Method>
     * This method will fire during initialization to generate the default
     * entries associated with the context generator. These will be
     * automatically added to the codex when the generator is initialized.
     *
     * This method should return an array of valid parsable subjects using
     * its own parse method. The generator object will not be considered
     * valid if it cannot parse its own default entries.
     *
     * You may return an empty array if there are no default entries to parse.
     * @return array
     */
    protected function _declareDefaults()
    {
        return array();
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    //private methods

}
