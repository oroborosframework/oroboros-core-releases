<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\codex\traits\generators\entry;

/**
 * <Codex Entry Generator Trait>
 * Provides baseline methods for generation of codex documentation entries
 * for various referential types.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @category codex
 * @package oroboros/codex
 * @subpackage codex
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\core\interfaces\contract\utilities\core\entry\EntryGeneratorContract
 */
trait EntryGeneratorTrait
{

    use \oroboros\core\traits\core\BaselineTrait
    {
        \oroboros\core\traits\core\BaselineTrait::initialize as private baseline_initialize;
    }

    private static $_context_generator_valid_type_declarations = array(
        'string',
        'array',
        'boolean',
        'bool',
        'object',
        'integer',
        'int',
        'float',
        'double',
        'number',
        'numeric',
        'scalar',
        'null',
        'void',
        'resource',
        'callable',
        'callback',
        'iterable',
        'generator',
        'file',
        'directory',
        'class',
        'interface',
        'trait',
    );

    /**
     * Represents the codex index identifier that the
     * generator creates contexts for.
     * @var string
     */
    private $_context_generator_context;

    /**
     * Represents the default context entries generated during initialization.
     * @var array
     */
    private $_context_generator_default_entries = array();

    /**
     * Represents a type or interface instance that corresponds to the
     * valid contextual generation types declared during initialization.
     * @var string
     */
    private $_context_generator_valid_type;

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\core\interfaces\contract\utilities\core\entry\EntryGeneratorContract
     *
     * @execution Default Execution Plan (minimal)
     *
     * @execution Default Execution Plan (commented)
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Context Generator Initialization Method>
     * Fires the context generator initialization.
     * @param mixed $params Any parameters that should be passed into initialization.
     * @param array $dependencies Any dependencies to inject into initialization.
     * @param array $flags
     * @return $this
     *
     * @throws \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract
     * This interface is attached to all exceptions that arise from this trait. If you want
     * to handle everything in one catch block, use this for your catch statement.
     *
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     * If provided parameters are not valid
     *
     * @throws \oroboros\core\utilities\exception\RuntimeException
     * If initialization has already occurred and reinitialization is disabled.
     *
     * @throws \oroboros\core\utilities\exception\LogicException
     * If the class is misconfigured. This is a serious error that
     * requires developer intervention on the part of whoever wrote
     * the override that caused the error, because it means the class
     * is entirely unusable.
     */
    public function initialize( $params = null, $dependencies = null,
        $flags = null )
    {
        $this->_baselineRegisterInitializationTask( '_codexContextGeneratorPostInitialization' );
        $this->baseline_initialize( $params, $dependencies, $flags );
        return $this;
    }

    /**
     * <Baseline Valid Flag Setter Method>
     * May be overridden to provide a validation array for provided flags.
     * If provided, any provided flags MUST exist in the given array of
     * valid values.
     *
     * The array should be a standard numerically keyed list of valid flags.
     *
     * If a non-empty validation array is given and any flags are passed
     * that do not exist in the validation array, an exception will be raised
     * that blocks initialization from resolving.
     *
     * @return array
     */
    protected function _baselineSetFlagsValid()
    {
        return array(
            '::generate-multi::',
        );
    }

    /**
     * <Context Getter Method>
     * Gets the codex index that is associated with the context generator.
     * @param type $element
     * @return type
     */
    public function getContext()
    {
        return $this->_getContext();
    }

    /**
     * <Context Defaults Getter Method>
     * Returns the default entries for the codex index associated with
     * the context generator.
     * @return array
     */
    public function getDefaults()
    {
        return $this->_getDefaults();
    }

    /**
     * <Context Generator Parse Method>
     * Parses a given subject that should match the declared subject type
     * for the generator, and returns a codex entry for it.
     * @param mixed $subject
     * @return \oroboros\codex\interfaces\contract\CodexEntryContract
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the given entry is not valid.
     */
    public function parse( $subject )
    {
        $this->_validateSubject( $subject );
        $response = $this->_parseSubject( $subject );
        if ( $this->checkFlag( '::generate-multi::' ) )
        {
            $result = array();
            foreach ( $response as
                $entry )
            {
                $result[] = $this->_generateContextEntry( $entry );
            }
            return $result;
        }
        return $this->_generateContextEntry( $response );
    }

    /**
     * <Context Generator Batch Parse Method>
     * Bulk parses a set of subjects that are expressed either
     * in an array or a collection. Will return a collection of codex entries.
     * @param array|\oroboros\collection\interfaces\contract\CollectionContract $subject
     * @return \oroboros\collection\interfaces\contract\CollectionContract
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the given subject parameter is not an array or collection.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If any of the given entries are not valid.
     */
    public function batch( $subjects )
    {
        if ( !is_array( $subjects ) && !( is_object( $subjects ) && ($subjects instanceof \oroboros\collection\interfaces\contract\CollectionContract )) )
        {
            throw self::_getException( 'invalid-argument-exception',
                'logic-bad-parameters',
                array(
                'array|\\oroboros\\collection\\interfaces\\contract\\CollectionContract' ),
                $subjects );
        }
        $results = new \oroboros\collection\Collection();
        foreach ( $subjects as
            $key =>
            $subject )
        {
            $results[$key] = $this->parse( $subject );
        }
        if ( $this->checkFlag( '::generate-multi::' ) )
        {
            return $this->_codexContextGeneratorFlattenMultiResultSet( $results );
        }
        return $results;
    }

    /**
     * <Context Generator Valid Subject Check Method>
     * Preflights a subject candidate and returns whether or not it is valid.
     * @return bool
     */
    public function isValid( $subject )
    {
        try
        {
            $this->_validateSubject( $subject );
            return true;
        } catch ( \oroboros\core\utilities\exception\InvalidArgumentException $e )
        {
            return false;
        }
    }

    /**
     * <Context Generator Valid Type Getter Method>
     * Returns the valid parameter type that the generator can parse.
     * This will be a valid php variable type, "class", "trait", "interface",
     * "file", "directory", or a class or interface name.
     * @return string
     */
    public function getValidType()
    {
        return $this->_context_generator_valid_type;
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * <Context Generator Context Declaration Method>
     * This method will fire during initialization, and is used to set the codex
     * context that the generators records are associated with.
     * @return string
     */
    abstract protected function _declareCodexContext();

    /**
     * <Context Generator Valid Type Declaration Method>
     * This method will fire during initialization, and is used to
     * determine what constitutes a valid generator parse subject.
     * The response should be a valid php variable type, "class",
     * "trait", "interface", or an interface
     * that designates a valid type of object.
     */
    abstract protected function _declareValidType();

    /**
     * <Context Generator Default Entry Declaration Method>
     * This method will fire during initialization to generate the default
     * entries associated with the context generator. These will be
     * automatically added to the codex when the generator is initialized.
     *
     * This method should return an array of valid parsable subjects using
     * its own parse method. The generator object will not be considered
     * valid if it cannot parse its own default entries.
     *
     * You may return an empty array if there are no default entries to parse.
     * @return array
     */
    protected function _declareDefaults()
    {
        return array();
    }

    /**
     * <Context Generator Subject Parser Method>
     * This method will fire whenever a request to parse a subject occurs
     * and a valid type that matches the designated type filter is passed.
     * Non-matches will be automatically rejected, so this method should
     * only be concerned with parsing a valid subject for the implementing
     * class's declared type.
     */
    abstract protected function _parseSubject( $subject );

    /**
     * <Context Generator Internal Context Getter Method>
     * Returns the declared codex index context that the
     * generator creates entries for.
     * @return string
     */
    protected function _getContext()
    {
        return $this->_context_generator_context;
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * Runs the post initialization operations for the context generator.
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the given context is not a string
     * @internal
     */
    private function _codexContextGeneratorPostInitialization()
    {
        $context = $this->_declareCodexContext();
        self::_validate( 'type-of', $context, 'string', true );
        $this->_context_generator_context = $context;
        $valid_types = array_merge( self::$_context_generator_valid_type_declarations,
            array(
            'valid interface',
            'valid class' ) );
        $declared_type = $this->_declareValidType();
        if ( !in_array( $declared_type,
                self::$_context_generator_valid_type_declarations ) && !interface_exists( $declared_type,
                true ) && !class_exists( $declared_type, true ) )
        {
//            d( $declared_type );
            self::_throwException( 'logic-exception',
                sprintf( 'Error encountered at [%s]. Class [%s] is misconfigured and '
                    . 'cannot resolve initialization. Declared valid type [%s] '
                    . 'must be one of the following: [%s]', __METHOD__,
                    get_class( $this ), $declared_type,
                    implode( '|', $valid_types ) ),
                self::_getExceptionCode( 'core-class-misconfigured' ) );
        }
        $this->_context_generator_valid_type = $declared_type;
        $this->_codexContextGeneratorValidateDefaultEntries();
        $this->_context_generator_default_entries = $this->_declareDefaults();
    }

    /**
     * Validates the default entries using the provided type declaration,
     * and returns the valid set if they pass validation.
     * @return array
     * @throws \oroboros\core\interfaces\exception\LogicException
     *     If any of the defaults do not pass validation.
     */
    private function _codexContextGeneratorValidateDefaultEntries()
    {
        $defaults = $this->_declareDefaults();
        try
        {
            foreach ( $defaults as
                $key =>
                $default )
            {
                $this->_validateSubject( $default );
            }
            return $defaults;
        } catch ( \Exception $e )
        {
            throw self::_recastException( $e, 'logic-exception' );
        }
    }

    /**
     * Returns the default context entries declared during initialization.
     * @return array
     * @internal
     */
    private function _getDefaults()
    {
        return $this->batch( $this->_context_generator_default_entries )->toArray();
    }

    /**
     * Preflights a candidate subject to insure it is a valid
     * type of what the generator handles.
     * @param mixed $subject
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the parameter is not valid for the declared valid type.
     * @internal
     */
    private function _validateSubject( $subject )
    {
        switch ( $this->_context_generator_valid_type )
        {
            case 'string':
                self::_validate( 'type-of', $subject, 'string', true );
                break;
            case 'array':
                self::_validate( 'type-of', $subject, 'array', true );
                break;
            case 'boolean':
            case 'bool':
                self::_validate( 'type-of', $subject, 'boolean', true );
                break;
            case 'object':
                self::_validate( 'type-of', $subject, 'object', true );
                break;
            case 'integer':
            case 'int':
                self::_validate( 'type-of', $subject, 'integer', true );
                break;
            case 'float':
            case 'double':
                self::_validate( 'type-of', $subject, 'float', true );
                break;
            case 'number':
            case 'numeric':
                self::_validate( 'number', $subject, true, true );
                break;
            case 'scalar':
                self::_validate( 'scalar', $subject, null, true );
                break;
            case 'null':
            case 'void':
                self::_validate( 'type-of', $subject, 'null', true );
                break;
            case 'resource':
                self::_validate( 'type-of', $subject, 'resource', true );
                break;
            case 'callable':
            case 'callback':
                self::_validate( 'callable', $subject, null, true );
                break;
            case 'iterable':
                if ( is_object( $subject ) )
                {
                    self::_validate( 'instance-of', $subject, '\\Traversable',
                        true );
                } else
                {
                    self::_validate( 'type-of', $subject, 'array', true );
                }
                break;
            case 'generator':
                self::_validate( 'type-of', $subject, 'generator', true );
                break;
            case 'file':
                self::_validate( 'file', $subject, true, true );
                break;
            case 'directory':
                self::_validate( 'directory', $subject, true, true );
                break;
            case 'class':
                self::_validate( 'class', $subject, null, true );
                break;
            case 'interface':
                self::_validate( 'interface', $subject, null, true );
                break;
            case 'trait':
                self::_validate( 'trait', $subject, null, true );
                break;
            default:
                //validate instance-of
                self::_validate( 'instance-of', $subject,
                    $this->_context_generator_valid_type, true );
                break;
        }
    }

    /**
     * Parses an array and returns a valid codex entry,
     * if a codex entry has not already been created.
     * @param array|\oroboros\codex\interfaces\contract\CodexEntryContract $data
     * @return \oroboros\codex\interfaces\contract\CodexEntryContract
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the data format is not valid for creating a codex entry.
     * @internal
     */
    private function _generateContextEntry( $data )
    {
        if ( $this->checkFlag( '::generate-multi::' ) )
        {

        }
        if ( is_object( $data ) && ( $data instanceof \oroboros\codex\interfaces\contract\CodexEntryContract) )
        {
            return $data;
        }
        self::_validate( 'type-of', $data, 'array', true );
        $data['context'] = $this->getContext();
        try
        {
            return new \oroboros\codex\CodexEntry( $data, null,
                \oroboros\codex\CodexEntry::getCompiledFlags() );
        } catch ( \oroboros\core\utilities\exception\InvalidArgumentException $e )
        {
//            d( $data, $e );
            self::_throwException( 'invalid-argument-exception',
                sprintf( 'Error encountered at [%s] in instance of [%s]. Failed to generate a valid codex entry with given details [%s]',
                    __METHOD__, get_class( $this ), print_r( $data, 1 ) ),
                self::_getExceptionCode( 'logic-bad-parameters' ), $e );
        }
    }

    /**
     * For generators that compile multi-tier results, this will recursively
     * extract them into a flattened array to return a proper flat set when
     * a batch operation occurs.
     * @param array $results
     * @internal
     */
    private function _codexContextGeneratorFlattenMultiResultSet( $results )
    {
        $res = array();
        foreach ( $results->toArray() as
            $result )
        {
            $res = array_merge( $res, $result );
        }
        return new \oroboros\collection\Collection( $res );
    }

}
