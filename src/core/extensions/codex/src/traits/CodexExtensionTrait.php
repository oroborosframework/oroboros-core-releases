<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\codex\traits;

/**
 * <Oroboros Static Codex Api Trait>
 * This trait provides the base accessor class with codex passthrough
 * methods compatible with the StaticControlApi declaration.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/codex
 * @subpackage codex
 * @version 0.2.5
 * @since 0.2.5
 */
trait CodexExtensionTrait
{

    /**
     * Designates the Codex object used for dynamically generating api
     * details and cross-referencing information about classes, traits,
     * interfaces, and functions declared in the core system, its extensions,
     * or other packages.
     * @var \oroboros\core\interfaces\contract\libraries\CodexContract
     */
    private static $_oroboros_codex;

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     *
     * @param type $key
     * @param type $subkey
     * @return type
     */
    protected static function _codexLookup( $key, $subkey = null )
    {
        return self::_getCodex()->lookup( $key, $subkey );
    }

    /**
     *
     * @param type $keyword
     * @param type $context
     * @param type $index
     * @return type
     */
    protected static function _codexSearch( $keyword, $context = null, $index = null )
    {
        return self::_getCodex()->search( $keyword, $context, $index );
    }

    /**
     *
     * @param type $indexes
     * @return type
     */
    protected static function _codexMap( $indexes )
    {
        return self::_getCodex()->map( $indexes );
    }

    /**
     *
     * @param type $index
     * @param type $entries
     * @return type
     */
    protected static function _codexMapTo( $index, $entries )
    {
        return self::_getCodex()->mapTo( $index, $entries );
    }

    /**
     *
     * @return type
     */
    protected static function _codexSave()
    {
        return self::_getCodex()->save();
    }

    /**
     *
     * @return type
     */
    protected static function _codexRestore()
    {
        return self::_getCodex()->restore();
    }

    /**
     *
     * @param type $index
     * @return type
     */
    protected static function _codexReport( $index )
    {
        return self::_getCodex()->report( $index );
    }

    /**
     *
     * @param type $index
     * @param type $entries
     * @return type
     */
    protected static function _codexRegisterIndex( $index, $entries = array() )
    {
        return self::_getCodex()->registerIndex( $index, $entries );
    }

    /**
     *
     * @param type $index
     * @param type $id
     * @param type $entry
     * @return type
     */
    protected static function _codexRegisterEntry( $index, $id, $entry )
    {
        return self::_getCodex()->registerEntry( $index, $id, $entry );
    }

    /**
     *
     * @param type $index
     * @return type
     */
    protected static function _codexSetIndex( $index )
    {
        return self::_getCodex()->setIndex( $index );
    }

    /**
     *
     * @param type $entry
     * @return type
     */
    protected static function _codexSetEntry( $entry )
    {
        return self::_getCodex()->setEntry( $entry );
    }

    /**
     *
     * @param type $index
     * @return type
     */
    protected static function _codexGetIndex( $index )
    {
        return self::_getCodex()->getIndex( $index );
    }

    /**
     *
     * @param type $index
     * @param type $entry
     * @return type
     */
    protected static function _codexGetEntry( $index, $entry )
    {
        return self::_getCodex()->getEntry( $index, $entry );
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * Fetches the localized codex instance by reference
     * @return \oroboros\codex\interfaces\contract\CodexContract
     * @throws \oroboros\core\utilities\exception\RuntimeException
     *     If the codex has not been set into the local dependencies
     */
    private static function &_getCodex()
    {
        $codex = self::_staticBaselineGetDependency( 'codex' );
        if ( is_null( $codex ) )
        {
            throw new \oroboros\core\utilities\exception\RuntimeException(
            sprintf( 'Error encountered at [%s]. Environment instance of [%s] '
                . 'must be set as a static baseline dependency to use '
                . 'this trait.', __METHOD__,
                '\\oroboros\\codex\\interfaces\\contract\\CodexContract' ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_CORE_EXTENSION_FAILURE );
        }
        return $codex;
    }

}
