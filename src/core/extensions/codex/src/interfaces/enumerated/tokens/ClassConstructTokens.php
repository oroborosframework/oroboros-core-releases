<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\codex\interfaces\enumerated\tokens;

/**
 * <Class Construct Token Enumerated Api Interface>
 * This enumerated interface provides more human readable descriptions
 * for PHP bitwise tokens, to ease the readability of error reporting
 * and logging of error messages.
 *
 * Class construct tokens are pure language constructs that enable standard
 * expected PHP operations in the object oriented scope. Unlike language
 * construct tokens, they are not valid for procedural usage.
 *
 * This is used by the Lexicon Tokenizer also for validating PHP tokens
 * when auto-parsing packages from the command line api.
 *
 * --------
 *
 * Enumerated Api Interfaces
 * Enumerated Api Interfaces provide sets of fixed,
 * enumerated values that must maintain consistency,
 * and should not be overridden at compile time.
 *
 * In some cases, these values are considered absolute,
 * and the program will always reference them as defined.
 * In other cases, they provide baseline definitions,
 * which the individual api implementing them may allow
 * for extension upon. In these cases, the fixed values
 * MAY NOT be removed or altered, but additions and aliases
 * will be allowed for. Check the api of the individual
 * contract being considered for extension for information
 * how to accomplish this as it applies.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/enumerated_api_interface.md
 * @category enumerated-interfaces
 * @package oroboros/codex
 * @subpackage codex
 * @version 0.2.5
 * @since 0.2.5
 */
interface ClassConstructTokens
extends \oroboros\codex\interfaces\enumerated\CodexBase
{

    //Class Constructs
    const TOKEN_CONSTRUCT_CLASS_CONTEXT_ABSTRACT = T_ABSTRACT;
    const TOKEN_CONSTRUCT_CLASS_CONTEXT_FINAL = T_FINAL;
    const TOKEN_CONSTRUCT_CLASS_SCOPE_STATIC = T_STATIC;
    const TOKEN_CONSTRUCT_CLASS_DECLARATION_CLASS = T_CLASS;
    const TOKEN_CONSTRUCT_CLASS_DECLARATION_INTERFACE = T_INTERFACE;
    const TOKEN_CONSTRUCT_CLASS_DECLARATION_TRAIT = T_TRAIT;
    const TOKEN_CONSTRUCT_CLASS_DECLARATION_TRAIT_INSTEADOF = T_INSTEADOF;
    const TOKEN_CONSTRUCT_CLASS_DECLARATION_CLASS_CONSTANT = T_CONST;
    const TOKEN_CONSTRUCT_CLASS_DECLARATION_VARIABLE_PUBLIC = T_VAR;
    const TOKEN_CONSTRUCT_CLASS_VISIBILITY_PRIVATE = T_PRIVATE;
    const TOKEN_CONSTRUCT_CLASS_VISIBILITY_PROTECTED = T_PROTECTED;
    const TOKEN_CONSTRUCT_CLASS_VISIBILITY_PUBLIC = T_PUBLIC;
    const TOKEN_CONSTRUCT_CLASS_INHERITANCE_EXTENDS = T_EXTENDS;
    const TOKEN_CONSTRUCT_CLASS_INHERITANCE_IMPLEMENTS = T_IMPLEMENTS;
    const TOKEN_CONSTRUCT_CLASS_INHERITANCE_USE_TRAIT = T_USE;
    const TOKEN_CONSTRUCT_CLASS_OPERATOR_STATIC_ALIAS = T_DOUBLE_COLON;
    const TOKEN_CONSTRUCT_CLASS_OPERATOR_STATIC = T_PAAMAYIM_NEKUDOTAYIM;
    const TOKEN_CONSTRUCT_CLASS_OPERATOR_OBJECT = T_OBJECT_OPERATOR;
    const TOKEN_CONSTRUCT_CLASS_INSTANTIATION_CLONE = T_CLONE;
    const TOKEN_CONSTRUCT_CLASS_INSTANTIATION_NEW = T_NEW;
    const TOKEN_CONSTRUCT_CLASS_REFERENTIAL_KEYWORD = T_STRING;
    const TOKEN_CONSTRUCT_CLASS_CONTEXT_ABSTRACT_NAME = 'T_ABSTRACT';
    const TOKEN_CONSTRUCT_CLASS_CONTEXT_FINAL_NAME = 'T_FINAL';
    const TOKEN_CONSTRUCT_CLASS_SCOPE_STATIC_NAME = 'T_STATIC';
    const TOKEN_CONSTRUCT_CLASS_DECLARATION_CLASS_NAME = 'T_CLASS';
    const TOKEN_CONSTRUCT_CLASS_DECLARATION_INTERFACE_NAME = 'T_INTERFACE';
    const TOKEN_CONSTRUCT_CLASS_DECLARATION_TRAIT_NAME = 'T_TRAIT';
    const TOKEN_CONSTRUCT_CLASS_DECLARATION_TRAIT_INSTEADOF_NAME = 'T_INSTEADOF';
    const TOKEN_CONSTRUCT_CLASS_DECLARATION_CLASS_CONSTANT_NAME = 'T_CONST';
    const TOKEN_CONSTRUCT_CLASS_DECLARATION_VARIABLE_PUBLIC_NAME = 'T_VAR';
    const TOKEN_CONSTRUCT_CLASS_VISIBILITY_PRIVATE_NAME = 'T_PRIVATE';
    const TOKEN_CONSTRUCT_CLASS_VISIBILITY_PROTECTED_NAME = 'T_PROTECTED';
    const TOKEN_CONSTRUCT_CLASS_VISIBILITY_PUBLIC_NAME = 'T_PUBLIC';
    const TOKEN_CONSTRUCT_CLASS_INHERITANCE_EXTENDS_NAME = 'T_EXTENDS';
    const TOKEN_CONSTRUCT_CLASS_INHERITANCE_IMPLEMENTS_NAME = 'T_IMPLEMENTS';
    const TOKEN_CONSTRUCT_CLASS_INHERITANCE_USE_TRAIT_NAME = 'T_USE';
    const TOKEN_CONSTRUCT_CLASS_OPERATOR_STATIC_ALIAS_NAME = 'T_DOUBLE_COLON';
    const TOKEN_CONSTRUCT_CLASS_OPERATOR_STATIC_NAME = 'T_PAAMAYIM_NEKUDOTAYIM';
    const TOKEN_CONSTRUCT_CLASS_OPERATOR_OBJECT_NAME = 'T_OBJECT_OPERATOR';
    const TOKEN_CONSTRUCT_CLASS_INSTANTIATION_CLONE_NAME = 'T_CLONE';
    const TOKEN_CONSTRUCT_CLASS_INSTANTIATION_NEW_NAME = 'T_NEW';
    const TOKEN_CONSTRUCT_CLASS_REFERENTIAL_KEYWORD_NAME = 'T_STRING';

}
