<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\codex\interfaces\contract;

/**
 * <Lexicon Index Contract Interface>
 * Enforces a set of methods for indexing related lexicon
 * entries constituting a package or api.
 *
 * Lexicon indexes represent a set of related constructs that work towards
 * a common objective, but do not necessarily share inheritance.
 * Lexicon indexes may also contain pointers to other lexicon indexes,
 * which denote that a construct is part of one package, but also fulfills
 * the objectives of another package seamlessly.
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @package oroboros/codex
 * @subpackage codex
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 */
interface LexiconIndexContract
extends \oroboros\core\interfaces\contract\libraries\LibraryContract,
 \Serializable
{

    /**
     * <Lexicon Index Unserialize Method>
     * This will restore a LexiconIndex from a serialized state,
     * and allow it to rebuild its index of LexiconEntry objects.
     * @param string $name The slug name that the LexiconIndex will be referred to by
     * @param array|\oroboros\collection\interfaces\contract\CollectionContract $entries (optional) The index may be constructed with a predefined set of LexiconEntries, or they may be defined later.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If an invalid parameter is passed, or if the LexiconIndex name is already taken and would cause an ambiguous reference
     */
    public function __construct( $name, $entries = array(), $flags = array() );

    /**
     * <Lexicon Index Cache Save Method>
     * Saves the LexiconIndex to a disk cache if
     * possible in the current environment.
     * @param string $path The absolute filepath of the save location.
     * @return bool Returns true if the save attempt was successful, false otherwise.
     */
    public function save( $path );

    /**
     * <Lexicon Index Cache Load Versions Method>
     * Loads all existing versions of an existing LexiconIndex from disk,
     * if any entries currently exist. If any do, it will return a collection
     * of the cached LexiconIndexes. If not, it will return false,
     * indicating that no previous versions have been saved.
     * @param string $key
     * @param string $path
     * @return bool|\oroboros\collection\interfaces\contract\CollectionContract
     */
    public static function versions( $key, $path );

    /**
     * <Lexicon Index Cache Load Method>
     * Loads an existing LexiconIndex from disk, if an entry currently exists.
     * If one does, it will return the cached LexiconIndex. If not,
     * it will return false, indicating that full instantiation is necessary.
     * @param string $key
     * @param string $path
     * @return bool|\oroboros\codex\interfaces\contract\LexiconIndexContract
     */
    public static function load( $key, $path );

    /**
     * <Lexicon Index Validation Method>
     * Validates a set of data to see if a valid LexiconIndex can be created from it.
     * @param array $data
     * @return bool Returns true if the data can successfully create a valid LexiconIndex, false otherwise.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the provided parameter is not an array
     */
    public static function validate( $data );

    /**
     * <Lexicon Index Last Update Getter>
     * Returns the timestamp that the LexiconIndex was last updated,
     * or null if it has not been recorded.
     * @return string|null
     */
    public function getLastUpdate();

    /**
     * <Lexicon Index Last Update Getter>
     * Returns the identifying key of the LexiconIndex.
     * @return string
     */
    public function getKey();

    /**
     * <Lexicon Index Serialize Method>
     * This will instruct PHP to only serialize
     * parameters that are unique to the entry itself.
     * @return string
     */
    public function serialize();

    /**
     * <Lexicon Index Unserialize Method>
     * Provides a means to restore a saved LexiconIndex from a lightweight
     * string to full functionality. Only the details required to restore the
     * the Lexicon to it's original unique state are reflected, and all others
     * are lazy loaded on request.
     * @param string $serialized
     * @return void
     */
    public function unserialize( $serialized );

    /**
     * <Lexicon Entry List Method>
     * Provides a list of the keys associated with all classes, traits, interfaces,
     * or functions in its collection of assets.
     * @return array
     */
    public function listEntries();

    /**
     * <Lexicon Entry Check Method>
     * Checks if the index has a specific class, trait, interface,
     * or function in its collection of assets.
     * @param string $name the fully qualified name (including namespace), or slug keyword associated with the LexiconEntry
     * @return bool
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if the given parameter is not a string
     */
    public function checkEntry( $name );

    /**
     * <Lexicon Entry Getter Method>
     * Fetches an instance of the LexiconEntry associated with the given name from the LexiconIndex
     * @param string $name the fully qualified name (including namespace), or slug keyword associated with the LexiconEntry
     * @return \oroboros\codex\interfaces\contract\LexiconEntryContract
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If an invalid LexiconEntry is passed or the key is not a string
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex if an unknown key is passed
     */
    public function pullEntry( $name );

    /**
     * <Lexicon Entry Setter Method>
     * Adds a new resource to the index.
     * @param string $name the fully qualified name (including namespace), or slug keyword associated with the LexiconEntry
     * @param \oroboros\codex\interfaces\contract\LexiconEntryContract $entry The LexiconEntry instance representing the given asset
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If an invalid LexiconEntry is passed or the key is not a string
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex is in read-only status
     */
    public function pushEntry( $name, $entry );

    /**
     * <Package Getter Method>
     * Gets the name of the package, subpackage, category, or subcategory represented by the LexiconIndex.
     * @return string|bool Returns false if no package name is set, otherwise returns the package name
     */
    public function getPackage();

    /**
     * <Package Setter Method>
     * Sets a name for the package, subpackage, category, or subcategory represented by the LexiconIndex
     * @param string $package the slug name of the package, subpackage, category, or subcategory
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if an invalid package name is passed
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex is in read-only status
     */
    public function setPackage( $package );

    /**
     * <Api Getter Method>
     * Fetches the name of the api associated with the LexiconIndex.
     * This will either be the namespace of an ApiInterface, or the
     * disk location of a package json or composer.json file.
     *
     * This method will return false if no api is associated
     * with the LexiconIndex.
     *
     * @return string|bool Either the namespace of the ApiInterface or the disk location of the source file, or false if no api is associated
     */
    public function getApi();

    /**
     * <Api Setter Method>
     * Sets an api source for the LexiconIndex. The source can be synchronized
     * with the Api, and if the Api is not locked, it can be automatically
     * updated also.
     * @param string $api Either the namespace of the ApiInterface or the disk location of the source file
     * @param string $type api|package|subpackage|composer
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if the provided api is not a string, or is not a valid api type
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex is in read-only status, or the given api type is not yet implemented
     */
    public function setApi( $api, $type = 'api' );

    /**
     * <Api Type Getter Method>
     * Fetches the type of api associated with the LexiconIndex.
     *
     * This method will return false if no api is associated
     * with the LexiconIndex.
     *
     * @return string|bool api|package|subpackage|composer, or false if no api is associated
     */
    public function getApiType();

    /**
     * <Api Lock Method>
     * Sets the lock status of the Lexicon Index to true, putting it into a read-only state.
     * The Lexicon Index and all further archives of it will be updated to be read-only until it is unlocked.
     * This can also be written into exports, making all further copies of the Lexicon Entry
     * loaded elsewhere also locked in a read-only state.
     *
     * This method will return a signature key for the Lexicon Index that can
     * be used to unlock it. This is done using a bcrypt hashing algorithm,
     * so no discernable details of the signature are stored in exports or
     * archives. The original developer can unlock any copy of the
     * Lexicon Entry with the provided key.
     *
     * @param string $signature (optional) An existing key may be provided to allow for a developer to lock multiple Apis under the same key. If this is not provided, a unique key will be generated for each api.
     * @return string This method will return a signature that can be used to unlock the Lexicon entry. If this is not retained, it will be permanently locked. unless it is purged and entirely rebuilt.
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException Not yet implemented
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex is in read-only status
     */
    public function lockApi( $signature = null );

    /**
     * <Api Unlock Method>
     * Sets the lock status of the Lexicon Index to false, allowing the
     * Lexicon Index to be edited further. This can only be done with the
     * provided key that was given when it was originally locked.
     *
     * @param string $signature (optional) An existing key may be provided to allow for a developer to lock multiple Apis under the same key. If this is not provided, a unique key will be generated for each api.
     * @return void
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException Not yet implemented
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex is in read-only status
     */
    public function unlockApi( $signature );

    /**
     * <Api Import Method>
     * Automatically generates a fully populated LexiconIndex to
     * represent a package or subpackage. Supports ApiInterfaces,
     * Oroboros package json files, and composer.json files.
     * @param string $source Either the namespace of the ApiInterface or the disk location of the source file if it is a composer.json or package json file
     * @param string $type api|package|subpackage|composer
     * @return \oroboros\codex\interfaces\contract\LexiconIndexContract
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException If an unimplemented method is called
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the api could not be parsed correctly.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If an invalid parameter is passed
     */
    public static function import( $source, $type = 'api' );

    /**
     * <Lexicon Index Export Method>
     * Exports the Lexicon Index into a production format,
     * which can be either an ApiInterface, package json file, or composer.json.
     *
     * Subpackages will export a package as if they were a primary package.
     * This is to enable quickly parting out subpackages into individual
     * deliverables as needed. If you would like the subpackage to be reflected
     * in the api, export from the parent package instead.
     *
     * @param array $options To be determined
     * @return string The namespace of the generated ApiInterface, or the disk location of the package json or composer.json
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException Not yet implemented
     */
    public function export( $options = array() );

    /**
     * <Lexicon Merge Method>
     * This method will merge another given LexiconIndex with the current object,
     * and return a new LexiconIndex that contains the full collection of both combined.
     * In the event of conflicting entries, the newer entry will be trusted.
     *
     * This method treats both current LexiconIndex objects as immutable,
     * and will not modify either, but instead will return a new
     * third LexiconIndex reflecting the changes.
     *
     * @param string $new_key the identifying key to issue to the new LexiconIndex
     * @param \oroboros\codex\interfaces\contract\LexiconIndexContract $index
     * @return \oroboros\codex\interfaces\contract\LexiconIndexContract
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException Not yet implemented
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex is in read-only status
     */
    public function merge( $new_key, $index );

    /**
     * <Lexicon Index Subpackage Setter Method>
     * Sets another LexiconIndex as a subpackage of the current LexiconIndex.
     * The current LexiconIndex will register itself as a dependency of the
     * subpackage in the process.
     * @param \oroboros\codex\interfaces\contract\LexiconIndexContract $index
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the supplied index is not a LexiconIndex
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex is in read-only status
     */
    public function setSubpackage( $index );

    /**
     * <Lexicon Index Subpackage Remove Method>
     * Removes a subpackage of the current LexiconIndex.
     * The current LexiconIndex will register itself as a dependency of the
     * subpackage in the process.
     *
     * @param \oroboros\codex\interfaces\contract\LexiconIndexContract $index
     * @return void
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException Not yet implemented
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex is in read-only status
     */
    public function removeSubpackage( $index );

    /**
     * <Lexicon Index Dependency Setter Method>
     * Adds another LexiconIndex as a dependency of the current one.
     * @param \oroboros\codex\interfaces\contract\LexiconIndexContract $index
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If an invalid LexiconEntry is passed
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex is in read-only status
     */
    public function setDependency( $index );

    /**
     * <Lexicon Index Dependency Delete Method>
     * Deletes a dependency of the current one.
     * @param string $index The identifying package of the dependency LexiconIndex, or the LexiconIndex itself to extract the package from
     * @return void
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException If the dependency is indirect, and cannot be removed without also removing the direct dependency (or chain of dependency leading to the specified dependent)
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex is in read-only status
     */
    public function removeDependency( $key );

    /**
     * <Lexicon Index Dependency Check Method>
     * Returns a boolean determination as to whether the LexiconIndex
     * has dependencies.
     * @return bool
     */
    public function hasDependencies();

    /**
     * <Lexicon Index Dependent Check Method>
     * Returns a boolean determination as to whether the LexiconIndex
     * depends on a specific dependency.
     * @param string $key The identifying package of the dependency to check.
     * @return bool
     */
    public function hasDependency( $key );

    /**
     * <Lexicon Index Dependency Getter Method>
     * Returns a collection of the existing dependencies of the LexiconIndex.
     * @return \oroboros\collection\interfaces\contract\CollectionContract
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException Not yet implemented
     */
    public function listDependencies();

    /**
     * <Lexicon Index Tag Setter Method>
     * Sets a searchable tag on the LexiconIndex.
     * These most often correspond to DocBlock parameters that
     * should be included in the Api or classes within it.
     * @param string $tag
     * @param string $value
     * @return void
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the tag already exists
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex is in read-only status
     */
    public function tag( $tag, $value );

    /**
     * <Lexicon Index Tag Update Method>
     * Alters the value of an existing tag.
     * @param string $tag
     * @param string $value
     * @return void
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the tag does not exist
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex is in read-only status
     */
    public function updateTag( $tag, $value );

    /**
     * <Lexicon Index Tag Delete Method>
     * Deletes an existing tag from the Lexicon Index.
     * @param type $tag
     * @return void
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex is in read-only status
     */
    public function deleteTag( $tag );

    /**
     * <Lexicon Index Tag Getter Method>
     * Gets a collection of the existing tags on the LexiconIndex.
     * @param type $tag
     * @return \oroboros\collection\interfaces\contract\CollectionContract
     */
    public function getTags();
}
