<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\codex\abstracts;

/**
 * <Abstract Codex Index>
 * Provides a set of methods for indexing a set of related
 * codex entries that have a related scope of focus.
 * @author Brian Dayhoff <mopsyd@me.com>
 */
abstract class AbstractCodexIndex
    extends \oroboros\core\abstracts\libraries\AbstractLibrary
    implements \oroboros\codex\interfaces\contract\CodexIndexContract
{

    use \oroboros\codex\traits\CodexIndexTrait;

    const OROBOROS_CLASS_TYPE = \oroboros\core\interfaces\enumerated\type\LibraryClassTypes::CLASS_TYPE_LIBRARY_CODEX;
    const OROBOROS_CLASS_SCOPE = \oroboros\core\interfaces\enumerated\scope\LibraryClassScopes::CLASS_SCOPE_LIBRARY_CODEX_INDEX;
    const OROBOROS_API = '\\oroboros\\codex\\interfaces\\api\\CodexApi';

}
