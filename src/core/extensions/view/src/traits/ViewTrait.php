<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\view\traits;

/**
 * <View Trait>
 * Provides the methods required to satisfy the ViewContract
 * @note This trait needs some refactoring and better abstraction,
 * because of the following problems inherited from it's original representation:
 * -Direct reliance on class constants in the implementing class (because it used to be a class, and did have these)
 * -Factor out the opinion. This representation ASSUMES http (which was appropriate in its original expression), and this MUST be factored out before it is offered as a concrete construct in release.
 * -Some opinion here is unavoidable, so it should be as follows: plaintext, utf-8 by default (with a means to change encoding without further extension of the trait), null headers (so it does not disrupt either cli or http, which both inherit from this), null template (it will use one that does nothing to satisfy the requirement, but SHALL NOT mutate content in any way from it's direct representation, EXCEPT to make it scalar if it is not, so that it may be printed efficiently)
 * -Factor the logic down to private methods, and factor the auth/validation to protected methods, as per the build specs
 * -As this is a base level abstraction, it MAY be required to provide some nulled methods that clear checks for http that may occur, but they MUST effect no operation in THIS view (child traits of this one may enact them at their discretion).
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage
 * @version
 * @since
 */
trait ViewTrait
{

    use \oroboros\core\traits\patterns\structural\ControlApiTrait;

    private $_valid_output_modes = array(
        'null' => \oroboros\core\interfaces\enumerated\flags\OutputFlags::FLAG_OUTPUT_NULL,
        'default' => \oroboros\core\interfaces\enumerated\flags\OutputFlags::FLAG_OUTPUT_DEFAULT,
        'plaintext' => \oroboros\core\interfaces\enumerated\flags\OutputFlags::FLAG_OUTPUT_PLAINTEXT,
        'cgi' => \oroboros\core\interfaces\enumerated\flags\OutputFlags::FLAG_OUTPUT_PLAINTEXT_CGI,
        'console' => \oroboros\core\interfaces\enumerated\flags\OutputFlags::FLAG_OUTPUT_PLAINTEXT_CGI_CONSOLE,
        'cron' => \oroboros\core\interfaces\enumerated\flags\OutputFlags::FLAG_OUTPUT_PLAINTEXT_CGI_CRON,
        'robots' => \oroboros\core\interfaces\enumerated\flags\OutputFlags::FLAG_OUTPUT_PLAINTEXT_ROBOTS,
        'html' => \oroboros\core\interfaces\enumerated\flags\OutputFlags::FLAG_OUTPUT_HTML,
        'css' => \oroboros\core\interfaces\enumerated\flags\OutputFlags::FLAG_OUTPUT_CSS,
        'sass' => \oroboros\core\interfaces\enumerated\flags\OutputFlags::FLAG_OUTPUT_CSS_SASS,
        'less' => \oroboros\core\interfaces\enumerated\flags\OutputFlags::FLAG_OUTPUT_CSS_LESS,
        'javascript' => \oroboros\core\interfaces\enumerated\flags\OutputFlags::FLAG_OUTPUT_JAVASCRIPT,
        'javascript-prettified' => \oroboros\core\interfaces\enumerated\flags\OutputFlags::FLAG_OUTPUT_JAVASCRIPT_PRETTIFIED,
        'javascript-prettified-debug' => \oroboros\core\interfaces\enumerated\flags\OutputFlags::FLAG_OUTPUT_JAVASCRIPT_PRETTIFIED_DEBUG,
        'javascript-minified' => \oroboros\core\interfaces\enumerated\flags\OutputFlags::FLAG_OUTPUT_JAVASCRIPT_MINIFIED,
        'javascript-minified-debug' => \oroboros\core\interfaces\enumerated\flags\OutputFlags::FLAG_OUTPUT_JAVASCRIPT_MINIFIED_DEBUG,
        'email' => \oroboros\core\interfaces\enumerated\flags\OutputFlags::FLAG_OUTPUT_EMAIL,
        'email-plaintext' => \oroboros\core\interfaces\enumerated\flags\OutputFlags::FLAG_OUTPUT_EMAIL_PLAINTEXT,
        'email-html' => \oroboros\core\interfaces\enumerated\flags\OutputFlags::FLAG_OUTPUT_EMAIL_HTML,
        'xml' => \oroboros\core\interfaces\enumerated\flags\OutputFlags::FLAG_OUTPUT_XML,
        'rss' => \oroboros\core\interfaces\enumerated\flags\OutputFlags::FLAG_OUTPUT_XML_RSS,
        'sitemap' => \oroboros\core\interfaces\enumerated\flags\OutputFlags::FLAG_OUTPUT_XML_SITEMAP,
        'sql' => \oroboros\core\interfaces\enumerated\flags\OutputFlags::FLAG_OUTPUT_SQL,
        'csv' => \oroboros\core\interfaces\enumerated\flags\OutputFlags::FLAG_OUTPUT_CSV,
        'img' => \oroboros\core\interfaces\enumerated\flags\OutputFlags::FLAG_OUTPUT_IMG,
        'jpg' => \oroboros\core\interfaces\enumerated\flags\OutputFlags::FLAG_OUTPUT_IMG_JPG,
        'jpeg' => \oroboros\core\interfaces\enumerated\flags\OutputFlags::FLAG_OUTPUT_IMG_JPEG,
        'gif' => \oroboros\core\interfaces\enumerated\flags\OutputFlags::FLAG_OUTPUT_IMG_GIF,
        'png' => \oroboros\core\interfaces\enumerated\flags\OutputFlags::FLAG_OUTPUT_IMG_PNG,
        'svg' => \oroboros\core\interfaces\enumerated\flags\OutputFlags::FLAG_OUTPUT_IMG_SVG,
        'mp3' => \oroboros\core\interfaces\enumerated\flags\OutputFlags::FLAG_OUTPUT_AUDIO_MP3,
        'aiff' => \oroboros\core\interfaces\enumerated\flags\OutputFlags::FLAG_OUTPUT_AUDIO_AIFF,
        'wav' => \oroboros\core\interfaces\enumerated\flags\OutputFlags::FLAG_OUTPUT_AUDIO_WAV,
        'mp4' => \oroboros\core\interfaces\enumerated\flags\OutputFlags::FLAG_OUTPUT_VIDEO_MP4,
    );
    private $_output_mode;
    private $_headers = array();
    private $_meta = array();
    private $_css = array();
    private $_scripts = array();
    private $_fonts = array();
    private $_content = array();
    private $_base_url = \oroboros\Oroboros::REQUEST_URL;

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies ...
     *
     * @execution Default Execution Plan (minimal)
     *
     * @execution Default Execution Plan (commented)
     *
     * -------------------------------------------------------------------------
     */
//    public function __construct( $params = null, $flags = null )
//    {
//        parent::__construct( $params, $flags );
//        ob_start();
//    }
//
//    public function __destruct()
//    {
//        parent::__destruct();
//        print ob_get_clean();
//    }
//
//    public function initialize( $params = null, $flags = null )
//    {
//        parent::initialize( $params, $flags );
//        if ( !\oroboros\core\is_cli() )
//        {
//            //web request setup
//            $headers = \apache_request_headers();
//            $this->_setParam( 'request_headers', $headers );
//        } else
//        {
//            //command line setup
//            $this->_setParam( 'request_headers', FALSE );
//        }
//    }

//    public function redirect( $location, $internal = 1 )
//    {
//        header( "Location: " . (($internal))
//                ? $this->_toUrl( $location )
//                : $location  );
//    }
//
//    public function render( $params = null, $flags = null )
//    {
//        if ( !\oroboros\core\is_cli() )
//        {
//            $this->_processHeaders();
//        }
//        if ( $this->_checkFlag( self::FLAG_TYPE_OPTIONS ) || $this->_checkFlag( self::FLAG_TYPE_HEAD ) )
//        {
//            return;
//        }
//        $this->_buildContent( $params, $flags );
//    }
//
//    public function reset( $params = null, $flags = null )
//    {
//        $this->_headers = array(
//            'Content-Type' => self::CONTENT_TYPE );
//        $this->_meta = array();
//        $this->_content = array();
//        $this->_css = array();
//        $this->_scripts = array();
//        $this->_fonts = array();
//    }
//
//    /**
//     * -------------------------------------------------------------------------
//     * Extension Methods (protected)
//     *
//     * These methods may be extended by inheriting constructs as needed.
//     * They represent the interal api.
//     * -------------------------------------------------------------------------
//     */
//    protected function _buildContent( $params = null, $flags = null )
//    {
//
//    }
//
//    protected function _resetHeaders()
//    {
//        $this->_headers = array(
//            'Content-Type' => self::CONTENT_TYPE );
//    }
//
//    protected function _setHeaders( array $headers )
//    {
//        foreach ( $headers as
//            $title =>
//            $value )
//        {
//            $this->_headers[$title] = $value;
//        }
//    }
//
//    protected function _processHeaders()
//    {
//        foreach ( $this->_headers as
//            $title =>
//            $value )
//        {
//            header( $title . '="' . $value . '"' );
//        }
//    }
//
//    protected function _toUrl( $filepath )
//    {
//        $basepath = realpath( dirname( \oroboros\OroborosInterface::OROBOROS_BASEPATH ) ) . DIRECTORY_SEPARATOR;
//        return str_replace( $basepath, $this->_base_url . '/', $filepath );
//    }
//
//    protected function _updateParameterConditions( $parameter_key )
//    {
//        $name = str_replace( ' ', NULL,
//            ucwords( str_replace( '-', ' ', $parameter_key ) ) );
//        if ( method_exists( $this, '_reset' . $name ) )
//        {
//            $this->{'_reset' . $name}();
//        }
//        if ( method_exists( $this, '_set' . $name ) )
//        {
//            $this->{'_set' . $name}( $this->_getParam( $parameter_key ) );
//        }
//    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */
    //private methods
}
