<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\message\abstracts;

/**
 * <Abstract Message>
 * Provides baseline methods for interacting with message streams.
 * @author Brian Dayhoff <mopsyd@me.com>
 * @category abstracts
 * @package oroboros/core
 * @subpackage psr7
 * @version 0.2.4
 * @since 0.2.4-alpha
 */
abstract class AbstractMessage
    extends AbstractStream
    implements \oroboros\message\interfaces\contract\MessageContract
{

    use \oroboros\message\traits\MessageTrait;

    const OROBOROS_CLASS_TYPE = \oroboros\core\interfaces\enumerated\type\LibraryClassTypes::CLASS_TYPE_LIBRARY_MESSAGE;
    const OROBOROS_CLASS_SCOPE = \oroboros\core\interfaces\enumerated\scope\LibraryClassScopes::CLASS_SCOPE_LIBRARY_MESSAGE_ABSTRACT;
    const OROBOROS_API = '\\oroboros\\message\\interfaces\\MessageApi';

}
