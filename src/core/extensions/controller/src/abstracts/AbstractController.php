<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\controller\abstracts;

/**
 * <Oroboros Abstract Controller>
 * @author Brian Dayhoff <brian@mopsyd.me>
 * @since 0.0.2a
 */
abstract class AbstractController
    extends \oroboros\core\abstracts\AbstractBase
    implements \oroboros\controller\interfaces\contract\ControllerContract
{

    use \oroboros\controller\traits\ControllerTrait
    {
        \oroboros\controller\traits\ControllerTrait::_getException insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\controller\traits\ControllerTrait::_getExceptionCode insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\controller\traits\ControllerTrait::_getExceptionCodeIdentifiers insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\controller\traits\ControllerTrait::_getExceptionIdentifier insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\controller\traits\ControllerTrait::_getExceptionIdentifiers insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\controller\traits\ControllerTrait::_getExceptionMessage insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\controller\traits\ControllerTrait::_recastException insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\controller\traits\ControllerTrait::_throwException insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\controller\traits\ControllerTrait::_validate insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\controller\traits\ControllerTrait::_validationModes insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\controller\traits\ControllerTrait::_baselineInternalsCheckValid insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\controller\traits\ControllerTrait::_baselineInternalsMergeArrayDefaults insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\controller\traits\ControllerTrait::_baselineInternalsThrowInvalidException insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\controller\traits\ControllerTrait::_baselineInternalsValidateValues insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\controller\traits\ControllerTrait::_baselineInternalsCheckConfiguration insteadof \oroboros\core\traits\core\StaticBaselineTrait;
    }
    use \oroboros\core\traits\core\StaticBaselineTrait;

    const OROBOROS_CLASS_TYPE = \oroboros\core\interfaces\enumerated\type\ClassType::CLASS_TYPE_CONTROLLER;
    const OROBOROS_CLASS_SCOPE = \oroboros\core\interfaces\enumerated\scope\ClassScope::CLASS_SCOPE_CONTROLLER_ABSTRACT;
    const OROBOROS_CLASS_CONTEXT = \oroboros\core\interfaces\enumerated\context\ClassContext::CLASS_CONTEXT_CONTROLLER;
    const OROBOROS_CONTROLLER = true;

}
