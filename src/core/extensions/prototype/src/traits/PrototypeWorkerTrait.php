<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\prototype\traits;

/**
 * <Prototyper Worker Trait>
 * Provides a prototyper that is also a worker that can be
 * registered with a manager/director, and prototypical cleanup,
 * garbage collection, and instantiation setup to the standard
 * underlying prototyper logic.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @category prototype
 * @package oroboros/core
 * @subpackage prototype
 * @version 0.2.5
 * @since 0.2.5
 * @see \oroboros\core\traits\patterns\creational\PrototyperTrait
 * @satisfies \oroboros\core\interfaces\contract\libraries\prototype\PrototyperContract
 */
trait PrototypeWorkerTrait
{

    use \oroboros\core\traits\patterns\creational\PrototyperTrait;
    use \oroboros\core\traits\patterns\creational\PrototypicalTrait;
    use \oroboros\core\traits\patterns\behavioral\WorkerTrait
    {
        \oroboros\core\traits\patterns\behavioral\WorkerTrait::initialize insteadof \oroboros\core\traits\patterns\creational\PrototyperTrait;
        \oroboros\core\traits\patterns\behavioral\WorkerTrait::initialize as private prototyper_initialize;
    }

    /**
     * Represents the slug identifiers of prototyped objects.
     * @var \oroboros\collection\Collection
     */
    private $_prototyper_prototypical_slugs;

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\core\interfaces\contract\libraries\prototype\PrototyperContract
     *
     * -------------------------------------------------------------------------
     */
    public function initialize( $params = null, $dependencies = null,
        $flags = null )
    {
        $this->_baselineSetParameterPersistence( true );
        $this->_baselineSetDependencyPersistence( true );
        $this->_baselineSetFlagPersistence( true );
        $this->_registerPrototypeTask( '_prototypeWorkerCleanupClone' );
        $this->_baselineRegisterInitializationTask( '_prototypeWorkerPostInitialization' );
        $this->_baselineRegisterDestructorTask( '_prototypeWorkerDestructorGarbageCollection' );
        $this->prototyper_initialize( $params, $dependencies, $flags );
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */
    //protected methods

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * Insures that the cloned copy does not have any hanging data that
     * is not relevant to its expected operation.
     *
     * This method automatically fires when any clone operation occurs.
     *
     * @return void
     * @internal
     */
    private function _prototypeWorkerCleanupClone()
    {
        $this->_object_fingerprint = null;
        $this->_baselineGetObjectFingerprint();
    }

    /**
     * Performs the post initialization operations for the prototyper.
     *
     * This method automatically fires immediately after initialization.
     *
     * @return void
     * @internal
     */
    private function _prototypeWorkerPostInitialization()
    {
        if ( is_null( $this->_prototyper_prototypical_slugs ) )
        {
            $this->_prototyper_prototypical_slugs = new \oroboros\collection\Collection();
        }
        $this->_init( $this->_baseline_parameters, $this->_baseline_flags );
    }

    /**
     * Releases memory and dependent resources accrued during
     * the lifecycle of the object.
     *
     * This method automatically fires when the destructor is called.
     *
     * @return void
     * @internal
     */
    private function _prototypeWorkerDestructorGarbageCollection()
    {

    }

}
