<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\stream\traits;

/**
 * <Directory Stream Trait>
 * Provides methods for creating a valid stream resource object that
 * can be integrated into PHP internals directly.
 *
 * This trait provides methods to produce a stream that interacts with a directory.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage streams
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\stream\interfaces\contract\DirectoryStreamContract
 */
trait DirectoryStreamTrait
{

    use StreamTrait;

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\stream\interfaces\contract\DirectoryStreamContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Stream Resource Closedir>
     * This method is called in response to closedir().
     *
     * Any resources which were locked, or allocated,
     * during opening and use of the directory stream
     * should be released.
     *
     * @return bool Returns TRUE on success or FALSE on failure.
     */
    public function dir_closedir()
    {

    }

    /**
     * <Stream Resource Opendir>
     * This method is called in response to opendir().
     *
     * @param string $path Specifies the URL that was passed to opendir().
     * @param int $options Whether or not to enforce safe_mode (0x04).
     * @return bool Returns TRUE on success or FALSE on failure.
     */
    public function dir_opendir( $path, $options )
    {

    }

    /**
     * <Stream Resource Readdir>
     * This method is called in response to readdir().
     *
     * @return string|bool Should return string representing the next filename, or FALSE if there is no next file.
     */
    public function dir_readdir()
    {

    }

    /**
     * <Stream Resource Rewinddir>
     * This method is called in response to rewinddir().
     *
     * @return bool Returns TRUE on success or FALSE on failure.
     */
    public function dir_rewinddir()
    {

    }

    /**
     * <Stream Resource Rewinddir>
     * This method is called in response to mkdir().
     *
     * In order for the appropriate error message to be returned
     * this method should not be defined if the wrapper does not
     * support creating directories.
     *
     * Emits E_WARNING if call to this method fails (i.e. not implemented).
     *
     * @param string $path Directory which should be created.
     * @param int $mode The value passed to mkdir().
     * @param int $options A bitwise mask of values, such as STREAM_MKDIR_RECURSIVE.
     * @return bool Returns TRUE on success or FALSE on failure.
     */
    public function mkdir( $path, $mode, $options )
    {

    }

    /**
     * <Stream Resource Rename>
     * This method is called in response to rename().
     *
     * Should attempt to rename $path_from to $path_to
     *
     * In order for the appropriate error message to be returned
     * this method should not be defined if the wrapper does not
     * support renaming files.
     *
     * Emits E_WARNING if call to this method fails (i.e. not implemented).
     *
     * @param string $path_from The URL to the current file.
     * @param string $path_to The URL which the path_from should be renamed to.
     * @return bool Returns TRUE on success or FALSE on failure.
     */
    public function rename( $path_from, $path_to )
    {

    }

    /**
     * <Stream Resource Rmdir>
     * This method is called in response to rmdir().
     *
     * In order for the appropriate error message to be returned
     * this method should not be defined if the wrapper does not
     * support removing directories.
     *
     * Emits E_WARNING if call to this method fails (i.e. not implemented).
     *
     * @param string $path The directory URL which should be removed.
     * @param int $options A bitwise mask of values, such as STREAM_MKDIR_RECURSIVE.
     * @return bool Returns TRUE on success or FALSE on failure.
     */
    public function rmdir( $path, $options )
    {

    }

    /**
     * <Stream Resource Metadata Setter>
     * This method is called to set metadata on the stream.
     * It is called when one of the following functions is
     * called on a stream URL:
     *
     * * touch()
     * * chmod()
     * * chown()
     * * chgrp()
     *
     * Please note that some of these operations may not be available on your system.
     *
     * $option will be one of:
     *
     * * STREAM_META_TOUCH (The method was called in response to touch())
     * * STREAM_META_OWNER_NAME (The method was called in response to chown() with string parameter)
     * * STREAM_META_OWNER (The method was called in response to chown())
     * * STREAM_META_GROUP_NAME (The method was called in response to chgrp())
     * * STREAM_META_GROUP (The method was called in response to chgrp())
     * * STREAM_META_ACCESS (The method was called in response to chmod())
     *
     * $value will differ based on the value of $option:
     *
     * * STREAM_META_TOUCH: Array consisting of two arguments of the touch() function.
     * * STREAM_META_OWNER_NAME or STREAM_META_GROUP_NAME: The name of the owner user/group as string.
     * * STREAM_META_OWNER or STREAM_META_GROUP: The value owner user/group argument as integer.
     * * STREAM_META_ACCESS: The argument of the chmod() as integer.
     *
     * @param string $path The file path or URL to set metadata. Note that in the case of a URL, it must be a :// delimited URL. Other URL forms are not supported.
     * @param int $option See comment above
     * @param mixed $value See comment above
     * @return bool Returns TRUE on success or FALSE on failure.
     */
    public function stream_metadata( $path, $option, $value )
    {

    }

    /**
     * <Stream Resource Open>
     * This method is called immediately after the wrapper
     * is initialized (f.e. by fopen() and file_get_contents()).
     *
     * The streamWrapper::$context property is updated if a valid context is passed to the caller function.
     *
     * Emits E_WARNING if call to this method fails (i.e. not implemented).
     *
     * @param string $path Specifies the URL that was passed to the original function.
     * @param string $mode The mode used to open the file, as detailed for fopen().
     * @param int $options Holds additional flags set by the streams API. It can hold one or more of the following values OR'd together.
     * @param string &$opened_path If the path is opened successfully, and STREAM_USE_PATH is set in options, opened_path should be set to the full path of the file/resource that was actually opened.
     * @return bool Returns TRUE on success or FALSE on failure.
     */
    public function stream_open( $path, $mode, $options, &$opened_path )
    {

    }

    /**
     * <Stream Resource Unlink>
     * This method is called in response to unlink().
     *
     * In order for the appropriate error message to be returned
     * this method should not be defined if the wrapper does not
     * support removing files.
     *
     * Emits E_WARNING if call to this method fails (i.e. not implemented).
     *
     * @note The streamWrapper::$context property is updated if a valid context is passed to the caller function.
     * @param string $path The file URL which should be deleted.
     * @return bool Returns TRUE on success or FALSE on failure.
     */
    public function unlink( $path )
    {

    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */
    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */
}
