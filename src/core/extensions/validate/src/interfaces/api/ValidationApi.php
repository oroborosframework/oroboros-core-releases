<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\validate\interfaces\api;

/**
 * <Validation Api Interface>
 * This interface represents the root level validation api interface.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/extensions/validation.md
 * @category api-interfaces
 * @package oroboros/validation
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.5
 */
interface ValidationApi
extends \oroboros\core\interfaces\api\utilities\UtilityApi
{

    /**
     * -------------------------------------------------------------------------
     *                          Api Details
     * -------------------------------------------------------------------------
     */

    /**
     * Designates the type of this Api Interface, and what realm of
     * responsibilities it is classified as.
     */
    const API_TYPE = 'core';

    /**
     * Determines the focused goal within the api type.
     * The Api Scope reveals the underlying goal of this specific Api Interface,
     * and what the specific purpose of this collection of classes, traits,
     * and interfaces is meant to collectively accomplish.
     */
    const API_SCOPE = 'validation';

    /**
     * Designates a namespace provided by the package if one exists.
     * Packages that are distributed as a sub-package of a
     * larger parent package should still declare this constant,
     * but set its value to false.
     */
    const API_PROVIDES_NAMESPACE = 'oroboros\\validate';

    /**
     * Designates a parent package or namespace that this Api adheres to.
     * This value should be false if no such parent exists,
     * but this constant should still be declared.
     */
    const API_PARENT_NAMESPACE = 'oroboros\\core';

    /**
     * Designates the codex index for this Api, which can be used to reference
     * this collection of classes, traits, and interfaces as a related family
     * working toward a specific goal.
     *
     * This value should always be provided for any valid Api Interface.
     */
    const API_CODEX = 'validate';

    /**
     * Designates the map file for determining sub-packages and parent packages,
     * which is parsed by the Codex. This should be false if the package does not
     * provide a package map. If this constant is not defined or the file cannot be
     * found from the package root, then the package will contain an incomplete
     * Codex assignment.
     *
     * This value should always be provided for any valid Api Interface.
     */
    const API_PACKAGE_MAP = false;

    /**
     * Designates the package name of the api.
     * All packages must declare a package name to be considered valid.
     *
     * This value should always be provided for any valid Api Interface.
     */
    const API_PACKAGE = 'oroboros/validate';

    /**
     * Designates the parent package of the package, if one exists,
     * which is parsed by the Codex. This should be false if the package does not
     * have a parent package. If this constant is not defined or the file cannot be
     * found from the package root, then the package will be assumed not to have
     * a parent package by the Codex.
     *
     * This value should always be provided for any valid Api Interface.
     */
    const API_PARENT_PACKAGE = 'oroboros/core';

    /**
     * Designates the primary category of responsibility of the api.
     * All packages must declare some category to be considered valid,
     * which allows them to be indexed by the Codex in terms of relationship
     * to other packages.
     *
     * This value should always be provided for any valid Api Interface.
     */
    const API_CATEGORY = 'utility';

    /**
     * Designates the subcategory of responsibility of the api.
     * Packages may not need to declare a subcategory, but should provide
     * this constant as false if no subcategory exists. The subcategory
     * determines what realm of responsibility the package has within
     * a broader category.
     *
     * If this constant is omitted, a default value of false will be assumed.
     */
    const API_SUBCATEGORY = 'data-validation';

    /**
     * -------------------------------------------------------------------------
     *                          Class Declarations
     * -------------------------------------------------------------------------
     */

    /**
     * Designates the default Codex accessor class. This class provides a top level
     * api to Codex functionality for direct programmatic use.
     * This class may be used directly, and will be used by default within this
     * system where no override substitution is provided.
     * @extends \oroboros\codex\abstracts\AbstractCodex
     * @satisfies \oroboros\codex\interfaces\contract\CodexContract
     * @satisfies \oroboros\core\interfaces\contract\patterns\structural\StaticControlApi
     * @extends \oroboros\codex\abstracts\AbstractCodex
     * @final
     */
//    const CODEX_CONCRETE_CLASS = '\\oroboros\\codex\\Codex';

    /**
     * Designates the abstract class used to satisfy \oroboros\codex\interfaces\contract\CodexContract.
     * This class may be extended to satisfy the requirements of the CodexContract.
     * @uses \oroboros\codex\traits\CodexTrait
     * @satisfies \oroboros\codex\interfaces\contract\CodexContract
     * @satisfies \oroboros\core\interfaces\contract\patterns\structural\StaticControlApi
     * @abstract
     */
//    const CODEX_ABSTRACT_CLASS = '\\oroboros\\codex\\abstracts\\AbstractCodex';

    /**
     * This interface requires the internal expected methods required to construct a valid
     * and functional instance of the Codex, within the context of Oroboros Core.
     *
     * Any other class successfully implementing this contract can be substituted
     * interchangeably throughout this system, and can be designated to be used
     * internally entirely in place of the existing default.
     *
     * @extends \oroboros\core\interfaces\contract\libraries\LibraryContract
     */
//    const CODEX_CONTRACT = '\\oroboros\\codex\\interfaces\\contract\\CodexContract';

    /**
     * Designates the trait used to satisfy
     * \oroboros\codex\interfaces\contract\CodexContract.
     *
     * This trait provides all of the methods required to satisfy the contract
     * interface, and may be used on any class where it's methods are not
     * colliding in order to make it compliant with this contract.
     *
     * Any other class successfully using this trait without method collision can
     * implement the aforementioned contract without issue, making it a
     * recognizable internal substitution in this system without additional effort.
     *
     * @uses \oroboros\core\traits\libraries\patterns\structural\StaticControlApiTrait
     * @satisfies \oroboros\codex\interfaces\contract\CodexContract
     * @satisfies \oroboros\core\interfaces\contract\patterns\structural\StaticControlApi
     */
//    const CODEX_TRAIT = '\\oroboros\\codex\\traits\\CodexTrait';

    /**
     * Designates the trait used to satisfy
     * \oroboros\codex\interfaces\contract\CodexContract.
     *
     * This trait provides all of the methods required to satisfy the contract
     * interface, and may be used on any class where it's methods are not
     * colliding in order to make it compliant with this contract.
     *
     * Any other class successfully using this trait without method collision can
     * implement the aforementioned contract without issue, making it a
     * recognizable internal substitution in this system without additional effort.
     *
     * @uses \oroboros\core\traits\libraries\patterns\structural\StaticControlApiTrait
     * @satisfies \oroboros\codex\interfaces\contract\CodexContract
     * @satisfies \oroboros\core\interfaces\contract\patterns\structural\StaticControlApi
     */
//    const CODEX_EXTENSION = '\\oroboros\\codex\\CodexExtension';

}
