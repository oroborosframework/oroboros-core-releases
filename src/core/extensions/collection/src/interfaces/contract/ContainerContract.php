<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\collection\interfaces\contract;

/**
 * <Container Contract>
 * This interface enforces the methods required for a valid library object.
 * These methods may be manually honored, or may be satisfied by using the
 * corresponding library trait. Traits that extend upon this functionality
 * should be expected to implement this for you.
 *
 * Containers honor the Psr-11 specification.
 * This interface indicates that any class implementing
 * it is both an extension of the Psr-11 container
 * specification, and also a valid Oroboros library.
 *
 * @see \oroboros\collection\traitss\ContainerTrait
 * @see \oroboros\core\interfaces\api\libraries\containers\ContainerApi
 * @see \Psr\Container\ContainerInterface
 * @satisfies \oroboros\core\interfaces\enumerated\type\ClassType::CLASS_TYPE_CONTAINER
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
interface ContainerContract
extends \oroboros\core\interfaces\contract\libraries\LibraryContract,
 \Psr\Container\ContainerInterface
{

}
