<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\collection\traits;

/**
 * <Container Trait>
 * An implementation of \Psr\Container\ContainerInterface as a trait,
 * which can be bound to any class to fulfill the requirements of
 * the container interface.
 * @see http://www.php-fig.org/psr/psr-11/
 * @satisfies \Psr\Container\ContainerInterface
 * @satisfies \oroboros\collection\interfaces\contract\ContainerContract
 * @author Brian Dayhoff <mopsyd@me.com>
 */
trait ContainerTrait
{

    use \oroboros\core\traits\patterns\behavioral\RegistryTrait;
    use \oroboros\core\traits\libraries\LibraryTrait;

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \Psr\Container\ContainerInterface
     * @satisfies \oroboros\collection\interfaces\contract\ContainerContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Container Getter Method>
     * Finds an entry of the container by its identifier and returns it.
     *
     * @param string $id Identifier of the entry to look for.
     *
     * @throws NotFoundExceptionInterface  No entry was found for **this** identifier.
     * @throws ContainerExceptionInterface Error while retrieving the entry.
     *
     * @return mixed Entry.
     */
    public function get( $id )
    {
        return $this->_get( $id );
    }

    /**
     * <Container Key Check Method>
     * Returns true if the container can return an entry for the given identifier.
     * Returns false otherwise.
     *
     * `has($id)` returning true does not mean that `get($id)` will not throw an exception.
     * It does however mean that `get($id)` will not throw a `NotFoundExceptionInterface`.
     *
     * @param string $id Identifier of the entry to look for.
     *
     * @return bool
     */
    public function has( $id )
    {
        return $this->_check( $id );
    }

    /**
     * <Container Value Check Method>
     * Checks if the container has a specified value,
     * and returns true if the container has an instance of the value,
     * or false if no instances exist.
     *
     * This method will check the entire multidimensional structure of the
     * container, not just the base level set.
     *
     * If this returns true, the same value may be passed to getValueKey to
     * get the identifying dot separated identifier for the value without
     * raising a NotFoundException
     *
     * @param mixed $value
     *
     * @return bool
     */
    public function hasValue( $value )
    {
        return $this->_checkValue( $value );
    }

    public function getValueKey( $value )
    {

    }

    /**
     * <Container Leaf Node Getter Method>
     * Returns an associative array of all leaf nodes of the container's
     * multidimensional structure, where the key is the fully qualified dot
     * separated path to through the multidimensional structure, and the
     * value is the leaf value.
     *
     * This method's return value allows for safe iteration over every value
     * in the container as if it were a flat set.
     *
     * @return array
     */
    public function getLeaves()
    {
        return $this->_getLeaves();
    }

    /**
     * <Container Full Tree Getter Method>
     * Returns a flat indexed set of all valid dot separated keys in the
     * container, representing the full multidimensional structure of the
     * container expressed in dot separated notation. Every key in the return
     * set is a valid parameter for the get method.
     * @return array
     */
    public function getTree()
    {
        return $this->_getRegistryTree();
    }

    /**
     * <Container Subtree Check Method>
     * Takes a dot separated segment representing one or more keys in the
     * multidimensional structure of the container, and returns a boolean
     * determination as to whether the given segment exists in any of the
     * nested subkeys.
     *
     * If true, getSubtree can safely be called without raising a NotFoundException
     * for the same argument.
     *
     * @param scalar $segment Represents one or more multidimensional array key
     *     identifiers in dot separated notation, eg: key1.subkey1
     * @return bool
     * @throws \oroboros\core\utilities\exception\container\ContainerException
     *     If a non-scalar key is passed
     */
    public function hasSubtree( $segment )
    {
        return $this->_checkSubtree( $segment );
    }

    /**
     * <Container Subtree Getter Method>
     * Searches the container's multidimensional structure for a specified
     * segment using dot-separated notation. Returns a flat associative array
     * of matches where the key is the fully qualified dot separated key, and
     * the value is the value.
     * @param scalar $segment
     * @return array
     * @throws \oroboros\core\utilities\exception\container\ContainerException
     *     If a non-scalar segment is passed
     * @throws \oroboros\core\utilities\exception\container\NotFoundException
     *     If the specified subtree segment does not exist
     */
    public function getSubtree( $segment )
    {
        return $this->_getSubtree( $segment );
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * <Container Setter>
     * @param scalar $key The key to identify the value by.
     * @param mixed $value The container value to set.
     * @param bool $safe If true, will not overwrite existing values.
     *     Default false.
     * @return bool
     * @throws \oroboros\core\utilities\exception\container\ContainerException
     *     If a non-scalar value is passed.
     */
    protected function _set( $key, $value, $safe = false )
    {
        if ( !is_scalar( $key ) )
        {
            throw new \oroboros\core\utilities\exception\container\ContainerException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'scalar value', gettype( $key ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS );
        }
        $this->_registryInitialize();
        return $this->_setRegistryValue( $key, $value, $safe );
    }

    /**
     * <Container Getter>
     * Returns the requested value if it exists in the container.
     * @param scalar $key
     * @return mixed
     * @throws \oroboros\core\utilities\exception\container\ContainerException
     *     If a non-scalar value is passed, or any other problem occurs with
     *     retrieval.
     * @throws \oroboros\core\utilities\exception\container\NotFoundException
     *     If the value does not exist in the container.
     */
    protected function _get( $key )
    {
        if ( !is_scalar( $key ) )
        {
            throw new \oroboros\core\utilities\exception\container\ContainerException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'scalar value', gettype( $key ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS );
        }
        $this->_registryInitialize();
        if ( !$this->_checkRegistryKey( $key ) )
        {
            throw new \oroboros\core\utilities\exception\container\NotFoundException(
            sprintf( 'No entry was found for identifier [%s].', $key ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_PHP_KEY_NOT_FOUND
            );
        }
        try
        {
            return $this->_getRegistryValue( $key );
        } catch ( \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract $e )
        {
            throw new \oroboros\core\utilities\exception\container\ContainerException( $e->getMessage(),
            $e->getCode(), $e );
        }
    }

    /**
     * <Container Key Check Method>
     * Returns a boolean determination as to whether a specific key exists.
     * This method is safe and does not throw exceptions.
     * @param scalar $key
     * @return bool
     */
    protected function _check( $key )
    {
        $this->_registryInitialize();
        if ( !is_scalar( $key ) )
        {
            return false;
        }
        return $this->_checkRegistryKey( $key );
    }

    /**
     * <Container Value Check Method>
     * Returns a boolean determination as to whether the specific value exists.
     * This method is safe and does not throw exceptions.
     * @param mixed $value
     * @return bool
     */
    protected function _checkValue( $value )
    {
        $this->_registryInitialize();
        if ( !is_array( $value ) )
        {
            $values = $this->_registryGetLeafNodes();
            return in_array( $value, $values );
        }
        throw new \oroboros\core\utilities\exception\container\ContainerException(
        sprintf( 'Error encountered at [%s]. Cannot tree search containers for values of type: [array], '
            . 'as this causes ambiguity collisions with internal logic.',
            __METHOD__ ),
        \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_AMBIGUOUS_REFERENCE );
    }

    protected function _getValueKey( $value )
    {
        $this->_registryInitialize();
        if ( !is_array( $value ) )
        {
            $values = $this->_registryGetLeafNodes();
            $key = array_search( $value, $values );
            if ( !$key )
            {
                throw new \oroboros\core\utilities\exception\container\NotFoundException(
                sprintf( 'No entry was found for provided value [%s].',
                    is_scalar( $value )
                        ? $value
                        : ( is_null( $value )
                            ? 'null'
                            : (is_object( $value )
                                ? get_class( $value )
                                : gettype( $value )))  ),
                \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_PHP_KEY_NOT_FOUND
                );
            }
            return $key;
        }
        throw new \oroboros\core\utilities\exception\container\ContainerException(
        sprintf( 'Error encountered at [%s]. Cannot tree search containers for values of type: [array], '
            . 'as this causes ambiguity collisions with internal logic.',
            __METHOD__ ),
        \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_AMBIGUOUS_REFERENCE );
    }

    /**
     * <Container Internal Leaf Node Getter Method>
     * Returns an associative array of all leaf nodes of the container's
     * multidimensional structure, where the key is the fully qualified dot
     * separated path to through the multidimensional structure, and the
     * value is the leaf value.
     *
     * This method's return value allows for safe iteration over every value
     * in the container as if it were a flat set.
     *
     * @return array
     */
    protected function _getLeaves()
    {
        $this->_registryInitialize();
        return $this->_getRegistryLeaves();
    }

    protected function _getLeavesBySegment( $segment )
    {
        if ( !is_scalar( $segment ) )
        {
            throw new \oroboros\core\utilities\exception\container\ContainerException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'scalar value', gettype( $segment ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS );
        }
        $this->_registryInitialize();
        $values = $this->_searchRegistryLeaves( $segment );
        if ( empty( $values ) )
        {

        }
        return $values;
    }

    /**
     * <Container Internal Subtree Check Method>
     * Takes a dot separated segment representing one or more keys in the
     * multidimensional structure of the container, and returns a boolean
     * determination as to whether the given segment exists in any of the
     * nested subkeys.
     *
     * If true, _getSubtree can safely be called without raising a NotFoundException
     * for the same argument.
     *
     * @param scalar $segment
     * @return bool
     * @throws \oroboros\core\utilities\exception\container\ContainerException If a non-scalar key is passed
     */
    protected function _checkSubtree( $segment )
    {
        if ( !is_scalar( $segment ) )
        {
            throw new \oroboros\core\utilities\exception\container\ContainerException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'scalar value', gettype( $segment ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS );
        }
        $this->_registryInitialize();
        return !empty( $this->_treeSearchKey( $segment ) )
            ? true
            : false;
    }

    /**
     * <Container Internal Subtree Getter Method>
     * Searches the container's multidimensional structure for a specified
     * segment using dot-separated notation. Returns a flat associative array
     * of matches where the key is the fully qualified dot separated key, and
     * the value is the value.
     * @param scalar $segment
     * @return array
     * @throws \oroboros\core\utilities\exception\container\ContainerException If a non-scalar segment is passed
     * @throws \oroboros\core\utilities\exception\container\NotFoundException If the specified subtree segment does not exist
     */
    protected function _getSubtree( $segment )
    {
        if ( !is_scalar( $segment ) )
        {
            throw new \oroboros\core\utilities\exception\container\ContainerException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'scalar value', gettype( $segment ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS );
        }
        $this->_registryInitialize();
        $subtree = $this->_treeSearchKey( $segment );
        if ( empty( $subtree ) )
        {
            throw new \oroboros\core\utilities\exception\container\NotFoundException(
            sprintf( 'No entry was found for subtree segment [%s].', $segment ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_PHP_KEY_NOT_FOUND
            );
        }
        return $subtree;
    }

    /**
     * <Container Internal Full Tree Getter Method>
     * Returns a flat indexed set of all valid dot separated keys in the
     * container, representing the full multidimensional structure of the
     * container expressed in dot separated notation. Every key in the return
     * set is a valid parameter for the get method.
     * @return array
     */
    protected function _getFullSubtree()
    {
        $this->_registryInitialize();
        return $this->_getRegistryTree();
    }

}
