<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\bootstrap\interfaces\contract;

/**
 * <Bootstrap Contract Interface>
 * This interface enforces the methods required for a valid bootstrap object.
 * These methods may be manually honored, or may be satisfied by using the
 * corresponding bootstrap trait.Traits that extend upon this functionality
 * should be expected to implement this for you.
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @package oroboros/core
 * @subpackage bootstrap
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 */
interface BootstrapContract
extends \oroboros\core\interfaces\contract\libraries\LibraryContract
{

    /**
     * <Bootstrap Reset>
     * Resets the Bootstrap object to its state immediately after execution
     * fires successfully, so it is initialized, but does not have any
     * remnant data that was obtained through any action that occurred
     * after initialization. The purpose of this method is to allow a
     * degree of external control over the execution plan, and allow it
     * to be called repeatedly if needed, without the weight of
     * reinitialization.
     *
     * @param array $params //null no-ops, empty array erases, populated array provides parameters. Parameters are not retained if replaced, reset, or reinitialized.
     * @param array $flags //null no-ops, empty array erases, populated array provides flags. Flags are not retained if replaced, reset, or reinitialized.
     * @return void
     */
    public function reset( $params = null, $flags = null );

    /**
     * <Bootstrap Launch>
     * Launches the application.
     * If the bootstrap has not already initialized fully, it will do so at
     * this point using it's default values (bootstraps should be capable of
     * autonomous operation, but not assume it).
     *
     * At this point it will obtain it's routes, check for a match,
     * load the appropriate controller if a match is found,
     * load the default (or supplied) error controller if no match is found,
     * initialize it's selected controller and pass any flags into it that
     * were given to the bootstrap, and execute the controller route.
     */
    public function launch();

    /**
     * <Bootstrap Shutdown>
     * This method terminates the bootstrap object, and causes it to release
     * all objects that it has instantiated along with their own chid objects.
     * This should clear it's entire stack and memory footprint, and leave the
     * bootstrap in an empty, uninitialized state. This SHALL NOT terminate
     * the object, but will leave it to be either reinitialized or unset as
     * determined by it's controlling logic.
     */
    public function shutdown();
}
