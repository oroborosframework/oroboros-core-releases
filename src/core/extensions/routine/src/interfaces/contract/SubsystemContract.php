<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\routine\interfaces\contract;

/**
 * <Subsystem Routine Contract Interface>
 * Subsystem routines wrap larger, independent php architecture.
 * This allows them to be approached in an integrated manner,
 * for bridging execution between two otherwise incompatible systems.
 *
 * Appropriate use cases for this construct include wrapping an entire
 * external system (Like Wordpress, Drupal, Magento, etc), and providing
 * a local object that can defer to that subsystems standard means of
 * execution for the purpose of streamlining integration between the two,
 * or providing a control layer around the subsystem that would not
 * otherwise exist within it's own architecture.
 *
 * This can then be used to spoof any specific environment for that subsystem,
 * and call the desired result directly, even if it would not otherwise be
 * possbile to do that as per it's own architectural constraints.
 * This also allows you to run post-execution logic that would otherwise not
 * be possible (provided the subsystem doesn't explicitly call exit during
 * it's execution).
 *
 * Working with a complex subsystem is a lot of work to do right.
 * This IS NOT a drop in answer for seamless integration,
 * but it does provide you a starting point to effect interraction
 * that would not otherwise be possible between two platforms.
 * The specific specifications that apply for any given subsystem
 * will ned to be reviewed by the developer in that systems own
 * documentation to figure out a best-case approach to this task.
 *
 * Should we develop a streamlined subsystem wrapper for any
 * specific platform, we will release that as a separate extension
 * package for uniform useage. Any such packages we release in the
 * future will build upon this construct, and extend from this to
 * implement the system-specific useage as needed for that specific
 * system.
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @package oroboros/core
 * @subpackage routines
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 */
interface SubsystemRoutineContract
extends RoutineContract
{

}
