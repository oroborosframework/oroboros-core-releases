<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\file\traits;

/**
 * <File Trait>
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage
 * @version
 * @since
 */
trait FileTrait
{

    use \oroboros\core\traits\core\StaticBaselineTrait;
    use \oroboros\core\traits\libraries\LibraryTrait
    {
        \oroboros\core\traits\libraries\LibraryTrait::__construct as private baseline_construct;
        \oroboros\core\traits\libraries\LibraryTrait::initialize as private baseline_initialize;
        \oroboros\core\traits\libraries\LibraryTrait::_validate insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\libraries\LibraryTrait::_validationModes insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\libraries\LibraryTrait::_throwException insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\libraries\LibraryTrait::_getException insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\libraries\LibraryTrait::_recastException insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\libraries\LibraryTrait::_getExceptionCode insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\libraries\LibraryTrait::_getExceptionMessage insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\libraries\LibraryTrait::_getExceptionIdentifier insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\libraries\LibraryTrait::_getExceptionIdentifiers insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\libraries\LibraryTrait::_getExceptionCodeIdentifiers insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\libraries\LibraryTrait::_baselineInternalsCheckValid insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\libraries\LibraryTrait::_baselineInternalsMergeArrayDefaults insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\libraries\LibraryTrait::_baselineInternalsThrowInvalidException insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\libraries\LibraryTrait::_baselineInternalsValidateValues insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\libraries\LibraryTrait::_baselineInternalsCheckConfiguration insteadof \oroboros\core\traits\core\StaticBaselineTrait;
    }

    /**
     * Represents a catalog of known file extensions,
     * and can return their extension, mime type,
     * human-readable title, and an information url as to what they do.
     * This is based on a information dictionary provided with the system.
     * @todo Migrate this to the Codex, when the Codex is sufficiently stable to handle static dictionary files. For now this will be compiled directly from the conf dictionary file.
     * @var array
     */
    private static $_file_extension_information = false;

    /**
     * Represents the directory path to the file
     * @var string
     */
    private $_file_path = null;

    /**
     * Represents the name of the file
     * @var string
     */
    private $_file_name = null;

    /**
     * Represents whether or not the file actually exists.
     * @var bool
     */
    private $_file_exists = false;

    /**
     * Represents the size in bytes of the file
     * @var int
     */
    private $_file_size = 0;

    /**
     * Represents the file type, as defined by it's suffix.
     * This may not represent the literal file type,
     * if it is accidentally or intentionally misnamed.
     * The default is binary if this cannot be determined.
     * @var string
     */
    private $_file_extension = null;

    /**
     * Represents the encoding of the file.
     * The default is binary if this cannot be determined.
     * @var string
     */
    private $_file_encoding = null;

    /**
     * Represents the mime type for http transit.
     * The default is binary if this cannot be determined.
     * @var string
     */
    private $_file_mime = null;

    /**
     * Represents a link to external documentation about the file type.
     * The default is false if this cannot be determined.
     * @var string|bool
     */
    private $_file_type_info = null;

    /**
     * Represents whether the file is readable by the server or not,
     * within the scope of the current runtime.
     * Note that this may differ between http and cli execution.
     * @var bool
     */
    private $_file_readable = false;

    /**
     * Represents whether the file is writeable by the server or not,
     * within the scope of the current runtime.
     * Note that this may differ between http and cli execution.
     * @var bool
     */
    private $_file_writeable = false;

    /**
     * Represents a boolean determination as to whether the file is a temp file or not.
     * This is based off of the system defined temp directory, and will not automatically
     * pick up whether or not the file exists in a custom temp directory not known
     * to the server.
     * @var bool
     */
    private $_file_is_temp = false;

    /**
     * Represents the desired name of the file, if it was uploaded as a temporary file.
     * The file will be automatically renamed to this name if it is moved from the
     * temp directory, if another name for it is not provided. This is to retain upload names,
     * which do not always correspond to temp save names.
     * @var string|bool
     */
    private $_file_temp_alias = false;

    /**
     * Determines whether or not the file object wrapper has been initialized.
     * @var bool
     */
    private $_file_is_initialized = false;

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the uploaded file contract
     *
     * @satisfies \oroboros\core\interfaces\contract\libraries\file\FileContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <File Object Constructor>
     *
     * This constructor should accept three optional parameters,
     * $file, $path, and $type. All three are optional.
     *
     * The object MUST NOT automatically initialize unless $file is provided.
     *
     * The object MUST NOT be considered functionally useable unless initialized.
     *
     * The object MAY provide an internal mechanism for initialization that is protected.
     *
     * If $file is passed, it MUST represent the string name of the fully
     * qualified filepath, or a string name of a resource pointer that can
     * be directly used to create a resource to the file.
     *
     * If $type is supplied, it MUST represent either a
     * file extension or mime type that corresponds to the file name.
     * This value MUST only be used to identify the file type, and MUST NOT
     * be used to build a path to the actual file, which prevents temp files
     * that do not have extensions from accidentally not being identified on disk.
     *
     * If $type is not supplied, it SHOULD be attempted to determine based on
     * $file as an extension if it exists, and MUST default to binary if it
     * cannot be determined to maximize portability.
     *
     * If $temp_alias is supplied, the file MUST be renamed to this name if it
     * is retained or moved from the temp directory to another location and no
     * other name for it is provided. This only applies if it is actually a
     * temp file, and otherwise this parameter has no effect, even if provided.
     *
     * The object should interpret the file as a temp file if it's directory corresponds to the runtime value of:
     * \oroboros\environment\interfaces\enumerated\CoreEnvironmentInterface::TEMP_DIRECTORY,
     * which will determine the fully qualified location of the system temp directory at runtime
     * if not overridden manually. This value will be false if it is not set or is not readable and writeable.
     *
     * If any arguments cannot be determined, this method MUST throw
     * a RuntimeException that implements
     * \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract
     * either directly or indirectly.
     *
     * @param string $file (optional)
     * @param string $type (optional)
     * @param string $temp_alias (optional)
     */
    public function __construct( $file_name = null, $type = null,
        $temp_alias = null )
    {
        try
        {
            if ( !is_null( $file ) )
            {
                $this->_initializeFileObject( $file, $type, $temp_alias );
            }
        } catch ( \Exception $e )
        {
            throw new \oroboros\core\utilities\exception\RuntimeException(
            sprintf( \oroboros\core\interfaces\enumerated\core\exception\ExceptionMessage::ERROR_CORE_LIBRARY_FAILURE_MESSAGE,
                get_class( $this ), $e->getMessage(), $e ),
            \oroboros\core\interfaces\enumerated\core\exception\ExceptionCode::ERROR_CORE_LIBRARY_FAILURE );
        }
        /**
         * This trait supports direct extension of \SplFileInfo classes
         */
        if ( $this instanceof \SplFileInfo )
        {
            parent::__construct( $file_name );
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * <File object initialization>
     * This method must be called to initialize.
     * The provided constructor will call this method
     * if parameters are passed to it. If the constructor
     * does not recieve parameters, or if another constructor
     * is supplied that overrides this one,
     * this method must be manually called.
     * If no parameters are provided, initialization will need to be manually called.
     * @param string $file (optional) if provided, will initialize against this filename. This may be either the local file name, or the fully qualified path.
     * @param string $path (optional) if provided, represents the fully qualified directory path to the file. If not provided, the program will attempt to extract the path from $file.
     * @param string $temp_alias (optional) if provided, represents the upload or original name of a temp file. If the file is moved out of the temp directory, it will be named with this value if it is a temp file and no other name is provided.
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if no parameters are supplied.
     */
    protected function _initializeFileObject( $file = null, $type = null,
        $temp_alias = null )
    {
        $this->_loadFileCodex();
        if ( is_null( $file ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\core\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __LINE__ . ' of ' . __METHOD__, 'string', 'null' ),
            \oroboros\core\interfaces\enumerated\core\exception\ExceptionCode::ERROR_LOGIC_MISSING_PARAMETERS );
        }
        //extract the path from $file
        $stack = explode( DIRECTORY_SEPARATOR, $file );
        $filename = array_pop( $stack );
        unset( $stack );
        $path = realpath( str_replace( $filename, null, $file ) ) . DIRECTORY_SEPARATOR;
        //set the file values with the private setters
        $this->_setFilePathValue( $path );
        $this->_setFileNameValue( $filename );
        $this->_setFileTypeValue( $type );
        $this->_setFileExistsValue();
        $this->_setFileReadableValue();
        $this->_setFileWriteableValue();
        $this->_setFileSizeValue();
        $this->_setFileTempAliasValue( $temp_alias );
        $this->_file_is_initialized = true;
    }

    /**
     * <Manual File Type Setter>
     * This may be used to manually select a file type extension
     * internally by classes or traits that use this trait.
     * @param string $type (optional) Should be either a valid file extension or mime type. Defaults to binary if not supplied or not valid, to prevent blocking operation.
     * @return void
     */
    protected function _setFileType( $type = 'bin' )
    {
        $this->_setFileTypeValue( $type );
    }

    /**
     * <File Extension Check>
     * Returns the file extension appropriate for the file.
     * If the file is a temp file that does not have an extension,
     * but the correct extension is known, it will return the correct
     * extension even if the file name does not reflect it.
     * Returns null if the correct extension is not known.
     * @return string|null
     */
    protected function _getFileExtension()
    {
        return $this->_file_extension;
    }

    /**
     * <File Mime Type Check>
     * Returns the mime-type of the file, if it is known.
     * @return string
     */
    protected function _getFileMimeType()
    {
        return $this->_file_mime;
    }

    /**
     * <File Info Link>
     * Returns a url pointing to information about the file specification,
     * if one is known, otherwise returns false.
     * @return string|bool
     */
    protected function _getFileInfoLink()
    {
        return $this->_file_type_info;
    }

    /**
     * <File Alias Check>
     * Returns the alias name of the file, if one exists, otherwise false.
     * @return string|bool
     */
    protected function _getFileAlias()
    {
        return $this->_file_temp_alias;
    }

    /**
     * <File Path Check>
     * Returns the directory path the file exists in.
     * @return string
     */
    protected function _getFilePath()
    {
        return $this->_file_path;
    }

    /**
     * <File Name Check>
     * Returns the filename.
     * @return string
     */
    protected function _getFileName()
    {
        return $this->_file_name;
    }

    protected function _getFileFullyQualifiedName()
    {
        return realpath( $this->_file_path ) . DIRECTORY_SEPARATOR . $this->_file_name;
    }

    /**
     * <File Size Check>
     * Returns the byte count of the filesize.
     * @return int
     */
    protected function _getFileSize()
    {
        return $this->_file_size;
    }

    /**
     * <Existing File Check>
     * Returns a boolean determination as to whether the file exists at all.
     * @return bool
     */
    protected function _fileExists()
    {
        return $this->_file_exists;
    }

    /**
     * <Readable File Check>
     * Returns a boolean determination as to whether the file is readable.
     * @return bool
     */
    protected function _fileReadable()
    {
        return $this->_file_readable;
    }

    /**
     * <Writeable File Check>
     * Returns a boolean determination as to whether the file is writeable.
     * @return bool
     */
    protected function _fileWriteable()
    {
        return $this->_file_writeable;
    }

    /**
     * <Temp File Check>
     * Returns a boolean determination as to whether the file is a temp file.
     * @return bool
     */
    protected function _fileIsTemp()
    {
        return $this->_file_is_temp;
    }

    /**
     * <File Initialized Check>
     * Returns a boolean determination as to whether the file is initialized.
     * If this is false, no other return method should be trusted to be accurate.
     * @return bool
     */
    protected function _fileIsInitialized()
    {
        return $this->_file_is_initialized;
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * Sets the supplied file directory.
     * @param string $file
     * @return void
     */
    private function _setFilePathValue( $path )
    {
        $this->_file_path = $path;
        if ( $path === \oroboros\Oroboros::TEMP_DIRECTORY )
        {
            $this->_file_is_temp = true;
        }
    }

    /**
     * Sets the supplied file name.
     * @param string $file
     * @return void
     */
    private function _setFileNameValue( $file )
    {
        $this->_file_name = $file;
    }

    /**
     * Sets the supplied temp alias value, if the file is a temp file.
     * @param string $value
     * @return void
     */
    private function _setFileTempAliasValue( $value = null )
    {
        if ( $this->_file_is_temp && !is_null( $value ) && $value )
        {
            $this->_file_temp_alias = $value;
        }
    }

    /**
     * Sets the supplied file type.
     * The file type reflects the actual
     * @return void
     */
    private function _setFileTypeValue( $type = null )
    {
        //attempt to automatically determine the file extension if it was not provided.
        if ( is_null( $type ) && strpos( $this->_file_name, '.' ) !== false )
        {
            $stack = explode( '.', $this->_file_name );
        }
        //we will also check by mime type, if that was provided and it can be resolved.
        elseif ( !is_null( $type ) && strpos( $type, '/' ) !== false )
        {
            foreach ( self::$_file_extension_information as
                $key =>
                $value )
            {
                if ( $value['mime'] === $type )
                {
                    $type = $key;
                    break;
                }
            }
        }
        //set the default if no file extension has yet been determined.
        //we will use binary if we don't know what
        //this extension is in the dictionary.
        if ( is_null( $type ) || !array_key_exists( $type,
                self::$_file_extension_information ) )
        {
            $type = 'bin';
        }
        $info = self::$_file_extension_information[$type];
        $this->_file_extension = $type;
        $this->_file_mime = $info['mime'];
        $this->_file_type_info = $info['info'];
        $this->_setFileEncodingValue( isset( $info['encoding'] )
                ? $info['encoding']
                : 'binary'  );
    }

    private function _setFileExtensionValue( $extension = null )
    {
        $this->_file_extension = $extension;
    }

    /**
     * Sets a boolean flag determining whether the file exists.
     * @return void
     */
    private function _setFileExistsValue()
    {
        $this->_file_exists = file_exists( realpath( $this->_file_path ) . DIRECTORY_SEPARATOR . $this->_file_name );
    }

    /**
     * Sets the supplied file size in bytes.
     * @return void
     */
    private function _setFileSizeValue()
    {
        if ( $this->_file_exists )
        {
            $this->_file_size = filesize( realpath( $this->_file_path ) . DIRECTORY_SEPARATOR . $this->_file_name );
        }
    }

    /**
     * Sets the supplied file encoding.
     * @return void
     */
    private function _setFileEncodingValue( $encoding = 'binary' )
    {
        $this->_file_encoding = $encoding;
    }

    /**
     * Sets a boolean determination as to whether the file
     * is readable by the server in the scope of the current runtime.
     * @return void
     */
    private function _setFileReadableValue()
    {
        if ( $this->_file_exists )
        {
            $this->_file_readable = is_readable( realpath( $this->_file_path ) . DIRECTORY_SEPARATOR . $this->_file_name );
        }
    }

    /**
     * Sets a boolean determination as to whether the file
     * is writeable by the server in the scope of the current runtime.
     * @return void
     */
    private function _setFileWriteableValue()
    {
        if ( $this->_file_exists )
        {
            $this->_file_writeable = is_writable( realpath( $this->_file_path ) . DIRECTORY_SEPARATOR . $this->_file_name );
        }
    }

    /**
     * Returns a file pointer resource to the file, or false on failure.
     * This method takes the standard fopen valid mode parameters for it's $mode input.
     * @param string $mode ['r', 'r+', 'w', 'w+', 'a', 'a+', 'x', 'x+', 'c', 'c+', 'x'] Default: 'r'
     * @return resource|false
     */
    private function _getFileStreamResource( $mode = 'r' )
    {
        $resource = fopen( $this->_getFileFullyQualifiedName(), $mode );
        return $resource;
    }

    /**
     * Moves the file to a new location.
     * If the same file exists in the new location, it will be replaced.
     * @param type $new_directory The fully qualified path to the new location.
     * @param type $new_filename (optional) If provided, will rename the file. If not provided, will retain the existing filename.
     */
    private function _moveFileTo( $new_directory, $new_filename = null )
    {

    }

    /**
     * Creates a copy of the file in the specified location.
     * If the same file exists in the new location, it will be replaced.
     * @param type $new_directory The fully qualified path to the new location.
     * @param type $new_filename (optional) If provided, will rename the file. If not provided, will retain the existing filename.
     */
    private function _copyFileTo( $new_directory, $new_filename = null )
    {

    }

    /**
     * Deletes the file, if it exists.
     * In most cases, this will render this object unuseable,
     * and it should be unset after this is called.
     * @return void
     */
    private function _deleteFile()
    {

    }

    /**
     * Truncates the file to zero length, without deleting it.
     * @return void
     */
    private function _truncateFile()
    {

    }

    /**
     * Updates the files 'last modified' property to the current time,
     * as defined by the server.
     * @return void
     */
    private function _touchFile()
    {

    }

    /**
     * Sets the file contents.
     * @param type $content The new content to set in the file.
     * @param type $truncate If true, will truncate the file to zero length before setting content. Default false.
     * @param type $append If true, the file will be suffixed with the new content. If false, it will be suffixed. Default true
     * @return void
     */
    private function _setFileContents( $content, $truncate = false,
        $append = true )
    {

    }

    /**
     * @todo Refactor this to use a dictionary object, so that the data can be
     * cached and not pull a massive set of json data into compiled memory of
     * every file object created.
     *
     * This data should eventually make its way to single serialized codex
     * entries that are pulled locally after a build is run, and eventually
     * after that be cacheable via memcached or a database, if a connection
     * to either of those is supplied by the build info.
     */
    private function _loadFileCodex()
    {
        if ( !self::$_file_extension_information )
        {
            $dict = \oroboros\Oroboros::CONFIG_DIRECTORY . 'dictionary' . DIRECTORY_SEPARATOR . 'files.json';
            self::$_file_extension_information = json_decode( file_get_contents( $dict ),
                1 );
        }
    }

}
