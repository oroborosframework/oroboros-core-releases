<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\file\traits;

/**
 * <Directory Trait>
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage
 * @version
 * @since
 */
trait DirectoryTrait
{

    use \oroboros\core\traits\core\StaticBaselineTrait;
    use \oroboros\core\traits\core\BaselineTrait
    {
        \oroboros\core\traits\core\BaselineTrait::__construct as private baseline_construct;
        \oroboros\core\traits\core\BaselineTrait::_validate insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\core\BaselineTrait::_validationModes insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\core\BaselineTrait::_throwException insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\core\BaselineTrait::_getException insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\core\BaselineTrait::_recastException insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\core\BaselineTrait::_getExceptionCode insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\core\BaselineTrait::_getExceptionMessage insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\core\BaselineTrait::_getExceptionIdentifier insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\core\BaselineTrait::_getExceptionIdentifiers insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\core\BaselineTrait::_getExceptionCodeIdentifiers insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\core\BaselineTrait::_baselineInternalsCheckValid insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\core\BaselineTrait::_baselineInternalsMergeArrayDefaults insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\core\BaselineTrait::_baselineInternalsThrowInvalidException insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\core\BaselineTrait::_baselineInternalsValidateValues insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\core\traits\core\BaselineTrait::_baselineInternalsCheckConfiguration insteadof \oroboros\core\traits\core\StaticBaselineTrait;
    }

    //declare

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies ...
     *
     * @execution Default Execution Plan (minimal)
     *
     * @execution Default Execution Plan (commented)
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <File Object Constructor>
     *
     * This constructor should accept three optional parameters,
     * $file, $path, and $type. All three are optional.
     *
     * The object MUST NOT automatically initialize unless $file is provided.
     *
     * The object MUST NOT be considered functionally useable unless initialized.
     *
     * The object MAY provide an internal mechanism for initialization that is protected.
     *
     * If $file is passed, it MUST represent the string name of the fully
     * qualified filepath, or a string name of a resource pointer that can
     * be directly used to create a resource to the file.
     *
     * If $type is supplied, it MUST represent either a
     * file extension or mime type that corresponds to the file name.
     * This value MUST only be used to identify the file type, and MUST NOT
     * be used to build a path to the actual file, which prevents temp files
     * that do not have extensions from accidentally not being identified on disk.
     *
     * If $type is not supplied, it SHOULD be attempted to determine based on
     * $file as an extension if it exists, and MUST default to binary if it
     * cannot be determined to maximize portability.
     *
     * If $temp_alias is supplied, the file MUST be renamed to this name if it
     * is retained or moved from the temp directory to another location and no
     * other name for it is provided. This only applies if it is actually a
     * temp file, and otherwise this parameter has no effect, even if provided.
     *
     * The object should interpret the file as a temp file if it's directory corresponds to the runtime value of:
     * \oroboros\environment\interfaces\enumerated\CoreEnvironmentInterface::TEMP_DIRECTORY,
     * which will determine the fully qualified location of the system temp directory at runtime
     * if not overridden manually. This value will be false if it is not set or is not readable and writeable.
     *
     * If any arguments cannot be determined, this method MUST throw
     * a RuntimeException that implements
     * \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract
     * either directly or indirectly.
     *
     * @param string $file (optional)
     * @param string $type (optional)
     * @param string $temp_alias (optional)
     */
    public function __construct( $file_name = null, $type = null,
        $temp_alias = null )
    {
        $parameters = array(
            'file-name' => $file_name,
            'file-type' => $type,
            'file-alias' => $temp_alias,
        );
        $this->initialize( $parameters );
        parent::_construct( $file_name );
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    //protected methods

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    //private methods

}
