<?php

namespace oroboros\environment;

/**
 *
 */
final class EnvironmentManager
    extends \oroboros\environment\abstracts\AbstractEnvironmentManager
    implements \oroboros\environment\interfaces\api\EnvironmentApi
{
    const OROBOROS_CLASS_SCOPE = \oroboros\core\interfaces\enumerated\scope\LibraryClassScopes::CLASS_SCOPE_LIBRARY_ENVIRONMENT_MANAGER;
}
