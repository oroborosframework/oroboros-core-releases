<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\environment\interfaces\enumerated;

/**
 * <HTTP Environment Enumerated Api Interface>
 *
 * This enumerated api interface defines the http runtime environment.
 *
 * --------
 *
 * Enumerated Api Interfaces
 * Enumerated Api Interfaces provide sets of fixed,
 * enumerated values that must maintain consistency,
 * and should not be overridden at compile time.
 *
 * In some cases, these values are considered absolute,
 * and the program will always reference them as defined.
 * In other cases, they provide baseline definitions,
 * which the individual api implementing them may allow
 * for extension upon. In these cases, the fixed values
 * MAY NOT be removed or altered, but additions and aliases
 * will be allowed for. Check the api of the individual
 * contract being considered for extension for information
 * how to accomplish this as it applies.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/enumerated_api_interface.md
 * @category enumerated-interfaces
 * @package oroboros/core
 * @subpackage environment
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 */
interface HttpEnvironment
extends EnvironmentBase
{

    /**
     * Defines if the current request is http based.
     * This value cannot be overridden.
     * Doing so will cause the system to reference cli resources
     * that are not available, and cause errors during http sessions.
     */
    const IS_HTTP = OROBOROS_IS_HTTP;

    /**
     * Defines if the current request is SSL.
     * This value may be overridden.
     * define('OROBOROS_IS_SSL', true); // or false
     *
     * Unless overridden, this value will always be false if cli,
     * and otherwise will detect ssl using the webservers global declaration.
     */
    const IS_SSL = OROBOROS_IS_SSL;

    /**
     * Defines if the current request is an AJAX call.
     * This value may be overridden.
     * define('OROBOROS_IS_AJAX', true); //or false
     *
     * Unless overridden, this value will always be false if cli,
     * and otherwise will check for the presence of:
     * HTTP_X_REQUESTED_WITH = xmlhttprequest;
     *
     * This is standardly passed in a header with an ajax call.
     * If your ajax calls are not firing correctly, make sure this header
     * is present on request.
     */
    const IS_AJAX = OROBOROS_IS_AJAX;

    /**
     * Defines the current host url.
     * This value may be overridden.
     * define('OROBOROS_URL', 'http://example.com');
     *
     * Unless overridden, this value will always be false if cli,
     * and otherwise will append the correct http/https prefix to HTTP_HOST,
     * and strip any trailing slashes from the resulting value.
     */
    const REQUEST_URL = OROBOROS_URL;

    /**
     * Defines the current request uri.
     * This value can be overridden.
     * define('OROBOROS_PAGE', 'http://example.com/some-page/');
     *
     * Unless overridden, this value will always be false if cli,
     * and otherwise will create the fully qualified request uri
     * with the correct ssl prefix, and a trailing slash at the end.
     */
    const REQUEST_URI = OROBOROS_PAGE;

    /**
     * Defines the current request method.
     * This value can be overridden.
     *
     * //Valid values: [cli, get, head, post, put, delete, options, trace, connect, patch]
     * //It is generally adviseable to not use trace. Others may be used at your discretion.
     * //All values other than cli are defined based on the w3c spec
     * //@see https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html
     * define('OROBOROS_REQUEST_METHOD', 'get');
     *
     * Unless overridden, this value will always be 'cli' if cli,
     * and otherwise will use the http request type.
     */
    const REQUEST_METHOD = OROBOROS_REQUEST_METHOD;
}
