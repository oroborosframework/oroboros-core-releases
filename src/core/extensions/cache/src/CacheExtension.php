<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\cache;

/**
 * <Oroboros Core Cache Extension>
 * Provides direct caching functionality to Oroboros Core.
 * @author Brian Dayhoff <brian@mopsyd.me>
 * @category extension
 * @category cache
 * @package oroboros/core
 * @subpackage cache
 * @version 0.2.5
 * @since 0.2.5
 */
final class CacheExtension
    implements \oroboros\core\interfaces\contract\extensions\core\CoreExtensionContract
{

    use \oroboros\core\traits\extensions\ExtensionTrait;

    const OROBOROS_CLASS_TYPE = \oroboros\core\interfaces\enumerated\type\ClassType::CLASS_TYPE_EXTENSION;
    const OROBOROS_CLASS_SCOPE = \oroboros\core\interfaces\enumerated\scope\ClassScope::CLASS_SCOPE_EXTENSION_CORE;
    const OROBOROS_API = '\\oroboros\\cache\\interfaces\\api\\CacheApi';

    private static $_core_extension_context = \oroboros\core\interfaces\api\OroborosApi::API_SCOPE;
    private static $_core_extension_id = 'cache';
    private static $_core_extension_indexes = array();

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies ...
     *
     * -------------------------------------------------------------------------
     */
    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * <Extension Id Declaration Method>
     * Declares the extension id used by the core class
     * to register the extension.
     * @return string
     */
    protected static function _declareExtensionId()
    {
        return self::$_core_extension_id;
    }

    /**
     * <Extension Context Declaration Method>
     * Declares the extension context used to associate
     * it with the core class.
     * @return string
     */
    protected static function _declareExtensionContext()
    {
        return self::$_core_extension_context;
    }

    /**
     * <Extension Control Api Index Declaration Method>
     * Declares the method prefixes to search for to
     * dynamically call command methods.
     * @return array
     */
    protected static function _declareExtensionApi()
    {
        return self::$_core_extension_indexes;
    }

    /**
     * <Extension Internal Pre-Initialization Setup Method>
     * This method will be called immediately prior to calling the baseline
     * initialization, which allows the extension to set any of the baseline
     * parameters required for valid initialization.
     * @return void
     */
    protected static function _extensionPreInitialization()
    {
        //no-op
    }

    /**
     * <Extension Internal Setup Method>
     * This method will be called immediately after initialization fires,
     * allowing the extension to perform any post-initialization setup tasks
     * required for valid setup.
     * @return void
     */
    protected static function _extensionSetup()
    {
        //no-op
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */
}
