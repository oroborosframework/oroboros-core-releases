<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\log\interfaces\enumerated;

/**
 * <Logger Defaults Api>
 * Provides the default loggers for various environments.
 * These are the generic logger objects that ship with the core package,
 * and are usable out of the box, provided any connection details they
 * require have been satisfied.
 *
 * --------
 *
 * Enumerated Api Interfaces
 * Enumerated Api Interfaces provide sets of fixed,
 * enumerated values that must maintain consistency,
 * and should not be overridden at compile time.
 *
 * In some cases, these values are considered absolute,
 * and the program will always reference them as defined.
 * In other cases, they provide baseline definitions,
 * which the individual api implementing them may allow
 * for extension upon. In these cases, the fixed values
 * MAY NOT be removed or altered, but additions and aliases
 * will be allowed for. Check the api of the individual
 * contract being considered for extension for information
 * how to accomplish this as it applies.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/enumerated_api_interface.md
 * @category enumerated-interfaces
 * @package oroboros/core
 * @subpackage psr3
 * @version 0.2.5
 * @since 0.2.5
 */
interface LoggerDefaults
extends LogBase
{

    /**
     * Designates the default logger that is automatically instantiated on bootload.
     * By default, only the null logger is active, so that log entries can be entered
     * throughout the project in a uniform way. Activating other loggers can be
     * achieved by either setting the log mode constant or editing the
     * settings file, in that order of preference.
     */
    const LOGGER_DEFAULT = self::LOGGER_NULL;

    /**
     * Designates the default null logger. This logger honors the Psr3 standard,
     * and silently discards any messages thrown at it.
     * This logger always logs.
     */
    const LOGGER_NULL = "\\oroboros\\log\\NullLogger";

    /**
     * Designates the default file logger. This logger honors the Psr3 standard,
     * and logs all entries thrown at it to the default log file determined by
     * the override constant, settings file, or system environment in that
     * order of preference. This logger always logs if enabled.
     */
    const LOGGER_FILE = "\\oroboros\\log\\FileLogger";

    /**
     * Designates the default database logger. This logger honors the Psr3 standard,
     * and logs all entries thrown at it to designated database table determined by
     * the override constant, settings file, or system environment in that
     * order of preference. This logger is only active if a valid database
     * connection is supplied for this purpose. This logger always logs if enabled.
     */
    const LOGGER_DATABASE = "\\oroboros\\log\\DatabaseLogger";

    /**
     * Designates the default screen logger. This logger honors the Psr3 standard,
     * and logs all entries thrown at it to a end-user friendly html output format.
     * This logger is only active when outputting html content, and only if
     * debug mode is enabled.
     */
    const LOGGER_SCREEN = "\\oroboros\\log\\ScreenLogger";


    /**
     * Designates the default cli logger. This logger honors the Psr3 standard,
     * and logs all entries thrown at it to the command line, using
     * system compatible color codes if available for the current cgi environment.
     * This logger is only active when outputting to the command line.
     * This logger always logs if enabled.
     */
    const LOGGER_CLI = "\\oroboros\\log\\CliLogger";

    /**
     * Designates the default css logger. This logger honors the Psr3 standard,
     * and logs all entries thrown at it to css comment syntax. This logger
     * is only active when outputting css format content,
     * and only if debug mode is enabled.
     */
    const LOGGER_CSS = "\\oroboros\\log\\CssLogger";

    /**
     * Designates the default javascript logger. This logger honors the Psr3 standard,
     * and logs all entries thrown at it to javascript console.error syntax.
     * This logger is only active when outputting javascript format content,
     * and only if debug mode is enabled.
     */
    const LOGGER_JS = "\\oroboros\\log\\JsLogger";

    /**
     * Designates the default ajax logger. This logger honors the Psr3 standard,
     * and logs all entries thrown at it to the designated output format for
     * the ajax request (usually JSON, but possibly also XML, HTML, or plaintext).
     * This logger determines its output mode based on the incoming accept header
     * from the request, and only logs if debug mode is enabled.
     */
    const LOGGER_AJAX = "\\oroboros\\log\\AjaxLogger";

}
