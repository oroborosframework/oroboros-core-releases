<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2013-2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\log\traits;

/**
 * <Log Manager Trait>
 * This trait provides methods required to manage logging events using the Psr-3 standard.
 * This represents the logic that will typically be directly interacted with by
 * other logic that requires logging functionality, acting as a facade
 * that abstracts out any details about the process of logging the event or
 * how it is recorded.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage psr3
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\core\interfaces\contract\patterns\behavioral\WorkerContract
 * @satisfies \oroboros\core\interfaces\contract\libraries\environment\LoggerContract
 */
trait LoggerTrait
{

    use \oroboros\core\traits\patterns\behavioral\WorkerTrait;

    /**
     * Designates the pool of existing loggers.
     * @var \oroboros\log\interfaces\contract\LogWriterContract
     */
    private $_logger_writer;

    /**
     * Designates any settings required to operate,
     * if any are required.
     * The default production settings are provided by default.
     * @var array
     */
    private $_logger_settings = array(
        "use_syslog" => false,
        "writer" => array(
            "use_header" => false,
            "use_buffer" => false,
            "header_title" => "Oroboros Error Log",
            "default_error_logfile" => "oroboros_errors.log",
            "header_timestamp" => true,
            "timestamp_format" => "Y-m-d H:i:s"
        ),
        "timestamp_format" => "Y-m-d H:i:s",
        "timezone" => "match_server",
        "default_logger" => "\\oroboros\\log\\NullLogger",
        "default_writer" => "\\oroboros\\log\\FileLogWriter",
        "verbose" => false,
        "level" => array(
            "emergency" => true,
            "alert" => true,
            "critical" => true,
            "error" => true,
            "warning" => true,
            "notice" => false,
            "info" => false,
            "debug" => false
        )
    );

    /**
     * Represents the valid Psr3 log levels expressed as syslog constants.
     * @var array
     */
    private $_logger_syslog_levels = array(
        \Psr\Log\LogLevel::EMERGENCY => LOG_EMERG,
        \Psr\Log\LogLevel::ALERT => LOG_ALERT,
        \Psr\Log\LogLevel::CRITICAL => LOG_CRIT,
        \Psr\Log\LogLevel::ERROR => LOG_ERR,
        \Psr\Log\LogLevel::WARNING => LOG_WARNING,
        \Psr\Log\LogLevel::NOTICE => LOG_NOTICE,
        \Psr\Log\LogLevel::INFO => LOG_INFO,
        \Psr\Log\LogLevel::DEBUG => LOG_DEBUG
    );

    /**
     * Represents the valid Psr3 log levels.
     * @var array
     */
    private $_logger_valid_levels = array(
        \Psr\Log\LogLevel::EMERGENCY,
        \Psr\Log\LogLevel::ALERT,
        \Psr\Log\LogLevel::CRITICAL,
        \Psr\Log\LogLevel::ERROR,
        \Psr\Log\LogLevel::WARNING,
        \Psr\Log\LogLevel::NOTICE,
        \Psr\Log\LogLevel::INFO,
        \Psr\Log\LogLevel::DEBUG
    );

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\core\interfaces\contract\codex\CodexContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Logger Constructor>
     * Oroboros Core loggers have a standardized constructor.
     * All parameters are nullable, but the logger will generally
     * not do anything if it does not have dependencies injected.
     *
     * The LogManager will inject the correct parameters in the normal
     * context of logger usage in this system, but will only do so for loggers
     * that honor the Oroboros Core logger contract in addition
     * to \Psr\Log\LoggerInterface.
     *
     * @param \oroboros\log\interfaces\contract\LogWriterContract $writer
     * @param array $settings
     */
    public function __construct( \oroboros\log\interfaces\contract\LogWriterContract $writer
    = null, array $settings = null, $flags = null )
    {
        $class = get_class( $this );
        $this->_setWorkerCategory( defined( $class . '::OROBOROS_CLASS_TYPE' )
                ? $class::OROBOROS_CLASS_TYPE
                : false  );
        $this->_setWorkerScope( defined( $class . '::OROBOROS_CLASS_SCOPE' )
                ? $class::OROBOROS_CLASS_SCOPE
                : false  );
        $params = $settings;
        $dependencies = null;
        if ( !is_null( $writer ) )
        {
            $dependencies = array(
                'writer' => $writer );
        }
        $this->initialize( $params, $dependencies, $flags );
    }

    /**
     * System is unusable.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function emergency( $message, array $context = array() )
    {
        $this->log( \Psr\Log\LogLevel::EMERGENCY, $message, $context );
    }

    /**
     * Action must be taken immediately.
     *
     * Example: Entire website down, database unavailable, etc. This should
     * trigger the SMS alerts and wake you up.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function alert( $message, array $context = array() )
    {
        $this->log( \Psr\Log\LogLevel::ALERT, $message, $context );
    }

    /**
     * Critical conditions.
     *
     * Example: Application component unavailable, unexpected exception.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function critical( $message, array $context = array() )
    {
        $this->log( \Psr\Log\LogLevel::CRITICAL, $message, $context );
    }

    /**
     * Runtime errors that do not require immediate action but should typically
     * be logged and monitored.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function error( $message, array $context = array() )
    {
        $this->log( \Psr\Log\LogLevel::ERROR, $message, $context );
    }

    /**
     * Exceptional occurrences that are not errors.
     *
     * Example: Use of deprecated APIs, poor use of an API, undesirable things
     * that are not necessarily wrong.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function warning( $message, array $context = array() )
    {
        $this->log( \Psr\Log\LogLevel::WARNING, $message, $context );
    }

    /**
     * Normal but significant events.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function notice( $message, array $context = array() )
    {
        $this->log( \Psr\Log\LogLevel::NOTICE, $message, $context );
    }

    /**
     * Interesting events.
     *
     * Example: User logs in, SQL logs.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function info( $message, array $context = array() )
    {
        $this->log( \Psr\Log\LogLevel::INFO, $message, $context );
    }

    /**
     * Detailed debug information.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function debug( $message, array $context = array() )
    {
        $this->log( \Psr\Log\LogLevel::DEBUG, $message, $context );
    }

    /**
     * Logs with an arbitrary level.
     *
     * @param mixed $level
     * @param string $message
     * @param array $context
     * @return null
     */
    public function log( $level, $message, array $context = array() )
    {
        if ( !in_array( $level, $this->_logger_valid_levels ) )
        {
            throw new \oroboros\core\utilities\exception\log\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__,
                implode( '|', $this->_logger_valid_levels,
                    is_string( $level )
                        ? $level
                        : gettype( $level )  ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        if ( $this->_logger_settings['use_syslog'] )
        {
            //Log directly to the system log
            $this->_loggerLogToSystemLog( $level, $message, $context );
        } else
        {
            //Log to the normal logger endpoint
            $log_entry = $this->_loggerGetFormattedMessage( $level, $message,
                $context );
            $writer = $this->_loggerGetWriter();
            $writer->write( $log_entry . PHP_EOL );
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * Interpolates context values into the message placeholders.
     * @return string
     */
    protected function _interpolate( $message, array $context = array() )
    {
        //Add the backtrace if verbose is set
        if ( $this->_baselineGetParameter( 'verbose' ) )
        {
            $message .= PHP_EOL . '| Backtrace: {backtrace}';
            $context['backtrace'] = $this->_loggerGetBacktrace();
        }
        // build a replacement array with braces around the context keys
        $replace = array();
        foreach ( $context as
            $key =>
            $val )
        {
            // check that the value can be casted to string
            if ( !is_array( $val ) && (!is_object( $val ) || method_exists( $val,
                    '__toString' )) )
            {
                $replace['{' . $key . '}'] = ($key === 'backtrace')
                    ? $val
                    : $this->_loggerGetContextualWrapper( $val );
            }
        }
        // interpolate replacement values into the message and return
        return strtr( $message, $replace );
    }

    /**
     * <Logger Writer Getter>
     * Returns a logger writer object, which handles the
     * actual process of sending the message to the endpoint,
     * or an adapter that can be handled like a stream.
     * @return resource
     */
    protected function &_loggerGetWriter()
    {
        if ( is_null( $this->_logger_writer ) )
        {
            $this->_logger_writer = $this->_loadLogWriter();
        }
        return $this->_logger_writer;
    }

    /**
     * <Logger Writer Default Setter>
     * Loads the logger writer if none is supplied through dependency injection.
     * This method should be overridden to use a new default.
     * @return \oroboros\log\interfaces\contract\LogWriterContract
     */
    protected function _loadLogWriter()
    {
        if ( array_key_exists( 'writer', $this->_baseline_dependencies ) )
        {
            //covers post-initialization
            $writer = $this->_baseline_dependencies['writer'];
            return $writer;
        } else
        {
            //covers pre-initialization
            $writer_class = $this->_baselineSetParametersDefault()['default_writer'];
            $writer_settings = $this->_baselineSetParametersDefault()['writer'];
            return new $writer_class( $writer_settings );
        }
    }

    /**
     * Defines the default logger dependencies if not supplied
     * @return array
     */
    protected function _baselineSetDependenciesDefault()
    {

        return array(
            'writer' => $this->_loadLogWriter(),
            'string_utility' => new \oroboros\format\StringFormatter(),
            'backtrace_utility' => new \oroboros\format\BacktraceFormatter(),
        );
    }

    /**
     * Defines the baseline default logger settings
     * @return array
     */
    protected function _baselineSetParametersDefault()
    {
        return $this->_logger_settings;
    }

    /**
     * Defines the parameters that must be defined to initialize.
     * @return array
     */
    protected function _baselineSetParametersRequired()
    {
        return array_keys( $this->_logger_settings );
    }

    /**
     * Defines the valid types of the parameters.
     * @return array
     */
    protected function _baselineSetParametersValid()
    {
        return array(
            'use_syslog' => 'boolean',
            'writer' => 'array',
            'timestamp_format',
            'timezone' => 'string',
            'default_logger' => "\\Psr\\Log\\LoggerInterface",
            'default_writer' => '\\oroboros\\log\\interfaces\\contract\\LogWriterContract',
            'verbose' => 'boolean',
            'level' => 'array'
        );
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */
    /**
     * Formats the date to the specifications of the current logger.
     * This method only produces a date string, and does not wrap it
     * in any contextual wrapper.
     * @param string $date
     * @return string
     */

    /**
     * Provides the header for a new log.
     * @return string|null
     */
    private function _loggerGetLogHeading()
    {
        return null;
    }

    /**
     * Provides the footer for a new log.
     * @return string|null
     */
    private function _loggerGetLogFooter()
    {
        return null;
    }

    /**
     * Provides a date formatted to the correct
     * specifications for the current logger.
     * @param string $date
     * @return string
     */
    private function _loggerGetFormattedDate( $date = null )
    {
        if ( is_string( $date ) )
        {
            //for formatted timestamps
            $time = new \DateTime( $date );
        } elseif ( is_int( $date ) )
        {
            //for UNIX timestamps
            $time = new \DateTime();
            $time->setTimestamp( $date );
        } else
        {
            //for undeclared time or invalid params,
            //as this should be a non-blocking method.
            $time = new \DateTime();
        }
        if ( array_key_exists( 'timezone', $this->_logger_settings ) && $this->_logger_settings['timezone']
            !== 'match_server' )
        {
            //set the timezone to the specified timezone
            try
            {
                $timezone = new \DateTimeZone( $this->_logger_settings['timezone'] );
            } catch ( \Exception $e )
            {
                //Handle bad timezone settings.
                throw new \oroboros\core\utilities\exception\log\InvalidArgumentException(
                sprintf( 'Error encountered at [%s]. Logger timzone setting [%s] is not valid.',
                    __METHOD__, $this->_logger_settings['timezone'] ),
                $e->getCode(), $e );
            }
            $time->setTimezone( $timezone );
        }
        return $time->format( $this->_logger_settings['timestamp_format'] );
    }

    /**
     * Provides a contextual string wrapper for the specified context key.
     * The context is the date, log level, or the class name, method name,
     * or file and line number where the log entry was generated.
     * @param string $content
     * return string
     */
    private function _loggerGetContextualWrapper( $content )
    {
        return '[ ' . $content . ' ]';
    }

    /**
     * Provides the wrapper for the message.
     * This is separate from the contextual
     * wrapper or backtrace trace wrapper.
     * @param string $message
     * @param array $context
     * @return string
     */
    private function _loggerGetMessageWrapper( $message )
    {
        return $message;
    }

    /**
     * Provides an output friendly wrapper for the log level.
     * @param array $level
     * @return string
     */
    private function _loggerGetFormattedLevel( $level )
    {
        return $this->_loggerGetContextualWrapper( $level );
    }

    /**
     * Provides an output friendly wrapper for the backtrace.
     * @param array $trace
     * @return string
     */
    private function _loggerGetFormattedBacktrace( $trace )
    {
        return print_r( $trace, 1 );
    }

    /**
     * Provides a buffer element between the
     * contextual elements and the message body.
     * @return string|null
     */
    private function _loggerGetMessageBufferElement()
    {
        return ' ';
    }

    /**
     * Fully formats the log entry to fit the current schema,
     * and returns a fully formatted log entry.
     * @param string $level
     * @param string $message
     * @param array $context
     * @return string
     */
    private function _loggerGetFormattedMessage( $level, $message,
        $context = array() )
    {
        $date = $this->_loggerGetContextualWrapper(
            $this->_loggerGetFormattedDate( isset( $context['time'] )
                ? $context['time']
                : date( "Y-m-d H:i:s" ) ) );
        $level = $this->_loggerGetContextualWrapper( $level );
        $message = $this->_loggerGetMessageWrapper( $date . $level
            . $this->_loggerGetMessageBufferElement() . $this->_interpolate( $message,
                $context ) );
        return $message;
    }

    private function _loggerGetBacktrace()
    {
        $btu = $this->_baselineGetDependency( 'backtrace_utility' );
        $stu = $this->_baselineGetDependency( 'string_utility' );
        $trace = debug_backtrace();
        $remove = 0;
        $bt = debug_backtrace();
        foreach ( $bt as
            $key =>
            $trace )
        {
            if ( isset( $trace[$key - 1] ) && array_key_exists( 'class', $trace ) &&
                ( $this->_validate( 'instance-of', $trace['class'],
                    '\\oroboros\\log\\interfaces\\contract\\LoggerContract' ) ||
                $this->_validate( 'instance-of', $trace['class'],
                    '\\oroboros\\Oroboros' ) || $this->_validate( 'instance-of', $trace['class'],
                    '\\oroboros\\log\\LogExtension' ) ) )
            {
                $remove++;
                continue;
            }
            break;
        }
        $realtrace = $btu->parse( $bt, $remove );
        if ( empty( $realtrace ) )
        {
            $origin = array_pop( $bt );
            return rtrim( PHP_EOL . $stu->indent( $stu->indent( $btu->toString( array(
                            $origin ), 1, 0 ), 3 ), 1, '|' ), PHP_EOL . '| ' );
        }
        return PHP_EOL . $stu->indent( $btu->toString( $bt, 1, 0 ), 2 );
    }

    /**
     * Fully formats the log entry to fit the system log format,
     * and logs directly to the system log, bypassing the writer.
     * @param string $level
     * @param string $message
     * @param array $context
     * @return void
     */
    private function _loggerLogToSystemLog( $level, $message, $context = array() )
    {
        $date = $this->_loggerGetFormattedDate( isset( $context['time'] )
            ? $context['time']
            : date( $this->_logger_settings['timestamp_format'] ) );
        $message = $this->_loggerGetContextualWrapper( $date )
            . $this->_loggerGetMessageBufferElement()
            . $this->_interpolate( $message, $context );
        openlog( "Oroboros", LOG_PID | LOG_PERROR, LOG_LOCAL0 );
        syslog( $this->_logger_syslog_levels[$level], $message );
        closelog();
    }

}
