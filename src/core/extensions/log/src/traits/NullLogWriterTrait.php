<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2013, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\log\traits;

/**
 * <Null Log Writer Trait>
 * This trait provides methods required to write to logs.
 * Logger writers deliver the actual message to its destination,
 * without regard to formatting concerns or message content.
 *
 * This logger writer trait silently discards messages.
 * It can be injected into any Oroboros Core logger instance,
 * and will prevent the logger from sending any messages,
 * regardless of whether the logger itself is a null logger.
 * This is presented as an option for universally disabling
 * logging at the logger scope, instead of relying on the
 * LogManager to do it. This can be useful if loggers have
 * been detached and are in independent use elsewhere.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage psr3
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\core\interfaces\contract\patterns\behavioral\WorkerContract
 * @satisfies \oroboros\log\interfaces\contract\LogWriterContract
 */
trait NullLogWriterTrait
{

    use LogWriterTrait;

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\log\interfaces\contract\LogWriterContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Log Writer Write Method>
     * Writes the message to the endpoint.
     * @param string $message
     * @return void
     */
    public function write( $message )
    {
        //no-op
    }

    /**
     * <Log Writer Flush Method>
     * Flushes the output buffer, if it is buffered.
     * This will print directly.
     * @return void
     */
    public function flush( $return )
    {
        //no-op
    }

    /**
     * <Log Writer Clear Method>
     * Clears the output buffer, if buffering is possible.
     * This will discard all buffered messages.
     * @return voind
     */
    public function clear()
    {
        //no-op
    }

}
