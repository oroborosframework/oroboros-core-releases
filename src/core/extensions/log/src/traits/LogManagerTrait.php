<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2013, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\log\traits;

/**
 * <Log Manager Trait>
 * This trait provides methods required to manage logging events using the Psr-3 standard.
 * This represents the logic that will typically be directly interacted with by
 * other logic that requires logging functionality, acting as a facade
 * that abstracts out any details about the process of logging the event or
 * how it is recorded.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage psr3
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\core\interfaces\contract\patterns\behavioral\WorkerContract
 * @satisfies \oroboros\core\interfaces\contract\patterns\behavioral\DirectorContract
 * @satisfies \oroboros\core\interfaces\contract\patterns\behavioral\ManagerContract
 * @satisfies \oroboros\core\interfaces\contract\libraries\environment\LoggerContract
 * @satisfies \oroboros\core\interfaces\contract\libraries\environment\LogManagerContract
 */
trait LogManagerTrait
{

    use LoggerTrait;
    use \oroboros\core\traits\patterns\behavioral\ManagerTrait
    {
        //baseline public methods
        LoggerTrait::__destruct insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        \oroboros\core\traits\patterns\behavioral\ManagerTrait::initialize insteadof LoggerTrait;
        LoggerTrait::isInitialized insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::getApi insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::getScope insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::getWorkerScope insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::getType insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::checkFlag insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::setFlag insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::unsetFlag insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::getFlags insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::getRequiredParameters insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::getRequiredDependencies insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::getRequiredFlags insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::getValidParameters insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::getValidDependencies insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::getValidFlags insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::getFingerprint insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::getWorkerCategory insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        //baseline protected methods
        LoggerTrait::_validate insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_validationModes insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_throwException insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_recastException insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_getException insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_getExceptionCode insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_getExceptionMessage insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_getExceptionIdentifier insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_getExceptionCodeIdentifiers insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_getExceptionIdentifiers insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineRegisterInitializationTask insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineRegisterDestructorTask insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineUnRegisterDestructorTask insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineShowDestructorTasks insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineDisableAutoInitialize insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselinePreventFlagSetting insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselinePreventFlagUnSetting insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineSetParameterPersistence insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineSetDependencyPersistence insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineSetFlagPersistence insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineDisableReinitialization insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineEnableParameterValidation insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineEnableDependencyValidation insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineEnableFlagValidation insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineInitialize insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineGetObjectFingerprint insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineSetParameters insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineSetParametersValid insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineSetParametersDefault insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineSetParametersRequired insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineSetDependencies insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineSetDependenciesValid insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineSetDependenciesDefault insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineSetDependenciesRequired insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineSetFlags insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineSetFlagsValid insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineSetFlagsDefault insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineSetFlagsRequired insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineGetParameter insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineSetParameter insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineGetDependency insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineSetDependency insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineSetFlag insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineUnsetFlag insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineCheckFlag insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        //baseline internals
        LoggerTrait::_baselineRunInitializationTasks insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineRunDestructorTasks insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineSetFingerprint insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineSetInitializationDefaults insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselinePreflightDependencies insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselinePreflightFlags insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselinePreflightParameters insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineCheckReinitializationAllowed insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineSetFlagInternal insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineUnsetFlagInternal insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineCheckFlagValid insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineGetDependencies insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineCheckDependencyValid insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineValidateDependencies insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineGetParameters insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineCheckParameterValid insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineValidateParameters insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineGetFlags insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineValidateFlags insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        //BaselineInternals private methods
        LoggerTrait::_baselineInternalsValidateValues insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineInternalsMergeArrayDefaults insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineInternalsCheckValid insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineInternalsCheckConfiguration insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
        LoggerTrait::_baselineInternalsThrowInvalidException insteadof \oroboros\core\traits\patterns\behavioral\ManagerTrait;
    }

    /**
     * Designates the pool of existing loggers.
     * @var array
     */
    private $_log_manager_loggers = array();

    /**
     * Designates which loggers are currently enabled.
     * @var array
     */
    private $_log_manager_loggers_enabled = array();

    /**
     * Designates which log levels are currently active for logging.
     * @var array
     */
    private $_log_manager_log_levels_enabled = array(
        \Psr\Log\LogLevel::EMERGENCY => true,
        \Psr\Log\LogLevel::ALERT => true,
        \Psr\Log\LogLevel::CRITICAL => true,
        \Psr\Log\LogLevel::ERROR => true,
        \Psr\Log\LogLevel::WARNING => true,
        \Psr\Log\LogLevel::NOTICE => true,
        \Psr\Log\LogLevel::INFO => true,
        \Psr\Log\LogLevel::DEBUG => true
    );

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\core\interfaces\contract\codex\CodexContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Manager Constructor>
     * This is the standard library constructor in Oroboros Core.
     * It presents the opportunity to inject dependencies, settings,
     * and flags to modify behavior throughout its entire category of work.
     *
     * This constructor is a proxy to the initialize method, which does the
     * actual effort of initializing. These methods are separated so that
     * re-initialization can occur on the same object without the weight of
     * re-instantiation, which allows objects to be recycled or reset more
     * performantly than creating a new object.
     *
     * @param type $dependencies (optional) An array of dependencies to inject
     * @param type $settings (optional) An array of settings to pass to the manager to determine its operation setup
     * @param type $flags (optional) An array of flags to pass to the manager to determine modifiers to execution
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if any parameter is provided that does not validate
     * @since 0.2.5
     */
    public function __construct( $params = null, $dependencies = null,
        $flags = null )
    {
        $this->_baselineEnableFlagValidation();
        $this->_baselineEnableParameterValidation( true );
        $this->initialize( $params, $dependencies, $flags );
    }

    /**
     * Returns a list of all of the current loggers scope keys that are
     * currently in the LogManagers pool of workers.
     * @return array
     */
    public function listLoggers()
    {
        return array_keys( $this->_log_manager_loggers );
    }

    /**
     * Enables a specific logger logging by its worker scope.
     * Has no effect if it was already enabled or an invalid scope is passed.
     * @param string $logger
     * @return void
     */
    public function enableLogger( $logger )
    {
        if ( is_string( $logger ) && array_key_exists( $logger,
                $this->_log_manager_loggers_enabled ) )
        {
            $this->_log_manager_loggers_enabled[$logger] = true;
        }
    }

    /**
     * Disables a specific logger from logging by its worker scope.
     * Has no effect if it was already disabled or an invalid scope is passed.
     * @param string $logger
     * @return void
     */
    public function disableLogger( $logger )
    {
        if ( is_string( $logger ) && array_key_exists( $logger,
                $this->_log_manager_loggers_enabled ) )
        {
            $this->_log_manager_loggers_enabled[$logger] = false;
        }
    }

    /**
     * Flushes buffered logs to content. If [$return] is true,
     * it will return an array of the content, with the key being
     * the logger scope that generated the content, and the value
     * being the output.
     * If [$return] is false, the content will instead be sent to whatever
     * stream or source it is attached to, and nothing will be returned.
     * @param $return (optional) whether or not to return the content as an array (default true)
     * @return array|void
     */
    public function flushLogs( $return = true )
    {
        foreach ( $this->_getDependencies() as
            $key =>
            $logger )
        {
            if ( $logger instanceof \oroboros\log\interfaces\contract\LoggerContract )
            {
                $logger->flush();
            }
        }
    }

    /**
     * Tells loggers that buffer logs instead of logging in realtime
     * to erase their buffer. This DOES NOT erase actual log files.
     * @return void
     */
    public function clearLogs()
    {
        foreach ( $this->_getDependencies() as
            $key =>
            $logger )
        {
            if ( $logger instanceof \oroboros\log\interfaces\contract\LoggerContract )
            {
                $logger->clear();
            }
        }
    }

    /**
     * Enables a specific log level being logged.
     * If the level is not valid, the method will simply return false.
     * @param string $level
     * @return bool
     */
    public function enableLogLevel( $level )
    {
        if ( !array_key_exists( $level, $this->_log_manager_log_levels_enabled ) )
        {
            return false;
        }
        $this->_log_manager_log_levels_enabled[$level] = true;
        return true;
    }

    /**
     * Disables a specific log level from being logged.
     * If the level is not valid, the method will simply return false.
     * @param string $level
     * @return bool
     */
    public function disableLogLevel( $level )
    {
        if ( !array_key_exists( $level, $this->_log_manager_log_levels_enabled ) )
        {
            return false;
        }
        $this->_log_manager_log_levels_enabled[$level] = false;
        return true;
    }

    /**
     * Sets a logger instance on the object
     *
     * @param \Psr\Log\LoggerInterface $logger
     * @param string $name (optional) An optional slug to identify the logger as. This will be automatically determined if it is not supplied.
     * @return null
     */
    public function setLogger( \Psr\Log\LoggerInterface $logger, $name = null )
    {
        $this->_registerLogger( $logger, true, $name );
    }

    /**
     * Logs with an arbitrary level.
     *
     * @param mixed $level
     * @param string $message
     * @param array $context
     * @return null
     */
    public function log( $level, $message, array $context = array() )
    {
        if ( !in_array( $level,
                array_keys( $this->_log_manager_log_levels_enabled ) ) )
        {
            throw new \oroboros\core\utilities\exception\log\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__,
                implode( '|',
                    array_keys( $this->_log_manager_log_levels_enabled ),
                    is_string( $level )
                        ? $level
                        : gettype( $level )  ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        if ( $this->_log_manager_log_levels_enabled[$level] )
        {
            foreach ( $this->_log_manager_loggers as
                $key =>
                $logger )
            {
                if ( $this->_log_manager_loggers_enabled[$key] )
                {
                    $logger->log( $level, $message, $context );
                }
            }
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * <Worker Category Setter>
     * Internally used to set the category for the worker.
     * This should be called in the constructor or initialization method
     * of implementing classes or traits.
     * @param string $category
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if a value is passed that is not a string
     */
    protected function _setWorkerCategory( $category )
    {
        $this->_validate( 'type-of', $category, 'string', true );
        $this->_worker_category = $category;
    }

    /**
     * <Worker Scope Setter>
     * Internally used to set the scope for the worker.
     * This should be called in the constructor or initialization method
     * of implementing classes or traits.
     * @param string $category
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if a value is passed that is not a string
     */
    protected function _setWorkerScope( $scope )
    {
        $this->_validate( 'type-of', $scope, 'string', true );
        $this->_worker_scope = $scope;
    }

    /**
     * Sets the directory category to the environment scope.
     * @return void
     */
    protected function _managerSetDirectorCategory()
    {
        $this->_setDirectorCategory( \oroboros\core\interfaces\enumerated\scope\ClassScope::CLASS_SCOPE_LIBRARY_LOGGER );
    }

    /**
     * Fetches the default parameters from the CoreConfig object.
     * @return array
     */
    protected function _baselineSetParametersDefault()
    {
        return \oroboros\core\utilities\core\CoreConfig::get( 'settings', 'core' )['log'];
    }

    /**
     * Defines the valid types of the parameters.
     * @return array
     */
    protected function _baselineSetParametersValid()
    {
        return array(
            'use_syslog' => 'boolean',
            'writer' => 'array',
            'timestamp_format',
            'timezone' => 'string',
            'default_logger' => "\\Psr\\Log\\LoggerInterface",
            'default_writer' => '\\oroboros\\log\\interfaces\\contract\\LogWriterContract',
            'verbose' => 'boolean',
            'level' => 'array'
        );
    }

    /**
     * Creates a default null logger if no other logger is provided.
     * @return array
     */
    protected function _baselineSetDependenciesDefault()
    {
        $writer_class = $this->_baselineSetParametersDefault()['default_writer'];
        $logger_class = $this->_baselineSetParametersDefault()['default_logger'];
        $logger = new $logger_class( new $writer_class( $this->_baselineSetParametersDefault()['writer'] ),
            $this->_baselineSetParametersDefault() );
        if ( $logger instanceof \oroboros\log\interfaces\contract\LoggerContract )
        {
            $key = $logger->getWorkerScope();
        } else
        {
            $key = get_class( $logger );
        }
        return array(
            $key => $logger );
    }

    /**
     * Register all of the loggers received at initialization.
     * @param array $dependencies
     * @return void
     */
    protected function _managerRegisterDependencies( $dependencies )
    {
        foreach ( $this->_baseline_parameters['level'] as
            $level =>
            $enabled )
        {
            //Check verbosely in case someone uses a bad configuration file.
            if ( array_key_exists( $level,
                    $this->_log_manager_log_levels_enabled ) )
            {
                $this->_log_manager_log_levels_enabled[$level] = ($enabled
                    ? true
                    : false);
            }
        }
        try
        {
            $logger = $this->_baselineGetParameter( 'default_logger' );
            $writer = $this->_baselineGetParameter( 'default_writer' );
            $settings = $this->_baselineGetParameter( 'writer' );
            $writer = new $writer( $settings );
            $logger = new $logger( $writer, $this->_baseline_parameters,
                $this->_baseline_flags );
            $this->_registerLogger( $logger );
        } catch ( \Exception $e )
        {
            throw $this->_recastException( $e, 'runtime-exception' );
        }
        foreach ( $dependencies as
            $logger )
        {
            $this->_registerLogger( $logger, true );
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * Registers a new instance of a logger
     * @param \Psr\Log\LoggerInterface $logger
     * @param bool $enabled
     * @throws \oroboros\core\utilities\exception\log\InvalidArgumentException if an invalid logger instance is presented
     */
    private function _registerLogger( $logger, $enabled = true, $key = null )
    {
        $this->_validate( 'instance-of', $logger, '\\Psr\\Log\\LoggerInterface',
            true );
        //If a scope was supplied, use that.
        if ( is_null( $key ) )
        {
            if ( $this->_validate( 'instance-of', $logger,
                    '\\oroboros\\log\\interfaces\\contract\\LoggerContract' ) )
            {
                //Expected contract loggers explicitly declare scope.
                $key = $logger->getWorkerScope();
            } elseif ( defined( get_class( $logger ) . '::OROBOROS_CLASS_SCOPE' ) )
            {
                //Allow exteral classes to define their
                //scope with a simple class scope constant,
                //so they can be compatible with zero coupling.
                $key = $logger::OROBOROS_CLASS_SCOPE;
            }
            if ( is_null( $key ) || !$key )
            {
                //Otherwise the scope is the class name.
                $key = get_class( $logger );
            }
        }
        $this->_log_manager_loggers[$key] = $logger;
        $this->_log_manager_loggers_enabled[$key] = ( $enabled
            ? true
            : false );
    }

}
