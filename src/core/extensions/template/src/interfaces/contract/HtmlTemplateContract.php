<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\template\interfaces\contract;

/**
 * <Oroboros Html Template Api>
 * Provides the standard methods for interacting with html templates.
 * @author Brian Dayhoff <brian@mopsyd.me>
 * @since 0.0.2a
 */
interface HtmlTemplateContract extends TemplateContract {

//    const FLAG_SHORT_TAG = "::short-tag::";

    /**
     * <Inline Tag Flag>
     * When passed to the tag generator,
     * will prevent entering a line break
     * at the end of the tag. This prevents
     * the inline-block render bug that adds
     * the line break as a space in markup,
     * and tends to break responsive layouts.
     * @since 0.0.2a
     */
//    const FLAG_INLINE_TAG = "::inline-tag::";

    /**
     * <Getter Methods>
     * The following methods can be used in
     * template sections to retrieve supplied
     * page information individually.
     */

    public function author();
    public function description();
    public function title();
    public function lang();
    public function meta();
    public function pagename();
    public function favicon();
    public function fonts();
    public function scripts();
    public function css();
    public function heading();
    public function metadata();

    /**
     * <Setter Methods>
     * The following methods can be used by a View
     * to set page information individually.
     */

    public function content($key);
    public function setAuthor($author);
    public function setDescription($description);
    public function setTitle($title);
    public function setLang($lang);
    public function setMeta($name, $meta);
    public function setPagename($id);
    public function setFavicon($name, $favicon);
    public function setFont($name, $font);
    public function setScript($name, $script);
    public function setCss($name, $stylesheet);
    public function setHeading($heading);
    public function setMetadata($name, $metadata);
    public function setContent($name, $content);

    /**
     * <Reset Methods>
     * The following methods can be used by a
     * View to reset page information individually.
     */

    public function resetContent();
    public function resetMetadata();
    public function resetHeading();
    public function resetCss();
    public function resetScripts();
    public function resetFonts();
    public function resetFavicon();
    public function resetPagename();
    public function resetMeta();
    public function resetLang();
    public function resetTitle();
    public function resetAuthor();
    public function resetDescription();
}
